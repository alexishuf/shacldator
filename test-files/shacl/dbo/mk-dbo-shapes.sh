#!/bin/bash
cat SettlementShape.ttl > dbo-shapes-1.ttl
cat SettlementShape.ttl PersonShape.ttl > dbo-shapes-2.ttl
cat SettlementShape.ttl PersonShape.ttl SoccerPlayerShape.ttl > dbo-shapes-3.ttl
cat SettlementShape.ttl PersonShape.ttl SoccerPlayerShape.ttl OfficeHolderShape.ttl > dbo-shapes-4.ttl
cat SettlementShape.ttl PersonShape.ttl SoccerPlayerShape.ttl OfficeHolderShape.ttl CompanyShape.ttl > dbo-shapes-5.ttl
cat SettlementShape.ttl PersonShape.ttl SoccerPlayerShape.ttl OfficeHolderShape.ttl CompanyShape.ttl MusicalArtistShape.ttl > dbo-shapes-6.ttl
cat SettlementShape.ttl PersonShape.ttl SoccerPlayerShape.ttl OfficeHolderShape.ttl CompanyShape.ttl MusicalArtistShape.ttl BankShape.ttl > dbo-shapes-7.ttl
cat SettlementShape.ttl PersonShape.ttl SoccerPlayerShape.ttl OfficeHolderShape.ttl CompanyShape.ttl MusicalArtistShape.ttl BankShape.ttl UniversityShape.ttl > dbo-shapes-8.ttl
cat SettlementShape.ttl PersonShape.ttl SoccerPlayerShape.ttl OfficeHolderShape.ttl CompanyShape.ttl MusicalArtistShape.ttl BankShape.ttl UniversityShape.ttl BridgeShape.ttl  > dbo-shapes-9.ttl
cat SettlementShape.ttl PersonShape.ttl SoccerPlayerShape.ttl OfficeHolderShape.ttl CompanyShape.ttl MusicalArtistShape.ttl BankShape.ttl UniversityShape.ttl BridgeShape.ttl BookShape.ttl  > dbo-shapes-10.ttl
cat SettlementShape.ttl PersonShape.ttl SoccerPlayerShape.ttl OfficeHolderShape.ttl CompanyShape.ttl MusicalArtistShape.ttl BankShape.ttl UniversityShape.ttl BridgeShape.ttl BookShape.ttl MusicGenreShape.ttl > dbo-shapes-11.ttl
cat SettlementShape.ttl PersonShape.ttl SoccerPlayerShape.ttl OfficeHolderShape.ttl CompanyShape.ttl MusicalArtistShape.ttl BankShape.ttl UniversityShape.ttl BridgeShape.ttl BookShape.ttl MusicGenreShape.ttl ConcentrationCampShape.ttl > dbo-shapes-12.ttl
cat SettlementShape.ttl PersonShape.ttl SoccerPlayerShape.ttl OfficeHolderShape.ttl CompanyShape.ttl MusicalArtistShape.ttl BankShape.ttl UniversityShape.ttl BridgeShape.ttl BookShape.ttl MusicGenreShape.ttl ConcentrationCampShape.ttl PopulatedPlaceShape.ttl > dbo-shapes-13.ttl
cat SettlementShape.ttl PersonShape.ttl SoccerPlayerShape.ttl OfficeHolderShape.ttl CompanyShape.ttl MusicalArtistShape.ttl BankShape.ttl UniversityShape.ttl BridgeShape.ttl BookShape.ttl MusicGenreShape.ttl ConcentrationCampShape.ttl PopulatedPlaceShape.ttl WorkShape.ttl > dbo-shapes-14.ttl

cat BankShape.ttl CompanyShape.ttl SettlementShape.ttl PopulatedPlaceShape.ttl BookShape.ttl WorkShape.ttl > dbo-short-inf-1.ttl
cat BankShape.ttl CompanyShape.ttl SettlementShape.ttl PopulatedPlaceShape.ttl BookShape.ttl WorkShape.ttl UniversityShape.ttl BridgeShape.ttl ConcentrationCampShape.ttl > dbo-short-inf-2.ttl
cat BankShape.ttl CompanyShape.ttl SettlementShape.ttl PopulatedPlaceShape.ttl BookShape.ttl WorkShape.ttl UniversityShape.ttl BridgeShape.ttl ConcentrationCampShape.ttl PersonShape.ttl > dbo-short-inf-3.ttl

cat CompanyShape.ttl > dbo-inf-shapes-1.ttl
cat CompanyShape.ttl PersonShape.ttl > dbo-inf-shapes-2.ttl
cat CompanyShape.ttl PersonShape.ttl PopulatedPlaceShape.ttl  > dbo-inf-shapes-3.ttl
cat CompanyShape.ttl PersonShape.ttl PopulatedPlaceShape.ttl WorkShape.ttl > dbo-inf-shapes-4.ttl
cat CompanyShape.ttl PersonShape.ttl PopulatedPlaceShape.ttl WorkShape.ttl SettlementShape.ttl > dbo-inf-shapes-5.ttl
cat CompanyShape.ttl PersonShape.ttl PopulatedPlaceShape.ttl WorkShape.ttl SettlementShape.ttl SoccerPlayerShape.ttl > dbo-inf-shapes-6.ttl
cat CompanyShape.ttl PersonShape.ttl PopulatedPlaceShape.ttl WorkShape.ttl SettlementShape.ttl SoccerPlayerShape.ttl OfficeHolderShape.ttl > dbo-inf-shapes-7.ttl
cat CompanyShape.ttl PersonShape.ttl PopulatedPlaceShape.ttl WorkShape.ttl SettlementShape.ttl SoccerPlayerShape.ttl OfficeHolderShape.ttl MusicalArtistShape.ttl > dbo-inf-shapes-8.ttl
cat CompanyShape.ttl PersonShape.ttl PopulatedPlaceShape.ttl WorkShape.ttl SettlementShape.ttl SoccerPlayerShape.ttl OfficeHolderShape.ttl MusicalArtistShape.ttl BankShape.ttl > dbo-inf-shapes-9.ttl
cat CompanyShape.ttl PersonShape.ttl PopulatedPlaceShape.ttl WorkShape.ttl SettlementShape.ttl SoccerPlayerShape.ttl OfficeHolderShape.ttl MusicalArtistShape.ttl BankShape.ttl UniversityShape.ttl > dbo-inf-shapes-10.ttl
cat CompanyShape.ttl PersonShape.ttl PopulatedPlaceShape.ttl WorkShape.ttl SettlementShape.ttl SoccerPlayerShape.ttl OfficeHolderShape.ttl MusicalArtistShape.ttl BankShape.ttl UniversityShape.ttl BridgeShape.ttl > dbo-inf-shapes-11.ttl
cat CompanyShape.ttl PersonShape.ttl PopulatedPlaceShape.ttl WorkShape.ttl SettlementShape.ttl SoccerPlayerShape.ttl OfficeHolderShape.ttl MusicalArtistShape.ttl BankShape.ttl UniversityShape.ttl BridgeShape.ttl BookShape.ttl > dbo-inf-shapes-12.ttl
cat CompanyShape.ttl PersonShape.ttl PopulatedPlaceShape.ttl WorkShape.ttl SettlementShape.ttl SoccerPlayerShape.ttl OfficeHolderShape.ttl MusicalArtistShape.ttl BankShape.ttl UniversityShape.ttl BridgeShape.ttl BookShape.ttl MusicGenreShape.ttl > dbo-inf-shapes-13.ttl
cat CompanyShape.ttl PersonShape.ttl PopulatedPlaceShape.ttl WorkShape.ttl SettlementShape.ttl SoccerPlayerShape.ttl OfficeHolderShape.ttl MusicalArtistShape.ttl BankShape.ttl UniversityShape.ttl BridgeShape.ttl BookShape.ttl MusicGenreShape.ttl ConcentrationCampShape.ttl > dbo-inf-shapes-14.ttl

