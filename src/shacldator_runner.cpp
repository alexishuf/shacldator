/*
 * shacldator_runner.cpp
 *
 *  Created on: Jun 18, 2018
 *      Author: alexis
 */

#include "shacldator_runner.hpp"
#include <map>
#include <iostream>
#include <omp.h>
#include <thread>
#include <cstdint>
#include <time.h>
#include <errno.h>
#include "graph/graph.hpp"
#include "graph/str/stl/store.hpp"
#include "graph/str/hdt/store.hpp"
#include "graph/str/raptor_builder.hpp"
#include "graph/raptor_out.hpp"
#include "qry/dsl_eval.hpp"
#include "shacl/shapes_graph.hpp"
#include "shacl/graph_validator.hpp"
#include "shacl/omp_graph_validator.hpp"
#include "inf/rulesets.hpp"
#include "inf/backward.hpp"
#include "inf/dsl_inf_eval.hpp"
#include "inf/microwave.hpp"
#include "shacl/none_graph_validator.hpp"


using namespace std;


namespace shacldator {

typedef graph<str::stl::store> stl_graph_t;
typedef graph<str::hdt::store> hdt_graph_t;


struct shacldator_runner::D {
    D() : inf("none"), ofmt("ntriples"), validator("serial"), nproc(-1),
          out(&cout), times_out(nullptr) {}

    struct stopwatch {
        stopwatch(const string& key, ostream* out) : key(key), last(-1),
                                                     us(0), out(out) {}
        stopwatch(stopwatch&& o) : key(o.key), last(o.last), us(o.us),
                                   out(o.out) {
            o.out = nullptr;
        }
        ~stopwatch() {
            stop();
            if (out)
                (*out) << key << " " << us/1000.0 << endl;
        }
        stopwatch& start() {
            if (last != -1) return *this;
            last = get_us();
            return *this;
        }
        stopwatch& stop() {
            us += (last == -1) ? 0 : (get_us() - last);
            last = -1;
            return *this;
        }

    private:
        static uint64_t get_us() {
            timespec ts = {0};
            if (clock_gettime(CLOCK_MONOTONIC, &ts))
                throw std::runtime_error("clock_gettime() failed: " + errno);
            return (uint64_t)ts.tv_sec*1000000 + ts.tv_nsec/1000;
        }
        string key;
        uint64_t last;
        uint64_t us;
        ostream* out;
    };

    static stl_graph_t load_stl(const string& path) {
        str::raptor_builder<str::stl::store::builder> rb;
        rb.parse("guess", path);
        return stl_graph_t(rb.builder.build());
    }

    static hdt_graph_t load_hdt(const string& path) {
        return hdt_graph_t(str::hdt::store(path));
    }

    static bool is_hdt(const std::string& path) {
        return path.length() > 4
                && path.find_last_of(".hdt") == path.length() - 1;
    }

    bool has_inf_bw() const {
        return inf == "rdfs-" || inf == "rdfs";
    }
    bool has_inf_mw() const {
        return inf == "rdfs--";
    }

    inf::ruleset get_ruleset() const {
        if      (inf == "rdfs" ) return inf::rdfs();
        else if (inf == "rdfs-") return inf::interesting_rdfs();
        else throw std::logic_error("Bad inf: " + inf);
    }

    template<template <class, class, class, class> class Validator,
             class DG, class SG>
    bool validate_plain(const DG& dg, const SG& sg) {
        stopwatch inf_setup_sw("inf_setup", times_out);
        inf_setup_sw.start();
        inf_setup_sw.stop();
        qry::def_evaluator<DG> dge(dg);
        str::stl::store::builder out_b;
        shacl::shapes_graph<SG> sgw(&sg);
        stopwatch validate_sw("validate", times_out);
        validate_sw.start();
        bool ok = shacl::validate<Validator>(dge, sgw, out_b);
        validate_sw.stop();
        stopwatch report_sw("report", times_out);
        report_sw.start();
        raptor_out ro(*out, ofmt.c_str(), "");
        ro << stl_graph_t(out_b.build());
        return ok;
    }
    template<template <class, class, class, class> class Validator,
                 class DG, class SG>
    bool validate_inf_bw(const DG& dg, const SG& sg, inf::ruleset rs) {
        stopwatch inf_setup_sw("inf_setup", times_out);
        inf_setup_sw.start();
        inf::backward<DG> bw(dg, std::move(rs));
        inf_setup_sw.stop();
        inf::evaluator<inf::backward<DG>> dge(&bw);
        str::stl::store::builder out_b;
        shacl::shapes_graph<SG> sgw(&sg);
        stopwatch validate_sw("validate", times_out);
        validate_sw.start();
        bool ok = shacl::validate<Validator>(dge, sgw, out_b);
        validate_sw.stop();
        stopwatch report_sw("report", times_out);
        report_sw.start();
        raptor_out ro(*out, ofmt.c_str(), "");
        ro << stl_graph_t(out_b.build());
        return ok;
    }

    template<class Graph>
    inf::detail::naive_closure_map<typename Graph::store_t::term_t>
    build_nc_map(const resource<Graph>& prop, bool reverse) {
        typedef inf::detail::naive_closure_map<typename Graph::store_t::term_t>
                nc_map_t;
        if (!inf_opts.count("build") || inf_opts.at("build") == "serial") {
            return nc_map_t::build(prop, reverse);
        } else if (inf_opts.at("build") == "omp") {
            return nc_map_t::omp_build(prop, reverse);
        } else if (inf_opts.at("build") == "omp_reuse") {
            return nc_map_t::omp_reuse_build(prop, reverse);
        }
        throw std::invalid_argument("build_nc_map: option build="
                                    + inf_opts.at("build"));
    }

    bool read_bool(const string& name, bool def_value) {
        if (!inf_opts.count(name)) return def_value;
        string value_str = inf_opts.at(name);
        std::stringstream ss;
        ss << value_str << " " << "true";
        bool value = true;
        ss >> boolalpha >> value;
        return value;
    }

    template<class DG> void setup_mw_opts(inf::microwave<DG>& mw) {
        mw.use_dr_with_var_subj(read_bool("use_dr_with_var_subj",
                                          mw.use_dr_with_var_subj()));
        mw.use_dr(read_bool("use_dr", mw.use_dr()));
        mw.nest_omp(read_bool("nest_omp", mw.nest_omp()));
    }

    template<template <class, class, class, class> class Validator,
                     class DG, class SG>
    bool validate_inf_mw_stage1_serial(const DG& dg, const SG& sg,
                                       str::stl::store::builder& out_b) {
        stopwatch inf_setup_sw("inf_setup", times_out),
                  validate_sw("validate", times_out);
        inf_setup_sw.start();
        inf::microwave<DG> mw(dg,
                build_nc_map(dg.resource(rdfs::subClassOf), true),
                build_nc_map(dg.resource(rdfs::subPropertyOf), true),
                inf::detail::domain_range_map<typename DG::store_t::term_t>
                           ::build(dg));
        setup_mw_opts(mw);
        inf_setup_sw.stop();
        inf::evaluator<inf::microwave<DG>> dge(&mw);
        shacl::shapes_graph<SG> sgw(&sg);
        validate_sw.start();
        bool ok = shacl::validate<Validator>(dge, sgw, out_b);
        validate_sw.stop();
        return ok;
    }
    template<template <class, class, class, class> class Validator,
                     class DG, class SG>
    bool validate_inf_mw_stage1_async(const DG& dg, const SG& sg,
                                      str::stl::store::builder& out_b) {
        typedef inf::detail::naive_closure_map<typename DG::store_t::term_t>
                nc_map_t;
        typedef inf::detail::domain_range_map<typename DG::store_t::term_t>
                dr_map_t;

        stopwatch inf_setup_sw("inf_setup", times_out);
        inf_setup_sw.start();
        nc_map_t  subcls, subprop;
        dr_map_t dr;
#pragma omp parallel
        {
#pragma omp single nowait
            subcls = build_nc_map(dg.resource(rdfs::subClassOf), true);
#pragma omp single nowait
            subprop = build_nc_map(dg.resource(rdfs::subPropertyOf), true);
#pragma omp single nowait
            dr = dr_map_t::omp_build(dg);
        }
        inf::microwave<DG> mw(dg, std::move(subcls),
                                        std::move(subprop), std::move(dr));
        setup_mw_opts(mw);
        inf_setup_sw.stop();
        inf::evaluator<inf::microwave<DG>> dge(&mw);
        shacl::shapes_graph<SG> sgw(&sg);
        stopwatch validate_sw("validate", times_out);
        validate_sw.start();
        bool ok = shacl::validate<Validator>(dge, sgw, out_b);
        validate_sw.stop();
        return ok;
    }
    template<template <class, class, class, class> class Validator,
                     class DG, class SG>
    bool validate_inf_mw_stage1(const DG& dg, const SG& sg,
                                str::stl::store::builder& out_b) {
        if (inf_opts.count("async_inf_setup"))
            return validate_inf_mw_stage1_async<Validator>(dg, sg, out_b);
        else
            return validate_inf_mw_stage1_serial<Validator>(dg, sg, out_b);

    }
    template<template <class, class, class, class> class Validator,
                 class DG, class SG>
    bool validate_inf_mw(const DG& dg, const SG& sg) {
        str::stl::store::builder out_b;
        bool ok = validate_inf_mw_stage1<Validator>(dg, sg, out_b);
        stopwatch report_sw("report", times_out);
        report_sw.start();
        raptor_out ro(*out, ofmt.c_str(), "");
        ro << stl_graph_t(out_b.build());
        return ok;
    }

    template<template <class, class, class, class> class V,
             class DG, class SG>
    bool validate(const DG& dg, const SG& sg) {
        if      (has_inf_bw()) return validate_inf_bw<V>(dg, sg, get_ruleset());
        else if (has_inf_mw()) return validate_inf_mw<V>(dg, sg);
        else              return validate_plain<V>(dg, sg);
    }

    template<template <class, class, class, class> class V>
    bool validate() {
        if (is_hdt(dg) && is_hdt(sg)) {
            return validate<V>(load_hdt(dg), load_hdt(sg));
        } else if (is_hdt(dg) && !is_hdt(sg)) {
            return validate<V>(load_hdt(dg), load_stl(sg));
        } else if (!is_hdt(dg) && is_hdt(sg)) {
            return validate<V>(load_stl(dg), load_hdt(sg));
        } else if (!is_hdt(dg) && !is_hdt(sg)) {
            return validate<V>(load_stl(dg), load_stl(sg));
        }
        throw std::logic_error("Unexpected combination of sg and dg formats");
    }

    string dg, sg, inf, ofmt, validator;
    map<string, string> inf_opts;
    int nproc;
    ostream* out;
    ostream* times_out;
};

shacldator_runner::shacldator_runner() : d(new D) {}
shacldator_runner::~shacldator_runner() = default;

const set<string>& shacldator_runner::list_inf() const {
    thread_local set<string> inf{"none", "rdfs", "rdfs-", "rdfs--"};
    return inf;
}

const set<string>& shacldator_runner::list_validators() const {
    thread_local set<string> inf{"serial", "omp", "none"};
    return inf;
}

string shacldator_runner::get_dg() const {return d->dg;}
shacldator_runner& shacldator_runner::set_dg(const string& path) {
    d->dg = path;
    return *this;
}

string shacldator_runner::get_sg() const {return d->sg;}
shacldator_runner& shacldator_runner::set_sg(const string& path) {
    d->sg = path;
    return *this;
}

shacldator_runner& shacldator_runner::set_out(ostream& out) {
    d->out = &out;
    return *this;
}

shacldator_runner& shacldator_runner::set_times_out(ostream& out) {
    d->times_out = &out;
    return *this;
}

string shacldator_runner::get_inf() const {return d->inf;}
shacldator_runner& shacldator_runner::set_inf(const string& name) {
    if (!list_inf().count(name))
        throw invalid_argument(name + ": invalid inference level name");
    d->inf = name;
    return *this;
}
shacldator_runner& shacldator_runner::set_inf_opt(const string& name,
                                                  const string& value) {
    d->inf_opts.insert(make_pair(name, value));
    return *this;
}

string shacldator_runner::get_validator() const {return d->validator;}
shacldator_runner& shacldator_runner::set_validator(const string& name) {
    if (!list_validators().count(name))
        throw invalid_argument(name + ": invalid validator name");
    d->validator = name;
    return *this;
}

string shacldator_runner::get_out_fmt() const {return d->ofmt;}
shacldator_runner& shacldator_runner::set_out_fmt(const string& fmt) {
    d->ofmt = fmt;
    return *this;
}

int shacldator_runner::get_nproc() const {return d->nproc;}
shacldator_runner& shacldator_runner::set_nproc(int nproc) {
    d->nproc = nproc;
    return *this;
}

bool shacldator_runner::run() {
    if (d->validator == "serial") {
        return d->validate<shacl::val::serial>();
    } else if (d->validator == "omp") {
        if (d->nproc > 0)
            omp_set_num_threads(d->nproc);
        return d->validate<shacl::val::omp_coarse>();
    } else if (d->validator == "none") {
        return d->validate<shacl::val::none>();
    }
    throw std::logic_error("Do not know how to run current configuration");
}

} /* namespace shacldator */
