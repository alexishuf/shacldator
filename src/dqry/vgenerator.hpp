/*
 * vgenerator.hpp
 *
 *  Created on: May 30, 2018
 *      Author: alexis
 */

#ifndef SRC_INF_VGENERATOR_HPP_
#define SRC_INF_VGENERATOR_HPP_

#include "../graph/str/triple.hpp"

namespace shacldator {namespace dqry {

template<class Term, class Triple=str::triple<Term>> struct vgenerator {
    typedef Term term_t;
    typedef Triple triple_t;

    vgenerator() = default;
protected:
    vgenerator(const vgenerator&) = default;
    vgenerator& operator=(const vgenerator&) = default;
public:
    virtual ~vgenerator() = default;

    virtual const triple_t& get() const = 0;
    virtual bool advance() = 0;
};

}}  // namespace shacldator::dqry



#endif /* SRC_INF_VGENERATOR_HPP_ */
