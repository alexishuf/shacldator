/*
 * generator_it.hpp
 *
 *  Created on: May 28, 2018
 *      Author: alexis
 */

#ifndef SRC_INF_GENERATOR_ITERATOR_HPP_
#define SRC_INF_GENERATOR_ITERATOR_HPP_

#include <type_traits>
#include <ostream>
#include "concepts/Generator.hpp"

namespace shacldator {namespace dqry {

template<class Gen> struct generator_iterator {
    typedef Gen gen_t;
    BOOST_CONCEPT_ASSERT((Generator<Gen>));

    typedef typename gen_t::term_t term_t;
    typedef str::triple<term_t> value_type;
    typedef std::ptrdiff_t difference_type;
    typedef std::forward_iterator_tag iterator_category;
    typedef const value_type& reference;
    typedef const value_type* pointer;

    static const std::size_t epos = std::numeric_limits<std::size_t>::max();

private:
    explicit generator_iterator(bool end) : gen(), pos(epos) {assert(end);}
public:
    static generator_iterator create_end() {return generator_iterator(true);}
    explicit generator_iterator(Gen&& gen) : gen(std::move(gen)), pos(0) {}
    explicit generator_iterator(const Gen& gen) : gen(gen), pos(0) {}
    generator_iterator(const generator_iterator&) = default;
    generator_iterator(generator_iterator&&) = default;
    generator_iterator& operator=(const generator_iterator&) = default;
    generator_iterator& operator=(generator_iterator&&) = default;

    bool at_end() const {return pos == epos;}

    bool operator==(const generator_iterator& o) const {
        return this == &o || (at_end() && o.at_end());
    }
    bool operator!=(const generator_iterator& o) const {return !operator==(o);}

    generator_iterator& operator++() {
        assert(pos != epos && gen);
        pos = gen.get().advance() ? pos+1 : epos;
        return *this;
    }
    generator_iterator operator++(int) {
        generator_iterator cp(*this);
        ++*this;
        return cp;
    }
    reference operator*() const {
        assert(pos != epos && gen);
        return gen.get().get();
    }
    reference operator->() const {return &operator*();}
private:
    template<class X> friend std::ostream&
    operator<<(std::ostream&, const generator_iterator<X>&);

    pack<Gen, false> gen;
    std::size_t pos;
};

template<class Gen>
std::ostream& operator<<(std::ostream& o,
                         const generator_iterator<Gen>& it) {
    return o << "generator_iterator(" << it.gen << ", pos=" << it.pos << ")";
}

template<class Gen> generator_iterator<typename std::decay<Gen>::type>
mk_generator_iterator(Gen&& gen) {
    BOOST_STATIC_ASSERT(!std::is_const<Gen>::value);
    typedef typename std::decay<Gen>::type gen_t;
    typedef generator_iterator<gen_t> it_t;
    bool at_end = gen.get() == typename gen_t::triple_t();
    if (at_end) at_end = !gen.advance();
    return at_end ? it_t::create_end() : it_t(std::forward<gen_t>(gen));
}

template<class Gen>
std::pair<generator_iterator<typename std::decay<Gen>::type>,
          generator_iterator<typename std::decay<Gen>::type>>
mk_generator_range(Gen&& gen) {
    return std::make_pair(mk_generator_iterator(std::forward<Gen>(gen)),
            generator_iterator<typename std::decay<Gen>::type>::create_end());
}

}}  // namespace shacldator::dqry


#endif /* SRC_INF_GENERATOR_ITERATOR_HPP_ */
