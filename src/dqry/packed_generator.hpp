/*
 * packed_generator.hpp
 *
 *  Created on: Jun 22, 2018
 *      Author: alexis
 */

#ifndef SRC_DQRY_PACKED_GENERATOR_HPP_
#define SRC_DQRY_PACKED_GENERATOR_HPP_

#include <type_traits>
#include "concepts/Generator.hpp"
#include "vgenerator.hpp"
#include "../utils.hpp"

namespace shacldator {namespace dqry {

template<class Term, class Triple=str::triple<Term>>
struct packed_generator : public vgenerator<Term, Triple> {
    typedef vgenerator<Term, Triple> vgen_t;

    template<class Gen>
    packed_generator(Gen&& gen)
                     : gen(new Gen(std::forward<Gen>(gen)),
                           &default_if_clone<vgen_t,
                                             typename std::decay<Gen>::type>) {}
    packed_generator(const packed_generator&) = default;
    packed_generator(packed_generator&&) = default;
    packed_generator& operator=(const packed_generator&) = default;
    packed_generator& operator=(packed_generator&&) = default;

    const Triple& get() const {return gen->get();}
    bool advance() {return gen->advance();}

private:
    copyable_ptr<vgen_t, vgen_t*(*)(const vgen_t&)> gen;
};

template<class Term, class Triple> packed_generator<Term, Triple>
pack_generator(packed_generator<Term, Triple>& gen) {return gen;}
template<class Term, class Triple> packed_generator<Term, Triple>
pack_generator(packed_generator<Term, Triple>&& gen) {return std::move(gen);}

template<class Generator>
packed_generator<typename Generator::term_t, typename Generator::triple_t>
pack_generator(Generator&& gen) {
    static_assert(std::is_base_of<vgenerator<typename Generator::term_t,
                                             typename Generator::triple_t>,
                                  typename std::decay<Generator>::type>::value,
                  "Generator must be a vgenerator");
    return packed_generator<typename Generator::term_t,
                            typename Generator::triple_t>(std::forward(gen));
}


}}  // namespace shacldator::dqry



#endif /* SRC_DQRY_PACKED_GENERATOR_HPP_ */
