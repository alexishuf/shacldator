/*
 * explicit_generator.hpp
 *
 *  Created on: May 30, 2018
 *      Author: alexis
 */

#ifndef SRC_INF_EXPLICIT_GENERATOR_HPP_
#define SRC_INF_EXPLICIT_GENERATOR_HPP_

#include <limits>
#include <boost/mpl/equal.hpp>
#include "../graph/node.hpp"
#include "../graph/str/triple.hpp"
#include "../graph/str/concepts/GraphStore.hpp"
#include "../viterator.hpp"
#include "../qry/dsl.hpp"
#include "../qry/sol.hpp"
#include "../qry/iterators/collection_iterator.hpp"
#include "triple.hpp"
#include "vgenerator.hpp"

namespace shacldator {namespace dqry {

namespace eg_helper {

template<class Term, class Placeholders> struct tr {
    typedef Placeholders phs_t;
    typedef str::triple<Term> result_type;
    typedef typename qry::sol<Term, phs_t>::type argument_type;
    typedef triple<Term> qt_t;

    BOOST_STATIC_ASSERT(
            boost::mpl::equal<phs_t,
                boost::mpl::vector<qry::ph<1>, qry::ph<2>, qry::ph<3>>
            >::type::value ||
            boost::mpl::equal<phs_t,
                boost::mpl::vector<qry::ph<1>, qry::ph<2>>
            >::type::value ||
            boost::mpl::equal<phs_t,
                boost::mpl::vector<qry::ph<1>>
            >::type::value);

    tr(const qt_t& q) : q(q) {}

    result_type operator()(const argument_type& sol) const {
        return result_type(
                q.subj.is_term() ? q.subj.term_v : sol.at_pos(q.subj.ph-1),
                q.pred.is_term() ? q.pred.term_v : sol.at_pos(q.pred.ph-1),
                q. obj.is_term() ? q. obj.term_v : sol.at_pos(q. obj.ph-1));
    }
private:
    qt_t q;
};

}  // namespace eg_helper

template<class Evaluator> struct explicit_generator :
        public vgenerator<typename Evaluator::graph_t::store_t::term_t> {
    typedef typename std::decay<Evaluator>::type ev_t;
    typedef typename ev_t::graph_t graph_t;
    typedef typename graph_t::store_t store_t;
    BOOST_CONCEPT_ASSERT((str::GraphStore<store_t>));
    typedef typename store_t::term_t term_t;
    typedef str::triple<term_t> triple_t;
private:
    typedef triple<term_t> qt_t;
    typedef fwd_viterator<triple_t> vit_t;
    typedef std::pair<vit_t, vit_t> range_t;
public:

    explicit_generator(ev_t ev, const qt_t& q)
                       : ev(std::move(ev)), query(q) {}
    explicit_generator(ev_t&& ev, const qt_t& q)
                       : ev(std::move(ev)), query(q) {}
    explicit_generator(const explicit_generator&) = default;
    explicit_generator(explicit_generator&&) = default;
    explicit_generator& operator=(explicit_generator&&) = default;
    explicit_generator& operator=(const explicit_generator&) = default;
    ~explicit_generator() = default;

    const triple_t& get() const {
        thread_local triple_t empty;
        if (!range) return empty;
        auto& r = range.get();
        return r.first == r.second ? empty : *r.first;
    }

    bool advance() {
        bool was_first = init_range();
        auto& r = range.get();
        if (r.first == r.second) return false;
        if (!was_first) {
            ++r.first;
            return r.first != r.second;
        }
        return true;
    }

private:
    static qt_t normalize(const qt_t& q) {
        typedef term<term_t> term;
        unsigned assigned = 0;
        term s(q.subj), p(q.pred), o(q.obj);
        if (q.subj.is_ph())          s = term::from_ph(++assigned);
        if (q.pred.is_ph()) {
            if (q.pred == q.subj)     p = s;
            else                      p = term::from_ph(++assigned);
        }
        if (q.obj.is_ph()) {
            if      (q.obj == q.subj) o = s;
            else if (q.obj == q.pred) o = p;
            else                      o = term::from_ph(++assigned);
        }
        return qt_t(s, p, o);
    }
    bool init_range() {
        using namespace eg_helper;
        using namespace qry;
        namespace mpl = boost::mpl;
        if (range) return false;
        query = normalize(query);
#define SB resource<graph_t>(query.subj.term_v, ev.graph())
#define PR resource<graph_t>(query.pred.term_v, ev.graph())
#define OB node<graph_t>(query. obj.term_v, ev.graph())
        tr<term_t, mpl::vector<ph<1>>> t1(query);
        tr<term_t, mpl::vector<ph<1>, ph<2>>> t2(query);
        tr<term_t, mpl::vector<ph<1>, ph<2>, ph<3>>> t3(query);
        int tgt = (query.subj.is_ph() ? query.subj.ph << 6 : 0)
                | (query.pred.is_ph() ? query.pred.ph << 4 : 0)
                | (query. obj.is_ph() ? query. obj.ph      : 0);
        switch (tgt) {
            case 0<<6 | 0<<4 | 0:
                range =  ev.ask(_t(SB, PR, OB))
                        ? mk_vsingleton_range(query.to_triple())
                        : empty_vrange<triple_t>();
                break;
            case 0<<6 | 0<<4 | 1:
                range =  mk_vrange(ev.str_eval(_t(SB, PR, _1)), t1);  break;
            case 0<<6 | 1<<4 | 0:
                range =  mk_vrange(ev.str_eval(_t(SB, _1, OB)), t1);  break;
            case 0<<6 | 1<<4 | 1:
                range =  mk_vrange(ev.str_eval(_t(SB, _1, _1)), t1);  break;
            case 0<<6 | 1<<4 | 2:
                range =  mk_vrange(ev.str_eval(_t(SB, _1, _2)), t2);  break;
            case 1<<6 | 0<<4 | 0:
                range =  mk_vrange(ev.str_eval(_t(_1, PR, OB)), t1);  break;
            case 1<<6 | 0<<4 | 1:
                range =  mk_vrange(ev.str_eval(_t(_1, PR, _1)), t1);  break;
            case 1<<6 | 0<<4 | 2:
                range =  mk_vrange(ev.str_eval(_t(_1, PR, _2)), t2);  break;
            case 1<<6 | 1<<4 | 0:
                range =  mk_vrange(ev.str_eval(_t(_1, _1, OB)), t1);  break;
            case 1<<6 | 1<<4 | 1:
                range =  mk_vrange(ev.str_eval(_t(_1, _1, _1)), t1);  break;
            case 1<<6 | 1<<4 | 2:
                range =  mk_vrange(ev.str_eval(_t(_1, _1, _2)), t2);  break;
            case 1<<6 | 2<<4 | 0:
                range =  mk_vrange(ev.str_eval(_t(_1, _2, OB)), t2);  break;
            case 1<<6 | 2<<4 | 1:
                range =  mk_vrange(ev.str_eval(_t(_1, _2, _1)), t2);  break;
            case 1<<6 | 2<<4 | 2:
                range =  mk_vrange(ev.str_eval(_t(_1, _2, _2)), t2);  break;
            case 1<<6 | 2<<4 | 3:
                range =  mk_vrange(ev.str_eval(_t(_1, _2, _3)), t3);  break;
#undef SB
#undef PR
#undef OB
            default: break;
        }
        if (!range) {
            std::stringstream ss;
            ss << "explicit_generator::mk_range: unexpected tgt "
                    << std::hex << tgt;
            throw std::logic_error(ss.str());
        }
        return true;
    }

    ev_t ev;
    qt_t query;
    pack<std::pair<vit_t, vit_t>, false> range;
};

}}  // namespace shacldator::dqry



#endif /* SRC_INF_EXPLICIT_GENERATOR_HPP_ */
