/*
 * qry_term.hpp
 *
 *  Created on: May 23, 2018
 *      Author: alexis
 */

#ifndef SRC_INF_QRY_TERM_HPP_
#define SRC_INF_QRY_TERM_HPP_

#include <limits>
#include <type_traits>
#include <boost/static_assert.hpp>
#include <ostream>
#include <cstring>
#include <map>
#include <sstream>
#include "../graph/lexical.hpp"
#include "../graph/exceptions.hpp"
#include "../graph/node.hpp"
#include "../graph/str/concepts/Term.hpp"
#include "../qry/lit.hpp"
#include "../qry/dsl.hpp"

namespace shacldator {namespace dqry {

template<class Term> struct term {
    BOOST_CONCEPT_ASSERT((str::Term<Term>));
    typedef Term term_t;

    const term_t term_v;
    const unsigned ph;

    term() = delete;
    term(const term&) = default;
    term& operator=(const term& o) {
        if (this != &o) {
            const_cast<term_t&>(term_v) = o.term_v;
            const_cast<unsigned&>(ph) = o.ph;
        }
        return *this;
    }

    static term from_term(term_t t) {
        if (!t)
            throw std::invalid_argument("dqry::term from null term");
        return term(t, std::numeric_limits<unsigned>::max());
    }
    inline static term from_ph(unsigned id) {return term(term_t(), id);}

    template<class GS> static term
    parse_resource(GS& gs, const char* uri,
                   std::map<std::string, unsigned>& name2ph) {
        BOOST_STATIC_ASSERT(std::is_same<typename GS::term_t, Term>::value);
        if (!std::strncmp("?", uri, 1)) {
            std::string key(uri);
            auto p = name2ph.insert(std::make_pair(key, name2ph.size()+1));
            return term::from_ph(p.first->second);
        } else {
            Term t = gs.resource(uri);
            if (!t)
                throw term_not_found(std::string(uri) + " not found");
            return term::from_term(t);
        }
    }

    template<class GS>
    static term parse(GS& gs, const char* c_string,
                      std::map<std::string, unsigned>& name2ph) {
        BOOST_STATIC_ASSERT(std::is_same<typename GS::term_t, Term>::value);
        std::string string(c_string);
        if (!std::strncmp("?", c_string, 1)) {
            auto p = name2ph.insert(std::make_pair(string, name2ph.size()+1));
            return term::from_ph(p.first->second);
        } else {
            lexical_info li = parse_lexical(string);
            Term t = Term();
            if (li.lex_begin) {
                t = gs.literal(li. get_lex(string).c_str(),
                               li.  get_dt(string).c_str(),
                               li.get_lang(string).c_str());
            } else {
                t = gs.resource(c_string);
            }
            if (!t)
                throw term_not_found(string + " not found");
            return term::from_term(t);
        }
    }

    template<class GS>
    static term parse_resource(GS& gs, const std::string& str,
                               std::map<std::string, unsigned>& name2ph) {
        return parse_resource(gs, str.c_str(), name2ph);
    }
    template<class GS>
    static term parse(GS& gs, const std::string& str,
                      std::map<std::string, unsigned>& name2ph) {
        return parse(gs, str.c_str(), name2ph);
    }

    template<class GS, class Graph>
    static term parse_resource(GS& gs, const node<Graph>& node,
                               std::map<std::string, unsigned>& name2ph) {
        if (!node)
            throw term_not_found("parse: null node<>");
        else if (!node.is_resource())
            throw term_not_found("parse_resource: given node is not a resource");
        else if (&node.graph().store() == &gs)
            return term::from_term(node.term());
        else
            return parse(gs, node.to_str(), name2ph);
    }
    template<class GS, class Graph>
    static term parse(GS& gs, const node<Graph>& node,
                      std::map<std::string, unsigned>& name2ph) {
        if (!node)
            throw term_not_found("parse: null node<>");
        else if (&node.graph().store() == &gs)
            return term::from_term(node.term());
        else
            return parse(gs, node.to_str(), name2ph);
    }

    template<class GS>
    static term parse(GS& gs, const qry::lit& lit,
                      std::map<std::string, unsigned>& ignored) {
        auto t = gs.literal(lit.lex.get(), lit.dt.get(), lit.lang.get());
        if (!t)
            throw term_not_found("parse: literal not found: " + lit.to_str());
        return term::from_term(t);
    }

    template<class GS, int n>
    static term parse_resource(GS& gs, const qry::ph<n>& ph,
                               std::map<std::string, unsigned>& ignored) {
        static_assert(n >= 0, "non-negative n required");
        return term::from_ph(n);
    }
    template<class GS, int n>
    static term parse(GS& gs, const qry::ph<n>& ph,
                      std::map<std::string, unsigned>& ignored) {
        static_assert(n >= 0, "non-negative n required");
        return term::from_ph(n);
    }

    template<class GS> std::string to_str(const GS& gs) const {
        if (is_term()) return gs.str(term_v);
        std::stringstream ss;
        ss << "?" << ph;
        return ss.str();
    }

    inline bool subsumes(const term& o) const {
        return is_ph() || *this == o;
    }
    inline bool is_term() const {return term_v;}
    inline bool is_ph() const {return !term_v;}

    bool operator==(const term& o) const {
        return term_v == o.term_v && ph == o.ph;
    }
    bool operator<(const term& o) const {
        return term_v < o.term_v || (term_v == o.term_v && ph < o.ph);
    }
    bool operator!=(const term& o) const {return !operator==(o);}
    bool operator<=(const term& o) const {return *this < o || *this == o;}
    bool operator>=(const term& o) const {return !operator<(o);}
    bool operator>(const term& o) const {return !operator<=(o);}


private:
    term(term_t term, unsigned ph) : term_v(term), ph(ph) {}
};

}}  // namespace shacldator::dqry

namespace std {
template<class Term> ostream&
operator<<(ostream& o, const shacldator::dqry::term<Term>& t) {
    if (t.is_term()) return o << t.term_v;
    return o << "_" << t.ph;
}
}  // namespace std

#endif /* SRC_INF_QRY_TERM_HPP_ */
