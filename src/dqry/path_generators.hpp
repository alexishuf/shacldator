/*
 * path_generator.hpp
 *
 *  Created on: Jun 2, 2018
 *      Author: alexis
 */

#ifndef SRC_DQRY_PATH_GENERATORS_HPP_
#define SRC_DQRY_PATH_GENERATORS_HPP_

#include <memory>
#include <cassert>
#include <atomic>
#include <utility>
#include <vector>
#include <deque>
#include <ostream>
#include <boost/range.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/range/algorithm_ext.hpp>
#include "explicit_generator.hpp"
#include "../utils.hpp"
#include "../graph/str/concepts/Term.hpp"

namespace shacldator {namespace dqry {namespace path {

template<class Term> struct generator {
    BOOST_CONCEPT_ASSERT((str::Term<Term>));
    typedef Term term_t;

    virtual ~generator() = default;
    virtual term_t get() const = 0;
    virtual void set_subj(term_t subj) = 0;
    virtual bool advance() = 0;
    virtual generator* clone() const = 0;

    struct cloner {
        typedef const generator<Term>& argument_type;
        typedef generator<Term>* result_type;
        result_type operator()(argument_type a) const {return a.clone();}
    };
};

template<class Term> struct packed_generator : public generator<Term> {
    typedef Term term_t;

    packed_generator(const packed_generator& o)  = default;
    packed_generator& operator=(const packed_generator& o) = default;
    packed_generator(packed_generator&& o) = default;
    packed_generator& operator=(packed_generator&& o) = default;
    packed_generator(const generator<Term>& g) : d(g.clone()) {}

    packed_generator* clone() const {return new packed_generator(*this);}
    term_t get() const {return d->get();}
    void set_subj(term_t subj) {d->set_subj(subj);}
    bool advance() {return d->advance();}
private:
    copyable_ptr<generator<term_t>, typename generator<term_t>::cloner> d;
};

template<class Gen> struct as_packed_h {
    typedef packed_generator<typename Gen::term_t> type;
};
template<class Term> struct as_packed_h<packed_generator<Term>> {
    typedef packed_generator<Term> type;
};
template<class Gen> using as_packed = typename as_packed_h<Gen>::type;

template<class Gen> struct packer {
    typedef const Gen& argument_type;
    typedef packed_generator<typename Gen::term_t> result_type;
    result_type operator()(argument_type arg) const {
        return result_type(arg);
    }
};
template<class Term> struct packer<packed_generator<Term>> {
    typedef packed_generator<Term> tp;
    tp& operator()(tp& arg) const      {return tp(arg);}
    tp operator()(tp&& arg) const      {return tp(std::move(arg));}
    tp operator()(const tp& arg) const {return tp(arg);}
};

template<class Term> struct generator_iterator {
    BOOST_CONCEPT_ASSERT((str::Term<Term>));
    typedef packed_generator<Term> gen_t;
    typedef Term value_type;
    typedef std::forward_iterator_tag iterator_category;
    typedef std::ptrdiff_t difference_type;
    typedef const value_type& reference;
    typedef const value_type* pointer;

    static const std::size_t epos = std::numeric_limits<std::size_t>::max();

    generator_iterator() : pos(epos), curr() {}
    generator_iterator(gen_t&& g): gen_p(std::move(g)), pos(), curr() {init();}
    generator_iterator(const gen_t& g) : gen_p(g), pos(), curr() {init();}
    generator_iterator(const generator_iterator&) = default;
    generator_iterator(generator_iterator&&) = default;
    generator_iterator& operator=(const generator_iterator&) = default;
    generator_iterator& operator=(generator_iterator&&) = default;

    inline bool at_end() const {return pos == epos;}

    bool operator==(const generator_iterator& o) const {
        return pos == o.pos;
    }
    bool operator!=(const generator_iterator& o) const {return !operator==(o);}

    reference operator*() const {
        assert(!at_end() && gen_p);
        return curr;
    }
    pointer operator->() const {return &operator*();}
    generator_iterator& operator++() {
        assert(!at_end() && gen_p);
        if (gen_p.get().advance()) ++pos;
        else                         pos = epos;
        curr = at_end() ? value_type() : gen_p.get().get();
        return *this;
    }
    generator_iterator operator++(int) {
        generator_iterator cp(*this);
        ++*this;
        return cp;
    }

private:
    void init() {
        bool e = !gen_p.get().get();
        if (e) e = !gen_p.get().advance();
        if (!e) {
            curr = gen_p.get().get();
            pos = 0;
        } else {
            pos = epos;
        }
    }

    template<class X> friend std::ostream&
            operator<<(std::ostream&, const generator_iterator<X>&);
    pack<gen_t, false> gen_p;
    std::size_t pos;
    value_type curr;
};

template<class Term>
std::ostream& operator<<(std::ostream& o,
                         const generator_iterator<Term>& i) {
    static const std::size_t epos = generator_iterator<Term>::epos;
    o << "path::gen_it(";
    if (i.pos != generator_iterator<Term>::epos)
        o << i.pos;
    return o << ")";
}

template<class Term> generator_iterator<Term>
mk_generator_begin(packed_generator<Term> gen) {
    return generator_iterator<Term>(std::move(gen));
}
template<class Generator> generator_iterator<typename Generator::term_t>
mk_generator_begin(Generator& gen) {
    return mk_generator_begin(as_packed<Generator>(gen));
}
template<class Generator> generator_iterator<typename Generator::term_t>
mk_generator_end(const Generator& dummy) {
    return generator_iterator<typename Generator::term_t>();
}
template<class Generator>
std::pair<generator_iterator<typename Generator::term_t>,
          generator_iterator<typename Generator::term_t>>
mk_generator_range(const Generator& gen) {
    return std::make_pair(mk_generator_begin(gen),
                          generator_iterator<typename Generator::term_t>());
}


template<class Term> struct empty : generator<Term> {
    typedef Term term_t;

    empty& operator=(const empty&) = default;

    empty* clone() const {return new empty();}
    term_t get() const {return term_t();}
    void set_subj(term_t) {}
    bool advance() {return false;}
};

template<class Evaluator> struct prop :
        public generator<typename Evaluator::graph_t::store_t::term_t> {
    typedef Evaluator ev_t;
    typedef typename Evaluator::graph_t::store_t::term_t term_t;
    typedef dqry::triple<term_t> qtriple_t;
    typedef dqry::term<term_t> qterm_t;

    prop(const term_t& pred, ev_t&& e) : pred(pred), ev(std::move(e)) {}
    prop(const term_t& pred, const ev_t& e) : pred(pred), ev(e) {}
    prop(const prop&) = default;
//    prop& operator=(const prop&) = default;
    prop(prop&&) = default;
    prop& operator=(prop&&) = default;

    prop* clone() const {return new prop(*this);}
    term_t get() const {return eg ? eg->get().obj() : term_t();}
    void set_subj(term_t subj) {
        if (!subj || !pred) {eg.reset(); return;}
        eg.reset(new explicit_generator<ev_t>(ev,
                qtriple_t(qterm_t::from_term(subj),
                          qterm_t::from_term(pred),
                          qterm_t::from_ph(1))));
    }
    bool advance() {return eg ? eg->advance() : false;}
private:
    term_t pred;
    ev_t ev;
    copyable_ptr<explicit_generator<ev_t>> eg;
};

template<class Evaluator> struct rev :
        public generator<typename Evaluator::graph_t::store_t::term_t> {
    typedef Evaluator ev_t;
    typedef typename Evaluator::graph_t::store_t::term_t term_t;
    typedef dqry::triple<term_t> qtriple_t;
    typedef dqry::term<term_t> qterm_t;

    rev(const term_t& pred, ev_t&& e) : pred(pred), ev(std::move(e)) {}
    rev(const term_t& pred, const ev_t& e) : pred(pred), ev(e) {}
    rev(const rev&) = default;
//    rev_prop& operator=(const rev_prop&) = default;
    rev(rev&&) = default;
    rev& operator=(rev&&) = default;

    rev* clone() const {return new rev(*this);}
    term_t get() const {return eg ? eg->get().subj() : term_t();}
    void set_subj(term_t subj) {
        if (!subj || !pred) {eg.reset(); return;}
        eg.reset(new explicit_generator<ev_t>(ev,
                qtriple_t(qterm_t::from_ph(1),
                          qterm_t::from_term(pred),
                          qterm_t::from_term(subj))));
    }
    bool advance() {return eg ? eg->advance() : false;}
private:
    term_t pred;
    ev_t ev;
    copyable_ptr<explicit_generator<ev_t>> eg;
};

template<class Evaluator> struct neg :
        public generator<typename Evaluator::graph_t::store_t::term_t> {
    typedef Evaluator ev_t;
    typedef typename Evaluator::graph_t::store_t::term_t term_t;
    typedef dqry::triple<term_t> qtriple_t;
    typedef dqry::term<term_t> qterm_t;

    neg(const term_t& pred, ev_t&& ev) : pred(pred), ev(std::move(ev)) {}
    neg(const term_t& pred, const ev_t& ev) : pred(pred), ev(ev) {}
    neg(const neg&) = default;
    neg(neg&&) = default;
//    neg& operator=(const neg&) = default;
    neg& operator=(neg&&) = default;

    neg* clone() const {return new neg(*this);}
    term_t get() const {return eg ? eg->get().obj() : term_t();}
    void set_subj(term_t subj) {
        if (!subj) {eg.reset(); return;}
        eg.reset(new explicit_generator<ev_t>(ev,
                qtriple_t(qterm_t::from_term(subj),
                          qterm_t::from_ph(1),
                          qterm_t::from_ph(2))));
    }
    bool advance() {
        if (!eg) return false; //no subj set
        bool has_value = false;
        while ((has_value = eg->advance()) && eg->get().pred() == pred) ;
        return has_value;
    }

private:
    term_t pred;
    ev_t ev;
    copyable_ptr<explicit_generator<ev_t>> eg;
};

template<class Term> struct alt : public generator<Term> {
    typedef Term term_t;
    static const std::size_t spos = std::numeric_limits<std::size_t>::max();
    alt(std::vector<packed_generator<term_t>>&& vec)
        : vec(std::move(vec)), i(spos) {}
    template<class Range> alt(const Range& rng) : i(spos) {
        boost::transform(rng, std::back_inserter(vec),
                         packer<generator<Term>>());
    }
    alt(const alt&) = default;
    alt(alt&&) = default;
    alt& operator=(const alt&) = default;
    alt& operator=(alt&&) = default;

    alt* clone() const {return new alt(*this);}
    term_t get() const {return i < vec.size() ? vec[i].get() : term_t();}
    void set_subj(term_t s) {
        i = 0;
        for(auto i = vec.begin(), e = vec.end(); i != e; ++i) i->set_subj(s);
    }
    bool advance() {
        if (i == spos) i = 0;
        for (; i < vec.size(); ++i) {
            if (vec[i].advance()) return true;
        }
        return false;
    }

private:
    std::vector<packed_generator<term_t>> vec;
    std::size_t i;
};

template<class Term> struct star : public generator<Term> {
    typedef Term term_t;
    template<class U> star(U&& u) : gen(std::forward<U>(u)), pen1(), pen2() {}
    star(const star&) = default;
    star(star&&) = default;
    star& operator=(const star&) = default;
    star& operator=(star&&) = default;

    star* clone() const {return new star(*this);}
    term_t get() const {return pen1 || pen2 ? pen1 : gen.get();}
    void set_subj(term_t s) {
        queue.clear();
        pen1 = term_t();
        pen2 = s;
        gen.set_subj(s);
    }
    bool advance() {
        if (pen2) {
            pen1 = pen2;
            pen2 = term_t();
            return true;
        } else if (pen1) {
            pen1 = term_t();
        }
        while (true) {
            if (gen.advance()) {
                queue.push_back(gen.get());
                return true;
            } else if (queue.empty()) {
                return false;
            } else {
                gen.set_subj(queue.front());
                queue.pop_front();
            }
        }
    }

private:
    packed_generator<term_t> gen;
    std::deque<term_t> queue;
    term_t pen1, pen2;
};

template<class Term> struct plus : public generator<Term> {
    typedef Term term_t;
    template<class U> plus(U&& u) : gen(std::forward<U>(u)){}
    plus(const plus&) = default;
    plus(plus&&) = default;
    plus& operator=(const plus&) = default;
    plus& operator=(plus&&) = default;

    plus* clone() const {return new plus(*this);}
    term_t get() const {return gen.get();}
    void set_subj(term_t s) {
        queue.clear();
        gen.set_subj(s);
    }
    bool advance() {
        while (true) {
            if (gen.advance()) {
                queue.push_back(gen.get());
                return true;
            } else if (queue.empty()) {
                return false;
            } else {
                gen.set_subj(queue.front());
                queue.pop_front();
            }
        }
    }

private:
    packed_generator<term_t> gen;
    std::deque<term_t> queue;
};

template<class Term> struct opt : public generator<Term> {
    typedef Term term_t;
    template<class U> opt(U&& u) : gen(std::forward<U>(u)), pen1(), pen2() {}
    opt(const opt&) = default;
    opt(opt&&) = default;
    opt& operator=(const opt&) = default;
    opt& operator=(opt&&) = default;

    opt* clone() const {return new opt(*this);}
    term_t get() const {return pen2 || pen1 ? pen1 : gen.get();}
    void set_subj(term_t s) {
        pen1 = term_t(); //before advance(), get() returns pen1
        gen.set_subj(pen2 = s);
    }
    bool advance() {
        if (pen2) { //assume !pen1
            pen1 = pen2; // get() will return pen1
            pen2 = term_t();
            return true;
        } else if (pen1) {
            pen1 = term_t(); //!pen2 && !pen1 makes get() delegate to gen
        }
        return gen.advance();
    }

private:
    packed_generator<term_t> gen;
    term_t pen1, pen2;
};

template<class Term> struct seq : public generator<Term> {
    typedef Term term_t;
    static const std::size_t npos = std::numeric_limits<std::size_t>::max()-1;
    seq(std::vector<packed_generator<term_t>>&& vec) : vec(std::move(vec)),
                                                     i(npos) {}
    template<class Range> seq(const Range& rng) : i(npos) {
        typedef typename boost::range_value<Range>::type v_t;
        boost::transform(rng, std::inserter(vec, vec.end()),
                         packer<v_t>());
    }
    seq(const seq&) = default;
    seq(seq&&) = default;
    seq& operator=(const seq&) = default;
    seq& operator=(seq&&) = default;

    seq* clone() const {return new seq(*this);}
    term_t get() const {return i == (vec.size()-1) ? vec[i].get() : term_t();}
    void set_subj(term_t subj) {
        i = 0;
        if (!vec.empty())
            vec[0].set_subj(subj);
    }
    bool advance() {
        if (vec.empty()) return false;
        std::size_t epos = vec.size()-1;
        while (i < vec.size()) {
            if (!vec[i].advance()) {
                --i; //may overflow to npos, which is > vec.size()
            } else if (i < epos) {
                ++i;
                vec[i].set_subj(vec[i-1].get());
            } else {
                break; // i == epos & vec[i].advance() == true
            }
        }
        return i == epos;
    }
private:
    std::vector<packed_generator<term_t>> vec;
    std::size_t i;
};

}}}  // namespace shacldator::dqry::path



#endif /* SRC_DQRY_PATH_GENERATORS_HPP_ */
