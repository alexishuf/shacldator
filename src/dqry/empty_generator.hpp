/*
 * empty_generator.hpp
 *
 *  Created on: Jun 22, 2018
 *      Author: alexis
 */

#ifndef SRC_DQRY_EMPTY_GENERATOR_HPP_
#define SRC_DQRY_EMPTY_GENERATOR_HPP_

#include "vgenerator.hpp"

namespace shacldator {namespace dqry {

template<class Term, class Triple=str::triple<Term>>
struct empty_generator : public vgenerator<Term, Triple> {
    typedef Term term_t;
    typedef Triple triple_t;

    const triple_t& get() const {
        thread_local triple_t e;
        return e;
    }
    bool advance() {return false;}
};


}}  // namespace shacldator::dqry




#endif /* SRC_DQRY_EMPTY_GENERATOR_HPP_ */
