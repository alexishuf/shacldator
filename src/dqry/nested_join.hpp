/*
 * join_generator.hpp
 *
 *  Created on: May 27, 2018
 *      Author: alexis
 */

#ifndef SRC_INF_NESTED_JOIN_HPP_
#define SRC_INF_NESTED_JOIN_HPP_

#include <vector>
#include <utility>
#include <sstream>
#include <cassert>
#include <boost/range/algorithm.hpp>
#include <boost/function.hpp>
#include "concepts/Generator.hpp"
#include "triple.hpp"

namespace shacldator {namespace dqry {

template<class Gen> struct nested_join {
    typedef typename Gen::term_t term_t;
    typedef triple<term_t> qry_triple_t;
    typedef term<term_t> qry_term_t;
    typedef str::triple<term_t> triple_t;
    typedef boost::function<Gen(const qry_triple_t&)> gen_fac_t;

    nested_join(gen_fac_t fac, std::vector<qry_triple_t>&& goals_rv)
                   : mk_gen(fac), goals(std::move(goals_rv)),
                     bound(get_solution_size(goals)) {
        order.reserve(goals.size());
        for (std::size_t i = 0; i < goals.size(); ++i) order.push_back(i);
        boost::sort(order, cost_lt(goals));
    }
    nested_join(gen_fac_t fac, std::vector<qry_triple_t>&& goals_rv,
                const qry_triple_t& subsuming, const qry_triple_t& subsumed)
                : nested_join(std::move(goals_rv), fac) {
        if (subsuming.max_ph() >= bound.size())
            throw std::invalid_argument("subsuming has out_of_range ph");
        if (!subsuming.subsumes(subsumed))
            order.clear();  //inconsistent, force no result
        if (subsuming.subj.is_ph() && subsumed.subj.is_term()) {
            bound[subsuming.subj.ph] = subsumed.subj.term_v;
            bound.hard[subsuming.subj.ph]  = true;
        }
        if (subsuming.pred.is_ph() && subsumed.pred.is_term()) {
            bound[subsuming.pred.ph] = subsumed.pred.term_v;
            bound.hard[subsuming.pred.ph]  = true;
        }
        if (subsuming. obj.is_ph() && subsumed. obj.is_term()) {
            bound[subsuming. obj.ph] = subsumed. obj.term_v;
            bound.hard[subsuming. obj.ph]  = true;
        }
    }
    nested_join(std::vector<qry_triple_t> goals, gen_fac_t fac)
                : nested_join(fac, std::move(goals)) {}
    nested_join(std::vector<qry_triple_t> goals, gen_fac_t fac,
                const qry_triple_t& subsuming, const qry_triple_t& subsumed)
                : nested_join(fac, std::move(goals), subsuming, subsumed) {}
    nested_join(const nested_join&) = default;
    nested_join(nested_join&&) = default;
    nested_join& operator=(const nested_join&) = default;
    nested_join& operator=(nested_join&&) = default;

    struct solution : public std::vector<term_t> {
        template<class X> friend struct nested_join;
        solution() : m_complete(false) {}
        solution(unsigned int max_phs)
                 : std::vector<term_t>(max_phs, term_t()),
                   hard(max_phs, false), m_complete(false) {}
        solution(solution&&) = default;
        solution& operator=(solution&&) = default;
        solution(const solution&) = default;
        solution& operator=(const solution&) = default;

        bool is_hard(unsigned ph) const {
            assert(ph < hard.size());
            return hard[ph];
        }
        bool contains(unsigned ph) const {
            assert(ph < this->size());
            return (*this)[ph];
        }
        void unset(unsigned ph) {
            assert(ph < this->size());
            (*this)[ph] = term_t();
        }

        bool complete() const {return m_complete;}

        qry_term_t partial_bind(const qry_term_t& t) const {
            if (t.is_term() || t.ph >= this->size()) return t;
            term_t val = (*this)[t.ph];
            return val ? qry_term_t::from_term(val) : t;
        }
        qry_triple_t partial_bind(const qry_triple_t& t) const {
            return qry_triple_t(partial_bind(t.subj), partial_bind(t.pred),
                                partial_bind(t. obj));
        }

        term_t bind(const qry_term_t& t) const {
            if (t.is_term()) return t.term_v;
            if (t.ph > this->size())
                throw std::invalid_argument("Placeholder out of range");
            if (!(*this)[t.ph])
                throw std::logic_error("Tried to bind(), put ph is undefined");
            return (*this)[t.ph];
        }
        triple_t bind(const qry_triple_t& t) const {
            return triple_t(bind(t.subj), bind(t.pred), bind(t.obj));
        }

    private:
        std::vector<bool> hard;
        bool m_complete;
    };

    solution get() const {
        std::size_t sz = order.size();
        if (sz && sz == gens.size()) return bound;
        solution s(bound.size());
        s.hard = bound.hard;
        for (unsigned i = 0; i < s.size(); ++i) {
            if (s.hard[i]) s[i] = bound[i];
        }
        return s;
    }

    bool advance() {
        if (order.empty()) return false; //at end
        std::ptrdiff_t i = gens.empty() ? 0 : gens.size()-1, e = order.size();
        while (i >= 0 && i < e) {
            if (i >= gens.size()) //crete generator, if needed
                gens.push_back(mk_gen(bound.partial_bind(goals[order[i]])));
            assert(i == (gens.size()-1)); //i is the last in gens
            if (!gens.back().advance()) { //if cannot advance, retract
                bound_remove(i);
                gens.erase(gens.end()-1);
                --i;
            } else {
                bound_add(i, gens.back().get());
                ++i;
            }
        }
        assert(i == -1 || i == e);
        return bound.m_complete = i == e;
    }

private:
    void bound_remove(std::size_t pos, const term<term_t>& t) {
        assert(t.is_ph());
        if (bound.is_hard(t.ph)) return;
        for (std::ptrdiff_t i = pos-1; i >= 0; --i) {
            if (goals[order[i]].contains_term(t)) return;
        }
        bound.unset(t.ph);
    }
    void bound_remove(std::size_t pos) {
        const qry_triple_t& t = goals[order[pos]];
        if (t.subj.is_ph()) bound_remove(pos, t.subj);
        if (t.pred.is_ph()) bound_remove(pos, t.pred);
        if (t. obj.is_ph()) bound_remove(pos, t. obj);
    }
    bool is_first_ph(std::size_t pos, const qry_term_t& t) {
        if (!t.is_ph() || bound.is_hard(t.ph)) return false;
        if (!bound[t.ph]) return true;
        for (std::size_t i = 0; i < pos; i++) {
            if (goals[i].contains_term(t)) return false;
        }
        return true;
    }
    void bound_add(std::size_t pos, const triple_t& t) {
        const qry_triple_t& g = goals[order[pos]];
        if (is_first_ph(pos, g.subj)) bound[g.subj.ph] = t.subj();
        if (is_first_ph(pos, g.pred)) bound[g.pred.ph] = t.pred();
        if (is_first_ph(pos, g. obj)) bound[g. obj.ph] = t. obj();
    }

    static unsigned get_solution_size(const std::vector<qry_triple_t>& g) {
        unsigned m = 0;
        for (auto i = g.begin(), e = g.end(); i != e; ++i)
            m = std::max(m, i->max_ph());
        return m+1;
    }

    struct cost_lt {
        typedef std::size_t first_argument_type;
        typedef std::size_t second_argument_type;
        typedef bool result_type;
        cost_lt(const std::vector<qry_triple_t>& goals) : goals(goals) {}
        bool operator()(const std::size_t l, const std::size_t r) const {
            return goals[l].phs_count() < goals[r].phs_count();
        }
        const std::vector<qry_triple_t>& goals;
    };

    gen_fac_t mk_gen;
    std::vector<qry_triple_t> goals;
    solution bound;
    std::vector<std::size_t> order;
    std::vector<Gen> gens;
};

}}  // namespace shacldator::dqry



#endif /* SRC_INF_NESTED_JOIN_HPP_ */
