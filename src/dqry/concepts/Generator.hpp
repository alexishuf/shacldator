/*
 * Generator.hpp
 *
 *  Created on: May 27, 2018
 *      Author: alexis
 */

#ifndef SRC_INF_CONCEPTS_GENERATOR_HPP_
#define SRC_INF_CONCEPTS_GENERATOR_HPP_

#include <type_traits>
#include <boost/concept_check.hpp>
#include "../../graph/str/triple.hpp"
#include "../../graph/str/concepts/Term.hpp"
#include "../triple.hpp"

namespace shacldator {namespace dqry {

template<class X> struct Generator {
    typedef typename X::term_t term_t;
    typedef typename X::triple_t triple_t;

    BOOST_CONCEPT_ASSERT((str::Term<term_t>));
    BOOST_STATIC_ASSERT(std::is_copy_constructible<X>::value);
    BOOST_CONCEPT_ASSERT((boost::DefaultConstructible<triple_t>));
    BOOST_CONCEPT_ASSERT((boost::CopyConstructible<triple_t>));
    BOOST_CONCEPT_ASSERT((boost::EqualityComparable<triple_t>));
    BOOST_CONCEPT_ASSERT((boost::LessThanComparable<triple_t>));

    BOOST_CONCEPT_USAGE(Generator) {
        bool ok = g.advance();
        triple_t t = cg.get();
        BOOST_STATIC_ASSERT(std::is_reference<decltype(cg.get())>::value);

        term_t t_s = t.subj();
        term_t t_p = t.pred();
        term_t t_o = t. obj();
    }
private:
    X g;
    const X cg;
};

}}  // namespace shacldator::dqry



#endif /* SRC_INF_CONCEPTS_GENERATOR_HPP_ */
