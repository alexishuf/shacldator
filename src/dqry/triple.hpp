/*
 * qry_triple.hpp
 *
 *  Created on: May 23, 2018
 *      Author: alexis
 */

#ifndef SRC_INF_QRY_TRIPLE_HPP_
#define SRC_INF_QRY_TRIPLE_HPP_

#include <ostream>
#include "../graph/str/triple.hpp"
#include "term.hpp"

namespace shacldator {namespace dqry {

template<class Term> struct triple {
    typedef term<Term> dqry_term_t;
    const dqry_term_t subj, pred, obj;

    explicit triple(const str::triple<Term>& t)
                        : triple(dqry_term_t::from_term(t.subj()),
                                 dqry_term_t::from_term(t.pred()),
                                 dqry_term_t::from_term(t. obj())) {}
    triple(const dqry_term_t& subj, const dqry_term_t& pred,
           const dqry_term_t& obj) : subj(subj), pred(pred), obj(obj) {}
    ~triple() = default;
    triple(const triple&) = default;
    triple& operator=(const triple& o) {
        if (this != &o) {
            const_cast<dqry_term_t&>(subj) = o.subj;
            const_cast<dqry_term_t&>(pred) = o.pred;
            const_cast<dqry_term_t&>( obj) = o. obj;
        }
        return *this;
    }

    template<class GS, class S, class P, class O> static triple
    parse(GS& gs, const S& s, const P& p, const O& o,
          std::map<std::string, unsigned>& name2id)  {
        return triple(dqry_term_t::parse_resource(gs, s, name2id),
                      dqry_term_t::parse_resource(gs, p, name2id),
                      dqry_term_t::parse(gs, o, name2id));
    }
    template<class GS, class S, class P, class O> inline static triple
    parse(GS& gs, const S& s, const P& p, const O& o)  {
        std::map<std::string, unsigned> name2id;
        return parse(gs, s, p, o, name2id);
    }

    template<class GS> std::string to_str(const GS& gs) const {
        std::stringstream ss;
        ss << subj.to_str(gs) << " " << pred.to_str(gs)
                              << " " << obj.to_str(gs);
        return ss.str();
    }

    bool contains_term(const dqry_term_t& t) const {
        return subj == t || pred == t || obj == t;
    }
    bool share_ph(const triple& o) const {
        if (subj.is_ph() && o.contains_term(subj)) return true;
        if (pred.is_ph() && o.contains_term(pred)) return true;
        if (obj .is_ph() && o.contains_term(obj )) return true;
        return false;
    }
    unsigned phs_count() const {
        return (subj.is_ph() ? 1 : 0) + (pred.is_ph() ? 1 : 0) +
               ( obj.is_ph() ? 1 : 0);
    }
    unsigned max_ph() const {
        return std::max(std::max((subj.is_ph() ? subj.ph : 0),
                                 (pred.is_ph() ? pred.ph : 0)),
                        (obj.is_ph() ? obj.ph : 0));
    }
private:
    static bool distinct(const dqry_term_t& a, const dqry_term_t& b) {
        return a != b && (a.is_term() || b.is_term());
    }
    inline static bool same_ph(const dqry_term_t& a, const dqry_term_t& b) {
        return a.is_ph() && a == b;
    }
public:
    bool subsumes(const triple& o) const {
        if (!subj.subsumes(o.subj) || !pred.subsumes(o.pred)
                                   || !obj .subsumes(o. obj))
            return false;
        if (same_ph(subj, pred) && distinct(o.subj, o.pred)) return false;
        if (same_ph(subj,  obj) && distinct(o.subj, o. obj)) return false;
        if (same_ph(pred,  obj) && distinct(o.pred, o. obj)) return false;
        return true;
    }
    str::triple<Term> to_triple() const {
        if (phs_count())
            throw std::logic_error("Cannot to_triple() with placeholders");
        return str::triple<Term>(subj.term_v, pred.term_v, obj.term_v);
    }

    bool operator==(const triple& o) const {
        return subj == o.subj && pred == o.pred && obj == o.obj;
    }
    bool operator<(const triple& o) const {
        if (subj < o.subj) return true;
        else if (subj != o.subj) return false;
        if (pred < o.pred) return true;
        else if (pred != o.pred) return false;
        if (obj < o.obj) return true;
        return false;
    }
    bool operator!=(const triple& o) const {return !operator==(o);}
    bool operator<=(const triple& o) const {return *this < o || *this == o;}
    bool operator>=(const triple& o) const {return !operator<(o);}
    bool operator>(const triple& o) const {return!operator<=(o);}
};

}}  // namespace shacldator::dqry

namespace std {
template<class Term> ostream&
operator<<(ostream& o, const shacldator::dqry::triple<Term>& t) {
    return o << "[" << t.subj << " " << t.pred << " " << t.obj << "]";
}
}  // namespace std



#endif /* SRC_INF_QRY_TRIPLE_HPP_ */
