/*
 * node_type.hpp
 *
 *  Created on: Mar 24, 2018
 *      Author: alexis
 */

#ifndef SRC_GRAPH_STR_TERMS_HPP_
#define SRC_GRAPH_STR_TERMS_HPP_

namespace shacldator{namespace terms {

typedef enum {
    NONE = 0x0,
    LITERAL = 0x01,
    BLANK = 0x02,
    URI = 0x03,
    _TYPE_COUNT
} type;

inline bool is_resource(type t) {return t &  0x02   ;}
inline bool is_blank   (type t) {return t == BLANK  ;}
inline bool is_literal (type t) {return t == LITERAL;}

}} //namespace shacldator::terms



#endif /* SRC_GRAPH_STR_TERMS_HPP_ */
