/*
 * exceptions.hpp
 *
 *  Created on: Jun 1, 2018
 *      Author: alexis
 */

#ifndef SRC_GRAPH_EXCEPTIONS_HPP_
#define SRC_GRAPH_EXCEPTIONS_HPP_

#include <stdexcept>

namespace shacldator {

struct term_not_found : public std::out_of_range {
    term_not_found(const char* s) : std::out_of_range(s) {}
    term_not_found(const std::string& s) : std::out_of_range(s) {}
};

}  // namespace shacldator



#endif /* SRC_GRAPH_EXCEPTIONS_HPP_ */
