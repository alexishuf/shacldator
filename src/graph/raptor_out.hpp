/*
 * raptor_out.hpp
 *
 *  Created on: May 2, 2018
 *      Author: alexis
 */

#ifndef SRC_GRAPH_RAPTOR_OUT_HPP_
#define SRC_GRAPH_RAPTOR_OUT_HPP_

#include <exception>
#include <cstring>
#include <cstdio>
#include <string>
#include <sstream>
#include <memory>
#include <cstdlib>
#include "graph.hpp"
#include "statement.hpp"
#include <raptor2/raptor2.h>
#include "priv/raptor_stream_adaptor.hpp"

namespace shacldator {

class raptor_out {
    typedef std::unique_ptr<raptor_statement, void (*)(raptor_statement*)>
            rp_statement_ptr;
    typedef std::unique_ptr<raptor_world, void (*)(raptor_world*)>
            rp_world_ptr;
    typedef std::unique_ptr<raptor_uri, void (*)(raptor_uri*)> rp_uri_ptr;
    typedef std::unique_ptr<raptor_iostream, void (*)(raptor_iostream*)>
            rp_iostream_ptr;
    typedef std::unique_ptr<raptor_serializer, void (*)(raptor_serializer*)>
            rp_serializer_ptr;

    inline static const unsigned char* uc(const char* ptr) {
        return reinterpret_cast<const unsigned char*>(ptr);
    }
public:
    raptor_out(std::ostream& out, const char* syntax,
               const char* base_uri)
               : w(raptor_new_world(), raptor_free_world),
                 b_uri(raptor_new_uri(w.get(), uc(base_uri)),
                       raptor_free_uri),
                 os(raptor_new_iostream_from_handler(w.get(), &out,
                                                    &raptor::cxxstream_handler),
                    raptor_free_iostream),
                 sr(raptor_new_serializer(w.get(), syntax),
                    raptor_free_serializer) {
        raptor_serializer_start_to_iostream(sr.get(), b_uri.get(), os.get());

    }
    raptor_out(std::ostream& out, const char* syn, const std::string& base_uri)
               : raptor_out(out, syn, base_uri.c_str()) {};


    raptor_out(const raptor_out&) = delete;
    raptor_out& operator=(const raptor_out&) = delete;
    virtual ~raptor_out() = default;

    template<class A1> raptor_out& operator<<(const graph<A1>& g) {
        for (auto i = g.begin(), e = g.end(); i != e; ++i)
            *this << *i;
        return *this;
    }
    template<class G> raptor_out& operator<<(const statement<G>& s) {
        raptor_serializer_serialize_statement(sr.get(), to_statement(s).get());
        return *this;
    }

private:
    template<class G>
    rp_statement_ptr to_statement(const statement<G>& s) const {
        auto* raw = raptor_new_statement_from_nodes(w.get(), to_term(s.subj()),
                                                             to_term(s.pred()),
                                                             to_term(s.obj()),
                                                             nullptr);
        return rp_statement_ptr(raw, &raptor_free_statement);
    }

    inline static const unsigned char* uc_bnode(const char* uri) {
        if (std::strncmp(uri, "_:", 2) == 0) uri += 2;
        return reinterpret_cast<const unsigned char*>(uri);
    }

    template<class G>
    raptor_term* to_term(const node<G>& node) const {
        if (node.is_blank()) {
            return raptor_new_term_from_blank(w.get(),
                                              uc_bnode(node.lex().c_str()));
        } else if (node.is_resource()) {
            return raptor_new_term_from_uri_string(w.get(), uc(node.lex().c_str()));
        } else if (node.is_literal()) {
            literal<G> l = node.as_literal();
            if (!l.lang().empty()) {
                return raptor_new_term_from_literal(w.get(),
                                                    uc(l.lex().c_str()),
                                                    nullptr,
                                                    uc(l.lang().c_str()));
            }
            auto dt = rp_uri_ptr(raptor_new_uri(w.get(),
                    uc(l.datatype().uri().c_str())),  raptor_free_uri);
            return raptor_new_term_from_literal(w.get(), uc(l.lex().c_str()),
                                                   dt.get(),  nullptr);
        }
        throw std::invalid_argument("bad node type");
    }

    rp_world_ptr w;
    rp_uri_ptr b_uri;
    rp_iostream_ptr os;
    rp_serializer_ptr sr;
};

template<class Graph> std::string graph_to_str(const Graph& g,
                                               const char* syntax="ntriples",
                                               const char* base="") {
    std::stringstream ss;
    raptor_out ro(ss, syntax, base);
    ro << g;
    return ss.str();
}

}  // namespace shacldator



#endif /* SRC_GRAPH_RAPTOR_OUT_HPP_ */
