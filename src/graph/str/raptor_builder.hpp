/*
 * raptor_builder.hpp
 *
 *  Created on: Mar 26, 2018
 *      Author: alexis
 */

#ifndef SRC_GRAPH_STR_RAPTOR_BUILDER_HPP_
#define SRC_GRAPH_STR_RAPTOR_BUILDER_HPP_

#include <exception>
#include <cstring>
#include <string>
#include <sstream>
#include <cstdlib>
#include <raptor2/raptor2.h>
#include "../priv/raptor_stream_adaptor.hpp"

namespace shacldator {namespace str {
template<class StoreBuilder> struct raptor_builder {
    typedef StoreBuilder builder_type;
    typedef typename builder_type::term_t term_t;

    raptor_builder() : r_w(nullptr), r_parser(nullptr), r_errorflag(false) {}
    raptor_builder(builder_type&& b) :
            builder(std::forward<builder_type>(b)), r_w(nullptr),
            r_parser(nullptr), r_errorflag(false) {}
    raptor_builder(const raptor_builder& rhs) = delete;
    ~raptor_builder() {
        if (r_parser) raptor_free_parser(r_parser);
        if (r_w) raptor_free_world(r_w);
    };
    raptor_builder operator=(const raptor_builder& rhs) = delete;

    term_t add_term(const raptor_term* term) {
        const raptor_term_value& v = term->value;
        term_t result;
        switch (term->type) {
            case RAPTOR_TERM_TYPE_URI:
                result = builder.uri_term((char*)raptor_uri_as_string(v.uri));
                break;
            case RAPTOR_TERM_TYPE_BLANK:
                result = builder.blank_term(fix_dot(v.blank.string).c_str());
                break;
            case RAPTOR_TERM_TYPE_LITERAL:
                result = builder.literal_term((char*)v.literal.string,
                        (char*)(v.literal.datatype
                                ? raptor_uri_as_string(v.literal.datatype)
                                : nullptr),
                        (char*)v.literal.language);
                break;
            default: break;
        }
        if (result) {
            return result;
        } else {
            std::stringstream ss;
            ss << "bad term type " << term->type;
            throw std::invalid_argument(ss.str());
        }
    }

    inline static void stmt_callback(void* ud, raptor_statement* stmt) {
        raptor_builder& me = *((raptor_builder*)ud);
        me.builder.add_triple(me.add_term(stmt->subject),
                              me.add_term(stmt->predicate),
                              me.add_term(stmt->object));
    }

    void parse(const char* syntax, std::string file) {
        setup(syntax);
        std::unique_ptr<unsigned char, void(&)(void*)> uri_content(
                raptor_uri_filename_to_uri_string(file.c_str()), std::free);
        std::unique_ptr<raptor_uri, void (&)(raptor_uri*)>
                uri(raptor_new_uri(r_w, uri_content.get()), raptor_free_uri),
                base(raptor_uri_copy(uri.get()), raptor_free_uri);
        int err = raptor_parser_parse_file(r_parser, uri.get(), base.get());
        if (err || r_errorflag){
            std::stringstream ss;
            ss << "Could not parse '" << file << "', log:"
                   << (!r_errors.str().empty() ? "\n" : "") << r_errors.str();
            throw std::runtime_error(ss.str());
        }
    }

    void parse(const char* syntax, std::istream& in, const char* base_uri) {
        setup(syntax);
        std::unique_ptr<raptor_uri, void (&)(raptor_uri*)>
                base(raptor_new_uri(r_w, (const unsigned char*)base_uri),
                     raptor_free_uri);
        std::unique_ptr<raptor_iostream, void (*)(raptor_iostream*)> stream(
                raptor_new_iostream_from_handler(r_w, &in,
                                                 &raptor::cxxstream_handler),
                raptor_free_iostream);
        int e =raptor_parser_parse_iostream(r_parser, stream.get(), base.get());
        if (e || r_errorflag){
            std::stringstream ss;
            ss << "Could not std::isteam, log:"
                   << (!r_errors.str().empty() ? "\n" : "") << r_errors.str();
            throw std::runtime_error(ss.str());
        }
    }

    StoreBuilder builder;
private:
    void setup(const char* syntax) {
        r_errors.str("");
        r_errorflag = false;
        std::string syntax_s(syntax);
        if (!r_parser || r_parser_syntax != syntax_s) {
            if (!r_w) {
                r_w = raptor_new_world();
                raptor_world_set_log_handler(r_w, this,
                                             &raptor_builder::log_callback);
            }
            if (r_parser) raptor_free_parser(r_parser);
            r_parser = raptor_new_parser(r_w, syntax);
            if (!r_parser) {
                std::stringstream ss;
                ss << "Unknwon parser: " << syntax;
                throw std::invalid_argument(ss.str());
            }
            r_parser_syntax = syntax_s;
            raptor_parser_set_statement_handler(r_parser, this,
                                                &raptor_builder::stmt_callback);
        }
    }

    inline static std::string fix_dot(const unsigned char* string) {
        if (!string || *string == '\0') return std::string();

        size_t len = std::strlen((const char*)string);
        const char* last = (const char*)string + (len -1);
        return std::string((const char*)string, *last == '.' ? len-1 : len);
    }

    inline static void log_callback(void* ud, raptor_log_message* msg) {
        raptor_builder& me = *((raptor_builder*)ud);
        me.r_errorflag |= msg->level == RAPTOR_LOG_LEVEL_ERROR;
        me.r_errorflag |= msg->level == RAPTOR_LOG_LEVEL_FATAL;
        me.r_errors << "[" << raptor_log_level_get_label(msg->level) << "]"
                << "[" << raptor_domain_get_label(msg->domain) << "]"
                << "[code " << msg->code << "]";
        if (msg->locator) {
            int lenOrErr = raptor_locator_format(0,0,msg->locator);
            if (lenOrErr > 0) {
                char* str = (char*)malloc(lenOrErr);
                raptor_locator_format(str, lenOrErr, msg->locator);
                me.r_errors << "(" << str << ")";
            }
        }
        me.r_errors << (msg->text ? msg->text : "");
    }


    raptor_world* r_w;
    raptor_parser* r_parser;
    std::string r_parser_syntax;
    std::stringstream r_errors;
    bool r_errorflag;
};

}}  // namespace shacldator::str


#endif /* SRC_GRAPH_STR_RAPTOR_BUILDER_HPP_ */
