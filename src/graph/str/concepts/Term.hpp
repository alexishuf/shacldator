/*
 * Term.hpp
 *
 *  Created on: Mar 24, 2018
 *      Author: alexis
 */

#ifndef SRC_GRAPH_CONCEPTS_TERM_HPP_
#define SRC_GRAPH_CONCEPTS_TERM_HPP_

#include <boost/concept_check.hpp>

namespace shacldator {namespace str {

template<class X> struct Term :
        public boost::Assignable<X>,
        public boost::DefaultConstructible<X>,
        public boost::CopyConstructible<X>,
        public boost::EqualityComparable<X> {

    BOOST_CONCEPT_USAGE(Term) {
        out << x;
        (void)((bool)x);
        (void)((bool)!x);
    }

private:
    const X x;
    std::ostream& out;
};

}} // namespace shacldator::str



#endif /* SRC_GRAPH_CONCEPTS_TERM_HPP_ */
