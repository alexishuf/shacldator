/*
 * GraphStore.hpp
 *
 *  Created on: Mar 24, 2018
 *      Author: alexis
 */

#ifndef SRC_GRAPH_GRAPHSTORE_HPP_
#define SRC_GRAPH_GRAPHSTORE_HPP_

#include <boost/concept_check.hpp>
#include "Term.hpp"
#include "../triple.hpp"

namespace shacldator {namespace str {

template<class X> class GraphStore {
public:
    typedef typename X::term_t term_t;
    typedef typename X::po_iterator po_iterator_type;
    typedef typename po_iterator_type::value_type po_iterator_value_type;
    typedef typename X::iterator iterator_type;
    typedef typename iterator_type::value_type iterator_value_type;

    BOOST_CONCEPT_ASSERT((Term<term_t>));

    BOOST_CONCEPT_ASSERT((boost::ForwardIterator<po_iterator_type>));
    BOOST_CONCEPT_ASSERT((boost::DefaultConstructible<po_iterator_type>));
    BOOST_CONCEPT_ASSERT((boost::ForwardIterator<iterator_type>));
    BOOST_CONCEPT_ASSERT((boost::Convertible<po_iterator_value_type, triple<term_t>>));
    BOOST_CONCEPT_ASSERT((boost::Convertible<iterator_value_type, triple<term_t>>));

    BOOST_CONCEPT_USAGE(GraphStore) {
        po_iterator_type it = x.begin_po(t);
        it = x.end_po(t);
        iterator_type it2 = x.begin();
        it2 = x.end();
        std::string string = x.lex(t);
        std::string string2 = x.str(t);
        term_t term2 = x.datatype(t);
        term_t r_term = x.resource("_:x");
        term_t l_term = x.literal("lexical", "datatype", "lang");
        term_t l_term2 = x.literal("lexical", t, "lang");
        bool contains = x.contains(t, t, t);
    }

private:
    X x;
    term_t t;
};

}} // namespace shacldator::str



#endif /* SRC_GRAPH_GRAPHSTORE_HPP_ */
