/*
 * GraphStoreBuilder.hpp
 *
 *  Created on: Mar 26, 2018
 *      Author: alexis
 */

#ifndef SRC_GRAPH_STR_CONCEPTS_GRAPHSTOREBUILDER_HPP_
#define SRC_GRAPH_STR_CONCEPTS_GRAPHSTOREBUILDER_HPP_

#include <boost/concept_check.hpp>

namespace shacldator {namespace str {

template<class X> class GraphStoreBuilder {
public:
    typedef typename X::term_t term_t;
    typedef typename X::store_type store_type;

    BOOST_CONCEPT_USAGE(GraphStoreBuilder) {
        term_t uri = x.uri_term(s);
        term_t blank = x.blank_term(s);
        term_t literal = x.literal_term(s, s, s);
        x.add_triple(uri, blank, literal);
        store_type store = x.build();
    }

private:
    X x;
    const char* s;
};

}}  // namespace shacldator::str


#endif /* SRC_GRAPH_STR_CONCEPTS_GRAPHSTOREBUILDER_HPP_ */
