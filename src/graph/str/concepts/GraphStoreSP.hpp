/*
 * GraphStoreSP.hpp
 *
 *  Created on: Apr 1, 2018
 *      Author: alexis
 */

#ifndef SRC_GRAPH_STR_CONCEPTS_GRAPHSTORESP_HPP_
#define SRC_GRAPH_STR_CONCEPTS_GRAPHSTORESP_HPP_

#include "../../index_tags.hpp"
#include "GraphStore.hpp"

namespace shacldator {namespace str {

template<class X> struct GraphStoreSP {
    typedef typename X::term_t term_t;
    typedef typename X::sp_iterator iterator_type;
    typedef typename X::sp_iterator::value_type iterator_value_type;

    BOOST_CONCEPT_ASSERT((GraphStore<X>));
    BOOST_CONCEPT_ASSERT((boost::ForwardIterator<iterator_type>));
    BOOST_CONCEPT_ASSERT((boost::DefaultConstructible<iterator_type>));
    BOOST_CONCEPT_ASSERT((boost::Convertible<iterator_value_type,
                                             triple<term_t>>));

    BOOST_CONCEPT_USAGE(GraphStoreSP) {
        BOOST_STATIC_ASSERT(has_sp_tag<X>::value);
        iterator_type it = x.begin_sp(t), it2 = x.end_sp(t);
        iterator_value_type value = *it;
        triple<term_t> triple = value;
    }

private:
    X x;
    term_t t;
};

}} //namespace shacldator::str



#endif /* SRC_GRAPH_STR_CONCEPTS_GRAPHSTORESP_HPP_ */
