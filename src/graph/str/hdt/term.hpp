/*
 * term.hpp
 *
 *  Created on: May 9, 2018
 *      Author: alexis
 */

#ifndef SRC_GRAPH_STR_HDT_TERM_HPP_
#define SRC_GRAPH_STR_HDT_TERM_HPP_

#include <cassert>
#include <cstdint>
#include <boost/static_assert.hpp>
#include "../../terms.hpp"

namespace shacldator {namespace str {namespace hdt {

typedef unsigned int id_t;
BOOST_STATIC_ASSERT(sizeof(id_t) == 4);

struct term {
    term(terms::type type, id_t subj_id, id_t pred_id, id_t obj_id)
         : subj_id(subj_id), pred_id(pred_id), obj_id(obj_id),
           type((subj_id || pred_id || obj_id) ? type : terms::NONE) {}
    term() : term(terms::NONE, 0, 0, 0) {}
    term(const term& o) = default;
    term& operator=(const term& o) = default;

    static term create_bogus(id_t id, terms::type type = terms::URI) {
        assert((id & 0x80000000) == 0);
        return term(type, 0, 0x80000000 | id, 0);
    }

    operator bool() const {
        assert((type == terms::NONE) == (!subj_id && !pred_id && !obj_id));
        return type != terms::NONE;
    }

    inline bool is_bogus() const {return pred_id & 0x80000000;}
    inline bool is_subj () const {return subj_id;}
    inline bool is_pred () const {return !is_bogus() && (pred_id&0x7fffffff);}
    inline bool is_obj  () const {return  obj_id;}

    inline id_t bogus_id() const {
        assert(is_bogus());
        return pred_id & ~0x80000000;
    }

    bool operator==(const term& o) const {
        return type == o.type
                && subj_id == o.subj_id
                && pred_id == o.pred_id
                && obj_id == o.obj_id;
    }
    bool operator<(const term& o) const {
        if (subj_id <  o.subj_id) return true;
        if (subj_id != o.subj_id) return false;
        if (pred_id <  o.pred_id) return true;
        if (pred_id != o.pred_id) return false;
        if (obj_id <  o.obj_id) return true;
        if (obj_id != o.obj_id) return false;
        return type < o.type;
    }
    bool operator<=(const term& o) const {
        if (subj_id <  o.subj_id) return true;
        if (subj_id != o.subj_id) return false;
        if (pred_id <  o.pred_id) return true;
        if (pred_id != o.pred_id) return false;
        if (obj_id <  o.obj_id) return true;
        if (obj_id != o.obj_id) return false;
        return type <= o.type;
    }
    bool operator!=(const term& o) const {return !(*this == o);}
    bool operator>=(const term& o) const {return !(*this <  o);}
    bool operator> (const term& o) const {return !(*this <= o);}

    id_t subj_id, pred_id, obj_id;
    terms::type type;
};

}}}  // namespace shacldator::str::hdt

namespace std {
template<> struct hash<shacldator::str::hdt::term> {
    typedef shacldator::str::hdt::term argument_type;
    typedef std::size_t result_type;
    result_type operator()(const argument_type& t) const {
        result_type h = t.type;
        h = 31 * h + std::hash<id_t>{}(t.subj_id);
        h = 31 * h + std::hash<id_t>{}(t.pred_id);
        h = 31 * h + std::hash<id_t>{}(t.obj_id);
        return h;
    }
};
}  // namespace std


#endif /* SRC_GRAPH_STR_HDT_TERM_HPP_ */
