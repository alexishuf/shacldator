/*
 * store.cpp
 *
 *  Created on: May 9, 2018
 *      Author: alexis
 */

#include "store.hpp"
#include <string>
#include <cassert>
#include <unordered_map>
#include <vector>
#include <mutex>
#include <libhdt/HDT.hpp>
#include <libhdt/HDTManager.hpp>
#include "../../lexical.hpp"
#include "../../../ns/rdf.hpp"
#include "../../../ns/xsd.hpp"

using namespace std;

#define PARSE_TERM(meth, var, valVar) \
    if (!var) throw invalid_argument("[" BOOST_PP_STRINGIZE(meth)  "] cannot have nullterm as " BOOST_PP_STRINGIZE(var)); \
    size_t valVar = term_id(var); \
    if (!valVar) throw invalid_argument("[" BOOST_PP_STRINGIZE(meth)  "] 0-valued term " BOOST_PP_STRINGIZE(var)); \
    --valVar; \

#define PARSE_TERM_WT(meth, var, valVar, typeVar) \
    PARSE_TERM(meth, var, valVar) \
    terms::type typeVar = term_type(var);

#define TP_CHECK(meth, typeCheck) \
    if (!(typeCheck)) throw invalid_argument("[" BOOST_PP_STRINGIZE(meth) "] expected " BOOST_PP_STRINGIZE(typeCheck));

#define NULL_CHECK(meth, var) \
    if (!var) throw invalid_argument("[" BOOST_PP_STRINGIZE(meth)  "] cannot have nullterm as " BOOST_PP_STRINGIZE(var));

#define NULL_AND_TP_CHECK(meth, var, typeCheck) \
    NULL_CHECK(meth, var) \
    TP_CHECK(meth, typeCheck)

namespace shacldator {namespace str {namespace hdt {

struct store::D {
    struct node_info : public lexical_info {
        node_info(string&& s) : lexical_info(parse_lexical(s)),
                                s(std::move(s)) {}
        node_info(node_info&&) = default;

        string s;
    };

    ~D() {delete hdt;}

    static bool fill(const term& t, ::hdt::TripleComponentRole& role,
                     id_t& id) {
        if      (t.is_obj ()) {role = ::hdt::OBJECT;    id = t.obj_id ;}
        else if (t.is_subj()) {role = ::hdt::SUBJECT;   id = t.subj_id;}
        else if (t.is_pred()) {role = ::hdt::PREDICATE; id = t.pred_id;}
        else return false; //bogus uri
        return true; //out params were set
    }

    string qry_string(term_t term) const {
        assert(term);
        ::hdt::TripleComponentRole role;
        id_t role_id;
        if (!fill(term, role, role_id)) {
            return bogus_uri(term);
        } else {
            try {
                return hdt->getDictionary()->idToString(role_id, role);
            } catch (runtime_error& e) { }
            // else, return a URI that should never be in the database
            return string("uuid:cdf19e44-9072-4ade-a22b-b0574029a7e5");
        }
    }

    string get_string(term_t t) const {
        ::hdt::TripleComponentRole role;
        id_t role_id;
        bool ok = fill(t, role, role_id);
        return ok ? hdt->getDictionary()->idToString(role_id, role) : string();
    }

    node_info parse(term_t t) const {
        ::hdt::TripleComponentRole role;
        id_t role_id;
        bool ok = fill(t, role, role_id);
        return node_info(ok ? hdt->getDictionary()->idToString(role_id, role)
                            : bogus_uri(t));
    }

    unsigned int get_id(const string& str,
                        ::hdt::TripleComponentRole role) const {
        try {
            return hdt->getDictionary()->stringToId(str, role);
        } catch (runtime_error& e) { }
        return 0;
    }

    term_t term(const string& str) const {
        terms::type tp = terms::URI;
        if (strncmp("_",  str.c_str(), 1) == 0) tp = terms::BLANK;
        if (strncmp("\"", str.c_str(), 1) == 0) tp = terms::LITERAL;
        bool is_res = terms::is_resource(tp);
        return term_t(tp, is_res ? get_id(str, ::hdt::SUBJECT) : 0,
                          is_res ? get_id(str, ::hdt::PREDICATE) : 0,
                          get_id(str, ::hdt::OBJECT));
    }

    term_t term_or_bogus_uri(const string& s)  {
        term_t t = this->term(s);
        if (t) {
            return t;
        } else {
            lock_guard<mutex> guard(bogus_mtx);
            assert(!s.empty() && *s.begin() != '_' && *s.begin() != '"');
            auto it = bogus_map.find(s);
            if (it != bogus_map.end()) return term::create_bogus(it->second);

            bogus_vec.push_back(s);
            id_t id = bogus_vec.size();
            bogus_map.insert(std::make_pair(s, id));
            return term::create_bogus(id);
        }
    }

    std::string bogus_uri(term_t t) const {
        assert(t.is_bogus());
        lock_guard<mutex> guard(bogus_mtx);
        return bogus_vec.at(t.bogus_id() - 1);
    }

    ::hdt::HDT* hdt;
    mutable mutex bogus_mtx;
    unordered_map<std::string, id_t> bogus_map;
    vector<std::string> bogus_vec;
};

struct store::it_pimpl {
    it_pimpl(const store* s) : s(s), pos(0), it(nullptr), end(true),
                  value(term_t(), term_t(), term_t()) {
        try_inc();
    }
    it_pimpl(const store* s, string&& aSubj, string&& aPred, string&& aObj)
             : s(s), pos(0), subj(move(aSubj)), pred(move(aPred)),
               obj(move(aObj)),
               it(s->d->hdt->search(subj.c_str(), pred.c_str(), obj.c_str())),
               end(!it->hasNext()),
               value(term_t(), term_t(), term_t()) {
        try_inc();
    }
    ~it_pimpl() = default;
    it_pimpl(it_pimpl&& o) = default;
    it_pimpl& operator=(it_pimpl&& o) = default;
    it_pimpl(const it_pimpl& o) : s(o.s), it(o.dup_it()), end(o.end),
                                  pos(o.pos),
                                  subj(o.subj), pred(o.pred), obj(o.obj),
                                  value(o.value) {}
    it_pimpl& operator=(const it_pimpl& o) {
        if (this != &o) {
            s = o.s;
            pos = o.pos;
            it = o.dup_it();
            end = o.end;
            subj = o.subj;
            pred = o.pred;
            obj  = o.obj;
            value = o.value;
        }
        return *this;
    }

    unique_ptr<::hdt::IteratorTripleString> dup_it() const {
        if (!it) return nullptr;
        unique_ptr<::hdt::IteratorTripleString> dup(
                s->d->hdt->search(subj.c_str(), pred.c_str(), obj.c_str()));
        dup->skip(pos);
        return dup;
    }

    bool equal(const it_pimpl& o) const {
        return (end && o.end)
                || (end == o.end && s == o.s && pos == o.pos  && subj == o.subj
                                             && pred == o.pred && obj == o.obj);
    }

    bool try_inc() {
        if (end) return false; //cannot inc if already at past-the-end
        assert(it);
        if (!(end = !it->hasNext())) {
            ::hdt::TripleString* ts = it->next();
            term_t st = s->d->term(ts->getSubject()),
                   pt = s->d->term(ts->getPredicate()),
                   ot = s->d->term(ts->getObject());
            assert(st);
            assert(pt);
            assert(ot);
            value = triple<term_t>(st, pt, ot);
        }
        return true;
    }

    void print(ostream& out) {
        out << "{s=" << s;
        if (!it) {
            out << "universal past-the-end";
        } else {
            out << ", qry: " << (subj.empty() ? "?" : subj) << " "
                             << (pred.empty() ? "?" : pred) << " "
                             << ( obj.empty() ? "?" :  obj);
            if (end)
                out << ", past-the-end";
            else
                out << ", pos=" << pos;
        }
        out << "}";
    }

    const store* s;
    unsigned int pos;
    string subj, pred, obj;
    unique_ptr<::hdt::IteratorTripleString> it;
    bool end;
    triple<term_t> value;
};

store::generic_it::generic_it() : d() {}
store::generic_it::generic_it(it_pimpl* d) : d(d) {}
store::generic_it::~generic_it() = default;
store::generic_it::generic_it(const generic_it& o)
                              : d(o.d ? new it_pimpl(*o.d) : nullptr) {}
store::generic_it::generic_it(generic_it&& o) = default;
store::generic_it& store::generic_it::operator=(const generic_it& o) {
    if (this != &o) d.reset(o.d ? new it_pimpl(*o.d) : nullptr);
    return *this;
}
store::generic_it& store::generic_it::operator=(generic_it&& o) = default;

bool store::generic_it::operator==(const generic_it& o) const {
    return this == &o || d == o.d || d->equal(*o.d);
}
store::generic_it::reference store::generic_it::operator*() const {
    return d->value;
}
store::generic_it& store::generic_it::operator++() {
    if (!d->try_inc())
        throw logic_error("Tried to increment past-the-end generic_it");
    return *this;
}



store::store(unique_ptr<D>&& d) : d(move(d)) {}
store::store(const char* filePath) : d(new D()) {
    d->hdt = ::hdt::HDTManager::mapIndexedHDT(filePath);
}
store::store(store&& o) : d(move(o.d)) {}
store::~store() {
}
store& store::operator=(store&& o) {
    if (this != &o) d = move(o.d);
    return *this;
}

string store::str(term_t node) const {
    if (!node) throw std::invalid_argument("str() requires non-null term");
    return d->get_string(node);
}
string store::lex(term_t node) const {
    D::node_info i = d->parse(node);
    return i.s.substr(i.lex_begin, i.lex_len);
}
store::term_t store::datatype(term_t literal) const {
    TP_CHECK(datatype, literal.type == terms::LITERAL)
    D::node_info i = d->parse(literal);

    if (i.lang != string::npos)
        return d->term_or_bogus_uri(rdf::langString);
    if (i.dt != string::npos) {
        string uri = i.get_dt(i.s);
        return d->term_or_bogus_uri(uri);
    }
    return d->term_or_bogus_uri(std::string(xsd::string_));
}
string store::langtag(term_t literal) const {
    TP_CHECK(datatype, literal.type == terms::LITERAL)
    D::node_info i = d->parse(literal);
    return i.lang == string::npos ? string()
            : i.s.substr(i.lang, i.s.length()-i.lang);
}

store::term_t store::resource(const char* lexical) const {
    return d->term(string(lexical));
}
store::term_t store::literal(const char* lexical, const char* datatype,
             const char* langtag) const {

    string plain = string("\"") + lexical + string("\"");
    string value = plain;
    if (langtag && *langtag) {
        assert(!datatype || !*datatype || !strcmp(datatype, rdf::langString));
        value += string("@") + string(langtag);
        return d->term(value);
    }
    if (datatype && *datatype)
        value += string("^^<") + string(datatype) + string(">");
    // else: value = "lexical"
    term_t t = d->term(value);
    if (t) return t;

    if (!datatype || !*datatype)
        value = plain + string("^^<") + xsd::string_ + string(">");
    else if (datatype && !strcmp(datatype, xsd::string_))
        value = plain;
    else
        return term_t();

    return d->term(value);
}

store::term_t store::literal(const char* lexical, term_t datatype,
             const char* langtag) const {
    return literal(lexical, lex(datatype).c_str(), langtag);
}

store::po_iterator store::begin_po(term_t subject) const {
    NULL_CHECK(begin_po, subject);
    return generic_it(new it_pimpl(this,
            d->qry_string(subject), "", ""));
}
store::po_iterator store::end_po(term_t subject) const {
    return generic_it(new it_pimpl(this));
}
store::so_iterator store::begin_so(term_t predicate) const {
    NULL_CHECK(begin_so, predicate);
    return generic_it(new it_pimpl(this,
            "", d->qry_string(predicate), ""));
}
store::so_iterator store::end_so(term_t predicate) const {
    return generic_it(new it_pimpl(this));
}
store::sp_iterator store::begin_sp(term_t object) const {
    NULL_CHECK(begin_sp, object);
    return generic_it(new it_pimpl(this,
            "", "", d->qry_string(object)));
}
store::sp_iterator store::end_sp(term_t object) const {
    return generic_it(new it_pimpl(this));
}

store::iterator store::begin() const {
    return generic_it(new it_pimpl(this, "", "", ""));
}
store::iterator store::end() const {
    return generic_it(new it_pimpl(this));
}

bool store::contains(term s, term p, term o) const {
    if (!s.subj_id || !p.pred_id || !o.obj_id) return false;
    ::hdt::TripleID q(s.subj_id, p.pred_id, o.obj_id);
    return unique_ptr<::hdt::IteratorTripleID>(d->hdt->getTriples()->search(q))
            ->hasNext();
}

ostream& operator<<(ostream& out, const store::generic_it& it) {
    it.d->print(out);
    return out;
}


}}}  // namespace shacldator::str::hdt
