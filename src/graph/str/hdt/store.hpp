/*
 * store.hpp
 *
 *  Created on: May 9, 2018
 *      Author: alexis
 */

#ifndef SRC_GRAPH_STR_HDT_STORE_HPP_
#define SRC_GRAPH_STR_HDT_STORE_HPP_

#include <memory>
#include <iterator>
#include <ostream>
#include <boost/mpl/set.hpp>
#include "../triple.hpp"
#include "term.hpp"
#include "../../index_tags.hpp"



namespace shacldator {namespace str {namespace hdt {

class store {
    struct it_pimpl;
    struct D;
    store(std::unique_ptr<D>&& d);
public:
    typedef term term_t;
    typedef boost::mpl::set<po_tag, so_tag, sp_tag> index_tags;

    class generic_it {
        generic_it(it_pimpl* d);
        friend class store;
        friend std::ostream& operator<<(std::ostream&, const generic_it&);
    public:
        typedef triple<term> value_type;
        typedef std::ptrdiff_t difference_type;
        typedef const value_type& reference;
        typedef const value_type* pointer;
        typedef std::forward_iterator_tag iterator_category;

        generic_it();
        ~generic_it();
        generic_it(const generic_it&);
        generic_it(generic_it&&);
        generic_it& operator=(const generic_it&);
        generic_it& operator=(generic_it&&);

        bool operator==(const generic_it& o) const;
        bool operator!=(const generic_it& o) const {return !(*this == o);}
        reference operator*() const;
        pointer operator->() const {return &operator*();}
        generic_it& operator++();
        generic_it operator++(int) {generic_it cp(*this); ++*this; return cp;}

    private:
        std::unique_ptr<it_pimpl> d;
    };

    typedef generic_it iterator;
    typedef generic_it po_iterator;
    typedef generic_it so_iterator;
    typedef generic_it sp_iterator;

public:
    store() = delete;
    store(store&&);
    store(const char* filePath);
    store(const std::string& filePath) : store(filePath.c_str()) {}
    //TODO add other constructors (e.g., streams)
    ~store();
    store& operator=(store&&);

    std::string str(term_t node) const;
    std::string lex(term_t node) const;
    term_t datatype(term_t literal) const;
    std::string langtag(term_t literal) const;
    inline terms::type type(term_t node) const {return node.type;}

    term_t resource(const char* lexical) const;
    term_t literal(const char* lexical, const char* datatype,
                 const char* langtag) const;
    term_t literal(const char* lexical, term_t datatype,
                 const char* langtag) const;

    po_iterator begin_po(term_t subject) const;
    po_iterator end_po(term_t subject) const;
    so_iterator begin_so(term_t predicate) const;
    so_iterator end_so(term_t predicate) const;
    sp_iterator begin_sp(term_t object) const;
    sp_iterator end_sp(term_t object) const;

    iterator begin() const;
    iterator end() const;

    bool contains(term subj, term prop, term obj) const;

private:
    std::unique_ptr<D> d;
};

std::ostream& operator<<(std::ostream& out, const store::generic_it& it);

}}}  // namespace shacldator::str::hdt


#endif /* SRC_GRAPH_STR_HDT_STORE_HPP_ */
