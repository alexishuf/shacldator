/*
 * store.hpp
 *
 *  Created on: May 23, 2018
 *      Author: alexis
 */

#ifndef SRC_GRAPH_STR_AUGMENTED_STORE_HPP_
#define SRC_GRAPH_STR_AUGMENTED_STORE_HPP_

#include <vector>
#include <set>
#include <deque>
#include <cstring>
#include <mutex>
#include <cassert>
#include <type_traits>
#include <ostream>
#include <iostream>
#include <boost/range/algorithm.hpp>
#include <boost/mpl/contains.hpp>
#include "../triple.hpp"
#include "../concepts/GraphStore.hpp"
#include "../../terms.hpp"
#include "../../../ns/xsd.hpp"
#include "../../../utils.hpp"
#include "../../index_tags.hpp"
#include "../../lexical.hpp"
#include "../../../ns/rdf.hpp"
#include "../../../ns/xsd.hpp"

namespace shacldator {namespace str {namespace augmented {

namespace helper {

struct nop_mtx {
    void lock() {}
    void try_lock() {}
    void unlock() {}
};

template<class GraphStore, class Tag,
         bool has = boost::mpl::contains<
                 typename get_index_tags<GraphStore>::type, Tag>::value
> struct iterator_type {
    typedef void type;
};
template<class GraphStore>
struct iterator_type<GraphStore, po_tag, true> {
    typedef typename GraphStore::po_iterator type;
};
template<class GraphStore>
struct iterator_type<GraphStore, so_tag, true> {
    typedef typename GraphStore::so_iterator type;
};
template<class GraphStore>
struct iterator_type<GraphStore, sp_tag, true> {
    typedef typename GraphStore::sp_iterator type;
};

struct ITERATOR_UNAVAILABLE_IN_RAW_STORE;
template<template <class X, class Y> class Wrapper, class RawIt, class Term>
struct wrap {
    typedef Wrapper<RawIt, Term> type;
};
template<template <class X, class Y> class Wrapper, class Term>
struct wrap<Wrapper, void, Term> {
    typedef ITERATOR_UNAVAILABLE_IN_RAW_STORE type;
};

} //namespace helper

template<class RawIt, class Term> struct wrapper_it {
    typedef str::triple<Term> value_type;
    typedef std::ptrdiff_t difference_type;
    typedef const value_type& reference;
    typedef const value_type* pointer;
    typedef std::forward_iterator_tag iterator_category;

    wrapper_it() = default;
    wrapper_it(RawIt&& it) : raw_it(std::forward<RawIt>(it)) {}
    wrapper_it(const wrapper_it&) = default;
    wrapper_it(wrapper_it&&) = default;
    ~wrapper_it() = default;
    wrapper_it& operator=(const wrapper_it&) = default;
    wrapper_it& operator=(wrapper_it&&) = default;

    const RawIt raw() const {return raw_it;}

    bool operator==(const wrapper_it& o) const {return raw_it == o.raw_it;}
    bool operator!=(const wrapper_it& o) const {return raw_it != o.raw_it;}
private:
    static value_type wrap(const typename RawIt::value_type& t) {
        return value_type(Term(t.subj()), Term(t.pred()), Term(t.obj()));
    }
public:
    reference operator*() const {
        return curr ? curr.get() : curr.emplace(*raw_it).get();
    }
    pointer operator->() const {return &operator*();}
    wrapper_it& operator++() {++raw_it; curr.unset(); return *this;}
    wrapper_it operator++(int) {wrapper_it cp(*this); ++*this; return cp;}

private:
    mutable RawIt raw_it;
    mutable pack<value_type, false> curr;
};

template<class RawTerm>
struct term {
    typedef RawTerm raw_term_t;
    term() : raw(), data(0) {}
    term(raw_term_t raw) : raw(raw), data(0) {}
    term(terms::type type, unsigned id)
         : raw(), data(type << 30 | (id & 0x3fffffff)) {
        assert((id & 0xc0000000) == 0);
        assert(type < terms::_TYPE_COUNT);
        assert((type == terms::NONE) == (id == 0));
    }
    term(const term&) = default;
    term& operator=(const term&) = default;
    ~term() = default;

    inline bool operator!() const {return !data && !raw;}
    inline operator bool() const {return data || raw;}

    bool operator==(const term& o) const {
        return raw == o.raw && data == o.data;
    }
    bool operator!=(const term& o) const {return !operator==(o);}
    bool operator<(const term& o) const {
        if (raw < o.raw) return true;
        if (raw > o.raw) return false;
        return data < o.data;
    }
    bool operator<=(const term& o) const {return *this < o || *this == o;}
    bool operator>=(const term& o) const {return !operator< (o);}
    bool operator> (const term& o) const {return !operator<=(o);}

    inline bool is_local() const {return data;}
    inline terms::type type() const {return (terms::type)(data >> 30);}
    inline unsigned id() const {return data & 0x3fffffff;}

    raw_term_t raw;
private:
    friend class std::hash<term>;
    unsigned data;
};

struct abstract_store {};
template<class X> struct is_augmented
        : public std::is_base_of<abstract_store, X> {};

template<class GraphStore, bool is_synchronized=false> struct store
        : public abstract_store {
    BOOST_CONCEPT_ASSERT((str::GraphStore<GraphStore>));
    typedef GraphStore raw_store_t;
    typedef typename raw_store_t::term_t raw_term_t;

    typedef term<raw_term_t> term_t;

    const raw_store_t* const raw;

    store(const raw_store_t* raw) : raw(raw) {}
    store(store&& o) : raw(o.raw),
                       sorted_strs(std::move(o.sorted_strs)),
                       strs(std::move(o.strs)) {}
    store& operator=(store&&) = default;
    ~store() = default;

private:
    raw_term_t get_raw_term(terms::type type, const std::string& str) const {
        if (terms::is_resource(type))
            return raw->resource(str.c_str());
        lexical_info li = parse_lexical(str);
        std::string lex(str.substr(li.lex_begin, li.lex_len)),
                    dt(li.get_dt(str)), lang(li.get_lang(str));
        return raw->literal(lex.c_str(), dt.c_str(), lang.c_str());
    }

private:
//    std::string dump_sorted_strs() const {
//        std::stringstream ss;
//        for (auto i = sorted_strs.begin(), e = sorted_strs.end(); i != e; ++i)
//            ss << strs.at(*i) << ", ";
//        return ss.str();
//    }
    unsigned get_str_id(const std::string& wanted) const {
        std::unique_lock<mtx_t> lock(mtx);
//        std::cout << "get_str_id(): " << dump_sorted_strs() << std::endl;
        auto i = boost::lower_bound(sorted_strs, ptr_cmp::wildcard,
                                    ptr_cmp(strs, wanted));
        if (i == sorted_strs.end() || strs.at(*i) != wanted) return 0;
        return *i + 1;
    }
    template<class String>
    term_t get_term_tpl(terms::type type, String&& str) const {
        using namespace std;
        if (str.empty())
            throw std::invalid_argument("Cannot get term of empty string");
        raw_term_t rt = get_raw_term(type, str);
        if (rt) return term_t(rt);
        std::unique_lock<mtx_t> lock(mtx);
        auto wc = ptr_cmp::wildcard;
        auto i = boost::lower_bound(sorted_strs, wc, ptr_cmp(strs, str));
        if (i != sorted_strs.end() && strs.at(*i) == str)
            return term_t(type, *i + 1);
        lexical_info li = parse_lexical(str);
        if (li.lex_begin && li.dt == string::npos && li.lang == string::npos) {
            string str2 = str + "^^<http://www.w3.org/2001/XMLSchema#string>";
            i = boost::lower_bound(sorted_strs, wc, ptr_cmp(strs, str2));
            if (i != sorted_strs.end() && strs.at(*i) == str2)
                return term_t(type, *i + 1);
        } else if (li.lex_begin && li.get_dt(str) == xsd::string_) {
            string str2 = str.substr(0, li.dt-3);
            i = boost::lower_bound(sorted_strs, wc, ptr_cmp(strs, str2));
            if (i != sorted_strs.end() && strs.at(*i) == str2)
                return term_t(type, *i + 1);
        }
        unsigned pos = strs.size();
        strs.push_back(str);
        sorted_strs.insert(i, pos);
        return term_t(type, pos+1);
    }
public:
    term_t create_term(terms::type type, const std::string& str) {
        return get_term_tpl(type, str);
    }
    term_t create_term(terms::type type, std::string&& str) {
        return get_term_tpl(type, std::move(str));
    }
    term_t create_term(terms::type type, const char* cstr) {
        return create_term(type, std::string(cstr));
    }
    term_t create_term(const std::string& str) {
        terms::type tp = terms::URI;
        if      (!std::strncmp("_:", str.c_str(), 2)) tp = terms::BLANK;
        else if (!std::strncmp("\"", str.c_str(), 1)) tp = terms::LITERAL;
        return create_term(tp, str);
    }
    term_t create_term(std::string&& str) {
        terms::type tp = terms::URI;
        if      (!std::strncmp("_:", str.c_str(), 2)) tp = terms::BLANK;
        else if (!std::strncmp("\"", str.c_str(), 1)) tp = terms::LITERAL;
        return create_term(tp, std::move(str));
    }

    /* --- forwards --- */

    typedef typename raw_store_t::index_tags index_tags;
    typedef typename raw_store_t::iterator raw_iterator;
    typedef typename helper::iterator_type<raw_store_t, po_tag>::type
            raw_po_iterator;
    typedef typename helper::iterator_type<raw_store_t, so_tag>::type
            raw_so_iterator;
    typedef typename helper::iterator_type<raw_store_t, sp_tag>::type
            raw_sp_iterator;

    typedef typename helper::wrap<wrapper_it,raw_po_iterator, term_t>::type
            po_iterator;
    typedef typename helper::wrap<wrapper_it,raw_so_iterator, term_t>::type
            so_iterator;
    typedef typename helper::wrap<wrapper_it,raw_sp_iterator, term_t>::type
            sp_iterator;
    typedef wrapper_it<raw_iterator, term_t>    iterator;

    std::string str(term_t node) const {
        if (!node) throw std::invalid_argument("str() requires non-null term");
        return node.is_local() ? strs.at(node.id()-1) : raw->str(node.raw);
    }
    std::string lex(term_t node) const {
        if (!node.is_local()) return raw->lex(node.raw);
        const std::string& str = strs.at(node.id()-1);
        if (terms::is_resource(node.type())) return str;
        lexical_info li = parse_lexical(str);
        return str.substr(li.lex_begin, li.lex_len);
    }
    term_t datatype(term_t literal) const {
        if (!literal.is_local()) return raw->datatype(literal.raw);
        if (!terms::is_literal(literal.type()))
            throw std::invalid_argument("datatype() expects a literal!");
        std::string str = strs.at(literal.id()-1);
        lexical_info li = parse_lexical(str);
        if (li.lang != std::string::npos) {
            return get_term_tpl(terms::URI, std::string(rdf::langString));
        } else if (li.dt != std::string::npos) {
            return get_term_tpl(terms::URI,
                                str.substr(li.dt, str.length()-li.dt-1));
        }
        return get_term_tpl(terms::URI, std::string(xsd::string_));
    }
    std::string langtag(term_t literal) const {
        if (!literal.is_local()) return raw->langtag(literal.raw);
        if (!terms::is_literal(literal.type()))
            throw std::invalid_argument("datatype() expects a literal!");
        std::string lex = strs.at(literal.id()-1);
        lexical_info li = parse_lexical(lex);
        if (li.lang == std::string::npos) return std::string();
        return lex.substr(li.lang, lex.length()-li.lang);
    }
    inline terms::type type(term_t node) const {
        return node.is_local() ? node.type() : raw->type(node.raw);
    }
    term_t resource(const char* lexical) const {
        term_t t = raw->resource(lexical);
        if (t) return t;
        assert(lexical && *lexical != '\0');
        size_t id = get_str_id(std::string(lexical));
        return id ? term_t(*lexical == '_' ? terms::BLANK : terms::URI, id)
                  : term_t();
    }
    term_t literal(const char* lexical, const char* datatype,
                   const char* langtag) const {
        term_t t = raw->literal(lexical, datatype, langtag);
        if (t) return t;


        std::size_t id(0);
        if (langtag && *langtag) {
            std::string str = std::string("\"")+lexical+"\"@"+langtag;
            id = get_str_id(str);
        } else {
            bool has_dt = datatype && *datatype;
            std::string str = std::string("\"") + lexical + "\""
                    + (has_dt ? std::string("^^<")+datatype+">" : "");
            id = get_str_id(str);
            if (!id) {
                str = std::string("\"") + lexical + "\""
                        + (has_dt ? "" : std::string("^^<")+xsd::string_+">");
                id = get_str_id(str);
            }
        }
        return id ? term_t(terms::LITERAL, id) : term_t();
    }
    term_t literal(const char* lexical, term_t dtype,
                   const char* lang) const {
        term_t t = raw->literal(lexical, dtype.raw, lang);
        return t ? t : literal(lexical, dtype ? lex(dtype).c_str() : "", lang);
    }

    po_iterator begin_po(term_t subject) const {
        return po_iterator(raw->begin_po(subject.raw));
    }
    po_iterator end_po(term_t subject) const {
        return po_iterator(raw->end_po(subject.raw));
    }
    so_iterator begin_so(term_t predicate) const {
        return so_iterator(raw->begin_so(predicate.raw));
    }
    so_iterator end_so(term_t predicate) const {
        return so_iterator(raw->end_so(predicate.raw));
    }
    sp_iterator begin_sp(term_t object) const {
        return sp_iterator(raw->begin_sp(object.raw));
    }
    sp_iterator end_sp(term_t object) const {
        return sp_iterator(raw->end_sp(object.raw));
    }

    iterator begin() const {return iterator(raw->begin());}
    iterator end() const {return iterator(raw->end());}

    bool contains(term_t subj, term_t prop, term_t obj) const {
        if (subj.is_local() || prop.is_local() || obj.is_local())
            return false;
        return raw->contains(subj.raw, prop.raw, obj.raw);
    }

private:
    typedef typename std::conditional<is_synchronized, std::mutex,
                                      helper::nop_mtx>::type mtx_t;
    struct ptr_cmp {
        typedef const std::string*  first_argument_type;
        typedef const std::string* second_argument_type;
        typedef bool result_type;

        ptr_cmp(const std::vector<std::string>& vec, const std::string& wanted)
                : vec(vec), wanted(wanted) {}
        ptr_cmp(const ptr_cmp&) = default;

        static const std::size_t wildcard =
                std::numeric_limits<std::size_t>::max();
        bool operator()(const std::size_t l, const std::size_t r) const {
            const std::string&  left = l == wildcard ? wanted : vec.at(l);
            const std::string& right = r == wildcard ? wanted : vec.at(r);
            return left < right;
        }
        const std::vector<std::string>& vec;
        const std::string& wanted;
    };

    mutable mtx_t mtx;
    mutable std::deque<std::size_t> sorted_strs;
    mutable std::vector<std::string> strs;
};

template<class GraphStore> using   safe_store = store<GraphStore, true>;
template<class GraphStore> using unsafe_store = store<GraphStore, false>;

struct safe_type_fac {
    template<class GS> struct apply {typedef safe_store<GS> type;};
};
struct unsafe_type_fac {
    template<class GS> struct apply {typedef safe_store<GS> type;};
};

}}} // namespace shacldator::str::augmented */

namespace std {
template<class RawTerm>
struct hash<typename shacldator::str::augmented::term<RawTerm>> {
    typedef typename shacldator::str::augmented::term<RawTerm> argument_type;
    typedef size_t result_type;
    result_type operator()(const argument_type& term) {
        return hash<RawTerm>()(term.raw)
                + 31 * hash<unsigned>()(term.data);
    }
};

template<class GS, bool iss>
ostream& operator<<(ostream& o, const typename shacldator::str::augmented
                                ::store<GS, iss>::term& t) {
    if (t.is_local())
        return o << "augmented_term(" << t.type() << ", " << t.id() << ")";
    return o << t.raw;
}
template<class RawIt, class Term>
ostream& operator<<(ostream& o, const shacldator::str::augmented
                                ::wrapper_it<RawIt, Term>& it) {
    return o << it.raw();
}

}  // namespace std

#endif /* SRC_GRAPH_STR_AUGMENTED_STORE_HPP_ */
