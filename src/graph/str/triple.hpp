/*
 * triple.hpp
 *
 *  Created on: Mar 24, 2018
 *      Author: alexis
 */

#ifndef SRC_GRAPH_STR_TRIPLE_HPP_
#define SRC_GRAPH_STR_TRIPLE_HPP_

#include <string>
#include <sstream>
#include "concepts/Term.hpp"

namespace shacldator {namespace str {

template<class Term> struct triple {
    BOOST_CONCEPT_ASSERT((str::Term<Term>));

    typedef Term term_t;

    triple() : m_subj(), m_pred(), m_obj() {}
    triple(const Term& subj, const Term& pred, const Term& obj) :
        m_subj(subj), m_pred(pred), m_obj(obj) {}
    triple(const triple& rhs) = default;
    template<class RTerm>
    triple(const triple<RTerm>& o, typename
           std::enable_if<std::is_convertible<RTerm, term_t>::value>::type* =0)
           : m_subj(o.subj()), m_pred(o.pred()), m_obj(o.obj()) {}
    triple& operator=(const triple& rhs) = default;

    inline Term subj() const {return m_subj;}
    inline Term pred() const {return m_pred;}
    inline Term  obj() const {return m_obj; }

    inline bool is_complete() const {return m_subj && m_pred && m_obj;}

    template<class GS> std::string to_str(const GS& gs) const {
        std::stringstream ss;
        ss << gs.str(subj()) << " " << gs.str(pred()) << " " << gs.str(obj());
        return ss.str();
    }

    template<class TermRhs> triple with_subj(const TermRhs& subj) const {
        BOOST_CONCEPT_ASSERT((boost::Convertible<TermRhs, term_t>));
        return triple(subj, m_pred, m_obj);
    }
    template<class TermRhs> triple with_pred(const TermRhs& pred) const {
        BOOST_CONCEPT_ASSERT((boost::Convertible<TermRhs, term_t>));
        return triple(m_subj, pred, m_obj);
    }
    template<class TermRhs> triple with_obj(const TermRhs& obj) const {
        BOOST_CONCEPT_ASSERT((boost::Convertible<TermRhs, term_t>));
        return triple(m_subj, m_pred, obj);
    }

    bool operator==(const triple<Term>& rhs) const {
        return m_subj == rhs.m_subj && m_pred == rhs.m_pred
                                    && m_obj == rhs.m_obj;
    }
    bool operator!=(const triple<Term>& rhs)  const {
        return !operator ==(rhs);
    }
    bool operator<(const triple<Term>& rhs) const {
#define SHACLDATOR_X(name)  if (name < rhs.name) {return true;} \
                            else if (name != rhs.name) {return false;}
        SHACLDATOR_X(m_subj) SHACLDATOR_X(m_pred) SHACLDATOR_X(m_obj)
#undef SHACLDATOR_X
        return false; //equals
    }
    bool operator<=(const triple<Term>& rhs) const {
        return operator<(rhs) || operator==(rhs);
    }
    bool operator>=(const triple<Term>& o) const {return !operator<(o);}
    bool operator>(const triple<Term>& o) const {return !operator<=(o);}

private:
    Term m_subj, m_pred, m_obj;
};

template<class Term> triple<Term>
mk_triple(const Term& subj, const Term& pred, const Term& obj) {
    return triple<Term>(subj, pred, obj);
}

template<class Term> std::ostream&
operator<<(std::ostream& out, const triple<Term>& t) {
    return out << "[" << t.subj() << " " << t.pred() << " " << t.obj() << "]";
}

}} // namespace shacldator::str



#endif /* SRC_GRAPH_STR_TRIPLE_HPP_ */
