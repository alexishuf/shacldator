/*
 * store.hpp
 *
 *  Created on: Mar 25, 2018
 *      Author: alexis
 */

#ifndef SRC_GRAPH_STR_STL_STORE_HPP_
#define SRC_GRAPH_STR_STL_STORE_HPP_

#include <string>
#include <memory>
#include <vector>
#include <utility>
#include <iterator>
#include <cassert>
#include <boost/mpl/set.hpp>
#include <boost/iterator/iterator_facade.hpp>
#include "term.hpp"
#include "../triple.hpp"
#include "../../index_tags.hpp"

namespace shacldator {namespace str {namespace stl {

class store {
public:
    typedef term term_t;
    typedef boost::mpl::set<po_tag> index_tags;
    class builder;
    struct iterator;

    struct po_iterator : public boost::iterator_facade<
            po_iterator, triple<term>, std::bidirectional_iterator_tag> {
        typedef std::vector<std::pair<term, std::vector<term> > > po_vec_t;
        typedef po_vec_t::iterator p_it_type;
        po_iterator() : current(0, 0, 0), subj(0) {assert(at_end());}
        po_iterator(term subj, p_it_type it, p_it_type end) :
                current(0, 0, 0), subj(subj), p_it(it), p_end(end) {
            if (!at_end()) {
                o_it = p_it->second.begin();
                walk_next();
            }
        }
        ~po_iterator() = default;
        po_iterator(const po_iterator& rhs) = default;
        po_iterator& operator=(const po_iterator& rhs) = default;

    private:
        typedef std::vector<term>::iterator o_it_type;
        friend class boost::iterator_core_access;
        friend std::ostream& operator<<(std::ostream&, const po_iterator&);
        friend std::ostream& operator<<(std::ostream&, const iterator&);

        bool at_end() const { return p_it == p_end; }

        void walk_next() {
            while (!at_end() && o_it == p_it->second.end()) {
                ++p_it;
                if (!at_end()) o_it = p_it->second.begin();
            }
            if (!at_end()) current = triple<term>(subj, p_it->first, *o_it);
        }

        void increment() {
            ++o_it;
            walk_next();
        }
        void decrement() {
            /* this implementation assumes at least one valid (i.e., not
             * past-the-end) increment occurred */
            if (at_end()) --p_it;
            while (o_it == p_it->second.begin())
                --(o_it = (--p_it)->second.end());
        }
        bool equal(const po_iterator& rhs) const {
            if (at_end() && rhs.at_end())return true;
            return p_it == rhs.p_it && o_it == rhs.o_it;
        }
        triple<term>& dereference() const {
            return current;
        }

        mutable triple<term> current;
        term subj;
        p_it_type p_it, p_end;
        o_it_type o_it;
    };

    struct iterator : public boost::iterator_facade<
            iterator, triple<term_t>, std::bidirectional_iterator_tag> {
        iterator();
        iterator(const store* str, size_t pos);
        ~iterator() = default;
        iterator(const iterator& rhs) = default;
        iterator& operator=(const iterator& rhs) = default;
    private:
        friend class boost::iterator_core_access;
        friend std::ostream& operator<<(std::ostream&, const iterator&);

        bool at_end() const { return s_pos == s_size; }
        term subj() const;
        void walk_next();
        void increment() {
            ++po_it;
            walk_next();
        }
        void decrement();
        bool equal(const iterator& rhs) const {
            assert(str == rhs.str || (!str ^ !rhs.str));
            if (at_end() && rhs.at_end()) return true;
            return s_pos == rhs.s_pos && po_it == rhs.po_it;
        }
        triple<term>& dereference() const {
            return current;
        }

        mutable triple<term> current;
        const store* str;
        size_t s_pos, s_size;
        po_iterator po_it, po_end;
    };

private:
    struct D;
    store(std::unique_ptr<D>& d);
public:
    store() = delete;
    store(const store& rhs) = delete;
    store(store&& rhs);
    ~store();
    store& operator=(const store& rhs) = delete;

    struct builder {
        typedef term term_t;
        typedef store store_type;

        builder();
        builder(builder&& rhs);
        builder(const builder& rhs) = delete;
        ~builder();
        builder& operator=(const builder& rhs) = delete;

        term uri_term(const char* uri);
        term blank_term(const char* id);
        term literal_term(const char* lex, const char* datatype,
                          const char* lang);
        void add_triple(term subj, term pred, term obj);

        store build();
    private:
        struct D;
        D* d;
    };

    std::string str(term node) const;
    std::string lex(term node) const;
    term datatype(term literal) const;
    std::string langtag(term literal) const;
    inline terms::type type(term node) const { return stl::term_type(node); }

    term resource(const char* lexical) const;
    term literal(const char* lexiscal, const char* datatype,
                 const char* langtag) const;
    term literal(const char* lexiscal, term datatype,
                 const char* langtag) const;

    po_iterator begin_po(term resource) const;
    po_iterator end_po(term resource) const;

    iterator begin() const;
    iterator end() const;

    bool contains(term subj, term prop, term obj) const;
private:
    std::unique_ptr<D> d;
};

std::ostream& operator<<(std::ostream& out, const store::po_iterator& it);

}}} // namespace shacldator::str::stl

#endif /* SRC_GRAPH_STR_STL_STORE_HPP_ */
