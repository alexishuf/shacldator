/*
 * store.cpp
 *
 *  Created on: Mar 25, 2018
 *      Author: alexis
 */

#include "store.hpp"
#include <unordered_map>
#include <vector>
#include <cstring>
#include <cassert>
#include <sstream>
#include <map>
#include <limits>
#include <stdexcept>
#include <algorithm>
#include <boost/preprocessor.hpp>
#include "../../../ns/xsd.hpp"
#include "../../../ns/rdf.hpp"

#define PARSE_TERM(meth, var, valVar) \
    if (!var) throw invalid_argument("[" BOOST_PP_STRINGIZE(meth)  "] cannot have nullterm as " BOOST_PP_STRINGIZE(var)); \
    size_t valVar = stl::term_value(var); \
    if (!valVar) throw invalid_argument("[" BOOST_PP_STRINGIZE(meth)  "] 0-valued term " BOOST_PP_STRINGIZE(var)); \
    --valVar; \

#define PARSE_TERM_WT(meth, var, valVar, typeVar, exType) \
    PARSE_TERM(meth, var, valVar) \
    terms::type typeVar = stl::term_type(var); \
    if (typeVar != exType) throw invalid_argument("[" BOOST_PP_STRINGIZE(meth) "] expected " BOOST_PP_STRINGIZE(exType)); \


using namespace std;

namespace shacldator {namespace str {namespace stl {

struct store::D {
    struct literal {
        literal(const literal& rhs) = default;
        literal(string str, term datatype, size_t langtag) : str(str),
                datatype(datatype), langtag(langtag) {}

        bool operator==(const literal& rhs) const {
            return str == rhs.str && datatype == rhs.datatype
                    && langtag == rhs.langtag;
        }
        bool operator!=(const literal& rhs) const {return !operator==(rhs);}

        bool operator<(const literal& rhs) const {
            if (str < rhs.str) return true;
            else if (str > rhs.str) return false;
            if (datatype < rhs.datatype) return true;
            else if (datatype > rhs.datatype) return false;
            if (langtag < rhs.langtag) return true;
            else if (langtag > rhs.langtag) return false;
            return false; // equal
        }

        string str;
        term datatype;
        size_t langtag;
    };

    template<class T> static typename vector<size_t>::iterator
    lower_bound(vector<T>& positional, vector<size_t>& sorted, const T& value) {
        size_t placeholder = numeric_limits<size_t>::max();
        auto it = std::lower_bound(sorted.begin(), sorted.end(), placeholder,
                [&](const size_t lhs, const size_t rhs) {
            if (lhs == placeholder) return value < positional[rhs];
            else if (rhs == placeholder) return positional[lhs] < value;
            return positional[lhs] < positional[rhs];
        });
        return it;
    }

    vector<string > resources;
    vector<literal> literals;
    vector<string > langs;

    vector<size_t> srt_resources;
    vector<size_t> srt_literals;
    vector<size_t> srt_langs;

    vector<vector<pair<term, vector<term> > > > adj;
};

struct store::builder::D {
    D() : sd(new store::D), next_blank(0) {}

    template<class T> pair<size_t, bool>
    add(vector<T>& positional, vector<size_t>& sorted, const T& val) {
        auto it = store::D::lower_bound(positional, sorted, val);
        size_t pos;
        bool changed = false;
        if ((changed = it == sorted.end() || positional[*it] != val)) {
            sorted.insert(it, pos = positional.size());
            positional.push_back(val);
        } else {
            pos = *it;
        }
        return make_pair(pos + 1, changed);
    }

    term add_resource(const string& string, terms::type type) {
        auto p = add(sd->resources, sd->srt_resources, string);
        if (p.second)
            sd->adj.push_back(decltype(sd->adj)::value_type());
        return mk_term(type, p.first);
    }

    std::unique_ptr<store::D> sd;
    term next_blank;
};

store::builder::builder() : d(new D) {}
store::builder::builder(builder&& rhs) : d(rhs.d) {rhs.d = nullptr;}
store::builder::~builder() {delete d;}
term store::builder::uri_term(const char* uri) {
    if (uri == nullptr || *uri == '\0')
        throw invalid_argument("Null or empty URI");
    return d->add_resource(uri, terms::URI);
}
term store::builder::blank_term(const char* idOrEmpty) {
    std::string lex;
    if (!idOrEmpty || *idOrEmpty == '\0') {
        stringstream ss;
        ss << "_:b" << ++d->next_blank;
        lex  = ss.str();
    } else {
        lex = std::string(idOrEmpty);
    }
    assert(!lex.empty());
    if (std::strncmp(lex.c_str(), "_:", 2) != 0)
        lex = "_:" + lex;
    return d->add_resource(lex.c_str(), terms::BLANK);
}
term store::builder::literal_term(const char* lex, const char* datatype,
                                  const char* lang) {
    store::D& sd = *d->sd;
    string lang_s(lang ? lang : "");
    string dt_s(datatype && *datatype ? datatype
                : !lang_s.empty() ? rdf::langString : xsd::string_);

    if (!lang_s.empty() && !dt_s.empty() && dt_s != rdf::langString) {
        stringstream ss;
        ss << "language tag implies rdf:langString. " << dt_s << " was given";
        throw invalid_argument(ss.str());
    }

    size_t lang_pos = d->add(sd.langs, sd.srt_langs, lang_s).first;
    term dt_term = uri_term(dt_s.c_str());
    store::D::literal value(lex, dt_term, lang_pos);
    return mk_term(terms::LITERAL,
                   d->add(sd.literals, sd.srt_literals, value).first);
}
void store::builder::add_triple(term subj, term pred, term obj) {
    PARSE_TERM(add_triple, subj, subj_pos)
    PARSE_TERM_WT(add_triple, pred, pred_pos, pred_type, terms::URI)
    PARSE_TERM(add_triple, obj, obj_pos)
    if (stl::term_type(subj) == terms::LITERAL)
        throw invalid_argument("Subject cannot be a literal");

    auto& vec = d->sd->adj[subj_pos];
    auto empty = make_pair(pred, vector<term>());
    auto pairIt = lower_bound(vec.begin(), vec.end(), empty,
            [](const pair<term, vector<term>>& lhs,
               const pair<term, vector<term>>& rhs) {
        return lhs.first < rhs.first;
    });
    if (pairIt == vec.end() || pairIt->first != pred)
        pairIt = vec.insert(pairIt, empty);
    auto objIt = lower_bound(pairIt->second.begin(), pairIt->second.end(), obj);
    if (objIt == pairIt->second.end() || *objIt != obj)
        pairIt->second.insert(objIt, obj);
}
store store::builder::build() {
    store s = store(d->sd);
    delete d;
    d = nullptr;
    return s;
}

store::store(unique_ptr<D>& d) : d(d.release(), d.get_deleter()) {}
store::store(store&& rhs) : d(rhs.d.release(), d.get_deleter()) {}
store::~store() {}

string store::str(term node) const {
    PARSE_TERM(str, node, pos);
    if (terms::is_resource(stl::term_type(node))) return d->resources.at(pos);
    const D::literal& l = d->literals.at(pos);
    string lang = l.langtag ? d->langs.at(l.langtag-1) : string();
    lang = lang.empty() ? lang : "@" + lang;
    string dt;
    if (lang.empty()) dt = l.datatype ? lex(l.datatype) : xsd::string_;
    dt = dt.empty() ? dt : "^^<" + dt + ">";
    return "\"" + l.str + "\"" + dt + lang;
}
string store::lex(term node) const {
    PARSE_TERM(lex, node, pos);
    return terms::is_resource(stl::term_type(node)) ? d->resources.at(pos)
            : d->literals.at(pos).str;
}
term store::datatype(term literal) const {
    PARSE_TERM_WT(datatype,  literal, pos, type, terms::LITERAL)
    return d->literals.at(pos).datatype;
}
string store::langtag(term literal) const {
    PARSE_TERM_WT(datatype,  literal, pos, type, terms::LITERAL)
    size_t langtag_val = d->literals.at(pos).langtag;
    return langtag_val ? d->langs.at(langtag_val-1) : std::string();
}

term store::resource(const char* lexical) const {
    std::string str(lexical);
    auto it = d->lower_bound(d->resources, d->srt_resources, str);
    term result = term();
    assert(!result);
    if (it != d->srt_resources.end()) {
        std::string& found = d->resources[*it];
        assert(!found.empty());
        if (found == str) {
            terms::type type = found[0] == '_' ? terms::BLANK : terms::URI;
            result = mk_term(type, *it+1);
        }
    }
    return result;

}

term store::literal(const char* lexical, term datatype,
             const char* langtag) const {
    langtag = langtag ? langtag : "";

    std::string lang_s(langtag);
    auto lang_it = d->lower_bound(d->langs, d->srt_langs, lang_s);
    if (lang_it == d->srt_langs.end()) return 0;

    D::literal lit(std::string(lexical), datatype, *lang_it+1);
    auto lit_it = d->lower_bound(d->literals, d->srt_literals, lit);
    if (lit_it == d->srt_literals.end()) return 0;
    D::literal found = d->literals[*lit_it];
    if (found != lit) return 0;
    return mk_term(terms::LITERAL, *lit_it + 1);
}

term store::literal(const char* lexical, const char* datatype,
                    const char* langtag) const {
    datatype = datatype && *datatype ? datatype
             : (langtag && *langtag ? rdf::langString : xsd::string_);
    term dt_term = resource(datatype);
    return literal(lexical, dt_term, langtag);
}

store::po_iterator store::begin_po(term resource) const {
    PARSE_TERM(begin_po, resource, pos)
    auto& po_vec = d->adj.at(pos);
    return po_iterator(resource, po_vec.begin(), po_vec.end());
}
store::po_iterator store::end_po(term resource) const {
    PARSE_TERM(begin_po, resource, pos)
    auto end = d->adj.at(pos).end();
    return po_iterator(resource, end, end);
}

store::iterator store::begin() const {
    return iterator(this, 0);
}
store::iterator store::end() const {
    return iterator(this, d->adj.size());
}

bool store::contains(term subj, term pred, term obj) const {
    auto& vec = d->adj[term_value(subj)-1];
    auto empty = make_pair(pred, vector<term>(0));
    auto pairIt = lower_bound(vec.begin(), vec.end(), empty,
            [](const pair<term, vector<term>>& lhs,
               const pair<term, vector<term>>& rhs) {
        return lhs.first < rhs.first;
    });
    if (pairIt == vec.end() || pairIt->first != pred) return false;
    auto objIt = lower_bound(pairIt->second.begin(), pairIt->second.end(), obj);
    return objIt != pairIt->second.end() && *objIt == obj;
}

store::iterator::iterator() : current(0, 0, 0), str(0), s_pos(0), s_size(0) {}
store::iterator::iterator(const store* str, size_t pos) : current(0, 0, 0),
                                                    str(str), s_pos(pos),
                                                    s_size(str->d->adj.size()) {
    if (s_pos < s_size) {
        term resource = subj();
        po_it = str->begin_po(resource);
        po_end = str->end_po(resource);
        walk_next();
    }
}
term store::iterator::subj() const {
    terms::type type = str->d->resources[s_pos][0] == '_'
            ? terms::BLANK : terms::URI;
    return mk_term(type, s_pos+1);
}
void store::iterator::walk_next() {
    while (!at_end() && po_it == po_end) {
        ++s_pos;
        if (!at_end()) {
            term resource = subj();
            po_it = str->begin_po(resource);
            po_end = str->end_po(resource);
        }
    }
    if (!at_end()) current = *po_it;

}
void store::iterator::decrement() {
    size_t marker = std::numeric_limits<size_t>::max();
    while (s_pos != marker && po_it == str->begin_po(current.subj())) {
        s_pos = s_pos ? s_pos - 1 : marker;
        po_it = po_end = str->end_po(subj());
    }
    if (s_pos != marker)
        current = *(--po_it);
}

ostream& operator<<(ostream& out, const store::po_iterator& it) {
    if (it.p_it == it.p_end) return out << "end";
    auto p_pos = it.p_end - it.p_it;
    auto o_pos = it.p_it->second.end() - it.o_it;
    return  out << "store::po_iterator(" << p_pos << ", " << o_pos
                << ", " << *it << ")";
}
ostream& operator<<(ostream& out, const store::iterator& it) {
    if (it.at_end()) return out << "end";
    auto p_pos = it.po_it.p_end - it.po_it.p_it;
    auto o_pos = it.po_it.p_it->second.end() - it.po_it.o_it;
    return out << "store::iterator(" << it.s_pos << ", " << p_pos << ", "
            << o_pos << ", " << *it << ")";
}


}}} // namespace shacldator::str::stl
