/*
 * term.hpp
 *
 *  Created on: Mar 25, 2018
 *      Author: alexis
 */

#ifndef SRC_GRAPH_STR_STL_TERM_HPP_
#define SRC_GRAPH_STR_STL_TERM_HPP_

#include <cstdint>

#include "../../terms.hpp"

namespace shacldator{namespace str{namespace stl{

typedef std::uint64_t term;

inline terms::type term_type(term val) {
    return (terms::type)((val & 0xc000000000000000) >> 62);
}
inline term term_value(term val) {
    return val & ~0xc000000000000000;
}

inline term mk_term(terms::type type, uint64_t val) {
    return ((term)type << 62) | (val & ~0xc000000000000000);
}


}}} // namespace shacldator::str::stl



#endif /* SRC_GRAPH_STR_STL_TERM_HPP_ */
