/*
 * graph.hpp
 *
 *  Created on: Mar 24, 2018
 *      Author: alexis
 */

#ifndef SRC_GRAPH_GRAPH_HPP_
#define SRC_GRAPH_GRAPH_HPP_

#include <memory>
#include <type_traits>
#include "str/concepts/GraphStore.hpp"
#include "terms.hpp"
#include "node.hpp"
#include "statement.hpp"
#include "lexical.hpp"
#include "../utils.hpp"

namespace shacldator {

template<class G> class node;
template<class Graph> struct resource;
template<class Graph> struct literal;

namespace graph_priv {

template<class T, class G> struct res_h;
template<class G, class G2> struct res_h<resource<G2>, G> {
    res_h(const resource<G2> &v, const G& g)
          : v((void*)&v.graph() == (void*)&g ? v : g.template here<G2>(v)) {
    }
    resource<G> v;
};
template<class G, class G2> struct res_h<node<G2>, G> {
    res_h(const node<G2> &v, const G& g)
          : v((void*)&v.graph() == (void*)&g ? v.as_resource()
                               : g.template here<G2>(v.as_resource())){
    }
    resource<G> v;
};
template<class G> struct res_h<char*, G> {
    res_h(const char* uri, const G& g) : v(g.resource(uri)) {}
    resource<G> v;
};
template<class G> struct res_h<const char*, G> {
    res_h(const char* uri, const G& g) : v(g.resource(uri)) {}
    resource<G> v;
};
template<class G> struct res_h<std::string, G> {
    res_h(const std::string& uri, const G& g)
                          : v(g.resource(uri.c_str())) {}
    resource<G> v;
};

template<class T, class G> struct node_h {
    static_assert(is_node<T>::value,
                  "T must be a node<G>");
    node_h(const T &v, const G& g)
           : v((void*)&v.graph() == (void*)&g
                   ? *reinterpret_cast<const node<G>*>(&v)
                   : g.template here<typename T::graph_type>(v)) {}
    node<G> v;
};
template<class G> struct node_h<char*, G> {
    node_h(const char* uri, const G& g) : v(g.resource(uri)) {}
    node<G> v;
};
template<class G> struct node_h<const char*, G> {
    node_h(const char* uri, const G& g) : v(g.resource(uri)) {}
    node<G> v;
};
template<class G> struct node_h<std::string, G> {
    node_h(const std::string& uri, const G& g)
                          : v(g.resource(uri.c_str())) {}
    node<G> v;
};

template<class T> struct null_h {
    static bool check(const T& v) {return !v;}
};
template<> struct null_h<std::string> {
    static bool check(const std::string& v) {return v.empty();}
};

template<class G> struct statement_upgrader {
    typedef str::triple<typename G::store_t::term_t> argument_type;
    typedef statement<G> result_type;

    statement_upgrader(const G* g) : g(g) {}
    statement_upgrader(const statement_upgrader&) = default;
    statement_upgrader& operator=(const statement_upgrader&) = default;

    result_type operator()(const argument_type& t) const {
        return result_type(resource<G>(t.subj(), *g), resource<G>(t.pred(), *g),
                           node<G>(t.obj(), *g));
    }

private:
    const G* g;
};

}  // namespace graph_priv

template<class GraphStore> class graph {
public:
    typedef GraphStore store_t;
    typedef shacldator::node<graph> node_t;
    typedef shacldator::resource<graph> resource_t;
    typedef shacldator::literal<graph> literal_t;
    typedef typename store_t::term_t term_t;

    BOOST_CONCEPT_ASSERT((str::GraphStore<GraphStore>));

    graph(store_t&& str) : m_str(std::forward<store_t>(str)) {}
    graph(graph&&) = default;

    store_t& store() {return m_str;};
    const store_t& store() const {return m_str;};

    template<class G2>
    resource_t here(const shacldator::resource<G2>& node) const{
        term_t t = term_t();
        if (node)
            t = m_str.resource(node.uri().c_str());
        return resource_t(t, *this);
    }
    template<class G2>
    literal_t here(const shacldator::literal<G2>& node) const{
        term_t t = term_t();
        if (node) {
            auto ln = node.as_literal();
            t = m_str.literal(ln.lex().c_str(), ln.datatype().uri().c_str(),
                              ln.lang().c_str());
        }
        return literal_t(t, *this);
    }
    template<class G2> node_t here(const shacldator::node<G2>& node) const {
        term_t t = term_t();
        assert(!t);
        if (node) {
            std::string lex = node.lex();
            if (node.is_literal()) {
                auto ln = node.as_literal();
                t = m_str.literal(lex.c_str(), ln.datatype().uri().c_str(),
                                  ln.lang().c_str());
            } else {
                t = m_str.resource(lex.c_str());
            }
        }
        return node_t(t, *this);
    }

    // forwards with upgrades
    resource_t resource(const char* lexical) const {
        return resource_t(m_str.resource(lexical), *this);
    }
    resource_t resource(const std::string& lexical) const {
        return resource(lexical.c_str());
    }
    literal_t literal(const char* lexical, const char* dtype,
                      const char* langtag) const {
        return literal_t(m_str.literal(lexical, dtype, langtag), *this);
    }
    literal_t literal(const std::string& lexical, const std::string& dtype,
                      const std::string& langtag) const {
        return literal(lexical.c_str(), dtype.c_str(), langtag.c_str());
    }
    literal_t literal(const char* lexical,
                      const resource_t& dtype) const {
        return literal_t(m_str.literal(lexical,dtype.term(),nullptr), *this);
    }
    template<class S, class P>
    statement<graph> parse_statement(S&& s_str, P&& p_str,
                                     const std::string& o_str) const {
        resource_t s = resource(std::forward<S>(s_str));
        if (!s) return statement<graph>();
        resource_t p = resource(std::forward<P>(p_str));
        if (!p) return statement<graph>();

        lexical_info li = parse_lexical(o_str);
        node_t o;
        if (li.lex_begin == 0) {
            o = resource(o_str);
        } else {
            std::string lex(li.get_lex(o_str)), dt(li.get_dt(o_str)),
                        lang(li.get_lang(o_str));
            o = literal(lex, dt, lang);
        }
        return !o ? statement<graph>() : statement<graph>(s, p, o);

    }
    template<class S, class P>
    statement<graph> parse_statement(S&& s, P&& p, const char* o) const {
        return parse_statement(std::forward<S>(s), std::forward<P>(p),
                               std::string(o));
    }

    statement<graph> upgrade(const str::triple<term_t>& t) const {
        return statement<graph>(resource_t(t.subj(), *this),
                                resource_t(t.pred(), *this),
                                literal_t (t. obj(), *this));
    }

    template<class S, class P, class O>
    bool contains(const S& subj, const P& pred, const O& obj) const {
        using namespace graph_priv;
        using std::invalid_argument;
        if (null_h<S>::check(subj)) throw invalid_argument("subj is null!");
        if (null_h<P>::check(pred)) throw invalid_argument("pred is null!");
        if (null_h<O>::check(obj )) throw invalid_argument( "obj is null!");
        return contains_impl(
                res_h <typename std::decay<S>::type, graph>(subj, *this).v,
                res_h <typename std::decay<P>::type, graph>(pred, *this).v,
                node_h<typename std::decay<O>::type, graph>(obj,  *this).v);
    }

    bool contains(const statement<graph>& s) const {
        return contains(s.subj(), s.pred(), s.obj());
    }

    typedef transform_fwd_it<typename store_t::iterator,
                             graph_priv::statement_upgrader<graph>> iterator;
    typedef iterator const_iterator;
    iterator begin() const {
        graph_priv::statement_upgrader<graph> t(this);
        return iterator(m_str.begin(), t);
    }
    iterator end() const {
        graph_priv::statement_upgrader<graph> t(this);
        return iterator(m_str.end(), t);
    }

//    //TODO: impl
//    po_iterator begin_po(const shacldator::resource& subject)
//    po_iterator end_po(const shacldator::resource& subject)
//
//    iterator begin()
//    iterator end()
private:
    bool contains_impl(const resource_t& subj, const resource_t& pred,
                       const node_t& obj) const {
        return m_str.contains(subj.term(), pred.term(), obj.term());
    }

    store_t m_str;
};

template<class Store> graph<Store> mk_graph(Store&& str) {
    return graph<Store>(std::forward<Store>(str));
}

} //namespace shacldator

#endif /* SRC_GRAPH_GRAPH_HPP_ */
