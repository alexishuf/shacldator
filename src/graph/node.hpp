/*
 * node.hpp
 *
 *  Created on: Mar 24, 2018
 *      Author: alexis
 */

#ifndef SRC_GRAPH_NODE_HPP_
#define SRC_GRAPH_NODE_HPP_

#include <memory>
#include <cstring>
#include <string>
#include <cassert>
#include <iostream>
#include <memory>
#include <type_traits>
#include <boost/concept_check.hpp>
#include <cstdio>
#include <ostream>
#include <istream>
#include <sstream>
#include <stdexcept>
#include <boost/optional.hpp>
#include <boost/convert/lexical_cast.hpp>
#include "bad_node_type.hpp"
#include "terms.hpp"
#include "../ns/xsd.hpp"

namespace shacldator {

template<class Graph> struct resource;
template<class Graph> struct literal;

struct abstract_node {};

template<class Graph> class node : public abstract_node {
public:
    typedef Graph graph_type;
    typedef typename graph_type::term_t term_t;

    node() : m_g(nullptr), m_term() {}
    node(term_t term, const graph_type& graph) : m_term(term), m_g(&graph) {}
    node(const node& rhs) = default;
    ~node() = default;
    node& operator=(const node& rhs) = default;

    inline operator bool() const { return m_term; }
    inline bool operator!() const { return !m_term; }

    inline std::string to_str() const {
        std::stringstream ss;
        if (!*this) {
            ss << "null";
        } else if (is_literal()) {
            const literal<Graph>& l = as_literal();
            ss << "\"" << l.lex() << "\"";
            std::string lang = l.lang();
            if (!lang.empty()) ss << "@"   << lang;
            else               ss << "^^<" << l.datatype().uri() << ">";
        } else {
            ss << lex();
        }
        return ss.str();
    }

    inline const graph_type& graph() const {return *m_g;}
    std::string lex() const {return m_g->store().lex(m_term);}
    inline term_t term() const {return m_term;}

    inline terms::type type() const {
        return m_term ? m_g->store().type(m_term) : terms::NONE;
    }
    bool is_resource() const {return terms::is_resource(type());}
    bool is_blank() const {return terms::is_blank(type());}
    bool is_literal() const {return terms::is_literal(type());}

    resource<graph_type> as_resource() const {
        if (m_term && !is_resource()) throw bad_node_type(bad_node_type::RESOURCE);
        return resource<graph_type>(m_term, *m_g);
    }
    literal<graph_type> as_literal() const {
        if (m_term && !is_literal()) throw bad_node_type(bad_node_type::LITERAL);
        return literal<graph_type>(m_term, *m_g);
    }

    bool operator<(const node& o) const {
        if (m_g > o.m_g) return false;
        if (m_g < o.m_g) return true;
        return m_term < o.m_term;
    }
    bool operator==(const node& o) const {
        if (!m_term && !o.m_term) return true;
        return m_g == o.m_g && m_term == o.m_term;
    }
    bool operator!=(const node& o) const {return !operator==(o);}
    bool operator<=(const node& o) const {return operator<(o) || operator==(o);}
    bool operator>=(const node& o) const {return !operator<(o);}
    bool operator>(const node& o) const {return !operator<=(o);}
protected:
    term_t m_term;
    const graph_type* m_g;
};

template<class X> struct is_node : public
std::integral_constant<bool, std::is_base_of<abstract_node, X>::value> {};
struct is_node_f {template<class X> struct apply : public is_node<X> {};};

struct abstract_resource {};

template<class Graph> struct resource : public node<Graph>,
                                        public abstract_resource {
    resource() = default;
    resource(typename Graph::term_t term,
             const Graph& graph) : node<Graph>(term, graph) {}
    resource(const resource&) = default;
    resource& operator=(const resource&) = default;
    std::string uri() const { return node<Graph>::lex(); }
    const resource<Graph>& as_resource() const {return *this;}
};

template<class X> struct is_resource : public
std::integral_constant<bool, std::is_base_of<abstract_resource, X>::value> {};
struct is_resource_f {template<class X> struct apply : public is_resource<X> {};};

template<class Graph> struct resource_upgrader : public
std::unary_function<const node<Graph>&, resource<Graph>> {
    resource<Graph> operator()(const node<Graph>& n) const {
        return n.as_resource();
    }
};

struct bad_literal_cast : public std::invalid_argument {
    explicit bad_literal_cast(const std::string& msg) noexcept
                              : std::invalid_argument(msg) {}
    bad_literal_cast(const bad_literal_cast&) noexcept = default;
};

template<class T, bool is_integral=std::is_integral<T>::value>
struct literal_parser {
    static T parse(const std::string& lex, const std::string& dt) {
        if (std::is_integral<T>::value && std::is_unsigned<T>::value) {
            if (xsd::is_signed(xsd::canonical(dt))
                    && lex.find('-') != std::string::npos) {
                std::stringstream ss;
                ss << "parsing " << dt << " into unsigned integral";
                throw bad_literal_cast(ss.str());
            }
        }
        return boost::lexical_cast<T>(lex);
    }
};

template<class Graph> struct literal : public node<Graph> {
    literal() = default;
    literal(typename Graph::term_t term,
            const Graph& graph) : node<Graph>(term, graph) {}
    literal(const literal&) = default;
    literal& operator=(const literal&) = default;
    std::string lang() const {return this->m_g->store().langtag(this->m_term);}
    resource<Graph> datatype() const {
        return resource<Graph>(this->m_g->store().datatype(this->m_term),
                               *this->m_g);
    }
    const literal<Graph>& as_literal() const {return *this;}
    template<class T> T as() const {
        return literal_parser<T>::parse(this->lex(), this->datatype().uri());
    }
};



template<class Graph> struct literal_upgrader : public
std::unary_function<const node<Graph>&, literal<Graph>> {
    literal<Graph> operator()(const node<Graph>& n) const {
        return n.as_literal();
    }
};

//template<class X> struct is_literal : public std::false_type {};
//template<class G> struct is_literal<literal<G>> : public std::true_type {};

template<class Term, class Graph>
std::ostream& operator<<(std::ostream& out, const node<Graph>& node) {
    if (!node) {
        return out << ":null:";
    } else if (node.is_literal()) {
        const literal<Graph>& l = node.as_literal();
        return out << l.lex() << "^^" << l.datatype()->uri();
    }
    return out << node.lex();
}

} //namespace shacldator

namespace std {

template<class Graph> struct hash<shacldator::node<Graph>> {
    typedef shacldator::node<Graph> argument_type;
    typedef std::size_t result_type;
    result_type operator()(const argument_type& a) const noexcept {
        return hash<typename argument_type::term_t>()(a.term()) << 2
                | ((reinterpret_cast<std::ptrdiff_t>(&a.graph()) >> 3) & 0x3);
    }
};

}  // namespace std


#endif /* SRC_GRAPH_NODE_HPP_ */
