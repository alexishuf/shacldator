/*
 * graph_tags.hpp
 *
 *  Created on: Apr 1, 2018
 *      Author: alexis
 */

#ifndef SRC_GRAPH_INDEX_TAGS_HPP_
#define SRC_GRAPH_INDEX_TAGS_HPP_

#include <boost/mpl/if.hpp>
#include <boost/mpl/bool.hpp>
#include <boost/mpl/contains.hpp>

namespace shacldator{

struct po_tag {};
struct so_tag {};
struct sp_tag {};

//specializing this will be faster if you do not want a index_tags member
template<class GraphStore> struct get_index_tags {
    typedef typename GraphStore::index_tags type;
};

template<class GraphStore, class Tag> struct has_tag : public
boost::mpl::contains<typename get_index_tags<GraphStore>::type, Tag>::type {};

template<class GraphStore>
struct has_po_tag : public has_tag<GraphStore, po_tag> {};
template<class GraphStore>
struct has_so_tag : public has_tag<GraphStore, so_tag> {};
template<class GraphStore>
struct has_sp_tag : public has_tag<GraphStore, sp_tag> {};

} //namespace shacldator

#endif /* SRC_GRAPH_INDEX_TAGS_HPP_ */
