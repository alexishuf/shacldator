/*
 * graph_sink.hpp
 *
 *  Created on: Apr 30, 2018
 *      Author: alexis
 */

#ifndef SRC_GRAPH_GRAPH_SINK_HPP_
#define SRC_GRAPH_GRAPH_SINK_HPP_

#include <type_traits>
#include <unordered_map>
#include <memory>
#include "str/concepts/GraphStoreBuilder.hpp"
#include "node.hpp"
#include "../utils.hpp"
#include "str/triple.hpp"
#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_generators.hpp>
#include <boost/uuid/uuid_io.hpp>

namespace shacldator {

namespace shacl {
template<class G> struct shape;
}  // namespace shacl

namespace priv {

template<class A> struct cstr_h;
template<> struct cstr_h<const char*> {
    cstr_h(const char* uri) : cstr(uri) {}
    const char* cstr;
};
template<int n> struct cstr_h<char[n]> {
    cstr_h(const char* uri) : cstr(uri) {}
    const char* cstr;
};
template<> struct cstr_h<std::string> {
    cstr_h(const std::string& uri) : cstr(uri.c_str()) {}
    const char* cstr;
};
template<class G> struct cstr_h<resource<G>> {
    cstr_h(const resource<G>& resource) : str(resource.uri()),
                                          cstr(str.c_str()) {}
    std::string str;
    const char* cstr;
};
template<class G> struct cstr_h<shacl::shape<G>> {
    cstr_h(const shacl::shape<G>& resource) : str(resource.uri()),
                                              cstr(str.c_str()) {}
    std::string str;
    const char* cstr;
};


}  // namespace priv

template<class Builder, class BuilderPtr> struct graph_sink;

template<class B, class BPtr, class T>
std::string write_resource(graph_sink<B, BPtr>& out, const T& obj);

template<class Builder, class BuilderPtr=Builder*> struct graph_sink {
    BOOST_CONCEPT_ASSERT((str::GraphStoreBuilder<Builder>));
    typedef typename Builder::term_t term_t;

    graph_sink(BuilderPtr builder) : builder(builder) {}
    graph_sink(const graph_sink&) = delete;
    graph_sink(graph_sink&&) = delete;
    graph_sink& operator=(const graph_sink&) = default;
    graph_sink& operator=(graph_sink&&) = default;

private:
    graph_sink& plain_add(const char* s, const char* p, const char* o) {
        auto s_term = builder->uri_term(s), p_term = builder->uri_term(p),
             o_term = builder->uri_term(o);
        builder->add_triple(s_term, p_term, o_term);
        return *this;
    }
    graph_sink& plain_add(const char* s, const char* p,
                    const char* lex, const char* dt, const char* lang) {
        auto s_term = builder->uri_term(s), p_term = builder->uri_term(p),
             o_term = builder->literal_term(lex, dt, lang);
        builder->add_triple(s_term, p_term, o_term);
        return *this;
    }

public:
    std::string create_blank() {
        std::string pseudo_uri = "_:"
                + boost::uuids::to_string(uuid_gen.initialized()());
        builder->blank_term(pseudo_uri.c_str());
        return pseudo_uri;
    }

//    graph_sink& add(const char* s, const char* p, const char* o) {
//        return plain_add(s, p, o);
//    }
//    graph_sink& add(const char* s, const char* p,
//                    const char* lex, const char* dt, const char* lang=0) {
//        return plain_add(s, p, lex, dt, lang);
//    }
//
//    graph_sink& add(const std::string& s, const std::string& p,
//                    const std::string& o){
//        return plain_add(s.c_str(), p.c_str(), o.c_str());
//    }
//    graph_sink& add(const std::string& s, const std::string& p,
//                    const std::string& lex, const std::string& dt,
//                    const std::string& lang=std::string()){
//        return plain_add(s.c_str(), p.c_str(),
//                         lex.c_str(), dt.c_str(), lang.c_str());
//    }

    template<class A1, class A2, class G3>
    graph_sink& add(const A1& s, const A2& p, const node<G3>& o) {
        using priv::cstr_h;
        if (o.is_literal()) {
            literal<G3> l = o.as_literal();
            return plain_add(cstr_h<A1>(s).cstr, cstr_h<A2>(p).cstr,
                             l.lex().c_str(), l.datatype().uri().c_str(),
                             l.lang().c_str());
        }
        return plain_add(cstr_h<A1>(s).cstr,cstr_h<A2>(p).cstr,o.lex().c_str());
    }
    template<class A1, class A2, class A3>
    graph_sink& add(const A1& s, const A2& p, const A3& o,
                    typename std::enable_if<
                            !is_node<A3>::value
                          || is_resource<A3>::value>::type* = 0) {
        using priv::cstr_h;
        return plain_add(cstr_h<A1>(s).cstr,  cstr_h<A2>(p).cstr,
                         cstr_h<A3>(o).cstr);
    }

    template<class A1, class A2, class A3, class A4, class A5>
    graph_sink& add(const A1& s, const A2& p,
                    const A3& lex, const A4& dt, const A5& lang) {
        using priv::cstr_h;
        return plain_add(cstr_h<A1>(s).cstr,   cstr_h<A2>(p).cstr,
                         cstr_h<A3>(lex).cstr, cstr_h<A4>(dt).cstr,
                         cstr_h<A5>(lang).cstr);
    }

    template<class T> std::string write_resource(const T& obj) {
        return shacldator::write_resource(*this, obj);
    }

    template<class T> graph_sink& operator<<(const T& obj) {
        shacldator::write_resource(*this, obj);
        return *this;
    }

private:
    BuilderPtr builder;
    pack<boost::uuids::random_generator, false> uuid_gen;
};


//template<class Builder> graph_sink<Builder> mk_graph_sink(Builder* ptr) {
//    return graph_sink<Builder>(ptr);
//}
//template<class Builder> graph_sink<Builder, std::shared_ptr<Builder>>
//mk_graph_sink(const std::shared_ptr<Builder>& ptr) {
//    return graph_sink<Builder, std::shared_ptr<Builder>>(ptr);
//}

}  // namespace shacldator



#endif /* SRC_GRAPH_GRAPH_SINK_HPP_ */
