/*
 * raptor_stream_adaptor.cpp
 *
 *  Created on: May 2, 2018
 *      Author: alexis
 */

#include <iostream>
#include <istream>
#include <ostream>
#include <limits>
#include "raptor_stream_adaptor.hpp"

namespace shacldator {namespace raptor {

int write_byte(void* c, const int byte) {
    char buf = (char)byte;
    std::ostream& out = *reinterpret_cast<std::ostream*>(c);
    out.write(&buf, 1);
    return out.fail();
}
int write_bytes(void* c, const void* ptr, std::size_t size,
                                           std::size_t nmemb) {
    std::ostream& out = *reinterpret_cast<std::ostream*>(c);
    out.write(reinterpret_cast<const char*>(ptr), size*nmemb);
    return out.fail();
}
int write_end(void* c) {
    std::ostream& out = *reinterpret_cast<std::ostream*>(c);
    out.flush();
    return out.fail();
}
int read_bytes(void* c, void* ptr, std::size_t size,
                                   std::size_t nmemb) {
    std::istream& in = *reinterpret_cast<std::istream*>(c);
    size_t count = in.read(reinterpret_cast<char*>(ptr), size*nmemb).gcount();
    if (count > std::size_t(std::numeric_limits<int>::max()))
        throw std::runtime_error("Too many bytes read!");
    return in.bad() ? -1 : int(count);
}
int read_eof(void* c) {
    std::istream& in = *reinterpret_cast<std::istream*>(c);
    return in.eof();
}

const raptor_iostream_handler cxxstream_handler = {
        2,
        nullptr, //init
        nullptr, //finish
        &write_byte, //write_byte
        &write_bytes, //write_bytes
        &write_end, //write_end
        &read_bytes, //read_bytes
        &read_eof //read_eof
};

}}  // namespace shacldator::raptor


