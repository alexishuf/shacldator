/*
 * raptor_stream_bridge.hpp
 *
 *  Created on: May 2, 2018
 *      Author: alexis
 */

#ifndef SRC_GRAPH_RAPTOR_STREAM_BRIDGE_HPP_
#define SRC_GRAPH_RAPTOR_STREAM_BRIDGE_HPP_

#include <raptor2/raptor2.h>

namespace shacldator {namespace raptor {

extern const raptor_iostream_handler cxxstream_handler;

}}  // namespace shacldator::raptor


#endif /* SRC_GRAPH_RAPTOR_STREAM_BRIDGE_HPP_ */
