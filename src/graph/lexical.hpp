/*
 * literal.hpp
 *
 *  Created on: May 24, 2018
 *      Author: alexis
 */

#ifndef SRC_GRAPH_LEXICAL_HPP_
#define SRC_GRAPH_LEXICAL_HPP_

#include <string>

namespace shacldator {

struct lexical_info {
    lexical_info(std::string::size_type lex_begin,
                 std::string::size_type lex_len,
                 std::string::size_type dt_begin,
                 std::string::size_type lang_begin) : lex_begin(lex_begin),
                                                      lex_len(lex_len),
                                                      dt(dt_begin),
                                                      lang(lang_begin){}
    lexical_info(const lexical_info&) = default;
    lexical_info& operator=(const lexical_info&) = default;


    std::string get_lex(const std::string& literal) const {
        return literal.substr(lex_begin, lex_len);
    }
    std::string get_dt(const std::string& literal) const {
        return dt == std::string::npos
                ? std::string()
                : literal.substr(dt, literal.length()-dt-1);
    }

    std::string get_lang(const std::string& literal) const {
        return lang == std::string::npos
                ? std::string()
                : literal.substr(lang, literal.length()-lang);
    }

    std::string::size_type lex_begin;
    std::string::size_type lex_len;
    std::string::size_type dt;
    std::string::size_type lang;
};

lexical_info parse_lexical(const std::string& str);
lexical_info parse_lexical(const char* cstr);

}  // namespace shacldator



#endif /* SRC_GRAPH_LEXICAL_HPP_ */
