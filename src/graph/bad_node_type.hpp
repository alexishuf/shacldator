/*
 * bad_node_type.hpp
 *
 *  Created on: Mar 25, 2018
 *      Author: alexis
 */

#ifndef SRC_GRAPH_BAD_NODE_TYPE_HPP_
#define SRC_GRAPH_BAD_NODE_TYPE_HPP_

#include <typeinfo>
#include <type_traits>


namespace shacldator {

class bad_node_type: public std::bad_cast {
public:
    typedef enum {RESOURCE, LITERAL} expected_type;

    bad_node_type(expected_type t) noexcept : m_t(t) {};
    virtual ~bad_node_type() = default;
    virtual const char* what() const noexcept {
        return m_t == RESOURCE ? "Bad node type, expected a resource"
                : m_t == LITERAL ? "Bad node type, expected a literal"
                        : "Bad node type, expected something else";
    }

private:
    expected_type m_t;
};

} /* namespace shacldator */

#endif /* SRC_GRAPH_BAD_NODE_TYPE_HPP_ */
