/*
 * lexical.cpp
 *
 *  Created on: May 24, 2018
 *      Author: alexis
 */

#include "lexical.hpp"

using namespace std;


namespace shacldator {

lexical_info parse_lexical(const std::string& str) {
    string::size_type lex_begin(0),           lex_len(0),
                      dt_begin(string::npos), lang_begin(string::npos);
    if (!str.empty()) {
        if (*str.begin() == '"') {
            lex_begin = 1;
            auto end = str.find_last_of("@");
            if (end != string::npos) {
                lex_len = end - 2 /*two "*/;
                lang_begin = end + 1;
            } else {
                end = str.find_last_of("^^");
                //                       ^-- end points here
                if (end != string::npos) {
                    lex_len = end - 1 - 2 /*two "*/;
                    dt_begin = end + 2; //^^<
                } else {
                    lex_len = str.length() - 2 /*two "*/;
                }
            }
        } else {
            lex_len = str.length();
        }
    }
    return lexical_info(lex_begin, lex_len, dt_begin, lang_begin);
}

lexical_info parse_lexical(const char* cstr) {
    return parse_lexical(std::string(cstr ? cstr : ""));
}

}  // namespace shacldator
