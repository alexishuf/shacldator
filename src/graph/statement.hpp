/*
 * statement.hpp
 *
 *  Created on: Mar 24, 2018
 *      Author: alexis
 */

#ifndef SRC_GRAPH_STATEMENT_HPP_
#define SRC_GRAPH_STATEMENT_HPP_

#include <cassert>
#include <memory>
#include "node.hpp"

namespace shacldator {

template<class Graph> class statement {
public:
    typedef Graph graph_type;
    typedef resource<graph_type> resource_t;
    typedef node<graph_type> node_t;

    statement() {}
    statement(resource_t subj, resource_t pred, node_t obj)
              : m_subj(subj), m_pred(pred), m_obj(obj) {
        assert((m_subj && m_pred && m_obj) ^ (!m_subj && !m_pred && !m_obj));
    }
    statement(const statement& rhs) = default;
    ~statement() = default;
    statement& operator=(const statement& rhs) = default;

    bool operator!() const {
        assert((m_subj && m_pred && m_obj) ^ (!m_subj && !m_pred && !m_obj));
        return !m_subj;
    }

    const resource_t& subj() const {return m_subj;}
    const resource_t& pred() const {return m_pred;}
    const node_t& obj() const {return m_obj;}

    std::string to_str() const {
        if (!*this) return "[null stmt]";
        std::stringstream ss;
        ss << m_subj.to_str() << " " << m_pred.to_str()
           << " " << m_obj.to_str();
        return ss.str();
    }

    bool operator==(const statement& o) const {
        return m_subj == o.m_subj && m_pred == o.m_pred
                                  && m_obj  == o.m_obj;
    }
    bool operator<(const statement& o) const {
        if (m_subj > o.m_subj) return false;
        if (m_subj < o.m_subj) return true;
        if (m_pred > o.m_pred) return false;
        if (m_pred < o.m_pred) return true;
        return m_obj < o.m_obj;
    }
    bool operator!=(const statement& o) const {return !operator==(o);}
    bool operator<=(const statement& o) const {
        return operator<(o) || operator==(o);
    }
    bool operator>=(const statement& o) const {return !operator<(o);}
    bool operator>(const statement& o) const {return !operator<=(o);}

private:
    resource_t m_subj;
    resource_t m_pred;
    node_t m_obj;
};

} //namespace shacldator


#endif /* SRC_GRAPH_STATEMENT_HPP_ */
