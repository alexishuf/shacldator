/*
 * dsl.cpp
 *
 *  Created on: Apr 1, 2018
 *      Author: alexis
 */

#include "dsl.hpp"

namespace shacldator {namespace qry {

#define SHACLDATOR_X(z, num, data) ph < num > BOOST_PP_CAT(_, num);
BOOST_PP_REPEAT_FROM_TO(1, SHACLDATOR_QRY_MAX_PLACEHOLDER, SHACLDATOR_X, _)
#undef SHACLDATOR_X

}} //namespace shacldator::qry


