/*
 * dsl.hpp
 *
 *  Created on: Apr 1, 2018
 *      Author: alexis
 */

#ifndef SRC_QRY_DSL_HPP_
#define SRC_QRY_DSL_HPP_

#include <ostream>
#include <boost/preprocessor/repetition/repeat_from_to.hpp>
#include <boost/preprocessor/cat.hpp>
#include <boost/mpl/bool.hpp>
#include <boost/mpl/int.hpp>
#include <boost/mpl/less.hpp>
#include <boost/mpl/vector.hpp>

namespace shacldator{namespace qry {

// _t(_1, foaf::name literal("Alice", xsd::string))

template<class X> struct dsl_node;

class placeholder_tag {};
template<int value_> struct ph {
    typedef placeholder_tag tag;
    typedef boost::mpl::int_<value_> type;
    static const int value = value_;
};

template<int n> std::ostream& operator<<(std::ostream& out,
                                         const ph<n>& ph) {
    return out << "_" << n;
}

#define SHACLDATOR_QRY_MAX_PLACEHOLDER 64
#define SHACLDATOR_X(z, num, data) extern ph < num > BOOST_PP_CAT(_, num);
BOOST_PP_REPEAT_FROM_TO(1, SHACLDATOR_QRY_MAX_PLACEHOLDER, SHACLDATOR_X, _)
#undef SHACLDATOR_X

template<class T> struct is_placeholder : public boost::mpl::false_ {};
template<int n>
struct is_placeholder<ph<n> > : public boost::mpl::true_ {};

namespace op {
template<class Arg, class Placeholders> struct projection {};
template<class S, class P, class O> struct triple_pattern {};
} //namespace op
template<class Arg, class Placeholders>
struct dsl_node<op::projection<Arg, Placeholders>> {
    typedef Placeholders placeholders_t;
    typedef Arg arg_t;
    Arg arg;
};
template<class Arg, class Placeholders> using
        projection = dsl_node<op::projection<Arg, Placeholders>>;


template<class S, class P, class O>
struct dsl_node<op::triple_pattern<S, P, O>> {
    typedef S subj_t;
    typedef P pred_t;
    typedef O  obj_t;
    typedef boost::mpl::vector<subj_t, pred_t, obj_t> terms_t;

    dsl_node(const subj_t& subj, const pred_t& pred,
             const obj_t& obj) : subj(subj), pred(pred), obj(obj) {}
    template<class... Args>
    qry::projection<dsl_node, boost::mpl::vector<Args...> >
    operator()(Args... args) const {
        return qry::projection<dsl_node, boost::mpl::vector<Args...> >{*this};
    }

    subj_t subj;
    pred_t pred;
    obj_t obj;
};
template<class S, class P, class O> using
        triple_pattern = dsl_node<op::triple_pattern<S, P, O>>;

template<class S, class P, class O> triple_pattern<S, P, O> _t(S s, P p, O o) {
    return triple_pattern<S, P, O>(s, p, o);
}

#define SHACLDATOR_QRY_BIN_OPS \
        SHACLDATOR_X(and_, &) \
        SHACLDATOR_X(or_, |)
#define SHACLDATOR_X(cls, sym) \
        namespace op {template<class L, class R> struct cls {};} \
        template<class L, class R> struct dsl_node<op::cls<L, R>> { \
            typedef L left_t; \
            typedef R right_t; \
            dsl_node(const L& l, const R& r) : left(l), right(r) {} \
            template<class... As> \
            qry::projection<dsl_node, boost::mpl::vector<As...> > \
            operator()(As... args) const { \
                return qry::projection<dsl_node, \
                        boost::mpl::vector<As...> >{*this}; \
            } \
            left_t left; \
            right_t right; \
        }; \
        template<class L, class R> using cls = dsl_node<op::cls<L, R>>; \
        template<class L, class R> \
        cls<dsl_node<L>, dsl_node<R>> \
        operator sym(const dsl_node<L>& l, const dsl_node<R>& r) {\
            return cls<dsl_node<L>, dsl_node<R>>(l, r); \
        }
SHACLDATOR_QRY_BIN_OPS
#undef SHACLDATOR_X

#define SHACLDATOR_QRY_UN_OPS \
        SHACLDATOR_X(not_, !)
#define SHACLDATOR_X(cls, sym) \
        namespace op {template<class A> struct cls {};} \
        template<class Arg> struct dsl_node<op::cls<Arg>> { \
            typedef Arg arg_t; \
            dsl_node(const arg_t& arg) : arg(arg) {} \
            template<class... As> \
            qry::projection<dsl_node, boost::mpl::vector<As...> > \
            operator()(As... args) const { \
                return qry::projection<dsl_node, \
                        boost::mpl::vector<As...> >{*this}; \
            } \
            arg_t arg; \
        }; \
        template<class A> using cls = dsl_node<op::cls<A>>; \
        template<class A> cls<dsl_node<A>> \
        operator sym(const dsl_node<A>& arg) { \
            return cls<dsl_node<A>>(arg); \
        }
SHACLDATOR_QRY_UN_OPS
#undef SHACLDATOR_X

namespace op {template<class A> struct unique {};}
template<class A> struct dsl_node<op::unique<A>> {
    typedef A arg_t;
    dsl_node(const arg_t& a) : arg(a) {}
    template<class... As> \
    qry::projection<dsl_node, boost::mpl::vector<As...> >
    operator()(As... args) const {
        return qry::projection<dsl_node, boost::mpl::vector<As...> >{*this};
    }
    arg_t arg;
};
template<class A> using unique = dsl_node<op::unique<A>>;

template<class A> dsl_node<op::unique<A>> _u(const A& a) {
    return dsl_node<op::unique<A>>(a);
}

/// remove double negation: !!x == x
template<class Arg> Arg operator!(const not_<Arg>& op) {return op.arg;}

}} //namespace shacldator::qry

namespace boost {namespace mpl {

template<>
struct less_impl<shacldator::qry::placeholder_tag,
                 shacldator::qry::placeholder_tag> {
    template<class A1, class A2> struct apply : public
    boost::mpl::less<typename A1::type, typename A2::type> {};
};

}}  // namespace boost::mpl


#endif /* SRC_QRY_DSL_HPP_ */
