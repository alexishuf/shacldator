/*
 * triple_pattern_matcher.hpp
 *
 *  Created on: Apr 2, 2018
 *      Author: alexis
 */

#ifndef SRC_QRY_PRIV_TP_MATCHER_HPP_
#define SRC_QRY_PRIV_TP_MATCHER_HPP_

#include <type_traits>
#include <boost/static_assert.hpp>
#include "../../graph/str/triple.hpp"
#include "../dsl.hpp"
#include "../lit.hpp"
#include "../../graph/terms.hpp"
#include <cassert>

namespace shacldator {

template<class Graph> struct node;

namespace qry {namespace priv {

template<class Term, class S, class P, class O> struct cooccur_matcher {
    BOOST_STATIC_ASSERT(!(is_placeholder<S>::value
                && (std::is_same<S, P>::value || std::is_same<S, O>::value)));
    BOOST_STATIC_ASSERT(!(is_placeholder<P>::value
                && (std::is_same<P, S>::value || std::is_same<P, O>::value)));
    BOOST_STATIC_ASSERT(!(is_placeholder<O>::value
                && (std::is_same<O, S>::value || std::is_same<O, P>::value)));
    bool operator()(const str::triple<Term>& t) const {return true;}
};

template<class T, int x> struct cooccur_matcher<T, ph<x>, ph<x>, ph<x>> {
    bool operator()(const str::triple<T>& t) const {
        return t.subj() == t.pred() && t.subj() == t.obj();
    }
};
template<class T, int x, class S> struct cooccur_matcher<T, S, ph<x>, ph<x>> {
    BOOST_STATIC_ASSERT(!std::is_same<S, ph<x>>::value);
    bool operator()(const str::triple<T>& t) const {return t.pred()==t.obj();}
};
template<class T, int x, class P> struct cooccur_matcher<T, ph<x>, P, ph<x>> {
    BOOST_STATIC_ASSERT(!std::is_same<P, ph<x>>::value);
    bool operator()(const str::triple<T>& t) const {
        volatile bool omfg = t.subj()==t.obj();
        return t.subj()==t.obj();
    }
};
template<class T, int x, class O> struct cooccur_matcher<T, ph<x>, ph<x>, O> {
    BOOST_STATIC_ASSERT(!std::is_same<O, ph<x>>::value);
    bool operator()(const str::triple<T>& t) const {return t.subj()==t.pred();}
};

template<class Term, class S, class P, class O, class CorefMatcher>
struct tp_matcher {
    typedef S subj_t;
    typedef P pred_t;
    typedef O  obj_t;
    typedef Term term_t;
    typedef CorefMatcher cooccur_matcher_t;
    typedef str::triple<term_t> triple_t;

    tp_matcher(subj_t subj, pred_t pred, obj_t obj)
               : subj(subj), pred(pred), obj(obj) {}
    tp_matcher(const tp_matcher& rhs) = default;
    tp_matcher& operator=(const tp_matcher& rhs) = default;

    bool operator()(const triple_t& t) {
        return subj(t.subj()) && pred(t.pred()) && obj(t.obj()) && cooccur(t);
    }

    subj_t subj;
    pred_t pred;
     obj_t obj;
    cooccur_matcher_t cooccur;
};

template<class Term> struct term_matcher {
    typedef Term term_t;
    term_matcher(const term_t& expected) : expected(expected) {}
    bool operator()(const term_t& actual) {
        return expected && actual == expected;
    }
    term_t expected;
};

template<class GraphStore> struct literal_matcher {
    typedef typename std::add_const<GraphStore>::type store_t;
    typedef typename store_t::term_t term_t;

    literal_matcher(store_t& str, const lit& l): str(str), l(l) {
        if (l.dt) {
            dt_expected = str.resource(l.dt.get());
            // if not found, leave null and operator() will never match
        }
    }
    bool operator()(const term_t act) const {
        if (!terms::is_literal(str.type(act))) return 0;
        if (l.lex && strcmp(l.lex.get(), str.lex(act).c_str())) return 0;
        if (l.lang && strcmp(l.lang.get(), str.langtag(act).c_str())) return 0;
        if (l.dt && str.datatype(act) != dt_expected) return 0;
        return true;
    }

    store_t& str;
    lit l;
    term_t dt_expected;
};

template<class GraphStore> term_matcher<typename GraphStore::term_t>
make_matcher(GraphStore& g, const char* url) {
    return term_matcher<typename GraphStore::term_t>(g.resource(url));
}

template<class GraphStore> term_matcher<typename GraphStore::term_t>
make_matcher(GraphStore& g, const std::string& url) {
    return make_matcher(g, url.c_str());
}

template<class Graph> term_matcher<typename Graph::term_t>
make_matcher(const typename Graph::store_t& g, const node<Graph>& n) {
    return term_matcher<typename Graph::term_t>(n.term());
}

template<class GraphStore> literal_matcher<GraphStore>
make_matcher(GraphStore& g, const qry::lit& literal) {
    return literal_matcher<GraphStore>(g, literal);
}

template<class GraphStore, int n> get_true
make_matcher(GraphStore& g, const ph<n>& ph) {return get_true();}


template<class GraphStore, class S, class P, class O> struct tp_matcher_maker {
    typedef tp_matcher<
                typename GraphStore::term_t,
                decltype(make_matcher(*(const GraphStore*)0, *(S*)0)),
                decltype(make_matcher(*(const GraphStore*)0, *(P*)0)),
                decltype(make_matcher(*(const GraphStore*)0, *(O*)0)),
                cooccur_matcher<typename GraphStore::term_t, S, P, O>
            > type;
    static type make(const GraphStore& str, const triple_pattern<S, P, O>& tp){
        return type(make_matcher(str, tp.subj), make_matcher(str, tp.pred),
                    make_matcher(str, tp.obj));
    }
};

template<class GraphStore, class S, class P, class O>
typename tp_matcher_maker<GraphStore, S, P, O>::type
mk_tp_matcher(const GraphStore& store, const triple_pattern<S, P, O>& tp) {
    return tp_matcher_maker<GraphStore, S, P, O>::make(store, tp);
}

}}} // namespace shacldator::qry::priv



#endif /* SRC_QRY_PRIV_TP_MATCHER_HPP_ */
