/*
 * triple_filter_sol_iteraetor.hpp
 *
 *  Created on: Apr 2, 2018
 *      Author: alexis
 */

#ifndef SRC_QRY_PRIV_TRIPLE_FILTER_SOL_ITERATOR_HPP_
#define SRC_QRY_PRIV_TRIPLE_FILTER_SOL_ITERATOR_HPP_

#include <boost/mpl/set.hpp>
#include <boost/mpl/for_each.hpp>
#include <boost/mpl/insert.hpp>
#include <boost/mpl/copy_if.hpp>
#include <boost/mpl/inserter.hpp>
#include <boost/iterator/iterator_facade.hpp>
#include <unordered_set>
#include "tp_matcher.hpp"
#include "../dsl.hpp"
#include "../sol.hpp"
#include "../../graph/str/triple.hpp"

namespace shacldator {namespace qry {

template<class Term, class PlaceholdersSet> struct sol;

namespace priv {

template<class Term, class S, class P, class O>
struct sol_iterator_transform {
    typedef Term term_t;
    typedef triple_pattern<S, P, O> tp_t;
    typedef typename tp_t::terms_t terms_t;
    typedef typename spo2sol<Term, S, P, O>::type type;

    static type run(const str::triple<term_t>& triple) {
        using namespace boost::mpl;
        type sol;
        assign(sol, (S*)nullptr, triple.subj());
        assign(sol, (P*)nullptr, triple.pred());
        assign(sol, (O*)nullptr, triple.obj());
        return sol;
    }

    template<class T> static void assign(type& sol, T* dummy,
                                         const term_t& term) {}
    template<int n> static void assign(type& sol, ph<n>* dummy,
                                       const term_t& term) {
        sol[ph<n>()] = term;
    }
};

template<class Term, class S, class P, class O>
struct sol_iterator_transformer {
    typedef sol_iterator_transform<Term, S, P, O> delegate;
    typedef str::triple<Term> argument_type;
    typedef typename delegate::type result_type;
    result_type operator()(const argument_type& a) const {
        return delegate::run(a);
    }
};

template<class TripleIterator, class TripleFilter,
         class S, class P, class O>
struct triple_filter_sol_iterator : public
boost::iterator_facade<triple_filter_sol_iterator<
                           TripleIterator, TripleFilter, S, P, O
                       >,
                       typename sol_iterator_transform<
                           typename TripleIterator::value_type::term_t,
                           S, P, O
                       >::type,
                       std::forward_iterator_tag> {
    typedef TripleIterator it_t;
    typedef TripleFilter filter_t;
    typedef typename it_t::value_type::term_t term_t;
    typedef typename spo2sol<term_t, S, P, O>::type value_type;

    triple_filter_sol_iterator() = default;
    triple_filter_sol_iterator(const it_t& it, const it_t& end,
                               const filter_t& filter) : it(it), end(end),
                                                         filter(filter) {
        walk_next();
    }
    triple_filter_sol_iterator(const triple_filter_sol_iterator& o)
                               : current(o.current), it(o.it), end(o.end),
                                 filter(o.filter) {}
    ~triple_filter_sol_iterator()  = default;
    triple_filter_sol_iterator& operator=(const triple_filter_sol_iterator&) =
            default;
private:
    friend class boost::iterator_core_access;

    value_type& dereference() const {return current;}
    bool equal(const triple_filter_sol_iterator& rhs) const {
        return it == rhs.it && end == rhs.end;
    }
    void increment() {
        assert(it != end);
        ++it;
        walk_next();
    }
    void walk_next() {
        while (it != end) {
            if (filter.get()(*it)) {
                current = sol_iterator_transform<term_t, S, P, O>::run(*it);
                break;
            }
            ++it;
        }
    }

    mutable value_type current;
    it_t it, end;
    pack<filter_t> filter;
};

template<class TripleIterator, class GraphStore, class S, class P, class O>
triple_filter_sol_iterator<
    TripleIterator,
    decltype(mk_tp_matcher(*(const GraphStore*)0,
                           *(const triple_pattern<S, P, O>*)0)),
    S, P, O
>
mk_triple_filter_sol_iterator(const TripleIterator& it,
                              const TripleIterator& end,
                              const GraphStore& str,
                              const triple_pattern<S, P, O>& tp) {
    auto matcher = mk_tp_matcher(str, tp);
    return triple_filter_sol_iterator
            <TripleIterator, decltype(matcher), S, P, O>(it, end, matcher);
}

}}} // namespace shacldator::qry::priv


#endif /* SRC_QRY_PRIV_TRIPLE_FILTER_SOL_ITERATOR_HPP_ */
