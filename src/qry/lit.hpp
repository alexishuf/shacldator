/*
 * literal.hpp
 *
 *  Created on: Apr 1, 2018
 *      Author: alexis
 */

#ifndef SRC_QRY_LIT_HPP_
#define SRC_QRY_LIT_HPP_

#include <string>
#include <memory>
#include <cstring>
#include <sstream>
#include "../graph/graph.hpp"
#include "../utils.hpp"

namespace shacldator {namespace qry {


class lit {
    lit(const std::shared_ptr<const char>& lex,
        const std::shared_ptr<const char>& dt,
        const std::shared_ptr<const char>& lang) : lex(lex), dt(dt), lang(lang) {}
public:
    static lit shallow(const char* lex, const char* dt, const char* lang=0) {
        using namespace std;
        sink sink;
        shared_ptr<const char> lex_p(lex, sink), dt_p(dt, sink), lang_p(lang, sink);
        return lit(lex_p, dt_p, lang_p);
    }
    static lit deep(const char* lex, const char* dt, const char* lang=0) {
        using namespace std;
        shared_ptr<char> lex_p, dt_p, lang_p;
        if (lex) {
            lex_p = shared_ptr<char>((char*)malloc(strlen(lex)), free);
            strcpy(lex_p.get(), lex);
        }
        if (dt) {
            dt_p = shared_ptr<char>((char*)malloc(strlen(dt)), free);
            strcpy(dt_p.get(), dt);
        }
        if (lang) {
            lang_p = shared_ptr<char>((char*)malloc(strlen(lang)), free);
            strcpy(lang_p.get(), lang);
        }
        return lit(lex_p, dt_p, lang_p);
    }
    static lit deep(const lit& rhs) {
        return deep(rhs.lex.get(), rhs.dt.get(), rhs.lang.get());
    }

    lit(const lit& rhs) = default;
    lit& operator=(const lit& rhs) = default;
    ~lit() = default;

    template<class G> literal<G> as_node(const G& g) const {
        return g.literal(lex.get(), dt.get(), lang.get());
    }

    std::string to_str() const {
        std::stringstream ss;
        ss << "\"" << lex.get() << "\"";
        if      (lang && *lang) ss << "@"   << lang.get();
        else if (dt   && *dt  ) ss << "^^<" << dt.get() << ">";
        return ss.str();
    }

    std::shared_ptr<const char> lex, dt, lang;
};

}} // namespace shacldator::qry

#endif /* SRC_QRY_LIT_HPP_ */
