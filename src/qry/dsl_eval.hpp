/*
 * dsl_eval.hpp
 *
 *  Created on: Apr 1, 2018
 *      Author: alexis
 */

#ifndef SRC_QRY_DSL_EVAL_HPP_
#define SRC_QRY_DSL_EVAL_HPP_

#include <functional>
#include <type_traits>
#include <boost/range.hpp>
#include "../graph/node.hpp"
#include "../graph/index_tags.hpp"
#include "../graph/str/triple.hpp"
#include "../graph/str/concepts/GraphStore.hpp"
#include "../viterator.hpp"
#include "dsl.hpp"
#include "sol.hpp"
#include "iterators/dsl_join_iterators.hpp"
#include "iterators/projection_iterator.hpp"
#include "iterators/unique_iterator.hpp"
#include "priv/triple_filter_sol_iterator.hpp"
#include <boost/mpl/size.hpp>
#include <boost/mpl/filter_view.hpp>
#include <boost/mpl/copy_if.hpp>
#include <boost/mpl/and.hpp>
#include <boost/mpl/find.hpp>
#include <boost/mpl/set.hpp>
#include <boost/mpl/sort.hpp>
#include <boost/mpl/vector.hpp>
#include <boost/mpl/order.hpp>
#include <boost/mpl/insert.hpp>
#include <boost/mpl/back_inserter.hpp>
#include <boost/mpl/joint_view.hpp>
#include <boost/mpl/inserter.hpp>
#include <boost/mpl/for_each.hpp>
#include <boost/iterator/transform_iterator.hpp>
#include <boost/static_assert.hpp>


namespace shacldator {namespace qry {

namespace priv {

template<class GS, class X> struct term_getter;
template<class GS> struct term_getter<GS, qry::lit> {
    typedef typename GS::term_t type;
    static type get(const GS& gs, const qry::lit& l) {
        return gs.literal(l.lex.get(), l.dt.get(), l.lang.get());
    }
};
template<class GS> struct term_getter<GS, const char*> {
    typedef typename GS::term_t type;
    static type get(const GS& gs, const char* uri) {
        return gs.resource(uri);
    }
};
template<class G>
struct term_getter<typename G::store_t, node<G>> {
    typedef typename G::store_t::term_t type;
    static type get(const typename G::store_t& gs, const node<G>& n) {
        return n.term();
    }
};
template<class GS, class X>       typename term_getter<GS, X>::type
get_term(const GS& gs, const X& x) {return term_getter<GS, X>::get(gs, x);}
template<class G> typename G::term_t
get_term(const typename G::store_t& gs, const node<G>& x) {
    return term_getter<typename G::store_t, node<G>>::get(gs, x);
}


template<bool s_ix, bool p_ix, bool o_ix> struct tp_dispatcher_ {
    template<class GS, class S, class P, class O> struct apply {
        typedef triple_pattern<S, P, O> triple_pattern_t;
        typedef typename GS::iterator t_it_t;
        typedef decltype(mk_triple_filter_sol_iterator(
                *(t_it_t*)0, *(t_it_t*)0, *((GS*)0), *((triple_pattern_t*)0)))
                it_t;
        typedef std::pair<it_t, it_t> type;
        BOOST_CONCEPT_ASSERT((str::GraphStore<
                typename std::remove_const<GS>::type>));

        static type str_eval(const GS& str, const triple_pattern_t& tp) {
            auto it = str.begin(), end = str.end();
            return std::make_pair(
                    mk_triple_filter_sol_iterator(it , end, str, tp),
                    mk_triple_filter_sol_iterator(end, end, str, tp));
        }
    };
};

template<bool p_ix, bool o_ix> struct tp_dispatcher_<true, p_ix, o_ix> {
    template<class GS, class S, class P, class O> struct apply {
        typedef triple_pattern<S, P, O> triple_pattern_t;
        typedef typename GS::po_iterator t_it_t;
        typedef decltype(mk_triple_filter_sol_iterator(t_it_t(), t_it_t(),
                *((GS*)0), *((triple_pattern_t*)0))) it_t;
        typedef std::pair<it_t, it_t> type;
        BOOST_CONCEPT_ASSERT((str::GraphStore<
                typename std::remove_const<GS>::type>));

        static type str_eval(const GS& str, const triple_pattern_t& tp) {
            auto term = get_term(str, tp.subj);
            t_it_t begin, end;
            if (term) {
                begin = str.begin_po(term);
                end = str.end_po(term);
            }
            return std::make_pair(
                    mk_triple_filter_sol_iterator(begin, end, str, tp),
                    mk_triple_filter_sol_iterator(  end, end, str, tp));
        }
    };
};

template<class GS, class S, class P, class O> struct tp_dispatcher
: public tp_dispatcher_<
      boost::mpl::and_<boost::mpl::not_<is_placeholder<S>>,
                       has_po_tag<GS>>::value,
      boost::mpl::and_<boost::mpl::not_<is_placeholder<P>>,
                       has_so_tag<GS>>::value,
      boost::mpl::and_<boost::mpl::not_<is_placeholder<O>>,
                       has_sp_tag<GS>>::value
>::template apply<GS, S, P, O> {};

template<class Graph, class Placeholders>
struct sol_upgrade_transformer : public std::unary_function<
        const typename qry::sol<typename Graph::term_t, Placeholders>::type&,
        typename qry::sol<node<Graph>, Placeholders>::type
> {
    sol_upgrade_transformer(const Graph* g) : g(g) {}
    typename qry::sol<node<Graph>, Placeholders>::type
    operator()(const typename qry::sol<typename Graph::term_t,
                                       Placeholders>::type& raw) const {
        return raw.template upgrade<Graph>(*g);
    }
    const Graph* g;
};

template<class Graph, class SolRange>
struct upgrade_transform_helper :
        public transform_range_h<SolRange,
                   sol_upgrade_transformer<Graph, typename
                       boost::range_value<SolRange>::type::placeholders_t> > {
};

//template<class Graph, class SolRange>
//struct upgrade_transform_helper {
//    typedef SolRange range_t;
//    typedef typename range_t::first_type sol_it_t;
//    typedef typename sol_it_t::value_type::placeholders_t placeholders_t;
//    typedef sol_upgrade_transformer<typename std::remove_const<Graph>::type,
//                                    placeholders_t> transformer_t;
//
//    typedef typename boost::transform_iterator<transformer_t, sol_it_t> it_t;
//    typedef std::pair<it_t, it_t> type;
//
//    static type run(const Graph* g, const range_t& pair) {
//        transformer_t t(g);
//        return std::make_pair(boost::make_transform_iterator(pair.first, t),
//                              boost::make_transform_iterator(pair.second, t));
//    }
//};



} // namespace shacldator::qry::priv



template<class Derived, class Graph> struct base_evaluator {
    typedef Derived derived_t;
    typedef Graph graph_t;
    typedef typename Graph::store_t g_str_t;
    BOOST_CONCEPT_ASSERT((str::GraphStore<g_str_t>));
    typedef typename g_str_t::term_t term_t;

    base_evaluator(const graph_t& graph) : m_graph(&graph) {}
    base_evaluator(const base_evaluator& rhs) = default;
    base_evaluator& operator=(const base_evaluator& rhs) = default;

    const graph_t& graph() const {return *m_graph;}

    template<class Left, class Right>
    typename combined_its<derived_t, term_t, Left, Right,
                          hash_join_iterator>::type
    str_eval(const and_<Left, Right>& op) {
        return combined_its<derived_t, term_t, Left, Right, hash_join_iterator>
        ::create(str_eval(op.left), str_eval(op.right));
    }

    template<class Left, class Right>
    typename combined_its<derived_t, term_t, Left, Right, cat_iterator>::type
    str_eval(const or_<Left, Right>& op) {
        return combined_its<derived_t, term_t, Left, Right, cat_iterator>
        ::create(str_eval(op.left), str_eval(op.right));
    }

    struct str_tp_evaluator {
        str_tp_evaluator(base_evaluator* e) : e(e) {}

        template<class S, class P, class O>
        typename priv::tp_dispatcher<g_str_t, S, P, O>::type
        apply(const triple_pattern<S, P, O>& tp) const {
            return priv::tp_dispatcher<g_str_t, S, P, O>
                       ::str_eval(e->m_graph->store(), tp);
        }

        base_evaluator* e;
    };

    template<class S, class P, class O>
    fwd_vrange<typename spo2sol<term_t, S, P, O>::type>
    str_eval(const triple_pattern<S, P, O>& tp) {
        typename derived_t::str_tp_evaluator e(static_cast<derived_t*>(this));
        return mk_vrange(e.template apply(tp));
    }

    template<class A, class Placeholders>
    decltype(mk_projection_range<Placeholders>(
                    ((base_evaluator*)0)->str_eval(*(A*)0)))
    str_eval(const projection<A, Placeholders>& op) {
        return mk_projection_range<Placeholders>(str_eval(op.arg));
    }

    template<class A>
    decltype(mk_hash_unique_range(std::declval<base_evaluator>()
                                      .str_eval(std::declval<A>())))
    str_eval(const unique<A>& op) {
        return mk_hash_unique_range(str_eval(op.arg));
    }

    template<class Node>
    typename transform_range_h<
            decltype(std::declval<base_evaluator>()
                        .str_eval(std::declval<Node>())),
            sol_unwrapper<term_t>>::type
    str_eval_term(const Node& op) {
        return transform_range(str_eval(op), sol_unwrapper<term_t>());
    }

    template<class Node> bool str_ask(const Node& op) {
        return !boost::empty(str_eval(op));
    }

    template<class Node> typename priv
    ::upgrade_transform_helper<graph_t,
            decltype(((base_evaluator*)0)->str_eval(*(Node*)0))>::type
    eval(const Node& op) {
        auto rng = str_eval(op);
        priv::sol_upgrade_transformer<graph_t, typename boost::range_value<
                       decltype(rng)>::type::placeholders_t> t(this->m_graph);
        return transform_range(rng, t);
    }

    template<class Node, class UnryF>
    typename transform_range_h<decltype(std::declval<base_evaluator>()
            .eval(std::declval<Node>())), UnryF>::type
    eval(const Node& op, const UnryF& t) {
        return transform_range(eval(op), t);
    }

    template<class Node>
    bool ask(const Node& op) {
        return !boost::empty(str_eval(op));
    }

    template<class Node>
    decltype(std::declval<base_evaluator>()
            .eval(std::declval<Node>(), sol_unwrapper<node<graph_t>>()))
    eval_node(const Node& op) {
        return eval(op, sol_unwrapper<node<graph_t>>());
    }
    template<class Node>
    decltype(std::declval<base_evaluator>()
                .eval(std::declval<Node>(),
                      sol_unwrapper<node<graph_t>, resource<graph_t>>()))
    eval_resource(const Node& op) {
        return eval(op, sol_unwrapper<node<graph_t>, resource<graph_t>>());
    }

    template<class Arg> bool str_eval(const not_<Arg>& op) {
        auto pair = str_eval(op.arg);
        return pair.first == pair.second;
    }

    template<class Arg> bool eval(const not_<Arg>& op) {
        return str_eval(op);
    }

    /** Same as eval(), but return a runtime-friendly type. */
    template<class Node>
    decltype(mk_vrange(std::declval<base_evaluator>()
                           .eval(std::declval<Node>())))
    deval(const Node& op) {
        return mk_vrange(eval(op));
    }
    template<class Node>
    decltype(mk_vrange(std::declval<base_evaluator>()
                           .eval_node(std::declval<Node>())))
    deval_node(const Node& op) {
        return mk_vrange(eval_node(op));
    }
    template<class Node>
    decltype(mk_vrange(std::declval<base_evaluator>()
                           .eval_resource(std::declval<Node>())))
    deval_resource(const Node& op) {
        return mk_vrange(eval_resource(op));
    }
    /** deval() applying a transformation on results*/
    template<class Node, class UnryF>
    decltype(mk_vrange(std::declval<base_evaluator>()
                       .eval(std::declval<Node>()), std::declval<UnryF>()))
    deval(const Node& op, UnryF transf) {
        return mk_vrange(eval(op), transf);
    }

protected:
    const graph_t* m_graph;
};

template<class Graph> struct def_evaluator
: public base_evaluator<def_evaluator<Graph>, Graph> {
    typedef base_evaluator<def_evaluator<Graph>, Graph> base_t;
    def_evaluator(const Graph& g) : base_t(g) {}
    def_evaluator(const def_evaluator& rhs) = default;
    def_evaluator& operator=(const def_evaluator& rhs) = default;

    struct fac {
        typedef def_evaluator result_type;
        typedef const Graph& argument_type;
        result_type operator()(const argument_type& g) const {
            return result_type(g);
        }
    };
};


template<class Graph> def_evaluator<Graph>
mk_evaluator(const Graph& g) {
    return def_evaluator<Graph>(g);
}

}} //namespace shacldator::qry




#endif /* SRC_QRY_DSL_EVAL_HPP_ */
