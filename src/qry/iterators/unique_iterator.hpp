/*
 * unique_iterators.hpp
 *
 *  Created on: Apr 23, 2018
 *      Author: alexis
 */

#ifndef SRC_QRY_ITERATORS_UNIQUE_ITERATOR_HPP_
#define SRC_QRY_ITERATORS_UNIQUE_ITERATOR_HPP_

#include <type_traits>
#include <iterator>
#include <cassert>
#include <memory>
#include <unordered_set>
#include <set>
#include <boost/iterator/iterator_facade.hpp>
#include "../../utils.hpp"

namespace shacldator {namespace qry {

template<class It, class Set,
         class Value = typename std::iterator_traits<It>::value_type>
struct unique_iterator : public
boost::iterator_facade<unique_iterator<It, Set, Value>,
                       Value, std::forward_iterator_tag, const Value&> {
    typedef boost::iterator_facade<unique_iterator<It, Set, Value>, Value,
                                   std::forward_iterator_tag, const Value&>
            base_t;
    typedef Set set_t;

    unique_iterator() = default;
    unique_iterator(const It& it, const It& end) : it(it), end(end),
                                                   set(new set_t()) {}
    ~unique_iterator() = default;
    unique_iterator(const unique_iterator&) = default;
    unique_iterator(unique_iterator&&) = default;
    unique_iterator& operator=(const unique_iterator&) = default;
    unique_iterator& operator=(unique_iterator&&) = default;

private:
    friend class boost::iterator_core_access;

    void increment() {
        assert(it != end);
        while (++it != end) {
            typename base_t::reference value = dereference();
        //            Value value = *it;
            if (set.use_count() > 1)
                set = std::shared_ptr<set_t>(new set_t(*set));
            if (set->insert(value).second) break; //value is new
        }
    }
    bool equal(const unique_iterator& o) const {
        assert(!o.set || !set || end == o.end); //its MUST share the same range
        if (!o.set && !set) return true;
        if (!o.set) return it == end;
        if (!set) return o.it == o.end;
        else return it == o.it;
    }
    typename base_t::reference dereference() const {
        assert(set && it != end);
        return deref_h(*it);
    }

    def_constr_in_it<It> it, end;
    deref_helper_for_it<Value, It> deref_h;
    std::shared_ptr<set_t> set;
};

template<class It, class Value=typename std::iterator_traits<It>::value_type>
using hash_unique_iterator =
        unique_iterator<It, std::unordered_set<Value>, Value>;
template<class It, class Value=typename std::iterator_traits<It>::value_type>
using set_unique_iterator =
        unique_iterator<It, std::set<Value>, Value>;

template<class It> std::pair<hash_unique_iterator<It>, hash_unique_iterator<It>>
mk_hash_unique_range(std::pair<It, It> p) {
    return std::make_pair(hash_unique_iterator<It>(p.first, p.second),
                          hash_unique_iterator<It>());
}

template<class It> std::pair<set_unique_iterator<It>, set_unique_iterator<It>>
mk_set_unique_range(std::pair<It, It> p) {
    return std::make_pair(set_unique_iterator<It>(p.first, p.second),
                          set_unique_iterator<It>());
}


}}  // namespace shacldator::qry



#endif /* SRC_QRY_ITERATORS_UNIQUE_ITERATOR_HPP_ */
