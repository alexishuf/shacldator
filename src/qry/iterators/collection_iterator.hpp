/*
 * collection_iterator.hpp
 *
 *  Created on: Apr 21, 2018
 *      Author: alexis
 */

#ifndef QRY_COLLECTION_ITERATOR_HPP_
#define QRY_COLLECTION_ITERATOR_HPP_

#include <memory>
#include <type_traits>
#include <array>
#include <boost/iterator/iterator_facade.hpp>
#include "../../viterator.hpp"

namespace shacldator {namespace qry {

template<class X> struct collection_iterator : public
boost::iterator_facade<collection_iterator<X>,
                       typename X::value_type,
                       typename std::forward_iterator_tag> {
public:
    collection_iterator() = default; //global at end

    //3 ctors at beginning of collection
    explicit collection_iterator(const std::shared_ptr<X>& p) : p(p),
                                                                i(p->begin()){}
    explicit collection_iterator(X&& x) : p(new X(std::forward<X>(x))),
                                          i(p->begin()) {}
    explicit collection_iterator(const X& x) : p(new X(x)), i(p->begin()) {}

    //no collection, only iterator
    explicit collection_iterator(const typename X::iterator& it) : i(it) {}

    //copy and assignment
    collection_iterator(const collection_iterator&) = default;
    collection_iterator(collection_iterator&& o) = default;
    collection_iterator& operator=(const collection_iterator&) = default;
    collection_iterator& operator=(collection_iterator&&) = default;

private:
    friend class boost::iterator_core_access;

    void increment() {++i;}
    bool equal(const collection_iterator& o) const {
        //these initial ifs are necessary if it i is an input iterator
        if (!p) return !o.p || o.i == o.p->end();
        if (!o.p) return !p || i == p->end();
        return i == o.i;
    }
    typename std::iterator_traits<typename X::iterator>::reference
    dereference() const {return *i;}

    std::shared_ptr<X> p;
    typename X::iterator i;
};
template<class T> using singleton_iterator_t
        = collection_iterator<std::array<T, 1>>;
template<class T>
singleton_iterator_t<T>
mk_singleton_iterator(const T& val) {
    return singleton_iterator_t<T>({val});
}
template<class T> using singleton_range_t =
        std::pair<collection_iterator<std::array<T, 1>>,
                  collection_iterator<std::array<T, 1>>>;
template<class T>
singleton_range_t<T>
mk_singleton_range(const T& val) {
    typedef collection_iterator<std::array<T, 1>> it_t;
    std::shared_ptr<std::array<T, 1>> p(new std::array<T, 1>({val}));
    return singleton_range_t<T>(it_t(p), it_t());
}

template<class T> std::pair<fwd_viterator<T>, fwd_viterator<T>>
mk_vsingleton_range(const T& val) {
    return mk_vrange(mk_singleton_range(val));
}

template<class X> collection_iterator<X>
mk_collection_iterator(const X& x) {return collection_iterator<X>(x);}
template<class X> collection_iterator<X>
mk_collection_iterator(X&& x) {return collection_iterator<X>(std::move(x));}

template<class X> std::pair<collection_iterator<X>, collection_iterator<X>>
mk_collection_range(const X& x) {
    return std::make_pair(collection_iterator<X>(x),
                          collection_iterator<X>());
}
template<class X> std::pair<collection_iterator<X>, collection_iterator<X>>
mk_collection_range(X&& x) {
    return std::make_pair(collection_iterator<X>(std::move(x)),
                          collection_iterator<X>());
}


}}  // namespace shacldator::qry


#endif /* QRY_COLLECTION_ITERATOR_HPP_ */
