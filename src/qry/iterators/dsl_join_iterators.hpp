/*
 * dsl_join_iterators.hpp
 *
 *  Created on: Apr 1, 2018
 *      Author: alexis
 */

#ifndef SRC_QRY_DSL_JOIN_ITERATORS_HPP_
#define SRC_QRY_DSL_JOIN_ITERATORS_HPP_

#include <type_traits>
#include <vector>
#include <deque>
#include <list>
#include <unordered_map>
#include <boost/concept/assert.hpp>
#include <boost/mpl/set.hpp>
#include <boost/mpl/assert.hpp>
#include <boost/mpl/comparison.hpp>
#include <boost/mpl/integral_c.hpp>
#include <boost/mpl/void.hpp>
#include <boost/mpl/if.hpp>
#include <boost/mpl/copy.hpp>
#include <boost/mpl/quote.hpp>
#include <boost/mpl/not.hpp>
#include <boost/mpl/logical.hpp>
#include <boost/mpl/empty.hpp>
#include <boost/mpl/copy_if.hpp>
#include <boost/mpl/contains.hpp>
#include <boost/mpl/filter_view.hpp>
#include <boost/mpl/inserter.hpp>
#include <boost/iterator/iterator_facade.hpp>
#include <boost/range/algorithm/find_if.hpp>
#include "../../graph/str/concepts/Term.hpp"


namespace shacldator {namespace qry {

template<class Term, class PlaceholdersSet> struct sol;

namespace priv {

template<class LeftIt, class RightIt> struct merge_phs {
    typedef typename boost::mpl::copy_if<
            typename RightIt::value_type::placeholders_t,
            boost::mpl::not_<
                boost::mpl::contains<
                    typename LeftIt::value_type::placeholders_t,
                    boost::mpl::placeholders::_1
                >
            >,
            boost::mpl::back_inserter<
                typename LeftIt::value_type::placeholders_t
            >
    >::type type;
};

template<class Derived, class Term, class LeftIt, class RightIt>
struct bin_iterator : public boost::iterator_facade<
        Derived,
        typename qry::sol<Term, typename merge_phs<LeftIt, RightIt>::type>::type,
        std::forward_iterator_tag> {
    typedef LeftIt left_it_t;
    typedef RightIt right_it_t;
    typedef typename qry:: sol<Term, typename merge_phs<LeftIt, RightIt>::type>::type
            value_type;
    typedef std::pair<left_it_t, left_it_t> left_range_t;
    typedef std::pair<right_it_t, right_it_t> right_range_t;
    typedef typename merge_phs<LeftIt, RightIt>::type placeholders_t;
    BOOST_CONCEPT_ASSERT((str::Term<Term>));

protected:
    bin_iterator(const left_it_t& left_end, const right_it_t& right_end) :
            left(left_end, left_end), right(right_end, right_end), at_end(true){
    }
    bin_iterator(const left_range_t& left,
                const right_range_t& right) : left(left), right(right),
                                              at_end(false) {}
    bin_iterator(const bin_iterator&) = default;
    bin_iterator& operator=(const bin_iterator&) = default;

    bool equal(const bin_iterator& rhs) const {
        if (at_end != rhs.at_end) return false;
        if (left != rhs.left) return false;
        if (right != rhs.right) return false;
        return true;
//        return at_end == rhs.at_end &&  left == rhs.left
//                                    && right == rhs.right;
    }
    value_type& dereference() const {
        assert(!at_end);
        return current;
    }

    mutable value_type current;
    mutable bool at_end;
    std::pair<left_it_t, left_it_t> left;
    std::pair<right_it_t, right_it_t> right;
};

} //namespace priv

template<class T, class LIt, class RIt> struct hash_join_iterator : public
priv::bin_iterator<hash_join_iterator<T, LIt, RIt>, T, LIt, RIt> {
    typedef priv::bin_iterator<hash_join_iterator<T, LIt, RIt>, T, LIt, RIt>
            base_t;

    hash_join_iterator(const LIt& left_end, const RIt& right_end) :
            base_t(left_end, right_end){}
    hash_join_iterator(const std::pair<LIt, LIt>& left,
                       const std::pair<RIt, RIt>& right) :
            base_t(left, right) {increment();}
    hash_join_iterator(const hash_join_iterator&) = default;
    hash_join_iterator& operator=(const hash_join_iterator&) = default;

private:
    friend class boost::iterator_core_access;
    typedef typename qry::sol<T,
                typename boost::mpl::copy_if<
                    typename RIt::value_type::placeholders_t,
                    boost::mpl::contains<
                        typename LIt::value_type::placeholders_t,
                        boost::mpl::placeholders::_1
                    >,
                    boost::mpl::back_inserter<boost::mpl::vector0<> >
                >::type
            >::type inters_sol_t;
    typedef std::vector<typename LIt::value_type> left_sols_t;
    typedef std::vector<typename RIt::value_type> right_sols_t;
    struct sides_solutions {
        typedef std::size_t left_pos_t;
        typedef std::size_t right_pos_t;

        void add_left(const typename LIt::value_type& sol) {
            left.push_back(sol);
        }

        void add_right(const typename RIt::value_type& sol) {
            right.push_back(sol);
            if (lpos)
                extras.push_back(std::make_pair(right.size()-1, lpos));
        }

        typename base_t::value_type next() {
            if (lpos != left.size()) {
                auto ret = base_t::value_type::merge(left[lpos], right[rpos]);
                rpos = (rpos + 1) % right.size();
                ++lpos;
                return ret;
            }
            assert(!extras.empty());
            auto& ex = extras.front();
            auto ret = base_t::value_type
                    ::merge(left.at(--ex.second), right.at(ex.first));
            if (ex.second == 0)
                extras.pop_front();
            return ret;
        }

        bool has_next() const {
            return (lpos != left.size() && rpos != right.size())
                   || !extras.empty();
        }

        left_sols_t left;
        right_sols_t right;
        left_pos_t lpos;
        right_pos_t rpos;
        std::list<std::pair<right_pos_t, left_pos_t>> extras;
    };

    bool exhausted_ranges() const {
        return this->left.first == this->left.second
            && this->right.first == this->right.second;
    }

    void increment_left() {
        if (this->left.first != this->left.second) {
            auto sol = *(this->left.first++);
            auto int_ = inters_sol_t::from_super(sol);
            auto it = map.find(int_);
            if (it == map.end())
                it = map.insert(std::make_pair(int_,sides_solutions())).first;
            bool was_empty = !it->second.has_next();
            it->second.add_left(sol);
            if (was_empty && it->second.has_next()) nonempty.push_back(int_);
        }
    }

    void increment_right() {
        if (this->right.first != this->right.second) {
            auto sol = *(this->right.first++);
            auto int_ = inters_sol_t::from_super(sol);
            auto it = map.find(int_);
            if (it == map.end())
                it = map.insert(std::make_pair(int_,sides_solutions())).first;
            bool was_empty = !it->second.has_next();
            it->second.add_right(sol);
            if (was_empty && it->second.has_next()) nonempty.push_back(int_);
        }
    }

    void increment() {
        this->at_end = true;
        int side = 0;
        while (nonempty.empty() && !exhausted_ranges()) {
            if (side++ & 0x1) {increment_right();} else {increment_left();}
        }
        if (!nonempty.empty()) {
            auto& ss = map.at(nonempty.front());
            assert(ss.has_next());
            this->current = ss.next();
            this->at_end = false;
            if (!ss.has_next())
                nonempty.pop_front();
            return; // at_end == false
        } // else: at_end == true
    }

    std::unordered_map<inters_sol_t, sides_solutions> map;
    std::list<inters_sol_t> nonempty;
};
template<class LIt, class RIt>
std::pair<
        hash_join_iterator<typename LIt::value_type::term_t, LIt, RIt>,
        hash_join_iterator<typename LIt::value_type::term_t, LIt, RIt> >
mk_hash_join_range(const std::pair<LIt, LIt>& left_rng,
            const std::pair<RIt, RIt>& right_rng) {
    typedef hash_join_iterator<typename LIt::value_type::term_t, LIt, RIt> it_t;
    return std::make_pair(it_t(left_rng, right_rng),
                          it_t(left_rng.second, right_rng.second));
}

template<class T, class LIt, class RIt> struct cat_iterator :
        public priv::bin_iterator<cat_iterator<T, LIt, RIt>, T, LIt, RIt> {
    typedef priv::bin_iterator<cat_iterator<T, LIt, RIt>, T, LIt, RIt> base_t;

    cat_iterator(const LIt& left_end, const RIt& right_end) :
            base_t(left_end, right_end) {}
    cat_iterator(const std::pair<LIt, LIt>& left,
                 const std::pair<RIt, RIt>& right) : base_t(left, right) {
        increment();
    }
    cat_iterator(const cat_iterator&) = default;
    cat_iterator& operator=(const cat_iterator&) = default;

private:
    friend class boost::iterator_core_access;

    void increment() {
        this->at_end = true;
        if (this->left.first != this->left.second) {
            this->current = *this->left.first;
            this->at_end = false;
            ++this->left.first;
            return;
        }
        if (this->right.first != this->right.second) {
            this->current = *this->right.first;
            this->at_end = false;
            ++this->right.first;
            return;
        }
    }
};
template<class LIt, class RIt>
std::pair<
        cat_iterator<typename LIt::value_type::term_t, LIt, RIt>,
        cat_iterator<typename LIt::value_type::term_t, LIt, RIt> >
mk_cat_range(const std::pair<LIt, LIt>& left_rng,
             const std::pair<RIt, RIt>& right_rng) {
    typedef cat_iterator<typename LIt::value_type::term_t, LIt, RIt> it_t;
    return std::make_pair(it_t(left_rng, right_rng),
                          it_t(left_rng.second, right_rng.second));
}

template<class It> struct ncat_iterator : public
boost::iterator_facade<ncat_iterator<It>,
                       typename std::iterator_traits<It>::value_type,
                       typename std::iterator_traits<It>::iterator_category,
                       typename std::iterator_traits<It>::reference> {
    typedef boost::iterator_facade<ncat_iterator<It>,
                typename std::iterator_traits<It>::value_type,
                typename std::iterator_traits<It>::iterator_category,
                typename std::iterator_traits<It>::reference
            > base_t;
    typedef std::pair<It, It> pair_t;

    explicit ncat_iterator() : pos(0) {}
    explicit ncat_iterator(std::vector<pair_t>&& ranges)
                         : pos(0),
                           ranges(std::forward<std::vector<pair_t>>(ranges)) {
        walk_next();
    }
    explicit ncat_iterator(const std::vector<pair_t>& ranges)
                         : pos(0), ranges(ranges) {walk_next();}
    ncat_iterator(const ncat_iterator& o) : pos(o.pos), ranges(o.ranges) {}
    ncat_iterator(ncat_iterator&& o)
                                     : pos(o.pos),
                                       ranges(std::move(o.ranges)) {}
    ncat_iterator& operator=(const ncat_iterator& rhs) {
        if (this != &rhs) {
            pos = rhs.pos;
            ranges = rhs.ranges;
        }
        return *this;
    }
    ncat_iterator& operator=(ncat_iterator&& rhs) {
        if (this != &rhs) {
            pos = rhs.pos;
            ranges.swap(rhs.ranges);
        }
        return *this;
    }

    struct builder {
        builder() : invalid(false) {}
        void reserve(std::size_t size) {ranges.reserve(size);}
        void add(const pair_t& p) {ranges.push_back(p);}
        ncat_iterator build() {
            assert(!invalid);
            invalid = true;
            return ncat_iterator(std::move(ranges));
        }
        std::pair<ncat_iterator, ncat_iterator> build_range() {
            return std::make_pair(build(), ncat_iterator());
        }

        bool invalid;
        std::vector<pair_t> ranges;
    };

private:
    friend class boost::iterator_core_access;

    void walk_next() {
        auto i = ranges.begin() + pos;
        for (; pos < ranges.size() && i->first == i->second; ++i) ++pos;
    }

    void increment() {
        assert(pos != ranges.size());
        assert(ranges[pos].first != ranges[pos].second);

        ++ranges[pos].first;
        walk_next();
    }
    bool equal(const ncat_iterator& rhs) const {
        if (rhs.ranges.empty()) return pos == ranges.size();
        else if (ranges.empty()) return rhs.pos == rhs.ranges.size();
        else return pos == rhs.pos && ranges == rhs.ranges;
    }
    typename base_t::reference dereference() const {
        return *ranges[pos].first;
    }

    std::size_t pos;
    std::vector<pair_t> ranges;
};
template<class It> ncat_iterator<It>
mk_ncat_iterator(std::initializer_list<std::pair<It, It>> list) {
    typename ncat_iterator<It>::builder b;
    b.reserve(list.size());
    for (auto it = list.begin(), e = list.end(); it != e; ++it)
        b.add(*it);
    return b.build();
}
template<class It> std::pair<ncat_iterator<It>, ncat_iterator<It> >
mk_ncat_range(std::initializer_list<std::pair<It, It>> list) {
    ncat_iterator<It> e;
    return std::make_pair(mk_ncat_iterator(list), e);
}


template<class Derived, class Term, class LeftNode, class RightNode,
             template<class T, class LIt, class RIt> class Iterator>
struct combined_its {
    typedef typename decltype(((Derived*)0)->str_eval(*(LeftNode*)0))
            ::first_type left_it_t;
    typedef typename decltype(((Derived*)0)->str_eval(*(RightNode*)0))
            ::first_type right_it_t;
    typedef Iterator<Term, left_it_t, right_it_t>  it_t;
    typedef std::pair<it_t, it_t> type;

    static type create(const std::pair<left_it_t, left_it_t>& left,
                       const std::pair<right_it_t, right_it_t>& right) {
        auto ret = std::make_pair(it_t(left,        right       ),
                                  it_t(left.second, right.second));
        return ret;
    }
};

}}  // namespace shacldator::qry




#endif /* SRC_QRY_DSL_JOIN_ITERATORS_HPP_ */
