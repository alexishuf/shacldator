/*
 * projection_iterator.hpp
 *
 *  Created on: Apr 21, 2018
 *      Author: alexis
 */

#ifndef SRC_QRY_ITERATORS_PROJECTION_ITERATOR_HPP_
#define SRC_QRY_ITERATORS_PROJECTION_ITERATOR_HPP_

#include <type_traits>
#include <tuple>
#include <boost/iterator/transform_iterator.hpp>
#include <boost/mpl/vector.hpp>
#include "../sol.hpp"

namespace shacldator {namespace qry {

template<class It, class Placeholders> struct projection_iterator {
    typedef typename std::iterator_traits<It>::value_type inner_type;
    typedef typename sol<typename inner_type::term_t, Placeholders>::type
            value_type;
    typedef std::iterator_traits<It> it_traits;
    typedef typename std::forward_iterator_tag iterator_category;
    typedef const value_type& reference;
    typedef const value_type* pointer;
    typedef std::ptrdiff_t difference_type;

    projection_iterator() = default;
    projection_iterator(const It& it) : it(it) {};
    ~projection_iterator() = default;
    projection_iterator(const projection_iterator&) = default;
    projection_iterator& operator=(const projection_iterator&) = default;

    reference operator*() const {
        typedef typename inner_type::placeholders_t phs_t;
        return current = value_type::template from_super<phs_t>(*it);
    }
    pointer   operator->() const {return &operator*();}
    projection_iterator operator++() {++it; return *this;}
    projection_iterator operator++(int) {auto old = *this; ++it; return old;}
    bool operator==(const projection_iterator& o) const {return it == o.it;}
    bool operator!=(const projection_iterator& o) const {return it != o.it;}

private:
    mutable value_type current;
    It it;
};

template<class Placeholders, class It>
projection_iterator<It, Placeholders>
mk_projection_iterator(const It& it) {
    return projection_iterator<It, Placeholders>(it);
}
template<class It, class... Placeholders>
projection_iterator<It, boost::mpl::vector<Placeholders...> >
mk_projection_iterator(const It& it, Placeholders... phs) {
    return mk_projection_iterator<boost::mpl::vector<Placeholders...> >(it);
}

template<class Placeholders, class It>
std::pair<projection_iterator<It, Placeholders>,
          projection_iterator<It, Placeholders>>
mk_projection_range(const std::pair<It, It>& rng) {
    typedef projection_iterator<It, Placeholders> it_t;
    return std::make_pair(it_t(rng.first), it_t(rng.second));
}
template<class It, class... Placeholders>
std::pair<projection_iterator<It, boost::mpl::vector<Placeholders...> >,
          projection_iterator<It, boost::mpl::vector<Placeholders...> >>
mk_projection_range(const std::pair<It, It>& rng, Placeholders... phs) {
    return mk_projection_range<boost::mpl::vector<Placeholders...>>(rng);
}

}}  // namespace shacldator::qry




#endif /* SRC_QRY_ITERATORS_PROJECTION_ITERATOR_HPP_ */
