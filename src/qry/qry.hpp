/*
 * qry.hpp
 *
 *  Created on: Apr 20, 2018
 *      Author: alexis
 */

#ifndef SRC_QRY_QRY_HPP_
#define SRC_QRY_QRY_HPP_

#include "priv/dsl_join_iterators.hpp"
#include "priv/triple_filter_sol_iterator.hpp"
#include "dsl.hpp"
#include "dsl_eval.hpp"
#include "sol_iterator.hpp"
#include "sol.hpp"
#include "lit.hpp"


#endif /* SRC_QRY_QRY_HPP_ */
