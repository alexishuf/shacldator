/*
 * spo.h
 *
 *  Created on: Apr 22, 2018
 *      Author: alexis
 */

#ifndef SRC_QRY_SPO_HPP_
#define SRC_QRY_SPO_HPP_

#include <iterator>
#include <type_traits>
#include <boost/iterator/transform_iterator.hpp>
#include "dsl.hpp"
#include "sol.hpp"
#include "dsl_eval.hpp"
#include "../utils.hpp"
#include "../graph/node.hpp"
#include "../graph/graph.hpp"
#include "../viterator.hpp"
#include "../utils.hpp"
#include "priv/tp_matcher.hpp"

namespace shacldator {namespace qry {

namespace detail {

template<class Result, class Graph, class S, class P, class O>
struct term_container {
    typedef typename std::add_const<typename Graph::store_t>::type g_str_t;
    typedef triple_pattern<S, P, O> tp_t;

    term_container(const Graph& g, const tp_t& tp) : g(&g), tp(tp) {}
    ~term_container() = default;
    term_container(const term_container&) = default;
    term_container& operator=(const term_container&) = default;

    bool contains(const node<Graph>& n) const {
        if (!n) return false;
        return g->contains(select_first<is_node_f>(tp.subj, n).as_resource(),
                           select_first<is_node_f>(tp.pred, n).as_resource(),
                           select_first<is_node_f>(tp.obj , n));
    }
    bool contains(const char* uri) const {return contains(g->resource(uri));}

private:
    typedef decltype(mk_evaluator(*(Graph*)0).eval(*(tp_t*)0)) e_range_t;
    typedef typename e_range_t::first_type e_it_t;
    typedef sol_unwrapper<
            typename std::iterator_traits<e_it_t>::value_type::term_t,
            Result> unwrapper_t;
public:
    typedef transform_fwd_it<e_it_t, unwrapper_t> iterator;
    typedef iterator const_iterator;
    static_assert(std::is_same<
                      typename std::iterator_traits<iterator>::value_type,
                      Result>::value,
                  "bad iterator value_type");

    iterator begin() const {
        return range().first;
    }
    iterator end() const {
        return range().second;
    }
    std::pair<iterator, iterator> range() const {
        using namespace std;
        if (!m_rng) {
            auto p = mk_evaluator(*g).eval(tp);
            unwrapper_t uw;
            m_rng.set(make_pair(iterator(p.first, uw), iterator(p.second, uw)));
        }
        return m_rng.get();
    }

    template<class UnryF>
    void for_each(UnryF f) const {
        for (auto r = range(); r.first != r.second; ++r.first)
            f(*r.first);
    }

    template<class UnryF>
    bool with_first(UnryF f) const {
        auto r = range();
        bool ran = !boost::empty(r);
        if (ran) f(*boost::begin(r));
        return ran;
    }

    Result operator()() const {
        auto r = range();
        return boost::empty(r) ? Result() : *boost::begin(r);
    }
private:
    const Graph* g;
    tp_t tp;
    mutable pack<std::pair<iterator, iterator>, false> m_rng;
};

}  // namespace detail


template<class G>
detail::term_container<resource<G>, G, ph<1>, resource<G>, node<G>>
s(const resource<G>& p, const node<G>& o) {
    return detail::term_container<resource<G>, G, ph<1>, resource<G>, node<G>>(
            p.graph(), _t(_1, p, o));
}
template<class G>
detail::term_container<resource<G>, G, ph<1>, resource<G>, node<G>>
s(const resource<G>& p, const char* o) {
    node<G> n = p.graph().resource(o);
    return detail::term_container<resource<G>, G, ph<1>, resource<G>, node<G>>(
            p.graph(), _t(_1, p, n));
}
template<class G>
detail::term_container<resource<G>, G, ph<1>, resource<G>, node<G>>
s(const resource<G>& p, const qry::lit& o) {
    node<G> n = o.as_node(p.graph());
    return detail::term_container<resource<G>, G, ph<1>, resource<G>, node<G>>(
            p.graph(), _t(_1, p, n));
}
template<class G>
detail::term_container<resource<G>, G, ph<1>, resource<G>, node<G>>
s(const char* p, const node<G>& o) {
    resource<G> n = o.graph().resource(p);
    return detail::term_container<resource<G>, G, ph<1>, resource<G>, node<G>>(
            o.graph(), _t(_1, n, o));
}

template<class G>
detail::term_container<resource<G>, G, resource<G>, ph<1>, node<G>>
p(const resource<G>& s, const node<G>& o) {
    return detail::term_container<resource<G>, G, resource<G>, ph<1>, node<G>>(
            s.graph(), _t(s, _1, o));
}
template<class G>
detail::term_container<resource<G>, G, resource<G>, ph<1>, node<G>>
p(const resource<G>& s, const char* o) {
    node<G> n = s.graph().resource(o);
    return detail::term_container<resource<G>, G, resource<G>, ph<1>, node<G>>(
            s.graph(), _t(s, _1, n));
}
template<class G>
detail::term_container<resource<G>, G, resource<G>, ph<1>, node<G>>
p(const resource<G>& s, const qry::lit& o) {
    node<G> n = o.as_node(s.graph());
    return detail::term_container<resource<G>, G, resource<G>, ph<1>, node<G>>(
            s.graph(), _t(s, _1, n));
}
template<class G>
detail::term_container<resource<G>, G, resource<G>, ph<1>, node<G>>
p(const char* s, const node<G>& o) {
    node<G> n = o.graph().resource(s);
    return detail::term_container<resource<G>, G, resource<G>, ph<1>, node<G>>(
            o.graph(), _t(n, _1, o));
}

template<class G>
detail::term_container<node<G>, G, resource<G>, resource<G>, ph<1>>
o(const resource<G>& s, const resource<G>& p) {
    return detail::term_container<node<G>, G, resource<G>, resource<G>, ph<1>>(
            s.graph(), _t(s, p, _1));
}
template<class G>
detail::term_container<node<G>, G, resource<G>, resource<G>, ph<1>>
o(const resource<G>& s, const char* p) {
    resource<G> n = s.graph().resource(p);
    return detail::term_container<node<G>, G, resource<G>, resource<G>, ph<1>>(
            s.graph(), _t(s, n, _1));
}
template<class G>
detail::term_container<node<G>, G, resource<G>, resource<G>, ph<1>>
o(const char* s, const resource<G>& p) {
    resource<G> n = p.graph().resource(s);
    return detail::term_container<node<G>, G, resource<G>, resource<G>, ph<1>>(
            p.graph(), _t(n, p, _1));
}


}}  // namespace shacldator::qry


#endif /* SRC_QRY_SPO_HPP_ */
