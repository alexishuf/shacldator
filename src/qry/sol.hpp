/*
 * sol.hpp
 *
 *  Created on: Apr 4, 2018
 *      Author: alexis
 */

#ifndef SRC_QRY_SOL_HPP_
#define SRC_QRY_SOL_HPP_

#include <type_traits>
#include <ostream>
#include <sstream>
#include <boost/static_assert.hpp>
#include <boost/mpl/size.hpp>
#include <boost/mpl/filter_view.hpp>
#include <boost/mpl/contains.hpp>
#include <boost/mpl/placeholders.hpp>
#include <boost/mpl/set.hpp>
#include <boost/mpl/vector.hpp>
#include <boost/mpl/not.hpp>
#include <boost/mpl/remove_if.hpp>
#include <boost/mpl/logical.hpp>
#include <boost/mpl/joint_view.hpp>
#include <boost/mpl/find.hpp>
#include <boost/mpl/sort.hpp>
#include <boost/mpl/less.hpp>
#include <boost/mpl/for_each.hpp>
#include "dsl.hpp"
#include "../graph/node.hpp"


namespace shacldator {namespace qry{

template<class Term>
struct abstract_sol {
    typedef Term term_t;
};

namespace priv {

template<class Term, class Placeholders> struct sol_;

template<class Term, class PlaceholdersLhs, class PlaceholdersRhs>
struct sol_assigner {
    sol_assigner(sol_<Term, PlaceholdersLhs>& dst,
                 const sol_<Term, PlaceholdersRhs>& src) : dst(dst), src(src){}
    template<class T> void operator()(const T& ph) const {
        dst[ph] = src[ph];
    }
private:
    sol_<Term, PlaceholdersLhs>& dst;
    const sol_<Term, PlaceholdersRhs>& src;
};
template<class Term, class PlaceholdersLhs, class PlaceholdersRhs>
sol_assigner<Term, PlaceholdersLhs, PlaceholdersRhs>
mk_sol_assigner(sol_<Term, PlaceholdersLhs>& dst,
                const sol_<Term, PlaceholdersRhs>& src) {
    return sol_assigner<Term, PlaceholdersLhs, PlaceholdersRhs>(dst, src);
}

template<class Term, class Placeholders> struct sol_printer {
    sol_printer(std::ostream& out,
                sol_<Term, Placeholders>& s) : out(out), s(s) {}
    template<class Ph> void operator()(const Ph& ph) const {
        return out << ph << "=" << s[ph] << ", ";
    }

    std::ostream& out;
    sol_<Term, Placeholders>& s;
};
template<class Term, class Placeholders>
sol_printer<Term, Placeholders>
mk_sol_printer(std::ostream& out, sol_<Term, Placeholders>& s) {
    return sol_printer<Term, Placeholders>(out, s);
}

template<class Graph, class PlaceholdersLhs, class PlaceholdersRhs>
struct sol_upgrader {
    typedef Graph graph_t;
    typedef typename Graph::term_t term_t;
    typedef node<Graph> node_t;

    sol_upgrader(sol_<node_t, PlaceholdersLhs>& dst,
                 const sol_<term_t, PlaceholdersRhs>& src,
                 const graph_t& graph) : graph(graph), dst(dst), src(src) {}
    template<class T> void operator()(const T& ph) {
        dst[ph] = node<graph_t>(src[ph], graph);
    }
private:
    const graph_t& graph;
    sol_<node_t, PlaceholdersLhs>& dst;
    const sol_<term_t, PlaceholdersRhs>& src;
};
template<class Graph, class PlaceholdersLhs, class PlaceholdersRhs>
sol_upgrader<Graph, PlaceholdersLhs, PlaceholdersRhs>
mk_sol_upgrader(sol_<node<Graph>, PlaceholdersLhs>& dst,
                const sol_<typename Graph::term_t, PlaceholdersRhs>& src,
                const Graph& g) {
    return sol_upgrader<Graph, PlaceholdersLhs, PlaceholdersRhs>(dst, src, g);
}

template<class Term, class PlaceholdersVec> struct sol_ :
        public abstract_sol<Term> {
    typedef Term term_t;
    typedef PlaceholdersVec placeholders_t;
    static const std::size_t len = boost::mpl
                                        ::size<placeholders_t>::type::value;
    std::array<term_t, len> terms = {};

    sol_() = default;
    sol_(const sol_& rhs) = default;
    /** conversion constructor that reorders placeholders as required */
    template<class RhsPlaceholders> sol_(const sol_<Term, RhsPlaceholders>& rhs) {
        using namespace boost::mpl;
        for_each<placeholders_t>(mk_sol_assigner(*this, rhs));
    }
    sol_& operator=(const sol_& rhs) = default;

    template<int n> inline term_t operator[](const ph<n>& pl) const {
        using namespace boost::mpl;
        typedef typename find<placeholders_t, ph<n>>::type pos_t;
        typedef typename end<placeholders_t>::type end_t;
        BOOST_STATIC_ASSERT(!std::is_same<pos_t, end_t>::value);
        return terms[pos_t::pos::value];
    }
    template<int n> inline term_t& operator[](const ph<n>& pl) {
        using namespace boost::mpl;
        typedef typename find<placeholders_t, ph<n>>::type pos_t;
        typedef typename end<placeholders_t>::type end_t;
        BOOST_STATIC_ASSERT(!std::is_same<pos_t, end_t>::value);
        return terms[pos_t::pos::value];
    }

    inline       term_t& at_pos(const std::size_t p)       {return terms[p];}
    inline const term_t& at_pos(const std::size_t p) const {return terms[p];}

    bool operator==(const sol_& rhs) const {
        auto i1 = terms.begin(), i2 = rhs.terms.begin(), e1 = terms.end();
        for (; i1 != e1 && *i1 == *i2; ++i1, ++i2);
        return i1 == e1;
    }
    bool operator!=(const sol_& rhs) const {return !operator==(rhs);}
    bool operator<(const sol_& rhs) const {
        auto i1 = terms.begin(), i2 = rhs.terms.begin(), e1 = terms.end();
        for (; i1 != e1 && *i1 == *i2; ++i1, ++i2) ;
        return i1 != e1 && *i1 < *i2;
    }

    /** Creates a new sol<term_t, placeholders_t> instance by copying all
     *  terms assigned to a sol object whose placeholders are a super-set of
     *  placeholders_t. */
    template<class RhsPlaceholderSet>
    static sol_ from_super(const sol_<term_t, RhsPlaceholderSet>& super) {
        using namespace boost::mpl;
        sol_<term_t, placeholders_t> result;
        for_each<placeholders_t>(mk_sol_assigner(result, super));
        return result;
    }

    /** Creates a sol<term_t, placeholder_t> whose tersm are copied first
     *  from lhs and then from rhs (may overwrite).
     *  Should be used when the union of phs_left_t and phs_right_t is a
     *  superset of (or equal to) placeholders_t, but this is not enforced. */
    template<class LhsPlaceholders, class RhsPlaceholders>
    static sol_ merge(sol_<term_t, LhsPlaceholders>& lhs,
                      sol_<term_t, RhsPlaceholders>& rhs) {
        namespace m = boost::mpl;
        sol_ result;
//        LhsPlaceholders x = std::vector<std::string>{};
//        LhsPlaceholders y = std::vector<std::string>{};
        typedef m::filter_view<placeholders_t,
                               m::contains<LhsPlaceholders,m::placeholders::_1>
                > phs_left_t;
        typedef m::filter_view<placeholders_t,
                               m::contains<RhsPlaceholders,m::placeholders::_1>
                > phs_right_t;
        m::for_each<phs_left_t>(mk_sol_assigner(result, lhs));
        m::for_each<phs_right_t>(mk_sol_assigner(result, rhs));
        return result;
    }

    template<class Graph> struct upgraded {
        typedef sol_<node<Graph>, placeholders_t> type;
    };

    template<class Graph> sol_<node<Graph>, placeholders_t>
    upgrade(const Graph& g) const {
        sol_<node<Graph>, placeholders_t> result;
        boost::mpl::for_each<placeholders_t>(mk_sol_upgrader(result, *this, g));
        return result;
    }
};

} // namespace priv

template<class Term> Term get_first_term(const abstract_sol<Term>& sol) {
    return *reinterpret_cast<const Term*>(&sol);
}

template<class Term, class Placeholders> struct sol {
    typedef priv::sol_<Term,
            typename boost::mpl::sort<Placeholders,
                boost::mpl::less<
                    boost::mpl::placeholders::_1,
                    boost::mpl::placeholders::_2
                >,
                boost::mpl::back_inserter<boost::mpl::vector0<>>
            >::type> type;
    sol() = delete;
    sol(const sol&) = delete;
};
template<class Term, class S, class P, class O> struct spo2sol {
    typedef typename sol<Term,
                typename boost::mpl::filter_view<
                    boost::mpl::set<S, P, O>,
                    is_placeholder<boost::mpl::placeholders::_1>
                >::type
            >::type type;
};
template<class Term, class LeftSol, class RightSol> struct merge_sols {
    typedef typename sol<Term,
                typename boost::mpl::joint_view<
                    typename LeftSol::placeholders_t,
                    typename RightSol::placeholders_t
                    >::type
            >::type type;
};

template<class Term, class Result=Term> struct sol_unwrapper {
    static_assert(std::is_convertible<Term, Result>::value,
            "Term MUST be convertible to Result");
    typedef abstract_sol<Term> argument_type;
    typedef Result result_type;
    result_type operator()(const argument_type& a) const {
        return get_first_term(a);
    }
};
template<class G>
struct sol_unwrapper<node<G>, resource<G>> {
    typedef abstract_sol<node<G>> argument_type;
    typedef resource<G> result_type;
    result_type operator()(const argument_type& a) const {
        return get_first_term(a).as_resource();
    }
};
template<class G>
struct sol_unwrapper<node<G>, literal<G>> {
    typedef abstract_sol<node<G>> argument_type;
    typedef resource<G> result_type;
    result_type operator()(const argument_type& a) const {
        return get_first_term(a).as_literal();
    }
};

template<class Term, class Placeholders>
std::ostream& operator<<(std::ostream& out,
                         const priv::sol_<Term, Placeholders>& s) {
    std::stringstream ss;
    boost::mpl::for_each<Placeholders>(priv::mk_sol_printer(ss, s));
    std::string str = ss.str();
    if (str.length() > 2)
        str.erase(str.end() - 2, str.end());
    return out << "sol(" << str << ")";
}

}}  // namespace shacldator::qry

namespace std {

template<class Term, class Placeholders>
struct hash<shacldator::qry::priv::sol_<Term, Placeholders> > {
    typedef shacldator::qry::priv::sol_<Term, Placeholders> argument_type;
    typedef std::size_t result_type;
    result_type operator()(const argument_type& arg) const {
        std::size_t code = 0;
        std::hash<Term> h;
        for (auto i = arg.terms.begin(), end = arg.terms.end(); i != end; ++i)
            code = 31 * code + h(*i);
        return code;
    }
};

}  // namespace std

#endif /* SRC_QRY_SOL_HPP_ */
