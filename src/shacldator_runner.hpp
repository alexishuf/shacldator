/*
 * shacldator_runner.h
 *
 *  Created on: Jun 18, 2018
 *      Author: alexis
 */

#ifndef SRC_SHACLDATOR_RUNNER_HPP_
#define SRC_SHACLDATOR_RUNNER_HPP_

#include <set>
#include <memory>
#include <string>
#include <ostream>
#include "utils.hpp"

namespace shacldator {

class shacldator_runner {
public:
    shacldator_runner();
    shacldator_runner(shacldator_runner&) = delete;
    ~shacldator_runner();

    const std::set<std::string>& list_inf() const;
    const std::set<std::string>& list_validators() const;

    std::string get_dg() const;
    shacldator_runner& set_dg(const std::string& path);

    std::string get_sg() const;
    shacldator_runner& set_sg(const std::string& path);

    shacldator_runner& set_out(std::ostream& out);
    shacldator_runner& set_times_out(std::ostream& out);

    std::string get_inf() const;
    shacldator_runner& set_inf(const std::string& name);
    shacldator_runner& set_inf_opt(const std::string& name,
                                   const std::string& value = std::string());

    std::string get_validator() const;
    shacldator_runner& set_validator(const std::string& name);

    std::string get_out_fmt() const;
    shacldator_runner& set_out_fmt(const std::string& format);

    int get_nproc() const;
    shacldator_runner& set_nproc(int nproc);

    bool run();

private:
    struct D;
    std::unique_ptr<D> d;
};

} /* namespace shacldator */

#endif /* SRC_SHACLDATOR_RUNNER_HPP_ */
