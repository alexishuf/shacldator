/*
 * serial_graph_validator.hpp
 *
 *  Created on: Apr 30, 2018
 *      Author: alexis
 */

#ifndef SRC_SHACL_GRAPH_VALIDATOR_HPP_
#define SRC_SHACL_GRAPH_VALIDATOR_HPP_

#include <vector>
#include <mutex>
#include <condition_variable>
#include <boost/range/algorithm.hpp>
#include "validation_ctxt.hpp"
#include "shapes_graph.hpp"
#include "../graph/graph.hpp"
#include "../graph/graph_sink.hpp"
#include "../graph/str/concepts/GraphStoreBuilder.hpp"
#include "../graph/str/stl/store.hpp"
#include "validators/def_factory.hpp"
#include "validation_report.hpp"


namespace shacldator {namespace shacl {namespace val {

template<class DGraph, class DGEvaluator, class SGraph, class GSBuilder>
struct serial {
    typedef validation_ctxt<DGraph, DGEvaluator, SGraph> val_ctxt_t;

    static validator_factory<val_ctxt_t> def_factory(const val_ctxt_t& vc) {
        return shacl::def_factory(vc);
    }

    serial() : dg_barrier(true) {}

    bool validate(const val_ctxt_t& vc,
                  const validator_factory<val_ctxt_t>& fac,
                  GSBuilder& out_str_builder) {
        typedef typename val_ctxt_t::sg_shape_t shape_t;
        typedef typename val_ctxt_t::validation_res_t res_t;
        BOOST_CONCEPT_ASSERT((str::GraphStoreBuilder<GSBuilder>));
        using namespace boost;
        typename val_ctxt_t::validation_report_t report;
        bool pass = true;
        typedef std::pair<shape_t, validator_ptr<val_ctxt_t>> work_t;
        std::vector<work_t> work;
        for_each(vc.sg->shapes_range(), [&](const shape_t& shape) {
            work.push_back(std::make_pair(shape, fac(shape)));
        });
        dg_barrier.wait();
        for_each(work, [&](const work_t& work) {
            typename val_ctxt_t::validation_res_vec_t reslts;
            typename val_ctxt_t::dg_node_set_t vis;
            validator_run_ctxt<val_ctxt_t> rc(
                    vc.sg->targets(work.first, vc.dg(), *vc.dge), reslts, vis);
            pass &= work.second->run(rc);
            report.add_all(reslts);
        });
        graph_sink<GSBuilder> sink(&out_str_builder);
        sink << report;
        return pass;
    }

    barrier dg_barrier;
};

}  // namespace val


template<template<class aDGraph, class aDGEvaluator, class aSGraph,
                  class aGSBuilder>
         class Validator, class DGEvaluator, class SGraph, class GSBuilder>
bool validate(DGEvaluator dge, const SGraph& sg, GSBuilder& out_str_builder) {
    typedef Validator<typename DGEvaluator::graph_t, DGEvaluator,
                      SGraph, GSBuilder>
            validator_t;
    typename validator_t::val_ctxt_t vc(&dge, &sg);
    validator_t v;
    return v.validate(vc, validator_t::def_factory(vc), out_str_builder);
}

template<class Validator, class DGEvaluator, class SGraph, class GSBuilder>
bool validate(Validator& v, DGEvaluator dge, const SGraph& sg,
              GSBuilder& out_str_builder) {
    typename Validator::val_ctxt_t vc(&dge, &sg);
    return v.validate(vc, Validator::def_factory(vc), out_str_builder);
}

}}  // namespace shacldator::shacl



#endif /* SRC_SHACL_GRAPH_VALIDATOR_HPP_ */
