/*
 * logic.hpp
 *
 *  Created on: Apr 26, 2018
 *      Author: alexis
 */

#ifndef SRC_SHACL_VALIDATORS_LOGIC_HPP_
#define SRC_SHACL_VALIDATORS_LOGIC_HPP_

#include <vector>
#include <boost/range.hpp>
#include <boost/dynamic_bitset.hpp>
#include <boost/range/algorithm.hpp>
#include "group_validators.hpp"
#include "../../qry/dsl.hpp"
#include "../../qry/spo.hpp"
#include "../../viterator.hpp"

namespace shacldator{namespace shacl{

namespace logic {
typedef enum op_type { AND, OR, XONE, NOT, op_type_SIZE_} type;

template<type op> struct id {static const bool value = false;};
template<> struct id<AND> {static const bool value = true;};

template<type op> struct bin;
template<>struct bin<AND > {static bool eval(bool l, bool r) {return l && r;}};
template<>struct bin<OR  > {static bool eval(bool l, bool r) {return l || r;}};
template<>struct bin<XONE> {static bool eval(bool l, bool r) {return l ^ r;}};
template<>struct bin<NOT > {
    static bool eval(bool l, bool r=false) {return !l && !r;}
};

const char* constr_comp(type op);

struct evaluator {
    evaluator(type op, std::size_t violations_reserve=32)
              : result(false), violations(violations_reserve),
                op(op), f(nullptr) {
        switch (op) {
            case  AND: f = &bin<AND >::eval; result = id<AND >::value; break;
            case   OR: f = &bin<OR  >::eval; result = id<OR  >::value; break;
            case XONE: f = &bin<XONE>::eval; result = id<XONE>::value; break;
            case  NOT: f = &bin<NOT >::eval; result = id<NOT >::value; break;
            default: break;
        }
        if (!f) {
            std::stringstream ss;
            ss << "Invalid op (" << op << ")";
            throw std::invalid_argument(ss.str());
        }
    }

    bool feed(bool validator_result) {
        result = (*f)(result, validator_result);
        if (op == OR && result) return false; //short-circuit
        return true;
    }

    bool result;
    boost::dynamic_bitset<> violations;
private:
    const type op;
    bool (*f)(bool, bool);
};

}  // namespace logic

template<class ValCtxt> struct logic_validator : base_validator<ValCtxt> {
    logic_validator(const ValCtxt& ctxt, logic::type op,
                    std::vector<validator_ptr<ValCtxt>>& children)
                    : base_validator<ValCtxt>(ctxt,
                                              logic::constr_comp(op)),
                      op(op), children(children) {}
    explicit logic_validator(const validator_factory_ctxt<ValCtxt>& c)
                             : base_validator<ValCtxt>(c,
                                                       sh::AndConstraintComponent),
                               op(logic::AND) {
        using namespace qry;
        using namespace boost;

        for (auto i1 = begin(c.objs), e1 = end(c.objs); i1 != e1; ++i1) {
            validator_factory_ctxt<ValCtxt> c2 = c;
            c2.shape = i1->as_resource();
            c2.prop = typename ValCtxt::sg_res_t();
            c2.objs = empty_vrange<typename ValCtxt::sg_node_t>();
            auto vec = group_validator<ValCtxt>::create(c2);
            children.insert(children.end(), vec.begin(), vec.end());
        }
        std::string uri = c.prop.uri();
        if      (uri == sh::or_ ) {
            op = logic::OR;   this->constr_comp=sh::OrConstraintComponent;
        } else if (uri == sh::xone) {
            op = logic::XONE; this->constr_comp=sh::XoneConstraintComponent;
        } else if (uri == sh::not_) {
            op = logic::NOT;  this->constr_comp=sh::NotConstraintComponent;
        } else if (uri != sh::and_) {
            throw std::invalid_argument("Bad logical operator: " + uri);
        }
    }
    virtual ~logic_validator() = default;

    static validator_ptrs<ValCtxt>
    create(const validator_factory_ctxt<ValCtxt>& c) {
        typedef ValCtxt v;
        return validator_ptrs<v>(1, validator_ptr<v>(new logic_validator(c)));
    }

    typedef typename ValCtxt::validation_res_vec_t vr_vec_t;
    virtual bool run(const validator_run_ctxt<ValCtxt>& rc){
        logic::evaluator l_ev(op);
        vr_vec_t vrs(children.size());
        for (auto i = children.begin(), e = children.end(); i != e; ++i) {
            if (!l_ev.feed((*i)->run(rc.with(vrs)))) break;
        }
        for(std::size_t i = 0, e = vrs.size(); i < e; ++i) {
            if (l_ev.violations.test(i)) rc.results.push_back(vrs[i]);
        }
        return l_ev.result;

    }
private:
    logic::type op;
    std::vector<validator_ptr<ValCtxt>> children;
};


}} //namespace shacldator::shacl


#endif /* SRC_SHACL_VALIDATORS_LOGIC_HPP_ */
