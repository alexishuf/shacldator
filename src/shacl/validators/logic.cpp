/*
 * logic.cpp
 *
 *  Created on: Jun 18, 2018
 *      Author: alexis
 */

#include "logic.hpp"

namespace shacldator {namespace shacl {namespace logic {

const char* constr_comp(type op) {
    const char* comp = nullptr;
    switch (op) {
        case  AND: return sh::AndConstraintComponent ;
        case   OR: return sh::OrConstraintComponent  ;
        case XONE: return sh::XoneConstraintComponent;
        case  NOT: return sh::NotConstraintComponent ;
        default: break;
    }
    throw std::invalid_argument("Unknown op value");
}


}}}  // namespace shacldator::shacl::logic



