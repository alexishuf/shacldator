/*
 * group_validators.hpp
 *
 *  Created on: Apr 29, 2018
 *      Author: alexis
 */

#ifndef SRC_SHACL_VALIDATORS_GROUP_VALIDATORS_HPP_
#define SRC_SHACL_VALIDATORS_GROUP_VALIDATORS_HPP_

#include <vector>
#include <assert.h>
#include "base_validator.hpp"
#include "../property_paths.hpp"
#include "../../qry/dsl.hpp"
#include "../../qry/spo.hpp"
#include "../../qry/dsl_eval.hpp"
#include "../../qry/iterators/collection_iterator.hpp"
#include "../../qry/iterators/collection_iterator.hpp"

namespace shacldator {namespace shacl {

template<class ValCtxt> class group_validator :
        public base_validator<ValCtxt> {
    typedef std::vector<validator_ptr<ValCtxt>> children_t;
public:
    explicit group_validator(const ValCtxt& c, const char* cnstr_comp=0)
                             : base_validator<ValCtxt>(c, cnstr_comp) {}
    group_validator(const ValCtxt& c, children_t&& children,
                    const char* cnstr_comp=0)
                    : base_validator<ValCtxt>(c, cnstr_comp),
                      children(std::move(children)) {}
    explicit group_validator(const validator_factory_ctxt<ValCtxt>& c,
                             const char* cnstr_comp=0)
                             : base_validator<ValCtxt>(c, cnstr_comp) {
        setup(c);
    }
    virtual ~group_validator() = default;

protected:
    void setup(const validator_factory_ctxt<ValCtxt>& c) {
        using namespace qry;
        this->late_setup(c);
        auto ps = c.vCtxt.sg->evaluator()
                .eval_resource(_u(_t(c.shape, _1, _2)(_1)));
        for (auto i = boost::begin(ps), e = boost::end(ps); i != e; ++i) {
            auto vec = c.fac(c.tl_shape, c.shape, *i);
            children.insert(children.end(), vec.begin(), vec.end());
        }
    }
public:
    static validator_ptrs<ValCtxt>
    create(const validator_factory_ctxt<ValCtxt>& c) {
        typedef ValCtxt v;
        return validator_ptrs<v>(1, validator_ptr<v>(new group_validator(c)));
    }

    virtual bool run(const validator_run_ctxt<ValCtxt>& rc) {
        bool pass = true;
        for (auto i = children.begin(), e = children.end(); i != e; ++i)
            pass &= (*i)->run(rc);
        return pass;
    }
private:
    children_t children;
};

template<class ValCtxt> struct node_shape_validator :
        public group_validator<ValCtxt> {

    node_shape_validator(const ValCtxt& ctxt, bool top_level=true)
                         : group_validator<ValCtxt>(ctxt,
                                 top_level ? nullptr
                                           : sh::NodeConstraintComponent) {}
    virtual ~node_shape_validator() = default;

    static validator_ptrs<ValCtxt>
    create(const validator_factory_ctxt<ValCtxt>& c) {
        typedef node_shape_validator my_t;
        typedef validator_factory_ctxt<ValCtxt> c_t;
        using namespace boost;
        validator_ptrs<ValCtxt> vec;
        auto setup_f = [&](my_t* obj) {obj->setup(c);};
        if (c.prop && c.prop.lex() == sh::node) {
            validator_factory_ctxt<ValCtxt> c2 = c;
            for (auto i = begin(c.objs), e = end(c.objs); i != e; ++i) {
                c2.shape = i->as_resource();
                auto p = c.fac.template get_node_validator <my_t>(c2,
                        [&](my_t* obj) {
                                obj->setup(c2);
                                obj->constr_comp = sh::NodeConstraintComponent;
                        });
                vec.push_back(p);
            }
        } else {
            assert(!c.prop || c.prop.lex() == rdf::type);
            auto p = c.fac.template get_node_validator <my_t>(c,
                    [&](my_t* obj) {obj->setup(c);});
            vec.push_back(p);
        }
        return vec;
    }

    virtual bool run(const validator_run_ctxt<ValCtxt>& rc) {
        using namespace qry;
        bool pass = true;
        typename ValCtxt::validation_res_vec_t tmp;
        for (auto i = rc.range.first, e = rc.range.second; i != e; ++i){
            if (!rc.visited.insert(*i).second)
                continue; //assume it passes
            tmp.clear();
            pass &= group_validator<ValCtxt>
                    ::run(rc.with(mk_vsingleton_range(*i), tmp));
            rc.visited.erase(*i);
            if (is_toplevel()) {
                for (auto vi = tmp.begin(), ve = tmp.end(); vi != ve; ++vi)
                    vi->with_focusNode(i->as_resource());
            } else {
                for (auto vi = tmp.begin(), ve = tmp.end(); vi != ve; ++vi)
                    vi->with_valueNode(*i);
                if (!tmp.empty())
                    rc.results.push_back(this->report(*i).with_valueNode(*i));
            }
            rc.results.insert(rc.results.end(), tmp.begin(), tmp.end());
        }
        return pass;
    }

private:
    bool is_toplevel() const {return this->constr_comp == nullptr;}
};

template<class ValCtxt> class property_shape_validator :
        public group_validator<ValCtxt> {
    typedef pp_generator_for_eval<typename ValCtxt::dg_eval_t> pp_gen_t;

    static validator_factory_ctxt<ValCtxt>
    prepare_recursion(const validator_factory_ctxt<ValCtxt>& c) {
        validator_factory_ctxt<ValCtxt> c2 = c;
        if (c.prop && c.prop.lex() == sh::property) {
            auto b = boost::begin(c.objs), e = boost::end(c.objs);
            if (b == e) throw std::invalid_argument("Empty c.objs");
            c2.shape = b->as_resource();
        } else { assert(!c.prop || c.prop.lex() == rdf::type); }
        return c2;
    }
    template<class PPGen> void assign_pp_gen(PPGen&& gen) {
        typedef typename std::decay<PPGen>::type type;
        pp_gen.reset(new type(std::forward<PPGen>(gen)));
    }
public:
    property_shape_validator(const ValCtxt& ctxt)
                             : group_validator<ValCtxt>(ctxt,
                                     sh::PropertyConstraintComponent) {}
    explicit property_shape_validator(const validator_factory_ctxt<ValCtxt>& c)
                                      : group_validator<ValCtxt>(
                                              prepare_recursion(c),
                                              sh::PropertyConstraintComponent) {
        using qry::o;
        validator_factory_ctxt<ValCtxt> c2 = prepare_recursion(c);
        auto path = c2.vCtxt.dg().here(o(c2.shape, sh::path)().as_resource());
        if (path) assign_pp_gen(parse_pp(path, *c.vCtxt.dge));
    }

    virtual ~property_shape_validator() = default;

    static validator_ptrs<ValCtxt>
    create(const validator_factory_ctxt<ValCtxt>& c) {
        return def_validator_create<property_shape_validator>(c);
    }

    virtual bool run(const validator_run_ctxt<ValCtxt>& rc) {
//        int old = 0;
//        if (!dbg_concurrency.compare_exchange_strong(old, 1))
//            std::logic_error("Concurrent use of property_shape_validator");
        bool pass = true;
        for (auto i = rc.range.first, e = rc.range.second; i != e; ++i) {
            auto value_rng = get_valueset(*i);
//            if (!boost::empty(value_rng))
            pass &= group_validator<ValCtxt>::run(rc.with(value_rng));
        }
//        dbg_concurrency.store(0);
        return pass;
    }

private:
    typename ValCtxt::dg_node_rng_t
    get_valueset(const typename ValCtxt::dg_node_t& focus) {
        assert(focus);
        if (!pp_gen) return empty_vrange<typename ValCtxt::dg_node_t>();
        auto local_gen = *pp_gen;
        local_gen.set_subj(focus.term());
        return to_node_vrange(focus.graph(), local_gen);
    }

//    std::atomic<int> dbg_concurrency;
    std::shared_ptr<pp_gen_t> pp_gen;
};


}}  // namespace shacldator::shacl



#endif /* SRC_SHACL_VALIDATORS_GROUP_VALIDATORS_HPP_ */
