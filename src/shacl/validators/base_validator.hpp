/*
 * base_validator.hpp
 *
 *  Created on: Apr 26, 2018
 *      Author: alexis
 */

#ifndef SRC_SHACL_VALIDATORS_BASE_VALIDATOR_HPP_
#define SRC_SHACL_VALIDATORS_BASE_VALIDATOR_HPP_

#include <type_traits>
#include <boost/range.hpp>
#include <boost/range/algorithm.hpp>
#include "validator.hpp"
#include "../../qry/spo.hpp"
#include "../validation_ctxt.hpp"
#include "factory.hpp"

namespace shacldator {namespace shacl {

namespace validator_types {
typedef enum {VALUE, VALUE_SET} type;
}  // namespace validator_types

template<class ValCtxt>
class base_validator : public validator<ValCtxt> {
    typedef typename ValCtxt::dg_node_t node_t;
    typedef typename ValCtxt::validation_res_t res_t;
public:

    base_validator(const ValCtxt& ctxt, const char* constr_comp)
                   : validator<ValCtxt>(ctxt),
                     severity(sh::Violation),
                     constr_comp(constr_comp) {}
    base_validator(const validator_factory_ctxt<ValCtxt>& fc,
                   const char* constr_comp)
                   : validator<ValCtxt>(fc.vCtxt),
                     severity(sh::Violation),
                     constr_comp(constr_comp) {
        late_setup(fc);
    }
    base_validator(const base_validator&) = default;
    base_validator& operator=(const base_validator&) = default;
    virtual ~base_validator() = default;

protected:
    void late_setup(const validator_factory_ctxt<ValCtxt>& fc) {
        source_shape = typename ValCtxt::sg_shape_t(fc.tl_shape);
        auto setter = [&](const typename ValCtxt::sg_node_t& n)
                {severity = sh::canonical(n.lex());};
        if (!qry::o(fc.shape, sh::severity).with_first(setter))
            qry::o(fc.tl_shape, sh::severity).with_first(setter);
    }

    res_t report(const node_t& n = node_t()) {
        return res_t(this->severity, this->constr_comp)
                .with_sourceShape(this->source_shape);
    }

    const char* severity;
    const char* constr_comp;
    typename ValCtxt::sg_shape_t source_shape;
};

template<class Derived, class ValCtxt> validator_ptrs<ValCtxt>
def_validator_create(const validator_factory_ctxt<ValCtxt>& c) {
    validator_ptrs<ValCtxt> vec;
    for (auto i = boost::begin(c.objs), e = boost::end(c.objs); i != e; ++i)
        vec.push_back(validator_ptr<ValCtxt>(new Derived(c)));
    return vec;
}

template<class ValCtxt, validator_types::type type>
class simple_validator : public base_validator<ValCtxt> {
    typedef typename ValCtxt::dg_node_t node_t;
    typedef typename ValCtxt::validation_res_t res_t;
public:
    simple_validator(const ValCtxt& ctxt, const char* constr_comp)
                   : base_validator<ValCtxt>(ctxt, constr_comp) {}
    simple_validator(const validator_factory_ctxt<ValCtxt>& fc,
                     const char* constr_comp)
                   : base_validator<ValCtxt>(fc, constr_comp) {
    }
    simple_validator(const simple_validator&) = default;
    simple_validator& operator=(const simple_validator&) = default;
    virtual ~simple_validator() = default;

    virtual bool run(const validator_run_ctxt<ValCtxt>& rc) {
        using namespace boost;
        if (empty(rc.range) && type != validator_types::VALUE_SET)
            return true; //no work
        bool pass = true;
        if (type == validator_types::VALUE_SET) {
            if (!(pass &= check_range(rc.range)))
                rc.results.push_back(report());
        } else if (type == validator_types::VALUE) {
            for (auto i = begin(rc.range), e = end(rc.range); i != e; ++i) {
                if (!(pass &= check_single(*i)))
                    rc.results.push_back(report(*i));
            }
        }
        return pass;
    }

protected:
    res_t report(const node_t& n = node_t()) {
        auto res = base_validator<ValCtxt>::report(n);
        if (type != validator_types::VALUE_SET) {assert(bool(n));}
        if (type == validator_types::VALUE) res.with_valueNode(n);
        res.with_resultPath(getResultPath());
        return res;
    }

    virtual typename ValCtxt::sg_res_t getResultPath() const {
        return typename ValCtxt::sg_res_t();
    }
    virtual bool check_range(const typename ValCtxt::dg_node_rng_t& range) {
        return true;
    }
    virtual bool check_single(const typename ValCtxt::dg_node_t& node) {
        return true;
    }
};

template<class ValCtxt> using value_validator
        = simple_validator<ValCtxt, validator_types::VALUE >;
template<class ValCtxt> using valueset_validator
        = simple_validator<ValCtxt, validator_types::VALUE_SET >;


}}  // namespace shacldator::shacl



#endif /* SRC_SHACL_VALIDATORS_BASE_VALIDATOR_HPP_ */
