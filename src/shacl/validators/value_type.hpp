/*
 * value_type.hpp
 *
 *  Created on: Apr 26, 2018
 *      Author: alexis
 */

#ifndef SRC_SHACL_VALIDATORS_VALUE_TYPE_HPP_
#define SRC_SHACL_VALIDATORS_VALUE_TYPE_HPP_

#include "../../qry/dsl.hpp"
#include "../../qry/dsl_eval.hpp"
#include "../../ns/rdfs.hpp"
#include "../../ns/rdf.hpp"
#include "../../ns/sh.hpp"
#include "base_validator.hpp"
#include "factory.hpp"

namespace shacldator {namespace shacl {

template<class ValCtxt>
struct class_validator : public value_validator<ValCtxt> {
    class_validator(const ValCtxt& ctxt, typename ValCtxt::sg_res_t& clazz)
                    : value_validator<ValCtxt>(ctxt,
                                               sh::ClassConstraintComponent),
                      clazz(clazz) {}
    explicit class_validator(const validator_factory_ctxt<ValCtxt>& c)
                             : value_validator<ValCtxt>(c,
                                                 sh::ClassConstraintComponent),
                               clazz(boost::begin(c.objs)->as_resource()) {}
    virtual ~class_validator() = default;
    static validator_ptrs<ValCtxt>
    create(const validator_factory_ctxt<ValCtxt>& c) {
        return def_validator_create<class_validator>(c);
    }

protected:
    virtual bool check_single(const typename ValCtxt::dg_node_t& node) {
        return this->ctxt.dg().contains(node, rdf::type, clazz);
    }
private:
    typename ValCtxt::sg_res_t clazz;
};

template<class ValCtxt>
struct datatype_validator : public value_validator<ValCtxt> {
    datatype_validator(const ValCtxt& c,
                       typename ValCtxt::sg_res_t& dtype)
                       : value_validator<ValCtxt>(c, sh::DatatypeConstraintComponent),
                         expected(c->dg.here(dtype)) {}
    explicit datatype_validator(const validator_factory_ctxt<ValCtxt>& c)
                                : value_validator<ValCtxt>(c, sh::DatatypeConstraintComponent),
                                  expected(boost::begin(c.objs)->as_resource())
                                  {}
    virtual ~datatype_validator() = default;

    static validator_ptrs<ValCtxt>
    create(const validator_factory_ctxt<ValCtxt>& c) {
        return def_validator_create<datatype_validator>(c);
    }

protected:
    virtual bool check_single(const typename ValCtxt::dg_node_t& value) {
        if (!value.is_literal()) return false;
        auto actual = value.as_literal().datatype();
        return actual == expected
                || this->ctxt.dg().contains(actual, rdfs::subClassOf, expected);
    }
private:
    typename ValCtxt::sg_res_t expected;
};

template<class ValCtxt>
struct nodekind_validator : public value_validator<ValCtxt> {
    nodekind_validator(const ValCtxt& ctxt,
                       const char* nodekind)
                       : value_validator<ValCtxt>(ctxt, sh::NodeKindConstraintComponent),
                         nodekind(nodekind) {}
    explicit nodekind_validator(const validator_factory_ctxt<ValCtxt>& c)
                                : value_validator<ValCtxt>(c, sh::NodeKindConstraintComponent),
                                  nodekind(sh::canonical(boost::begin(c.objs)
                                                                ->lex())) {
    }
    virtual ~nodekind_validator() = default;

    static validator_ptrs<ValCtxt>
    create(const validator_factory_ctxt<ValCtxt>& c) {
        return def_validator_create<nodekind_validator>(c);
    }

protected:
    virtual bool check_single(const typename ValCtxt::dg_node_t& value) {
        bool l = value.is_literal(), iri = false, bn = false;
        if (value.is_resource()) iri = !(bn = value.as_resource().is_blank());

        if (nodekind == sh::IRI) return iri;
        if (nodekind == sh::BlankNode) return bn;
        if (nodekind == sh::Literal) return l;
        if (nodekind == sh::BlankNodeOrIRI) return bn || iri;
        if (nodekind == sh::BlankNodeOrLiteral) return bn || l;
        if (nodekind == sh::IRIOrLiteral) return iri || l;
        assert(false);
        return false;
    }
private:
    const char* nodekind;
};

}}  // namespace shacldator::shacl




#endif /* SRC_SHACL_VALIDATORS_VALUE_TYPE_HPP_ */
