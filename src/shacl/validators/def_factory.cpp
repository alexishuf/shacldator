/*
 * def_factory.cpp
 *
 *  Created on: Jun 18, 2018
 *      Author: alexis
 */

#include "def_factory.hpp"

namespace shacldator {namespace shacl {namespace priv {

std::map<std::string, const char*> nodekind_strs{
    std::make_pair(std::string(sh::IRI), sh::IRI),
    std::make_pair(std::string(sh::BlankNode), sh::BlankNode),
    std::make_pair(std::string(sh::Literal), sh::Literal),
    std::make_pair(std::string(sh::BlankNodeOrIRI), sh::BlankNodeOrIRI),
    std::make_pair(std::string(sh::BlankNodeOrLiteral), sh::BlankNodeOrLiteral),
    std::make_pair(std::string(sh::IRIOrLiteral), sh::IRIOrLiteral),
};

}}} // namespace shacldator::shacl::priv

