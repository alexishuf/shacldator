/*
 * factory_functions.hpp
 *
 *  Created on: Apr 28, 2018
 *      Author: alexis
 */

#ifndef SRC_SHACL_VALIDATORS_DEF_FACTORY_HPP_
#define SRC_SHACL_VALIDATORS_DEF_FACTORY_HPP_

#include "factory.hpp"
#include "value_type.hpp"
#include "logic.hpp"
#include "cardinality.hpp"
#include "group_validators.hpp"
#include "../../ns/sh.hpp"
#include "../../qry/spo.hpp"

namespace shacldator {namespace shacl {

namespace priv {

extern std::map<std::string, const char*> nodekind_strs;

}  // namespace priv

template<class ValCtxt>
validator_factory<ValCtxt> def_factory(const ValCtxt& vc) {
    typedef ValCtxt vc_t;
    typedef const validator_factory_ctxt<ValCtxt>& vfc_t;
    using namespace std;
    using namespace qry;
    using priv::nodekind_strs;
    return validator_factory<vc_t>(vc
     ).register_function(sh::property, &property_shape_validator<vc_t>::create
     ).register_function(sh::node, &node_shape_validator<vc_t>::create
     ).register_function(sh::class_,   &class_validator<vc_t>::create
     ).register_function(sh::datatype, &datatype_validator<vc_t>::create
     ).register_function(sh::nodeKind, &nodekind_validator<vc_t>::create
     ).register_function(sh::minCount, &mincount_validator<vc_t>::create
     ).register_function(sh::maxCount, &maxcount_validator<vc_t>::create
     ).register_function(sh::and_, &logic_validator<vc_t>::create
     ).register_function(sh::or_ , &logic_validator<vc_t>::create
     ).register_function(sh::xone, &logic_validator<vc_t>::create
     ).register_function(sh::not_, &logic_validator<vc_t>::create
     );
}

}}  // namespace shacldator::shacl



#endif /* SRC_SHACL_VALIDATORS_DEF_FACTORY_HPP_ */
