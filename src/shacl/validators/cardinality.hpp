/*
 * cardinality.hpp
 *
 *  Created on: Apr 27, 2018
 *      Author: alexis
 */

#ifndef SRC_SHACL_VALIDATORS_CARDINALITY_HPP_
#define SRC_SHACL_VALIDATORS_CARDINALITY_HPP_

#include "base_validator.hpp"

namespace shacldator {namespace shacl {

template<class ValCtxt> struct mincount_validator :
        public valueset_validator<ValCtxt> {
    mincount_validator(const ValCtxt& c, std::size_t expected)
                       : valueset_validator<ValCtxt>(c, sh::MinCountConstraintComponent),
                         expected(expected) {}
    explicit mincount_validator(const validator_factory_ctxt<ValCtxt>& c)
                                : valueset_validator<ValCtxt>(c, sh::MinCountConstraintComponent),
                                  expected(boost::begin(c.objs)->as_literal()
                                                .template as<std::size_t>()) {}
    virtual ~mincount_validator() = default;

    static validator_ptrs<ValCtxt>
    create(const validator_factory_ctxt<ValCtxt>& c) {
        return def_validator_create<mincount_validator>(c);
    }

protected:
    virtual bool check_range(const typename ValCtxt::dg_node_rng_t& range) {
        return boost::size(range) >= expected;
    }
private:
    std::size_t expected;
};

template<class ValCtxt> struct maxcount_validator :
        public valueset_validator<ValCtxt> {
    maxcount_validator(const ValCtxt& c, std::size_t expected)
                       : valueset_validator<ValCtxt>(c, sh::MaxCountConstraintComponent),
                         expected(expected) {}
    explicit maxcount_validator(const validator_factory_ctxt<ValCtxt>& c)
                                : valueset_validator<ValCtxt>(c, sh::MaxCountConstraintComponent),
                                  expected(boost::begin(c.objs)->as_literal()
                                                .template as<std::size_t>()) {}
    virtual ~maxcount_validator() = default;

    static validator_ptrs<ValCtxt>
    create(const validator_factory_ctxt<ValCtxt>& c) {
        return def_validator_create<maxcount_validator>(c);
    }
protected:
    virtual bool check_range(const typename ValCtxt::dg_node_rng_t& range) {
        return boost::size(range) <= expected;
    }
private:
    std::size_t expected;
};

}}  // namespace shacldator::shacl



#endif /* SRC_SHACL_VALIDATORS_CARDINALITY_HPP_ */
