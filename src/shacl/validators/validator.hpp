/*
 * validation_task.hpp
 *
 *  Created on: Apr 26, 2018
 *      Author: alexis
 */

#ifndef SRC_SHACL_VALIDATORS_HPP_
#define SRC_SHACL_VALIDATORS_HPP_

#include <cassert>
#include <sstream>
#include <stdexcept>
#include <vector>
#include <boost/range/numeric.hpp>
#include "../shape.hpp"
#include "../../graph/graph.hpp"
#include "../../graph/graph_sink.hpp"

namespace shacldator {namespace shacl {

template<class ValCtxt> struct validator_run_ctxt {
    validator_run_ctxt(const typename ValCtxt::dg_node_rng_t& range,
             typename ValCtxt::validation_res_vec_t& results,
             typename ValCtxt::dg_node_set_t& visited)
             : range(range), results(results), visited(visited) {}
//    validator_run_ctxt(typename ValCtxt::dg_node_rng_t&& range,
//             typename ValCtxt::validation_res_vec_t& results,
//             typename ValCtxt::dg_node_set_t& visited)
//             : range(std::move(range)), results(results),
//               visited(visited) {}
    validator_run_ctxt(validator_run_ctxt&&) = default;
//    validator_run_ctxt(const validator_run_ctxt&) = default;
    validator_run_ctxt(const validator_run_ctxt& o)
                       : range(o.range),
                         results(o.results),
                         visited(o.visited) {}

    validator_run_ctxt with(const typename ValCtxt::dg_node_rng_t& r) const {
        return validator_run_ctxt(r, results, visited);
    }
    validator_run_ctxt with(typename ValCtxt::dg_node_rng_t&& r) const {
        return validator_run_ctxt(std::move(r), results, visited);
    }
    validator_run_ctxt with(typename ValCtxt::validation_res_vec_t& rs) const {
        return validator_run_ctxt(range, rs, visited);
    }
    validator_run_ctxt with(const typename ValCtxt::dg_node_rng_t& rng,
                            typename ValCtxt::validation_res_vec_t& rs) const {
        return validator_run_ctxt(rng, rs, visited);
    }
    validator_run_ctxt with(typename ValCtxt::dg_node_rng_t&& rng,
                            typename ValCtxt::validation_res_vec_t& rs) const {
        return validator_run_ctxt(std::move(rng), rs, visited);
    }

    typename ValCtxt::dg_node_rng_t range;
    typename ValCtxt::validation_res_vec_t& results;
    typename ValCtxt::dg_node_set_t& visited;
};

template<class ValCtxt>
struct validator {
    validator(const ValCtxt& ctxt) : ctxt(ctxt) {}
    validator(const validator&) = default;
    validator& operator=(const validator&) = default;
    virtual ~validator() = default;

    virtual bool run(const validator_run_ctxt<ValCtxt>& rc) = 0;
    const ValCtxt ctxt;
};

template<class ValCtxt> using validator_ptr
        = std::shared_ptr<validator<ValCtxt>>;
template<class ValCtxt> using validator_ptrs
        = std::vector<validator_ptr<ValCtxt>>;

}

}  // namespace shacldator::shacl



#endif /* SRC_SHACL_VALIDATORS_HPP_ */
