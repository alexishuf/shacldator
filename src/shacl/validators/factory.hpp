/*
 * validator_factory.hpp
 *
 *  Created on: Apr 27, 2018
 *      Author: alexis
 */

#ifndef SRC_SHACL_VALIDATORS_FACTORY_HPP_
#define SRC_SHACL_VALIDATORS_FACTORY_HPP_

#include <memory>
#include <map>
#include <functional>
#include <cstring>
#include <sstream>
#include <boost/function.hpp>
#include "validator.hpp"
#include "../../qry/dsl.hpp"
#include "../../ns/sh.hpp"
#include "../../ns/rdf.hpp"
#include "../../viterator.hpp"

namespace shacldator {namespace shacl {

template<class ValCtxt> struct validator_factory;

template<class ValCtxt> struct validator_factory_ctxt {
    validator_factory_ctxt(const ValCtxt& valCtxt,
            const validator_factory<ValCtxt>& fac,
            const typename ValCtxt::sg_res_t& tl_shape,
            const typename ValCtxt::sg_res_t& shape,
            const typename ValCtxt::sg_res_t& prop,
            const typename ValCtxt::sg_node_rng_t& objs)
            : vCtxt(valCtxt), fac(fac), tl_shape(tl_shape), shape(shape),
              prop(prop), objs(objs) {}
    validator_factory_ctxt(const ValCtxt& valCtxt,
            const validator_factory<ValCtxt>& fac,
            const typename ValCtxt::sg_res_t& tl_shape,
            const typename ValCtxt::sg_res_t& shape,
            const typename ValCtxt::sg_res_t& prop,
            typename ValCtxt::sg_node_rng_t&& objs)
            : vCtxt(valCtxt), fac(fac), tl_shape(tl_shape), shape(shape),
              prop(prop), objs(std::move(objs)) {}
    validator_factory_ctxt(const validator_factory_ctxt&) = default;
    validator_factory_ctxt(validator_factory_ctxt&&) = default;

    std::string to_str() const {
        std::stringstream ss;
        ss << "{vCtxt=0x" << std::hex << &vCtxt << "; fac=0x" << &fac
           << "; tl_shape=" << tl_shape.to_str()
           << "; shape=" << shape.to_str()
           << "; prop=" << prop.to_str()
           << "; objs=" << rng_to_str(objs) << "}";
        return ss.str();
    }

    const ValCtxt& vCtxt;
    const validator_factory<ValCtxt>& fac;
    typename ValCtxt::sg_res_t tl_shape, shape, prop;
    typename ValCtxt::sg_node_rng_t objs;
};

template<class ValCtxt> using validator_factory_fn =
        boost::function<validator_ptrs<ValCtxt> (
                const validator_factory_ctxt<ValCtxt>&)>;


template<class ValCtxt> struct validator_factory {
    typedef typename ValCtxt::sg_res_t res_t;
    typedef validator_ptr<ValCtxt> validator_p;
    typedef validator_ptrs<ValCtxt> result_type;

    validator_factory(const ValCtxt& vc) : vc(vc) {}
    validator_factory(validator_factory&& o) = default;
    validator_factory(const validator_factory& o) = default;
    validator_factory& operator=(validator_factory&& o) = default;
    validator_factory& operator=(const validator_factory& o) = default;

    validator_p operator()(const res_t& shape) const {
        using namespace qry;
        auto ev = vc.sg->evaluator();
        auto empty = empty_vrange<typename ValCtxt::sg_node_t>();
        validator_factory_ctxt<ValCtxt> fc(vc, *this, shape, shape,
                                                      res_t(), empty);
        if (ev.ask(_t(shape, rdf::type, sh::NodeShape))) {
            auto r = nv_cache.get(shape, shape, true);
            return r ? r : prop2fn.at(sh::node)(fc).at(0);
        } else if (vc.sg->graph().contains(shape, rdf::type, sh::PropertyShape)
                   || ev.ask(_t(shape, sh::path,  _1))) {
            return prop2fn.at(sh::property)(fc).at(0);
        }
        throw std::invalid_argument("Only sh:NodeShape and sh:PropertyShape "
                "instances can be constructed with the unary call operator. "
                + shape.to_str() + " appears to be neither.");
    }

    result_type operator()(const res_t& tl_shape, const res_t& shape,
                           const res_t& prop) const {
        auto os = vc.sg->evaluator().deval_node(_t(shape, prop, qry::_1));
        return os.first == os.second ? result_type(0)
                                  : (*this)(tl_shape, shape, prop, os);
    }
    result_type operator()(const res_t& shape,
                           const res_t& prop) const {
        return operator ()(shape, shape, prop);
    }

    result_type operator()(const res_t& tl_shape, const res_t& shape,
                           const res_t& prop,
                           const typename ValCtxt::sg_node_rng_t& objs) const {
        using namespace boost;
        typedef validator_factory_ctxt<ValCtxt> fc_t;
        auto it = prop2fn.find(prop.uri().c_str());
        if (it == prop2fn.end()) return result_type();
        return it->second(fc_t(vc, *this, tl_shape, shape, prop, objs));
    }
    result_type operator()(const res_t& shape,
                           const res_t& prop,
                           const typename ValCtxt::sg_node_rng_t& objs) const {
        return operator()(shape, shape, prop, objs);
    }

    validator_factory& register_function(const char* vocabProperty,
                                         validator_factory_fn<ValCtxt> fn) {
        prop2fn.insert(std::make_pair(vocabProperty, fn));
        return *this;
    }

    template<class Derived, class UnryF> validator_p
    get_node_validator(const validator_factory_ctxt<ValCtxt>& c, UnryF f) const {
        bool is_tl = !c.prop || c.prop.lex() != sh::node;
        auto p = nv_cache.get(c.tl_shape, c.shape, is_tl);
        if (!p) {
            auto* raw = new Derived(c.vCtxt);
            p = validator_p(raw);
            nv_cache.put(c.tl_shape, c.shape, is_tl, p);
            f(raw);
        }
        return p;
    }

private:
    struct comparator : public std::binary_function<const char*, const char*,
                                                    bool> {
        bool operator()(const char* left, const char* right) const {
            return std::strcmp(left, right) < 0;
        }
    };

    struct cache {
        typedef std::tuple<res_t, res_t, bool> key;

        validator_p get(const res_t& tl_shape, const res_t& shape,
                        bool is_tl) const {
            auto it = map.find(key(tl_shape, shape, is_tl));
            return it != map.end() ? it->second : nullptr;
        }
        void put(const res_t& tl_shape, const res_t& shape, bool is_tl,
                 validator_p value){
            map.insert(std::make_pair(key(tl_shape, shape, is_tl), value));
        }

        std::map<key, validator_p> map;
    };

    std::map<const char*, validator_factory_fn<ValCtxt>, comparator> prop2fn;
    mutable cache nv_cache;
    ValCtxt vc;
};

}}  // namespace shacldator::shacl



#endif /* SRC_SHACL_VALIDATORS_FACTORY_HPP_ */
