/*
 * shapes_graph.hpp
 *
 *  Created on: Apr 19, 2018
 *      Author: alexis
 */

#ifndef SRC_SHACL_SHAPES_GRAPH_HPP_
#define SRC_SHACL_SHAPES_GRAPH_HPP_

#include <functional>
#include <boost/concept_check.hpp>
#include <boost/range.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/iterator/iterator_facade.hpp>
#include <boost/iterator/transform_iterator.hpp>
#include "../graph/graph.hpp"
#include "../graph/node.hpp"
#include "../ns/sh.hpp"
#include "../ns/rdf.hpp"
#include "../ns/owl.hpp"
#include "../viterator.hpp"
#include "../qry/spo.hpp"
#include "../qry/dsl.hpp"
#include "../qry/dsl_eval.hpp"
#include "../qry/iterators/collection_iterator.hpp"
#include "shape.hpp"

namespace shacldator {namespace shacl {


template<class Graph, class GraphPtr=const typename std::decay<Graph>::type*,
         class Evaluator = qry::def_evaluator<typename std::decay<Graph>::type>
> struct shapes_graph {
    typedef typename std::decay<Graph>::type graph_t;
    typedef GraphPtr graph_ptr_t;
    typedef node<Graph> node_t;
    typedef resource<Graph> resource_t;
    typedef shacl::shape<Graph> shape_t;
    typedef Evaluator evaluator_t;
    typedef typename qry::sol<node_t, boost::mpl::vector<qry::ph<1> > >::type
            sol1_t;
    typedef priv::sol_shape_unwrapper<node_t> shape_unwrapper_t;

    shapes_graph(graph_ptr_t g) : m_g(g), m_evaltr(*g) {}
    shapes_graph(const shapes_graph& rhs) : m_g(rhs.m_g) {}
    shapes_graph& operator=(const shapes_graph& rhs) {
        if (this == &rhs) return *this;
        m_g = rhs.m_g;
        return *this;
    }

    inline evaluator_t evaluator() const {return m_evaltr;}
    inline const graph_t& graph() const {return *m_g;}

    shape_t shape(const char* uri) const {return shape_t(m_g->resource(uri));}

    typedef decltype(std::declval<Evaluator>().eval(
            qry::_u(qry::_t(qry::_1, "", "")
                  | qry::_t(qry::_1, "", "")),
            std::declval<shape_unwrapper_t>()
    )) shapes_range_t;
    shapes_range_t shapes_range() const {
        using namespace qry;
        shape_unwrapper_t t;
        return m_evaltr.eval(_u(_t(_1, rdf::type, sh::NodeShape)
                              | _t(_1, rdf::type, sh::PropertyShape)),  t);
    }

    typedef typename boost::range_iterator<shapes_range_t> iterator;
    iterator begin_shapes() const {return shapes_range().first;}
    iterator end_shapes() const {return shapes_range().second;}

    template<class DG> struct dg_to_targets_range {
        typedef std::pair<fwd_viterator<resource<DG>>,
                          fwd_viterator<resource<DG>>> type;
    };

    template<class DGE> struct dge_to_targets_range {
        typedef std::pair<fwd_viterator<resource<typename DGE::graph_t>>,
                          fwd_viterator<resource<typename DGE::graph_t>>> type;
    };

    template<class DataGraph>
    typename dg_to_targets_range<DataGraph>::type
    targets(const shape_t& shape, const DataGraph& dg) const {
        auto eval = qry::mk_evaluator(dg);
        return targets(shape, dg, eval);
    }

    template<class DataGraph, class DGEvaluator>
    typename dge_to_targets_range<DGEvaluator>::type
    targets(const shape_t& shape, const DataGraph& dg,
                                  DGEvaluator& ev) const {
        static_assert(std::is_same<typename DGEvaluator::graph_t,
                                   DataGraph>::value,
                      "DataGraph != DGEvaluator::graph_t");
        using namespace qry;
        typedef ncat_iterator<fwd_viterator<resource<DataGraph>> > it_t;
        sol_unwrapper<node<DataGraph>, resource<DataGraph>> unw;
//        decltype(ev.deval(_t(_1, "", ""), upg)) xx = 0;
        typename it_t::builder b;
        boost::for_each(o(shape, sh::targetClass), [&](const node_t& n) {
            b.add(ev.deval(_t(_1, rdf::type, n.lex()), unw));
        });
        boost::for_each(o(shape, sh::targetNode), [&](const node_t& n) {
            if (n.is_resource())
                b.add(mk_singleton_range(ev.graph().here(n.as_resource())));
        });

        if (o(shape, rdf::type).contains(owl::Class))
            b.add(ev.deval(_t(_1, rdf::type, shape.uri()), unw));
        boost::for_each(o(shape, sh::targetSubjectsOf), [&](const node_t& n) {
            b.add(ev.deval(_t(_1, n.lex(), _2)(_1), unw));
        });
        boost::for_each(o(shape, sh::targetObjectsOf), [&](const node_t& n) {
            b.add(ev.deval(_t(_1, n.lex(), _2)(_2), unw));
        });
        return b.build_range();
    }


private:
    graph_ptr_t m_g;
    mutable Evaluator m_evaltr;
};

}}  // namespace shacldator::shacl



#endif /* SRC_SHACL_SHAPES_GRAPH_HPP_ */
