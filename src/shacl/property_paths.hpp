/*
 * property_paths.hpp
 *
 *  Created on: Jun 3, 2018
 *      Author: alexis
 */

#ifndef SRC_SHACL_PROPERTY_PATHS_HPP_
#define SRC_SHACL_PROPERTY_PATHS_HPP_

#include <utility>
#include <stack>
#include "../viterator.hpp"
#include "../graph/node.hpp"
#include "../qry/spo.hpp"
#include "../dqry/path_generators.hpp"
#include "../ns/rdf.hpp"
#include "../ns/sh.hpp"

namespace shacldator {namespace shacl {

template<class DataGraph> using pp_generator_for_graph =
        dqry::path::packed_generator<typename DataGraph::store_t::term_t>;

template<class DGEvaluator> using pp_generator_for_eval =
        pp_generator_for_graph<typename DGEvaluator::graph_t>;

namespace pp_detail {

template<class G> struct node_upgrader {
    typedef const typename G::store_t::term_t& argument_type;
    typedef node<G> result_type;
    const G* g;
    result_type operator()(argument_type term) const {
        return result_type(term, *g);
    }
};

template<class SG, class DGEvaluator> struct parser {
    typedef DGEvaluator ev_t;
    typedef pp_generator_for_eval<ev_t> generator_t;
    typedef typename ev_t::graph_t::store_t::term_t term_t;

    parser(ev_t ev) : ev(ev) {}

    generator_t parse(const resource<SG>& path) {
        namespace p = dqry::path;
        if (!path)
            return p::empty<term_t>();
        resource<SG> r;
        if (qry::o(path, rdf::first)() || path.uri() == rdf::nil)
            return p::seq<term_t>(parse_vec(path));
        if ((r = qry::o(path, sh::alternativePath)().as_resource()))
            return p::alt<term_t>(parse_vec(r));
        if ((r = qry::o(path, sh::inversePath)().as_resource())) {
            push_reverse();
            generator_t g = parse(r);
            pop_reverse();
            return g;
        }
        if ((r = qry::o(path, sh::zeroOrMorePath)().as_resource()))
            return p::star<term_t>(parse(r));
        if ((r = qry::o(path, sh::oneOrMorePath)().as_resource()))
            return p::plus<term_t>(parse(r));
        if ((r = qry::o(path, sh::zeroOrOnePath)().as_resource()))
            return p::opt<term_t>(parse(r));
        term_t pred = ev.graph().store().resource(path.lex().c_str());
        if (!pred)
            return p::empty<term_t>();
        if (is_reversed())
            return p::rev<ev_t>(pred, ev);
        return p::prop<ev_t>(pred, ev);
    }

private:
    std::vector<generator_t> parse_vec(resource<SG> l) {
        std::vector<generator_t> vec;
        for (; l && l.uri()!=rdf::nil; l=qry::o(l, rdf::rest)().as_resource())
            vec.push_back(parse(qry::o(l, rdf::first)().as_resource()));
        return vec;
    }

    bool is_reversed() const {
        return !reverse_stack.empty() && reverse_stack.top();
    }
    void pop_reverse() {
        assert(!reverse_stack.empty());
        reverse_stack.pop();
    }
    void push_reverse() {
        reverse_stack.push(!is_reversed());
    }

    ev_t ev;
    std::stack<bool> reverse_stack;

};

}  // namespace pp_detail

template<class SG, class DGEvaluator>
pp_generator_for_eval<DGEvaluator>
parse_pp(const resource<SG>& path, DGEvaluator ev) {
    using namespace pp_detail;
    parser<SG, DGEvaluator> p(ev);
    return p.parse(path);
}

template<class DG> std::pair<fwd_viterator<node<DG>>, fwd_viterator<node<DG>>>
to_node_vrange(const DG& data_graph, const pp_generator_for_graph<DG>& gen) {
    return mk_vrange(dqry::path::mk_generator_range(gen),
                     pp_detail::node_upgrader<DG>{&data_graph});
}

}}  // namespace shacldator::shacl



#endif /* SRC_SHACL_PROPERTY_PATHS_HPP_ */
