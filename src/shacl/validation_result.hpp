/*
 * validation_result.hpp
 *
 *  Created on: May 3, 2018
 *      Author: alexis
 */

#ifndef SRC_SHACL_VALIDATION_RESULT_HPP_
#define SRC_SHACL_VALIDATION_RESULT_HPP_

#include "../graph/node.hpp"
#include "../ns/sh.hpp"
#include "../ns/rdf.hpp"
#include "shape.hpp"

namespace shacldator {

template<class B, class BPtr> struct graph_sink;

namespace shacl {

template<class DataGraph, class ShapesGraph>
struct validation_result {
    typedef resource<typename ShapesGraph::graph_t> sg_res_t;
    typedef shape<typename ShapesGraph::graph_t> sg_shape_t;

    validation_result(const char* resultSeverity=sh::Violation,
                      const char* sourceConstraintComponent=0)
                      : resultSeverity(resultSeverity),
                        sourceConstraintComponent(sourceConstraintComponent) {}
    validation_result(const validation_result&) = default;
    validation_result& operator=(const validation_result&) = default;

    validation_result& with_resultPath(const sg_res_t&n) {
        resultPath = n;
        return *this;
    }
    validation_result& with_sourceShape(const sg_shape_t&n) {
        sourceShape = n;
        return *this;
    }
    validation_result& with_focusNode(const resource<DataGraph>& n) {
        focusNode = n;
        if (valueNode == focusNode) valueNode = node<DataGraph>();
        return *this;
    }
    validation_result& with_valueNode(const node<DataGraph>& n) {
        valueNode = n;
        return *this;
    }
    validation_result& with_resultSeverity(const char* uri) {
        resultSeverity = uri;
        return *this;
    }
    validation_result& with_sourceConstraintComponent(const char* uri) {
        sourceConstraintComponent = uri;
        return *this;
    }

    inline bool is_error() const {
        assert(!focusNode ||
                (sourceShape && resultSeverity && sourceConstraintComponent));
        return focusNode;
    }

    sg_shape_t sourceShape;
    sg_res_t resultPath;
    resource<DataGraph> focusNode;
    node<DataGraph> valueNode;
    const char* resultSeverity; //a value in sh::
    const char* sourceConstraintComponent; // a value in sh::
};

} // namespace shacl

template<class B, class BPtr, class DataGraph, class ShapesGraph>
std::string
write_resource(graph_sink<B, BPtr>& out,
               const shacl::validation_result<DataGraph, ShapesGraph>& r) {
    std::string bn = out.create_blank();
    out.add(bn, rdf::type, sh::ValidationResult);
    if (r.sourceShape)
        out.add(bn, sh::sourceShape, r.sourceShape);
    if (r.resultPath)
        out.add(bn, sh::resultPath, r.resultPath);
    if (r.focusNode)
        out.add(bn, sh::focusNode, r.focusNode);
    if (r.valueNode)
        out.add(bn, sh::value, r.valueNode);
    if (r.sourceConstraintComponent)
        out.add(bn, sh::sourceConstraintComponent, r.sourceConstraintComponent);
    if (r.resultSeverity)
        out.add(bn, sh::resultSeverity, r.resultSeverity);
    return bn;
}


}  // namespace shacldator



#endif /* SRC_SHACL_VALIDATION_RESULT_HPP_ */
