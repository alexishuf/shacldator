/*
 * serial_graph_validator.hpp
 *
 *  Created on: Apr 30, 2018
 *      Author: alexis
 */

#ifndef SRC_SHACL_NONE_GRAPH_VALIDATOR_HPP_
#define SRC_SHACL_NONE_GRAPH_VALIDATOR_HPP_

#include "validation_ctxt.hpp"
#include "shapes_graph.hpp"
#include "../graph/graph.hpp"
#include "../graph/graph_sink.hpp"
#include "validators/def_factory.hpp"
#include "validation_report.hpp"


namespace shacldator {namespace shacl {namespace val {

template<class DGraph, class DGEvaluator, class SGraph, class GSBuilder>
struct none {
    typedef validation_ctxt<DGraph, DGEvaluator, SGraph> val_ctxt_t;
public:
    none() : dg_barrier(true) {}

    static validator_factory<val_ctxt_t> def_factory(const val_ctxt_t& vc) {
        return validator_factory<val_ctxt_t>(vc);
    }

    bool validate(const val_ctxt_t& vc,
                  const validator_factory<val_ctxt_t>& fac,
                  GSBuilder& out_str_builder) {
        return true;
    }

    barrier dg_barrier;
};

}}}  // namespace shacldator::shacl::val



#endif /* SRC_SHACL_NONE_GRAPH_VALIDATOR_HPP_ */
