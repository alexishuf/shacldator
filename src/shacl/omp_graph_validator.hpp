/*
 * serial_graph_validator.hpp
 *
 *  Created on: Apr 30, 2018
 *      Author: alexis
 */

#ifndef SRC_SHACL_OMP_GRAPH_VALIDATOR_HPP_
#define SRC_SHACL_OMP_GRAPH_VALIDATOR_HPP_

#include <vector>
#include <boost/range/algorithm.hpp>
#include "validation_ctxt.hpp"
#include "shapes_graph.hpp"
#include "../qry/iterators/collection_iterator.hpp"
#include "../graph/graph.hpp"
#include "../graph/graph_sink.hpp"
#include "../graph/str/concepts/GraphStoreBuilder.hpp"
#include "../graph/str/stl/store.hpp"
#include "validators/def_factory.hpp"
#include "validation_report.hpp"


namespace shacldator {namespace shacl {namespace val {

template<class DGraph, class DGEvaluator, class SGraph, class GSBuilder>
struct omp_coarse {
    typedef validation_ctxt<DGraph, DGEvaluator, SGraph> val_ctxt_t;
private:
    typedef typename val_ctxt_t::sg_shape_t shape_t;
    typedef typename val_ctxt_t::validation_res_t res_t;
    typedef validator_run_ctxt<val_ctxt_t> run_ctxt_t;
    typedef typename val_ctxt_t::validation_report_t report_t;
public:

    omp_coarse() : dg_barrier(true) {}

    static validator_factory<val_ctxt_t> def_factory(const val_ctxt_t& vc) {
        return shacl::def_factory(vc);
    }

    bool validate(const val_ctxt_t& vc,
                  const validator_factory<val_ctxt_t>& fac,
                  GSBuilder& out_str_builder) {
        BOOST_CONCEPT_ASSERT((str::GraphStoreBuilder<GSBuilder>));
        using namespace boost;
        report_t report;
        auto r = vc.sg->shapes_range();
        #pragma omp parallel
        {
            #pragma omp single
            {
                for (auto i = r.first, e = r.second; i != e; ++i) {
                    #pragma omp task firstprivate(i)
                    this->work(vc, fac, *i, report);
                }
            }
        }
        graph_sink<GSBuilder> sink(&out_str_builder);
        sink << report;
        return report.conforms();
    }

private:
    void work(const val_ctxt_t& vc,
              const validator_factory<val_ctxt_t>& fac,
              const shape_t& shape, report_t& rep) {
        typedef typename val_ctxt_t::dg_node_t dg_node_t;
        validator_ptr<val_ctxt_t> val = fac(shape);
        dg_barrier.wait();
        auto tgts = vc.sg->targets(shape, vc.dg(), *vc.dge);
        for (auto i = tgts.first, e = tgts.second; i != e; ++i) {
            #pragma omp task shared(rep) firstprivate(i)
            {
                typename val_ctxt_t::validation_res_vec_t res;
                typename val_ctxt_t::dg_node_set_t vis;
                /* BEGIN workaround lapesd-tesla GCC 5.4 */
                qry::singleton_range_t<dg_node_t> sr(
                        qry::singleton_iterator_t<dg_node_t>({*i}),
                        qry::singleton_iterator_t<dg_node_t>()
                );
                run_ctxt_t rc(sr, res, vis);
                val->run(rc);
                /* END workaround lapesd-tesla GCC 5.4 */
                /* not critical: either a tasks starts a new contention group,
                 * or the compiler tried to be smart, failed to see data
                 * sharing and erased the lock */
//                #pragma critical
//                boost::for_each(res, [&](const res_t& res){rep.add(res);});
                rep.add_all(res); //add_all locks a mutex
            }
        }
    }

public:
    barrier dg_barrier;
};

}}}  // namespace shacldator::shacl::val



#endif /* SRC_SHACL_OMP_GRAPH_VALIDATOR_HPP_ */
