/*
 * validation_report.hpp
 *
 *  Created on: May 3, 2018
 *      Author: alexis
 */

#ifndef SRC_SHACL_VALIDATION_REPORT_HPP_
#define SRC_SHACL_VALIDATION_REPORT_HPP_

#include <deque>
#include <mutex>
#include <boost/range/algorithm.hpp>
#include "validation_result.hpp"
#include "../ns/xsd.hpp"

namespace shacldator {namespace shacl {

template<class DataGraph, class ShapesGraph> struct validation_report {
    validation_report() = default;
    validation_report(const validation_report&) = default;
    validation_report& operator=(const validation_report&) = default;
    ~validation_report() = default;

    typedef validation_result<DataGraph, ShapesGraph> result_t;
    typedef std::deque<result_t> results_t;
    typedef typename results_t::const_iterator iterator;
    iterator begin() const {return results.begin();}
    iterator   end() const {return results.end();}

    template<class Range>
    void add_all(const Range& r) {
        std::lock_guard<std::mutex> guard(mtx);
        results.insert(results.end(), boost::begin(r), boost::end(r));
    }

    bool conforms() const {return results.empty();}

    results_t results;
    std::mutex mtx;
};

}

template<class B, class BPtr, class DataGraph, class ShapesGraph>
std::string
write_resource(graph_sink<B, BPtr>& out,
               const shacl::validation_report<DataGraph, ShapesGraph>& r) {
    std::string bn = out.create_blank();
    out.add(bn, rdf::type, sh::ValidationReport);
    out.add(bn, sh::conforms, r.conforms() ? "true" : "false", xsd::boolean_, "");
    for (auto i = r.begin(), e = r.end(); i != e; ++i)
        out.add(bn, sh::result, out.write_resource(*i));
    return bn;
}

}  // namespace shacldator::shacl


#endif /* SRC_SHACL_VALIDATION_REPORT_HPP_ */
