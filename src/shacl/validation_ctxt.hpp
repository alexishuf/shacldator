/*
 * validation_ctxt.hpp
 *
 *  Created on: Apr 26, 2018
 *      Author: alexis
 */

#ifndef SRC_SHACL_VALIDATION_CTXT_HPP_
#define SRC_SHACL_VALIDATION_CTXT_HPP_

#include <type_traits>
#include <vector>
#include <utility>
#include <unordered_set>
#include <memory>
#include <boost/function.hpp>
#include "../graph/node.hpp"
#include "../qry/dsl_eval.hpp"
#include "../viterator.hpp"
#include "shape.hpp"
#include "validation_report.hpp"

namespace shacldator {namespace shacl {

template<class ValCtxt> struct validator;


namespace detail {
struct abstract_validation_ctxt {};
}  // namespace detail


template<class DataGraph, class DGEvaluator, class ShapesGraph>
struct validation_ctxt : public detail::abstract_validation_ctxt {
    typedef DataGraph dg_t;
    typedef DGEvaluator dg_eval_t;
    typedef ShapesGraph sg_t;
    typedef typename sg_t::graph_t sg_graph_t;

    typedef node<dg_t> dg_node_t;
    typedef resource<dg_t> dg_res_t;
    typedef literal<dg_t> dg_literal_t;
    typedef std::vector<node<dg_t>> dg_node_vec_t;
    typedef std::vector<resource<dg_t>> dg_resource_vec_t;
    typedef std::vector<literal<dg_t>> dg_literal_vec_t;
    typedef std::unordered_set<dg_node_t> dg_node_set_t;

    typedef fwd_viterator<dg_node_t> dg_node_it_t;
    typedef std::pair<dg_node_it_t, dg_node_it_t> dg_node_rng_t;

    typedef node<sg_graph_t> sg_node_t;
    typedef resource<sg_graph_t> sg_res_t;
    typedef shape<sg_graph_t> sg_shape_t;
    typedef literal<sg_graph_t> sg_literal_t;
    typedef std::vector<sg_node_t> sg_node_vec_t;
    typedef std::vector<sg_res_t> sg_resource_vec_t;
    typedef std::vector<sg_shape_t> sg_shape_vec_t;
    typedef std::vector<sg_literal_t> sg_literal_vec_t;
    typedef std::unordered_set<sg_node_t> sg_node_set_t;

    typedef fwd_viterator<sg_node_t> sg_node_it_t;
    typedef std::pair<sg_node_it_t, sg_node_it_t> sg_node_rng_t;

    typedef validator<validation_ctxt> validator_t;
    typedef std::shared_ptr<validator_t> validator_ptr_t;
    typedef boost::function<validator_ptr_t (
                const validation_ctxt&, const sg_res_t&) > validator_fac_t;
    typedef validation_result<dg_t, sg_t> validation_res_t;
    typedef validation_report<dg_t, sg_t> validation_report_t;
    typedef std::deque<validation_res_t> validation_res_vec_t;

    validation_ctxt() : dge(0), sg(0) {}
    validation_ctxt(dg_eval_t* dge, const sg_t* sg)
                    : dge(dge), sg(sg) {}
    ~validation_ctxt() = default;
    validation_ctxt(const validation_ctxt&) = default;
    validation_ctxt& operator=(const validation_ctxt&) = default;

    inline const dg_t& dg() const {return dge->graph();}

    dg_eval_t* dge;
    const sg_t* sg;
};
template<class X> using is_validation_ctxt
        = std::is_base_of<detail::abstract_validation_ctxt, X>;


}}  // namespace shacldator::shacl



#endif /* SRC_SHACL_VALIDATION_CTXT_HPP_ */
