/*
 * shape.hpp
 *
 *  Created on: Apr 19, 2018
 *      Author: alexis
 */

#ifndef SRC_SHACL_SHAPE_HPP_
#define SRC_SHACL_SHAPE_HPP_

#include "../graph/node.hpp"
#include "../qry/sol.hpp"

namespace shacldator {namespace shacl {

template<class Graph> struct shape : public resource<Graph> {
    shape() = default;
    shape(typename Graph::term_t term,
          const Graph& graph) : resource<Graph>(term, graph) {}
    explicit shape(const resource<Graph>& rhs) : resource<Graph>(rhs) {}
    shape(const shape&) = default;
    shape& operator=(const shape&) = default;
    ~shape() = default;
};

namespace priv {

template<class Term>
struct sol_shape_unwrapper {
    typedef qry::abstract_sol<Term> argument_type;
    typedef shape<typename Term::graph_type> result_type;
    result_type operator()(const argument_type& s) const {
        return result_type(qry::get_first_term(s).as_resource());
    }
};

template<class Graph> struct shape_upgrader : public
std::unary_function<const resource<Graph>&, shape<Graph>> {
    shape<Graph> operator()(const resource<Graph>& res) const {
        return shape<Graph>(res);
    }
};

}  // namespace priv

}}  // namespace shacldator::shacl



#endif /* SRC_SHACL_SHAPE_HPP_ */
