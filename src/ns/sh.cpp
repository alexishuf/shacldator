/*
 * sh.cpp
 *
 *  Created on: Mar 31, 2018
 *      Author: alexis
 */

#include <map>
#include <boost/preprocessor.hpp>
#include "priv/canonical_map.h"
#include "sh.hpp"

namespace shacldator{namespace sh{

const char* prefix = "http://www.w3.org/ns/shacl#";

#define SHACLDATOR_X(name) SHACLDATOR_X2(name, BOOST_PP_STRINGIZE(name))
#define SHACLDATOR_X2(name, realName) const char* name = \
    "http://www.w3.org/ns/shacl#" realName;
SHACLDATOR_SH_RESOURCES
#undef SHACLDATOR_X
#undef SHACLDATOR_X2

priv::canonical_map sh_map {
#define SHACLDATOR_X(name) SHACLDATOR_X2(name, "~")
#define SHACLDATOR_X2(name, rn) std::make_pair(std::string(name), name),
    SHACLDATOR_SH_RESOURCES
#undef SHACLDATOR_X
#undef SHACLDATOR_X2
};

const char* canonical(const std::string& s) {
    return sh_map.at(s);
}

}} //namespace shacldator::sh

