/*
 * rdfs.hpp
 *
 *  Created on: Apr 26, 2018
 *      Author: alexis
 */

#ifndef SRC_NS_RDFS_HPP_
#define SRC_NS_RDFS_HPP_


namespace shacldator {namespace rdfs {

extern const char* prefix;
#define SHACLDATOR_RDFS_RESOURCES \
        SHACLDATOR_X(Resource) \
        SHACLDATOR_X(Class) \
        SHACLDATOR_X(subClassOf) \
        SHACLDATOR_X(subPropertyOf) \
        SHACLDATOR_X(comment) \
        SHACLDATOR_X(label) \
        SHACLDATOR_X(domain) \
        SHACLDATOR_X(range) \
        SHACLDATOR_X(seeAlso) \
        SHACLDATOR_X(isDefinedBy) \
        SHACLDATOR_X(Literal) \
        SHACLDATOR_X(Container) \
        SHACLDATOR_X(ContainerMembershipProperty) \
        SHACLDATOR_X(member) \
        SHACLDATOR_X(Datatype)

#define SHACLDATOR_X(name) extern const char* name;
SHACLDATOR_RDFS_RESOURCES
#undef SHACLDATOR_X

}}  // namespace shacldator::rdfs


#endif /* SRC_NS_RDFS_HPP_ */
