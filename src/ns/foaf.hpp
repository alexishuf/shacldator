/*
 * foaf.hpp
 *
 *  Created on: Mar 26, 2018
 *      Author: alexis
 */

#ifndef SRC_NS_FOAF_HPP_
#define SRC_NS_FOAF_HPP_

namespace shacldator {namespace foaf {

extern const char* prefix;
#define SHACLDATOR_FOAF_RESOURCES \
    SHACLDATOR_X(knows) \
    SHACLDATOR_X(firstName) \
    SHACLDATOR_X(icqChatID) \
    SHACLDATOR_X(birthday) \
    SHACLDATOR_X(givenname) \
    SHACLDATOR_X(focus) \
    SHACLDATOR_X(accountServiceHomepage) \
    SHACLDATOR_X(isPrimaryTopicOf) \
    SHACLDATOR_X(depicts) \
    SHACLDATOR_X(phone) \
    SHACLDATOR_X(openid) \
    SHACLDATOR_X(givenName) \
    SHACLDATOR_X(Project) \
    SHACLDATOR_X(account) \
    SHACLDATOR_X(skypeID) \
    SHACLDATOR_X(Image) \
    SHACLDATOR_X(page) \
    SHACLDATOR_X(OnlineEcommerceAccount) \
    SHACLDATOR_X(lastName) \
    SHACLDATOR_X(membershipClass) \
    SHACLDATOR_X(yahooChatID) \
    SHACLDATOR_X(homepage) \
    SHACLDATOR_X(assurance) \
    SHACLDATOR_X(topic_interest) \
    SHACLDATOR_X(dnaChecksum) \
    SHACLDATOR_X(gender) \
    SHACLDATOR_X(mbox_sha1sum) \
    SHACLDATOR_X(logo) \
    SHACLDATOR_X(description) \
    SHACLDATOR_X(img) \
    SHACLDATOR_X(funded_by) \
    SHACLDATOR_X(interest) \
    SHACLDATOR_X(familyName) \
    SHACLDATOR_X(status) \
    SHACLDATOR_X(msnChatID) \
    SHACLDATOR_X(sha1) \
    SHACLDATOR_X(PersonalProfileDocument) \
    SHACLDATOR_X(workInfoHomepage) \
    SHACLDATOR_X(currentProject) \
    SHACLDATOR_X(mbox) \
    SHACLDATOR_X(schoolHomepage) \
    SHACLDATOR_X(Organization) \
    SHACLDATOR_X(holdsAccount) \
    SHACLDATOR_X(maker) \
    SHACLDATOR_X(pastProject) \
    SHACLDATOR_X(OnlineChatAccount) \
    SHACLDATOR_X(jabberID) \
    SHACLDATOR_X(accountName) \
    SHACLDATOR_X(name) \
    SHACLDATOR_X(OnlineAccount) \
    SHACLDATOR_X(tipjar) \
    SHACLDATOR_X(age) \
    SHACLDATOR_X(primaruTopic) \
    SHACLDATOR_X(myerBriggs) \
    SHACLDATOR_X(Agent) \
    SHACLDATOR_X(OnilneGamingAccount) \
    SHACLDATOR_X(depiction) \
    SHACLDATOR_X(workplaceHomepage) \
    SHACLDATOR_X(weblog) \
    SHACLDATOR_X(title) \
    SHACLDATOR_X(thumbnail) \
    SHACLDATOR_X(Person) \
    SHACLDATOR_X(made) \
    SHACLDATOR_X(LabelProperty) \
    SHACLDATOR_X(nick) \
    SHACLDATOR_X(based_near) \
    SHACLDATOR_X(surname) \
    SHACLDATOR_X(plan) \
    SHACLDATOR_X(aimChatID) \
    SHACLDATOR_X(group) \
    SHACLDATOR_X(geekcode) \
    SHACLDATOR_X(Document) \
    SHACLDATOR_X(topic) \
    SHACLDATOR_X(family_name) \
    SHACLDATOR_X(member) \
    SHACLDATOR_X(theme) \
    SHACLDATOR_X(publications)
#define SHACLDATOR_X(name) extern const char* name;
SHACLDATOR_FOAF_RESOURCES
#undef SHACLDATOR_X


}} //namespace shacldator::foaf


#endif /* SRC_NS_FOAF_HPP_ */
