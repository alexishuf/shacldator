/*
 * rdfs.cpp
 *
 *  Created on: Apr 26, 2018
 *      Author: alexis
 */

#include "rdfs.hpp"
#include <boost/preprocessor/stringize.hpp>

namespace shacldator {namespace rdfs {

const char* prefix = "http://www.w3.org/2000/01/rdf-schema#";
#define SHACLDATOR_X(name) const char* name \
    = "http://www.w3.org/2000/01/rdf-schema#" BOOST_PP_STRINGIZE(name);
SHACLDATOR_RDFS_RESOURCES
#undef SHACLDATOR_X

}} //namespace shacldator::rdfs


