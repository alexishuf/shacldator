/*
 * xsd.hpp
 *
 *  Created on: Mar 26, 2018
 *      Author: alexis
 */

#ifndef SRC_NS_XSD_HPP_
#define SRC_NS_XSD_HPP_

#include <string>
#include <boost/preprocessor.hpp>


namespace shacldator {
template<class G> struct node;

namespace xsd{

#define SHACLDATOR_XSD_TYPES \
    SHACLDATOR_X(string) \
    SHACLDATOR_X(normalizedString) \
    SHACLDATOR_X(boolean) \
    SHACLDATOR_X(float) \
    SHACLDATOR_X(double) \
    SHACLDATOR_X(int) \
    SHACLDATOR_X(short) \
    SHACLDATOR_X(long) \
    SHACLDATOR_X(unsignedInt) \
    SHACLDATOR_X(unsignedShort) \
    SHACLDATOR_X(unsignedLong) \
    SHACLDATOR_X(decimal) \
    SHACLDATOR_X(integer) \
    SHACLDATOR_X(nonNegativeInteger) \
    SHACLDATOR_X(positiveInteger) \
    SHACLDATOR_X(nonPositiveInteger)

extern const char* prefix;
#define SHACLDATOR_X(name) extern const char* BOOST_PP_CAT(name, _);
    SHACLDATOR_XSD_TYPES
#undef SHACLDATOR_X

const char* canonical(const std::string& s);
template<class G> const char* canonical(const node<G>& n) {
    return canonical(n.lex());
}
bool is_signed(const char* canonical_uri);
bool is_unsigned(const char* canonical_uri);


}} //namespace shacldator::xsd



#endif /* SRC_NS_XSD_HPP_ */
