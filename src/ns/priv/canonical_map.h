/*
 * canonicalmap.h
 *
 *  Created on: Apr 29, 2018
 *      Author: alexis
 */

#ifndef SRC_NS_PRIV_CANONICAL_MAP_H_
#define SRC_NS_PRIV_CANONICAL_MAP_H_

#include <string>
#include <map>
#include <initializer_list>

namespace shacldator {namespace priv {

class canonical_map : protected std::map<std::string, const char*> {
private:
    typedef std::map<std::string, const char*> base_t;
public:
    canonical_map(const std::initializer_list<value_type>& init);
    virtual ~canonical_map() = default;
    const char* at(const key_type& k) const;
    const char* operator[](const key_type& k) const {return at(k);}
};

}}  // namespace shacldator::priv

#endif /* SRC_NS_PRIV_CANONICAL_MAP_H_ */
