/*
 * canonicalmap.cpp
 *
 *  Created on: Apr 29, 2018
 *      Author: alexis
 */

#include <sstream>
#include "canonical_map.h"

namespace shacldator {namespace priv {

canonical_map::canonical_map(const std::initializer_list<value_type>& list)
                             : base_t(list) {
}

const char* canonical_map::at(const key_type& k) const {
    auto it = find(k);
    if (it == end()) {
        std::stringstream ss;
        ss << "No key \"" << k << "\" in canonical_map";
        throw std::out_of_range(ss.str());
    }
    return it->second;
}

}}  // namespace shacldator::priv
