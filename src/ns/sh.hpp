/*
 * sh.hpp
 *
 *  Created on: Mar 31, 2018
 *      Author: alexis
 */

#ifndef SRC_NS_SH_HPP_
#define SRC_NS_SH_HPP_

#include <string>

namespace shacldator{

template<class Graph> struct node;

namespace sh{

const char* canonical(const std::string& s);
template<class Graph> const char* canonical(const shacldator::node<Graph>& n) {
    return canonical(n.lex());
}

extern const char* prefix;
#define SHACLDATOR_SH_RESOURCES \
    SHACLDATOR_X(Shape) \
    SHACLDATOR_X(NodeShape) \
    SHACLDATOR_X(PropertyShape) \
    SHACLDATOR_X(deactivated) \
    SHACLDATOR_X(targetClass) \
    SHACLDATOR_X(targetNode) \
    SHACLDATOR_X(targetObjectsOf) \
    SHACLDATOR_X(targetSubjectsOf) \
    SHACLDATOR_X(message) \
    SHACLDATOR_X(severity) \
    SHACLDATOR_X(NodeKind) \
    SHACLDATOR_X(BlankNode) \
    SHACLDATOR_X(BlankNodeOrIRI) \
    SHACLDATOR_X(BlankNodeOrLiteral) \
    SHACLDATOR_X(IRI) \
    SHACLDATOR_X(IRIOrLiteral) \
    SHACLDATOR_X(Literal) \
    SHACLDATOR_X(ValidationReport) \
    SHACLDATOR_X(conforms) \
    SHACLDATOR_X(result) \
    SHACLDATOR_X(shapesGraphWellFormed) \
    SHACLDATOR_X(AbstractResult) \
    SHACLDATOR_X(ValidationResult) \
    SHACLDATOR_X(Severity) \
    SHACLDATOR_X(Info) \
    SHACLDATOR_X(Violation) \
    SHACLDATOR_X(Warning) \
    SHACLDATOR_X(detail) \
    SHACLDATOR_X(focusNode) \
    SHACLDATOR_X(resultMessage) \
    SHACLDATOR_X(resultPath) \
    SHACLDATOR_X(resultSeverity) \
    SHACLDATOR_X(sourceConstraint) \
    SHACLDATOR_X(sourceShape) \
    SHACLDATOR_X(sourceConstraintComponent) \
    SHACLDATOR_X(value) \
    SHACLDATOR_X(shapesGraph) \
    SHACLDATOR_X(suggestedShapesGraph) \
    SHACLDATOR_X(entailment) \
    SHACLDATOR_X(path) \
    SHACLDATOR_X(inversePath) \
    SHACLDATOR_X(alternativePath) \
    SHACLDATOR_X(zeroOrMorePath) \
    SHACLDATOR_X(oneOrMorePath) \
    SHACLDATOR_X(zeroOrOnePath) \
    SHACLDATOR_X(Parameterizable) \
    SHACLDATOR_X(parameter) \
    SHACLDATOR_X(labelTemplate) \
    SHACLDATOR_X(Parameter) \
    SHACLDATOR_X(optional) \
    SHACLDATOR_X(ConstraintComponent) \
    SHACLDATOR_X(validator) \
    SHACLDATOR_X(nodeValidator) \
    SHACLDATOR_X(propertyValidator) \
    SHACLDATOR_X(Validator) \
    SHACLDATOR_X(SPARQLAskValidator) \
    SHACLDATOR_X(SPARQLSelectValidator) \
    SHACLDATOR_X(AndConstraintComponent) \
    SHACLDATOR_X2(AndConstraintComponent_and, "AndConstraintComponent-and") \
    SHACLDATOR_X2(and_, "and") \
    SHACLDATOR_X(ClassConstraintComponent) \
    SHACLDATOR_X2(ClassConstraintComponent_class, "ClassConstraintComponent-class") \
    SHACLDATOR_X2(class_, "class") \
    SHACLDATOR_X(ClosedConstraintComponent) \
    SHACLDATOR_X2(ClosedConstraintComponent_closed, "ClosedConstraintComponent-closed") \
    SHACLDATOR_X2(ClosedConstraintComponent_ignoredProperties, "ClosedConstraintComponent-ignoredProperties") \
    SHACLDATOR_X(closed) \
    SHACLDATOR_X(ignoredProperties) \
    SHACLDATOR_X(DatatypeConstraintComponent) \
    SHACLDATOR_X2(DatatypeConstraintComponent_datatype, "DatatypeConstraintComponent-datatype") \
    SHACLDATOR_X(datatype) \
    SHACLDATOR_X(DisjointConstraintComponent) \
    SHACLDATOR_X2(DisjointConstraintComponent_disjoint, "DisjointConstraintComponent-disjoint") \
    SHACLDATOR_X(disjoint) \
    SHACLDATOR_X(EqualsConstraintComponent) \
    SHACLDATOR_X2(EqualsConstraintComponent_equals, "EqualsConstraintComponent-equals") \
    SHACLDATOR_X(equals) \
    SHACLDATOR_X(HasValueConstraintComponent) \
    SHACLDATOR_X2(HasValueConstraintComponent_hasValue, "HasValueConstraintComponent-hasValue") \
    SHACLDATOR_X(hasValue) \
    SHACLDATOR_X(InConstraintComponent) \
    SHACLDATOR_X2(InConstraintComponent_in, "InConstraintComponent-in") \
    SHACLDATOR_X(in) \
    SHACLDATOR_X(LanguageInConstraintComponent) \
    SHACLDATOR_X2(LanguageInConstraintComponent_languageIn, "LanguageInConstraintComponent-languageIn") \
    SHACLDATOR_X(languageIn) \
    SHACLDATOR_X(LessThanConstraintComponent) \
    SHACLDATOR_X2(LessThanConstraintComponent_lessThan, "LessThanConstraintComponent-lessThan") \
    SHACLDATOR_X(lessThan) \
    SHACLDATOR_X(LessThanOrEqualsConstraintComponent) \
    SHACLDATOR_X2(LessThanOrEqualsConstraintComponent_lessThanOrEquals, "LessThanOrEqualsConstraintComponent-lessThanOrEquals") \
    SHACLDATOR_X(lessThanOrEquals) \
    SHACLDATOR_X(MaxCountConstraintComponent) \
    SHACLDATOR_X2(MaxCountConstraintComponent_maxCount, "MaxCountConstraintComponent-maxCount") \
    SHACLDATOR_X(maxCount) \
    SHACLDATOR_X(MaxExclusiveConstraintComponent) \
    SHACLDATOR_X2(MaxExclusiveConstraintComponent_maxExclusive, "MaxExclusiveConstraintComponent-maxExclusive") \
    SHACLDATOR_X(maxExclusive) \
    SHACLDATOR_X(MaxInclusiveConstraintComponent) \
    SHACLDATOR_X2(MaxInclusiveConstraintComponent_maxInclusive, "MaxInclusiveConstraintComponent-maxInclusive") \
    SHACLDATOR_X(maxInclusive) \
    SHACLDATOR_X(MaxLengthConstraintComponent) \
    SHACLDATOR_X2(MaxLengthConstraintComponent_maxLength, "MaxLengthConstraintComponent-maxLength") \
    SHACLDATOR_X(maxLength) \
    SHACLDATOR_X(MinCountConstraintComponent) \
    SHACLDATOR_X2(MinCountConstraintComponent_minCount, "MinCountConstraintComponent-minCount") \
    SHACLDATOR_X(minCount) \
    SHACLDATOR_X(MinExclusiveConstraintComponent) \
    SHACLDATOR_X2(MinExclusiveConstraintComponent_minExclusive, "MinExclusiveConstraintComponent-minExclusive") \
    SHACLDATOR_X(minExclusive) \
    SHACLDATOR_X(MinInclusiveConstraintComponent) \
    SHACLDATOR_X2(MinInclusiveConstraintComponent_minInclusive, "MinInclusiveConstraintComponent-minInclusive") \
    SHACLDATOR_X(minInclusive) \
    SHACLDATOR_X(MinLengthConstraintComponent) \
    SHACLDATOR_X2(MinLengthConstraintComponent_minLength, "MinLengthConstraintComponent-minLength") \
    SHACLDATOR_X(minLength) \
    SHACLDATOR_X(NodeConstraintComponent) \
    SHACLDATOR_X2(NodeConstraintComponent_node, "NodeConstraintComponent-node") \
    SHACLDATOR_X(node) \
    SHACLDATOR_X(NodeKindConstraintComponent) \
    SHACLDATOR_X2(NodeKindConstraintComponent_nodeKind, "NodeKindConstraintComponent-nodeKind") \
    SHACLDATOR_X(nodeKind) \
    SHACLDATOR_X(NotConstraintComponent) \
    SHACLDATOR_X2(NotConstraintComponent_not, "NotConstraintComponent-not") \
    SHACLDATOR_X2(not_, "not") \
    SHACLDATOR_X(OrConstraintComponent) \
    SHACLDATOR_X2(OrConstraintComponent_or, "OrConstraintComponent-or") \
    SHACLDATOR_X2(or_, "or") \
    SHACLDATOR_X(PatternConstraintComponent) \
    SHACLDATOR_X2(PatternConstraintComponent_pattern, "PatternConstraintComponent-pattern") \
    SHACLDATOR_X2(PatternConstraintComponent_flags, "PatternConstraintComponent-flags") \
    SHACLDATOR_X(flags) \
    SHACLDATOR_X(pattern) \
    SHACLDATOR_X(PropertyConstraintComponent) \
    SHACLDATOR_X2(PropertyConstraintComponent_property, "PropertyConstraintComponent-property") \
    SHACLDATOR_X(property) \
    SHACLDATOR_X(QualifiedMaxCountConstraintComponent) \
    SHACLDATOR_X2(QualifiedMaxCountConstraintComponent_qualifiedMaxCount, "QualifiedMaxCountConstraintComponent-qualifiedMaxCount") \
    SHACLDATOR_X2(QualifiedMaxCountConstraintComponent_qualifiedValueShape, "QualifiedMaxCountConstraintComponent-qualifiedValueShape") \
    SHACLDATOR_X2(QualifiedMaxCountConstraintComponent_qualifiedValueShapesDisjoint, "QualifiedMaxCountConstraintComponent-qualifiedValueShapesDisjoint") \
    SHACLDATOR_X(QualifiedMinCountConstraintComponent) \
    SHACLDATOR_X2(QualifiedMinCountConstraintComponent_qualifiedMinCount, "QualifiedMinCountConstraintComponent-qualifiedMinCount") \
    SHACLDATOR_X2(QualifiedMinCountConstraintComponent_qualifiedValueShape, "QualifiedMinCountConstraintComponent-qualifiedValueShape") \
    SHACLDATOR_X2(QualifiedMinCountConstraintComponent_qualifiedValueShapesDisjoint, "QualifiedMinCountConstraintComponent-qualifiedValueShapesDisjoint") \
    SHACLDATOR_X(qualifiedMaxCount) \
    SHACLDATOR_X(qualifiedMinCount) \
    SHACLDATOR_X(qualifiedValueShape) \
    SHACLDATOR_X(qualifiedValueShapesDisjoint) \
    SHACLDATOR_X(UniqueLangConstraintComponent) \
    SHACLDATOR_X2(UniqueLangConstraintComponent_uniqueLang, "UniqueLangConstraintComponent-uniqueLang") \
    SHACLDATOR_X(uniqueLang) \
    SHACLDATOR_X(XoneConstraintComponent) \
    SHACLDATOR_X2(XoneConstraintComponent_xone, "XoneConstraintComponent-xone") \
    SHACLDATOR_X(xone) \
    SHACLDATOR_X(SPARQLExecutable) \
    SHACLDATOR_X(SPARQLAskExecutable) \
    SHACLDATOR_X(ask) \
    SHACLDATOR_X(SPARQLConstructExecutable) \
    SHACLDATOR_X(construct) \
    SHACLDATOR_X(SPARQLSelectExecutable) \
    SHACLDATOR_X(select) \
    SHACLDATOR_X(SPARQLUpdateExecutable) \
    SHACLDATOR_X(update) \
    SHACLDATOR_X(prefixes) \
    SHACLDATOR_X(PrefixDeclaration) \
    SHACLDATOR_X(declare) \
    SHACLDATOR_X2(prefix_, "prefix") \
    SHACLDATOR_X2(namespace_, "namespace") \
    SHACLDATOR_X(SPARQLConstraintComponent) \
    SHACLDATOR_X2(SPARQLConstraintComponent_sparql, "SPARQLConstraintComponent-sparql") \
    SHACLDATOR_X(sparql) \
    SHACLDATOR_X(SPARQLConstraint) \
    SHACLDATOR_X(defaultValue) \
    SHACLDATOR_X(description) \
    SHACLDATOR_X(group) \
    SHACLDATOR_X(name) \
    SHACLDATOR_X(order) \
    SHACLDATOR_X(PropertyGroup) \
    SHACLDATOR_X(target) \
    SHACLDATOR_X(Target) \
    SHACLDATOR_X(TargetType) \
    SHACLDATOR_X(SPARQLTarget) \
    SHACLDATOR_X(SPARQLTargetType) \
    SHACLDATOR_X(Function) \
    SHACLDATOR_X(returnType) \
    SHACLDATOR_X(SPARQLFunction) \
    SHACLDATOR_X(resultAnnotation) \
    SHACLDATOR_X(ResultAnnotation) \
    SHACLDATOR_X(annotationProperty) \
    SHACLDATOR_X(annotationValue) \
    SHACLDATOR_X(annotationVarName) \
    SHACLDATOR_X2(this_, "this") \
    SHACLDATOR_X(filterShape) \
    SHACLDATOR_X(nodes) \
    SHACLDATOR_X(intersection) \
    SHACLDATOR_X2(union_, "union") \
    SHACLDATOR_X(ExpressionConstraintComponent) \
    SHACLDATOR_X(ExpressionConstraintComponent_expression) \
    SHACLDATOR_X(expression) \
    SHACLDATOR_X(Rule) \
    SHACLDATOR_X(rule) \
    SHACLDATOR_X(condition) \
    SHACLDATOR_X(TripleRule) \
    SHACLDATOR_X(subject) \
    SHACLDATOR_X(predicate) \
    SHACLDATOR_X(object) \
    SHACLDATOR_X(SPARQLRule) \
    SHACLDATOR_X(JSExecutable) \
    SHACLDATOR_X(JSTarget) \
    SHACLDATOR_X(JSTargetType) \
    SHACLDATOR_X(JSConstraint) \
    SHACLDATOR_X(JSConstraintComponent) \
    SHACLDATOR_X(JSConstraint_js) \
    SHACLDATOR_X(js) \
    SHACLDATOR_X(jsFunctionName) \
    SHACLDATOR_X(jsLibrary) \
    SHACLDATOR_X(jsLibraryURL) \
    SHACLDATOR_X(JSFunction) \
    SHACLDATOR_X(JSLibrary) \
    SHACLDATOR_X(JSRule) \
    SHACLDATOR_X(JSValidator)
#define  SHACLDATOR_X(name)           extern const char* name;
#define SHACLDATOR_X2(name, realName) extern const char* name;
SHACLDATOR_SH_RESOURCES
#undef SHACLDATOR_X
#undef SHACLDATOR_X2

}} //namespace shacldator::sh



#endif /* SRC_NS_SH_HPP_ */
