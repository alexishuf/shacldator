/*
 * owl.cpp
 *
 *  Created on: Apr 21, 2018
 *      Author: alexis
 */

#include "owl.hpp"
#include <boost/preprocessor.hpp>

namespace shacldator {namespace owl {

const char* prefix = "http://www.w3.org/2002/07/owl#";

#define SHACLDATOR_X(name) const char* name \
    = "http://www.w3.org/2002/07/owl#" BOOST_PP_STRINGIZE(name);
SHACLDATOR_OWL_RESOURCES
#undef SHACLDATOR_X

}}  // namespace shacldator::owl


