/*
 * rdf.cpp
 *
 *  Created on: Mar 26, 2018
 *      Author: alexis
 */

#include "rdf.hpp"
#include <boost/preprocessor.hpp>

namespace shacldator {namespace rdf {


const char* prefix = "http://www.w3.org/1999/02/22-rdf-syntax-ns#";

#define SHACLDATOR_X(name) const char* name \
    = "http://www.w3.org/1999/02/22-rdf-syntax-ns#" BOOST_PP_STRINGIZE(name);
SHACLDATOR_RDF_RESOURCES
#undef SHACLDATOR_X

}} // namespace shacldator::rdf
