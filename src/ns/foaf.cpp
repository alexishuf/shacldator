/*
 * foaf.cpp
 *
 *  Created on: Mar 26, 2018
 *      Author: alexis
 */

#include "foaf.hpp"
#include <boost/preprocessor.hpp>

namespace shacldator {namespace foaf {

const char* prefix = "http://xmlns.com/foaf/0.1/";
#define SHACLDATOR_X(name) const char* name = \
        "http://xmlns.com/foaf/0.1/" BOOST_PP_STRINGIZE(name);
SHACLDATOR_FOAF_RESOURCES
#undef SHACLDATOR_X

}}  // namespace shacldator::foaf


