/*
 * xsd.cpp
 *
 *  Created on: Mar 26, 2018
 *      Author: alexis
 */

#include <set>
#include "xsd.hpp"
#include "priv/canonical_map.h"

namespace shacldator {namespace xsd {

const char* prefix = "http://www.w3.org/2001/XMLSchema#";

#define SHACLDATOR_X(name) const char* BOOST_PP_CAT(name, _) \
        = "http://www.w3.org/2001/XMLSchema#" BOOST_PP_STRINGIZE(name);
SHACLDATOR_XSD_TYPES
#undef SHACLDATOR_X

priv::canonical_map xsd_map {
#define SHACLDATOR_X(name) \
    std::make_pair(std::string(BOOST_PP_CAT(name, _)), BOOST_PP_CAT(name, _)),
SHACLDATOR_XSD_TYPES
#undef SHACLDATOR_X
};
const char* canonical(const std::string& s) {
    return xsd_map.at(s);
}

std::set<const char*> xsd_signed{
    float_, double_, int_, short_, long_, decimal_, integer_,
    nonPositiveInteger_
};
std::set<const char*> xsd_unsigned{
    unsignedInt_, unsignedShort_, unsignedLong_, nonNegativeInteger_,
    positiveInteger_
};

bool is_signed(const char* canonical_uri) {
    return xsd_signed.count(canonical_uri);
}
bool is_unsigned(const char* canonical_uri) {
    return xsd_unsigned.count(canonical_uri);
}

}}  // namespace shacldator::xsd
