/*
 * rdf.hpp
 *
 *  Created on: Mar 26, 2018
 *      Author: alexis
 */

#ifndef SRC_NS_RDF_HPP_
#define SRC_NS_RDF_HPP_

namespace shacldator {namespace rdf {

extern const char* prefix;
#define SHACLDATOR_RDF_RESOURCES \
    SHACLDATOR_X(HTML) \
    SHACLDATOR_X(langString) \
    SHACLDATOR_X(PlainLiteral) \
    SHACLDATOR_X(type) \
    SHACLDATOR_X(Property) \
    SHACLDATOR_X(Statement) \
    SHACLDATOR_X(subject) \
    SHACLDATOR_X(predicate) \
    SHACLDATOR_X(object) \
    SHACLDATOR_X(Bag) \
    SHACLDATOR_X(Seq) \
    SHACLDATOR_X(Alt) \
    SHACLDATOR_X(List) \
    SHACLDATOR_X(nil) \
    SHACLDATOR_X(first) \
    SHACLDATOR_X(rest) \
    SHACLDATOR_X(XMLLiteral)
#define SHACLDATOR_X(name) extern const char* name;
SHACLDATOR_RDF_RESOURCES
#undef SHACLDATOR_X


}} //namespace shacldator::rdf



#endif /* SRC_NS_RDF_HPP_ */
