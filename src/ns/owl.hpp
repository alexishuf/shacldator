/*
 * owl.hpp
 *
 *  Created on: Apr 21, 2018
 *      Author: alexis
 */

#ifndef SRC_NS_OWL_HPP_
#define SRC_NS_OWL_HPP_


namespace shacldator {namespace owl{

extern const char* prefix;
#define SHACLDATOR_OWL_RESOURCES \
        SHACLDATOR_X(AllDifferent) \
        SHACLDATOR_X(AllDisjointClasses) \
        SHACLDATOR_X(AllDisjointProperties) \
        SHACLDATOR_X(Annotation) \
        SHACLDATOR_X(AnnotationProperty) \
        SHACLDATOR_X(AsymmetricProperty) \
        SHACLDATOR_X(Axiom) \
        SHACLDATOR_X(Class) \
        SHACLDATOR_X(DataRange) \
        SHACLDATOR_X(DatatypeProperty) \
        SHACLDATOR_X(DeprecatedClass) \
        SHACLDATOR_X(DeprecatedProperty) \
        SHACLDATOR_X(FunctionalProperty) \
        SHACLDATOR_X(InverseFunctionalProperty) \
        SHACLDATOR_X(IrreflexiveProperty) \
        SHACLDATOR_X(NamedIndividual) \
        SHACLDATOR_X(NegativePropertyAssertion) \
        SHACLDATOR_X(Nothing) \
        SHACLDATOR_X(ObjectProperty) \
        SHACLDATOR_X(Ontology) \
        SHACLDATOR_X(OntologyProperty) \
        SHACLDATOR_X(ReflexiveProperty) \
        SHACLDATOR_X(Restriction) \
        SHACLDATOR_X(SymmetricProperty) \
        SHACLDATOR_X(TransitiveProperty) \
        SHACLDATOR_X(Thing) \
        SHACLDATOR_X(allValuesFrom) \
        SHACLDATOR_X(annotatedProperty) \
        SHACLDATOR_X(annotatedSource) \
        SHACLDATOR_X(annotatedTarget) \
        SHACLDATOR_X(assertionProperty) \
        SHACLDATOR_X(backwardCompatibleWith) \
        SHACLDATOR_X(bottomDataProperty) \
        SHACLDATOR_X(bottomObjectProperty) \
        SHACLDATOR_X(cardinality) \
        SHACLDATOR_X(complementOf) \
        SHACLDATOR_X(datatypeComplementOf) \
        SHACLDATOR_X(deprecated) \
        SHACLDATOR_X(differentFrom) \
        SHACLDATOR_X(disjointUnionOf) \
        SHACLDATOR_X(disjointWith) \
        SHACLDATOR_X(distinctMembers) \
        SHACLDATOR_X(equivalentClass) \
        SHACLDATOR_X(equivalentProperty) \
        SHACLDATOR_X(hasKey) \
        SHACLDATOR_X(hasSelf) \
        SHACLDATOR_X(hasValue) \
        SHACLDATOR_X(imports) \
        SHACLDATOR_X(incompatibleWith) \
        SHACLDATOR_X(intersectionOf) \
        SHACLDATOR_X(inverseOf) \
        SHACLDATOR_X(maxCardinality) \
        SHACLDATOR_X(maxQualifiedCardinality) \
        SHACLDATOR_X(members) \
        SHACLDATOR_X(minCardinality) \
        SHACLDATOR_X(minQualifiedCardinality) \
        SHACLDATOR_X(onClass) \
        SHACLDATOR_X(onDataRange) \
        SHACLDATOR_X(onDatatype) \
        SHACLDATOR_X(oneOf) \
        SHACLDATOR_X(onProperties) \
        SHACLDATOR_X(onProperty) \
        SHACLDATOR_X(priorVersion) \
        SHACLDATOR_X(propertyChainAxiom) \
        SHACLDATOR_X(propertyDisjointWith) \
        SHACLDATOR_X(qualifiedCardinality) \
        SHACLDATOR_X(sameAs) \
        SHACLDATOR_X(someValuesFrom) \
        SHACLDATOR_X(sourceIndividual) \
        SHACLDATOR_X(targetIndividual) \
        SHACLDATOR_X(targetValue) \
        SHACLDATOR_X(topDataProperty) \
        SHACLDATOR_X(topObjectProperty) \
        SHACLDATOR_X(unionOf) \
        SHACLDATOR_X(versionInfo) \
        SHACLDATOR_X(versionIRI) \
        SHACLDATOR_X(withRestrictions)
#define SHACLDATOR_X(name) extern const char* name;
SHACLDATOR_OWL_RESOURCES
#undef SHACLDATOR_X
}} //namespace shacldator::owl


#endif /* SRC_NS_OWL_HPP_ */
