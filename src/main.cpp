/*
 * main.cpp
 *
 *  Created on: Mar 24, 2018
 *      Author: alexis
 */

#include <iostream>
#include <fstream>
#include <map>
#include <boost/program_options.hpp>
#include <boost/range/algorithm.hpp>

#include "shacldator_runner.hpp"

using namespace std;
using namespace shacldator;


int main(int argc, char** argv) {
    using boost::for_each;
    namespace po = boost::program_options;
    po::options_description od("All options");
    od.add_options()
            ("help", "Show help")
            ("dg", po::value<string>(), "Data graph")
            ("sg", po::value<string>(), "Shapes graph")
            ("out", po::value<string>()->default_value("-"),
                    "Output file")
            ("out-times", po::value<string>()->default_value(""),
                    "Output of times file. Disabled by default")
            ("format", po::value<string>()->default_value("ntriples"),
                       "RDF format of output graph")
            ("validator", po::value<string>()->default_value("serial"),
                          "Validator implementation. "
                          "Currently: serial, omp or none")
            ("np", po::value<int>()->default_value(-1),
                  "Number of processors to use for parallel validation. "
                  "-1 uses all cores available")
            ("inf", po::value<string>()->default_value("none"),
                   "Inference level: none (default) rdfs, rdfs- "
                   "or rdfs-- (recommended)")
            ("inf-opt", po::value<vector<string>>()->composing(),
                    "Options, with syntax name[=value] to be forwarded to "
                    "the reasoner");
    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, od), vm);
    po::notify(vm);

    int mo = 0;
    for_each(vector<string>{"dg", "sg"}, [&](string n)
            {if (!vm.count(n)) {cout << n << " is required!" << endl; mo++;}});
    if (mo > 0 || vm.count("help")) {
        cout << od << endl;
        return mo > 0 ? 1 : 0;
    }

    shacldator_runner r;
    try {
        r.set_dg(vm["dg"].as<string>());
        r.set_sg(vm["sg"].as<string>());
        r.set_inf(vm["inf"].as<string>());
        if (vm.count("inf-opt")) {
            for_each(vm["inf-opt"].as<vector<string>>(), [&](const string& s) {
                size_t p = s.find_first_of("=");
                if (p == string::npos)
                    r.set_inf_opt(s.substr(0, p));
                else
                    r.set_inf_opt(s.substr(0, p), s.substr(p+1));
            });
        }
        r.set_validator(vm["validator"].as<string>());
        r.set_nproc(vm["np"].as<int>());

        std::unique_ptr<std::ostream> my_ostream, my_times_ostream;
        string out = vm["out"].as<string>();
        if (out.empty() || out == "-") {
            r.set_out(std::cout);
        } else {
            my_ostream.reset(new std::ofstream(out.c_str(),
                                           ios_base::out|ios_base::trunc));
            r.set_out(*my_ostream);
        }
        string out_times = vm["out-times"].as<string>();
        if (out_times == "-") {
            r.set_times_out(std::cout);
        } else if (!out_times.empty()) {
            my_times_ostream.reset(new std::ofstream(out_times,
                                               ios_base::out|ios_base::trunc));
            r.set_times_out(*my_times_ostream);
        }

        try {
            r.run();
        } catch (const std::exception& e) {
            cerr << "Exception during execution: " << e.what() << endl;
            return 3;
        }
    } catch (const std::exception& e) {
        cerr << "Exception during configuration: " << e.what() << endl;
        return 2;
    }


    return 0;
}

