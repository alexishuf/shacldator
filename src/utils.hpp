/*
 * utils.hpp
 *
 *  Created on: Apr 2, 2018
 *      Author: alexis
 */

#ifndef SRC_UTILS_HPP_
#define SRC_UTILS_HPP_

#include <mutex>
#include <condition_variable>
#include <utility>
#include <type_traits>
#include <sstream>
#include <boost/range.hpp>
#include <boost/iterator/transform_iterator.hpp>


namespace shacldator {

template<class T>
struct identity_f : public std::unary_function<T, T> {
    T operator()(T in) const {return in;}
};

struct sink {
    template<class A1> void operator()(const A1& a1) const {}
    template<class A1, class A2>
    void operator()(const A1& a1, const A2& a2) const {}
    template<class A1, class A2, class A3>
    void operator()(const A1& a1, const A2& a2, const A3& a3) const {}
};

struct get_false {
    template<class A1> bool operator()(const A1& a1) const {return false;}
    template<class A1, class A2>
    bool operator()(const A1& a1, const A2& a2) const {return false;}
    template<class A1, class A2, class A3>
    bool operator()(const A1& a1, const A2& a2, const A3& a3) const {
        return false;
    }
};

struct get_true {
    template<class A1> bool operator()(const A1& a1) const {return true;}
    template<class A1, class A2>
    bool operator()(const A1& a1, const A2& a2) const {return true;}
    template<class A1, class A2, class A3>
    bool operator()(const A1& a1, const A2& a2, const A3& a3) const {
        return true;
    }
};

template<bool select, class T>
struct selector_if {
    static T run(const T& arg, const T& fallback) {return fallback;}
};
template<class T>
struct selector_if<true, T> {
    static T run(const T& arg, const T& fallback) {return arg;}
};

template<bool select, class T>
T select_if(const T& arg, const T& fallback) {
    return selector_if<select, T>::run(arg, fallback);
}


template<class Predicate, class A1, class A2,
         int n = Predicate::template apply<A1>::value ? 1
                 : (Predicate::template apply<A2>::value ? 2 : 0)>
struct selector_first {
    typedef void type;
    static void run(const A1& a1, const A2& a2) {}
};
template<class Predicate, class A1, class A2>
struct selector_first<Predicate, A1, A2, 1> {
    typedef A1 type;
    static A1 run(const A1& a1, const A2& a2) {return a1;}
};
template<class Predicate, class A1, class A2>
struct selector_first<Predicate, A1, A2, 2> {
    typedef A2 type;
    static A2 run(const A1& a1, const A2& a2) {return a2;}
};

template<class Predicate, class A1, class A2>
typename selector_first<Predicate, A1, A2>::type
select_first(const A1& a1, const A2& a2) {
    return selector_first<Predicate, A1, A2>::run(a1, a2);
}


template<class Predicate, class UnaryFunction,
         class    Arg = typename UnaryFunction::argument_type,
         class Result = typename UnaryFunction::result_type,
         bool     sel = Predicate::template apply<Arg>::value>
struct transformer_if_else {
    static Result run(const UnaryFunction& f, const Arg& arg,
                      const Result& fallback) {
        return fallback;
    }
};
template<class Predicate, class UnaryFunction, class Arg, class Result>
struct transformer_if_else<Predicate, UnaryFunction, Arg, Result, true> {
    static Result run(const UnaryFunction& f, const Arg& arg,
                      const Result& fallback) {
        return f(arg);
    }
};

template<class Predicate, class UnaryF, class Arg, class Result>
Result transform_if_else(const UnaryF& f, const Arg& a,
                         const Result& fb) {
    return transformer_if_else<Predicate, UnaryF, Arg, Result>::run(f, a, fb);
}

template<class T, bool construct=std::is_default_constructible<T>::value>
struct pack {
    typedef T packed_type;
    pack()             {                  buf[sizeof(T)] = 0;}
    pack(const T& obj) {new (buf) T(obj); buf[sizeof(T)] = 1;}
    ~pack() {unset();}
    pack(const pack& o) {
        if (o) new (buf) T(o.get());
        buf[sizeof(T)] = o ? 1 : 0;
    }
    pack(pack&& o) {
        if (o) new (buf) T(std::move(o.get()));
        buf[sizeof(T)] = o ? 1 : 0;
    }
    pack& operator=(const pack& o) {
        return this == &o ? *this : (o ? set(o.get()) : unset());
    }
    pack& operator=(pack&& o) {
        return this == &o ? *this : (o ? set(std::move(o.get())) : unset());
    }

    operator bool() const {return buf[sizeof(T)];}

    bool operator==(const pack& o) const {
        return bool(*this) == bool(o) && (!*this || get() == o.get());
    }
    bool operator!=(const pack& o) const {return !operator==(o);}

    pack& unset() {
        if (*this) {
            get().~T();
            buf[sizeof(T)] = 0;
        }
        return *this;
    }
    T& initialized() {
        if (!*this) {
            new (buf) T();
            buf[sizeof(T)] = 1;
        }
        return get();
    }
    pack& set(T&&  obj) {
        if (*this) get() = std::move(obj);
        else       new (buf) T(std::move(obj));
        buf[sizeof(T)] = 1;
        return *this;
    }
    pack& set(const T&  obj) {
        if (*this) get() = obj;
        else       new (buf) T(obj);
        buf[sizeof(T)] = 1;
        return *this;
    }
    template<class... Ts> pack& emplace(Ts&&... ts) {
        if (*this) {get().~T();}
        new (buf) T(std::forward<Ts>(ts)...);
        buf[sizeof(T)] = 1;
        return *this;
    }
    const T& get() const {
        assert(bool(*this));
        return *reinterpret_cast<const T*>(buf);
    }
    T& get() {
        assert(bool(*this));
        return *reinterpret_cast<T*>(buf);
    }
private:
    char buf[sizeof(T)+1];
};
template<class T>
struct pack<T, true> {
    typedef T packed_type;
    pack()             {new (buf) T();}
    pack(const T& obj) {new (buf) T(obj);}
    ~pack() {get().~T();}
    pack(const pack& o) {new (buf) T(o.get());}
    pack& operator=(const pack& o) {return this == &o ? *this : set(o.get());}

    inline   operator bool(             ) const {return true;            }
    inline bool operator==(const pack& o) const {return get() == o.get();}
    inline bool operator!=(const pack& o) const {return get() != o.get();}
    pack& set(const T&  obj) {
        get() = obj;
        return *this;
    }
    pack& set(T&&  obj) {
        get() = std::move(obj);
        return *this;
    }
    template<class... Ts> pack& emplace(Ts&&... ts) {
        get() = T(std::forward<Ts>(ts)...);
        return *this;
    }
    const T& get() const {return *reinterpret_cast<const T*>(buf);}
          T& get()       {return *reinterpret_cast<      T*>(buf);}
private:
    char buf[sizeof(T)];
};


template<class T> struct default_cloner {
    typedef const T& argument_type;
    typedef T* result_type;
    T* operator()(const T& t) const {return new T(t);}
};

template<class Interface, class Implementation>
Interface* default_if_clone(const Interface& obj) {
    assert(dynamic_cast<const Implementation*>(&obj));
    return new Implementation(reinterpret_cast<const Implementation&>(obj));
}

template<class T, class Cloner=default_cloner<T>
> struct copyable_ptr : public std::unique_ptr<T> {
    typedef std::unique_ptr<T> base_t;
    copyable_ptr() = default;

    explicit copyable_ptr(T* ptr, Cloner cloner=Cloner()) : base_t(ptr),
                                                            cloner(cloner) {}
    copyable_ptr(const base_t& o) : base_t(o ? Cloner()(*o) : nullptr),
                                    cloner() {}
    copyable_ptr(base_t&& o) : base_t(std::move(o)), cloner() {}

    copyable_ptr(copyable_ptr&&) = default;
    copyable_ptr& operator=(copyable_ptr&&) = default;
    copyable_ptr(const copyable_ptr& o)
                 : base_t(o ? o.cloner(*o) : nullptr), cloner(o.cloner) {}
    copyable_ptr& operator=(const copyable_ptr& o) {
        if (this != &o) {
            this->reset(o ? o.cloner(*o) : nullptr);
            cloner = o.cloner;
        }
        return *this;
    }

    void assign(const T& lval) {
        this->reset(cloner(&lval));
    }

private:
    Cloner cloner;
};

template<class It> struct def_constr_in_it {
    typedef std::iterator_traits<It> itt;
    typedef typename itt::value_type value_type;
    typedef std::forward_iterator_tag iterator_category;
    typedef typename itt::reference reference;
    typedef typename itt::pointer pointer;
    typedef typename itt::difference_type difference_type;

    inline def_constr_in_it() = default;
    inline def_constr_in_it(const It& it) : p(it) {}
    inline ~def_constr_in_it()  = default;
    inline def_constr_in_it(const def_constr_in_it&) = default;
    def_constr_in_it& operator=(const def_constr_in_it&) = default;

    inline bool operator==(const def_constr_in_it& o) const {return p == o.p;}
    inline bool operator!=(const def_constr_in_it& o) const {return p != o.p;}
    inline def_constr_in_it operator++(int) {
        def_constr_in_it copy = *this;
        ++*this;
        return copy;
    }
    inline def_constr_in_it& operator++() {
        ++p.get();
        return *this;
    }
    inline pointer   operator->()       {return p.get().operator->();}
    inline reference operator* () const {return p.get().operator* ();}
private:
    pack<It> p;
};

template<class Reference, class Pointer> struct arrow_dispatcher {
    struct container {
        BOOST_STATIC_ASSERT(!std::is_lvalue_reference<Reference>::value);
        Reference val;
        Pointer operator->() const {return &val;}
        operator Pointer() const {return &val;}
    };

    inline static Pointer apply(Reference ref) {return container{ref};}
};
template<class T> struct arrow_dispatcher<T&, T*> {
    inline static T* apply(T& ref) {return &ref;}
};

template<class T, class Reference>
struct deref_helper {
    static_assert(std::is_same<typename std::remove_cv<
            typename std::remove_reference<T>::type>::type, T>::value,
            "Reference MUST be T addorned with cv-qualifiers and a reference");
    static_assert(!std::is_const<T>::value, "T must have no cv-qualifiers");
    T& operator()(T& x) const {return x;}
    const T& operator()(const T& x) const {return x;}
};

template<class T>
struct deref_helper<T, T> {
    static_assert(!std::is_const<T>::value, "T must have no cv-qualifiers");
    T& operator()(T x) const {return copy.set(x).get();}
    mutable pack<T> copy;
};

template<class T, class It> using deref_helper_for_it =
        deref_helper<T, typename std::iterator_traits<It>::reference>;

template<class It, class UnryF> struct transform_fwd_it {
    typedef typename std::decay<UnryF>::type unryf_t;
    static_assert(std::is_convertible<typename std::iterator_traits<It>
                                          ::value_type,
                                      typename unryf_t::argument_type>::value,
                  "It's value_type must be convertible to UnryF argument_type");
    typedef typename unryf_t::result_type value_type;
    typedef std::forward_iterator_tag iterator_category;
    typedef std::ptrdiff_t difference_type;
    typedef value_type& reference;
    typedef value_type* pointer;

    transform_fwd_it() = default;
    transform_fwd_it(const It& in, const unryf_t& t) : in(in), t(t) {}
    transform_fwd_it(const transform_fwd_it&) = default;
    transform_fwd_it(transform_fwd_it&&) = default;
    transform_fwd_it& operator=(const transform_fwd_it&) = default;
    transform_fwd_it& operator=(transform_fwd_it&&) = default;

    reference operator*() const {
        return (!curr ? curr.set(t(*in.get())) : curr).get();
    }
    pointer   operator->() const {return &operator*();}
    transform_fwd_it& operator++() {++in.get(); curr.unset(); return *this;}
    transform_fwd_it operator++(int) {
        transform_fwd_it cpy(*this);
        ++*this;
        return cpy;
    }
    bool operator==(const transform_fwd_it& o) const {return in == o.in;}
    bool operator!=(const transform_fwd_it& o) const {return in != o.in;}

private:
    pack<It> in;
    unryf_t t;
    mutable pack<value_type, false> curr;
};

template<class Range, class UnryF>
struct transform_range_h {
    typedef transform_fwd_it<typename boost::range_iterator<const Range>::type,
                             UnryF> it;
    typedef std::pair<it, it> type;
    static type run(const Range& in, const UnryF& t) {
        return std::make_pair(it(boost::begin(in), t),
                              it(boost::end(in),   t));
    }
};
template<class Range, class UnryF> typename transform_range_h<Range, UnryF>::type
transform_range(const Range& range, const UnryF& transformer) {
    return transform_range_h<Range, UnryF>::run(range, transformer);
}

template<class Rng> std::string rng_to_str(const Rng& r,
                           typename std::enable_if<boost::has_range_iterator<
                                         const Rng>::type::value>::type* = 0) {
    std::stringstream ss;
    ss << "{";
    for (auto i = boost::begin(r), e = boost::end(r); i != e; ++i)
        ss << i->to_str() << ", ";

    auto str = ss.str();
    return str.substr(0, str.length()-2) + "}";
}

struct barrier {
    barrier(bool initial) : m_open(initial) {}

    void set(bool state) {
        std::unique_lock<std::mutex> l(mtx);
        m_open = state;
        cond.notify_all();
    }
    void wait() const {
        std::unique_lock<std::mutex> l(mtx);
        cond.wait(l, [&](){return m_open;});
    }

private:
    mutable std::mutex mtx;
    mutable std::condition_variable cond;
    bool m_open;
};

}  // namespace shacldator



#endif /* SRC_UTILS_HPP_ */
