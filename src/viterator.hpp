/*
 * viterator.hpp
 *
 *  Created on: Apr 23, 2018
 *      Author: alexis
 */

#ifndef SRC_VITERATOR_HPP_
#define SRC_VITERATOR_HPP_

#include <iterator>
#include <type_traits>
#include <utility>
#include <boost/range.hpp>
#include <boost/iterator/iterator_facade.hpp>
#include "utils.hpp"

namespace shacldator {

template<class Value, class ItTag, class Reference> struct viterator;

namespace priv {

template<class X> struct is_viterator : public std::false_type {};
template<class Value, class Tag, class Reference>
struct is_viterator<viterator<Value, Tag, Reference>> : public std::true_type {};

template<class Value, class Reference>
struct viterator_ipimpl {
    typedef Value value_type;
    typedef Reference reference;

    virtual ~viterator_ipimpl() = default;

    virtual viterator_ipimpl* clone() const = 0;
    virtual void increment() = 0;
//    virtual void decrement() = 0;
    virtual bool equal(const viterator_ipimpl& rhs) const = 0;
    virtual reference dereference() const  = 0;
};

template<class It,
         class Value = typename std::iterator_traits<It>::value_type,
         class Reference = typename std::iterator_traits<It>::reference>
struct viterator_pimpl
: public viterator_ipimpl<Value, Reference>{
    typedef It it_t;
    typedef Value value_type;

    viterator_pimpl() = default;
    viterator_pimpl(const it_t& it) : m_it(it) {}
    viterator_pimpl(const viterator_pimpl&) = default;
    viterator_pimpl& operator=(const viterator_pimpl&) = default;
    virtual ~viterator_pimpl() = default;

    it_t it() const {return m_it;}

    virtual viterator_ipimpl<value_type, Reference>* clone() const {
        return new viterator_pimpl(*this);
    }
    virtual void increment() {++m_it;}
//    virtual void decrement() {--m_it;}
    virtual bool equal(const viterator_ipimpl<Value, Reference>& rhs) const {
        const viterator_pimpl* dwn =
                dynamic_cast<const viterator_pimpl*>(&rhs);
        return dwn && m_it == dwn->m_it;
    }
    virtual Reference dereference() const {return *m_it;}

private:
    mutable it_t m_it;
};

template<class It, class UnryF,
         class Value = typename UnryF::result_type>
struct viterator_trans_pimpl
: public viterator_ipimpl<Value,
          typename std::add_lvalue_reference<
              typename std::add_const<Value>::type>::type> {
    typedef typename std::add_lvalue_reference<
            typename std::add_const<Value>::type>::type reference;
    typedef viterator_ipimpl<Value, reference> base_t;
    typedef It it_t;

    viterator_trans_pimpl() = default;
    viterator_trans_pimpl(const it_t& it, UnryF t) : m_it(it), trans(t) {}
    viterator_trans_pimpl(const viterator_trans_pimpl&) = default;
    viterator_trans_pimpl& operator=(const viterator_trans_pimpl&) = default;
    virtual ~viterator_trans_pimpl() = default;

    it_t it() const {return m_it;}

    virtual base_t* clone() const {return new viterator_trans_pimpl(*this);}
    virtual void increment() {++m_it;}
//    virtual void decrement() {--m_it;}
    virtual bool equal(const base_t& rhs) const {
        const viterator_trans_pimpl* dwn =
                dynamic_cast<const viterator_trans_pimpl*>(&rhs);
        return dwn && m_it == dwn->m_it;
    }
    virtual reference dereference() const {return curr = trans(*m_it);}

private:
    mutable it_t m_it;
    mutable Value curr;
    UnryF trans;
};

template<class ItTag, class X,
         class Value=typename std::iterator_traits<X>::value_type,
         class Reference=typename std::iterator_traits<X>::reference>
struct dispatch_viterator_pimpl {
    typedef priv::viterator_ipimpl<Value, Reference>* type;
    static type run(const X& x) {
        return new viterator_pimpl<X, Value, Reference>(x);
    }

    template<class UnryF> static priv::viterator_ipimpl<
        typename std::decay<typename UnryF::result_type>::type,
        typename std::add_lvalue_reference<typename std::add_const<
                typename UnryF::result_type>::type>::type
    >* run(const X& x, UnryF t) {
        using namespace std;
        static_assert(is_same<typename decay<typename UnryF::result_type>::type,
                              typename decay<Value>::type>::value,
                      "Bad UnryF::result_type");
        return new viterator_trans_pimpl<X, UnryF>(x, t);
    }
};

template<class LhsValue, class LhsReference,
         class Value, class ItTag, class Reference>
struct dispatch_viterator_pimpl<ItTag, viterator<Value, ItTag, Reference>,
                                LhsValue, LhsReference > {
    typedef priv::viterator_ipimpl<LhsValue, LhsReference>* type;
    static type run(const viterator<Value, ItTag, Reference>& x) {
//        static_assert(std::is_convertible<RhsItTag, ItTag>::value,
//                     "RhsItTag must be convertible to ItTag");
        static_assert(std::is_base_of<LhsValue, Value>::value,
                      "Value must be a subclass of (or same as) LhsValue");
        static_assert(std::is_same<LhsValue, Value>::value
                        || (std::is_lvalue_reference<LhsReference>::value
                                && std::is_lvalue_reference<Reference>::value),
                      "For a polymorphic copy, both References must be actual "
                      "references (i.e., ItTag is forward_iterator)");
        return reinterpret_cast<type>(x.pimpl->clone());
    }

    template<class UnryF> static priv::viterator_ipimpl<
        typename std::decay<typename UnryF::result_type>::type,
        typename std::add_lvalue_reference<typename std::add_const<
                typename UnryF::result_type>::type>::type
    >* run(const viterator<Value, ItTag, Reference>& x, UnryF t) {
        using namespace std;
        static_assert(is_same<typename decay<typename UnryF::result_type>::type,
                              typename decay<LhsValue>::type>::value,
                      "Bad UnryF::result_type");
        return new viterator_trans_pimpl<viterator<Value, ItTag, Reference>,
                                         UnryF>(x, t);
    }
};

template<class Cat> using viterator_category =
        std::conditional<std::is_convertible<Cat,
                                            std::forward_iterator_tag>::value,
                         std::forward_iterator_tag,
                         std::input_iterator_tag>;
template<class It> using viterator_it_category = viterator_category<
        typename std::iterator_traits<It>::iterator_category>;

}  // namespace priv

template<class Value, class ItTag=std::input_iterator_tag,
         class Reference=typename std::conditional<
                 std::is_convertible<ItTag, std::forward_iterator_tag >::value,
                 const Value&,
                 Value
             >::type>
class viterator {
    typedef priv::viterator_ipimpl<Value, Reference>* ipimpl_ptr_t;
public:
    typedef Value value_type;
    typedef ItTag iterator_category;
    typedef std::ptrdiff_t difference_type;
    typedef Reference reference;
    typedef typename std::remove_reference<reference>::type* pointer;

    viterator() : pimpl(nullptr) {}
    viterator(const viterator& rhs)
              : pimpl(rhs.pimpl ? rhs.pimpl->clone() : nullptr) {}
    /** Either a regular or a copy constructor. Copy if It is a viterator,
     *  otherwise will create a new pimpĺ around x (as x is an iterator over
     *  Value which is not a viterator). */
    template<class It> viterator(const It& x)
            : pimpl(priv::dispatch_viterator_pimpl<ItTag,It,Value,Reference>
                        ::run(x)) {}
    template<class It, class UnryF> viterator(const It& x, UnryF& t)
            : pimpl(priv::dispatch_viterator_pimpl<ItTag,It,Value,Reference>
                        ::run(x, t)) {}
    viterator(viterator&& rhs) : pimpl(rhs.pimpl) {rhs.pimpl = nullptr;}
    viterator& operator=(const viterator& rhs) {
        if (this == &rhs) return *this;
        delete pimpl;
        pimpl = rhs.pimpl ? rhs.pimpl->clone() : nullptr;
        return *this;
    }
    viterator& operator=(viterator&& rhs) {
        if (this == &rhs) return *this;
        delete pimpl;
        pimpl = rhs.pimpl;
        rhs.pimpl = nullptr;
        return *this;
    }
    ~viterator() { delete pimpl; }

    bool operator==(const viterator& rhs) const {
        if (pimpl == rhs.pimpl) return true;
        assert(pimpl && rhs.pimpl);
        return pimpl->equal(*rhs.pimpl);
    }
    bool operator!=(const viterator& rhs) const {return !operator==(rhs);}

    viterator& operator++() {pimpl->increment(); return *this;}
    viterator operator++(int) {
        viterator cp(*this);
        ++*this;
        return cp;
    }
    reference operator*() const {return pimpl->dereference();}
    pointer operator->() const {
        typedef arrow_dispatcher<reference, pointer> disp;
        return disp::apply(pimpl->dereference());
    }
private:
    template<class FValue, class FTag, class FReference> friend class viterator;
    template<class FValue, class FTag, class FX>
    friend class priv::dispatch_viterator_pimpl;

    ipimpl_ptr_t pimpl;
};
template<class Value> using fwd_viterator
        = viterator<Value, std::forward_iterator_tag>;
template<class Value> using fwd_vrange
        = std::pair<fwd_viterator<Value>, fwd_viterator<Value>>;

template<class It> struct viterator_from {
    typedef typename std::iterator_traits<It>::value_type vt;
    typedef viterator<vt, typename priv::viterator_it_category<It>::type> type;
};

template<class It, class UnryF> struct viterator_from_transf {
    typedef viterator<typename UnryF::result_type,
                      typename priv::viterator_it_category<It>::type,
                      const typename UnryF::result_type&>
            type;
};

template<class T> using empty_vrange
        = std::pair<fwd_viterator<T>, fwd_viterator<T>>;

template<class It, class UnryF>
typename viterator_from_transf<It, UnryF>::type
mk_viterator(const It& it, const UnryF& t) {
    return typename viterator_from_transf<It, UnryF>::type(it, t);
}
template<class Value, class ItTag, class Reference>
viterator<Value, ItTag, Reference>
mk_viterator(const viterator<Value, ItTag, Reference>& it) {
    return it;
}
template<class Value, class ItTag, class Reference>
viterator<Value, ItTag, Reference>
mk_viterator(viterator<Value, ItTag, Reference>&& it) {
    return std::move(it);
}
template<class It> typename viterator_from<It>::type
mk_viterator(const It& it) {
    return typename viterator_from<It>::type(it);
}

template<class Range, class UnryF,
         class It=typename boost::range_iterator<const Range>::type>
std::pair<typename viterator_from_transf<It, UnryF>::type,
          typename viterator_from_transf<It, UnryF>::type>
mk_vrange(const Range& range, const UnryF& t) {
    typedef std::pair<typename viterator_from_transf<It, UnryF>::type,
                      typename viterator_from_transf<It, UnryF>::type> pair_t;
    return pair_t(mk_viterator(boost::begin(range), t),
                  mk_viterator(boost::end  (range), t));
}
template<class Value, class ItTag, class Reference>
std::pair<viterator<Value, ItTag, Reference>,
          viterator<Value, ItTag, Reference>>
mk_vrange(const std::pair<viterator<Value, ItTag, Reference>,
                    viterator<Value, ItTag, Reference>>& rng) {
    return rng;
}
template<class Value, class ItTag, class Reference>
std::pair<viterator<Value, ItTag, Reference>,
          viterator<Value, ItTag, Reference>>
mk_vrange(std::pair<viterator<Value, ItTag, Reference>,
                    viterator<Value, ItTag, Reference>>&& rng) {
    return std::move(rng);
}

template<class Range, class It=typename boost::range_iterator<const Range>::type>
std::pair<typename viterator_from<It>::type,
          typename viterator_from<It>::type>
mk_vrange(const Range& range) {
    typedef std::pair<typename viterator_from<It>::type,
                      typename viterator_from<It>::type> pair_t;
    return pair_t(mk_viterator(boost::begin(range)),
                  mk_viterator(boost::end  (range)));
}

}  // namespace shacldator



#endif /* SRC_VITERATOR_HPP_ */
