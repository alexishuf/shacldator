/*
 * rulesets.hpp
 *
 *  Created on: Jun 12, 2018
 *      Author: alexis
 */

#ifndef SRC_INF_RULESETS_HPP_
#define SRC_INF_RULESETS_HPP_

#include "ruleset.hpp"
#include "backward.hpp"

namespace shacldator {namespace inf {

ruleset rdfs();
ruleset interesting_rdfs();

}}  // namespace shacldator::inf



#endif /* SRC_INF_RULESETS_HPP_ */
