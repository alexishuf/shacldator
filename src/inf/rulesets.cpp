/*
 * rulesets.cpp
 *
 *  Created on: Jun 12, 2018
 *      Author: alexis
 */

#include "rulesets.hpp"

#include "../ns/rdf.hpp"
#include "../ns/rdfs.hpp"

namespace shacldator {namespace inf {

ruleset rdfs() {
    /*https://www.w3.org/TR/rdf11-mt/#patterns-of-rdfs-entailment-informative*/
    ruleset::builder b;
    return b.rule()
            .head("?y", rdf::type, "?x")
            .body("?p", rdfs::domain, "?x").body("?y", "?p", "?x")
            .create().rule() // ^^^-- rdfs2
            .head("?z", rdf::type, "?x")
            .body("?prop", rdfs::range, "?x").body("?y", "?prop" ,"?z")
            .create().rule() // ^^^-- rdfs3
            .head("?x", rdf::type, rdfs::Resource)
            .body("?x", "?prop", "?y")
            .create().rule() // ^^^-- rdfs4a
            .head("?y", rdf::type, rdfs::Resource)
            .body("?x", "?prop", "?y")
            .create().rule() // ^^^-- rdfs4b
            .head("?x", rdfs::subPropertyOf, "?z")
            .body("?x", rdfs::subPropertyOf, "?y")
            .body("?y", rdfs::subPropertyOf, "?z")
            .create().rule() // ^^^-- rdfs5
            .head("?x", rdfs::subPropertyOf, "?x")
            .body("?x", rdf::type, rdf::Property)
            .create().rule() // ^^^-- rdfs6
            .head("?x", "?b", "?y")
            .body("?a", rdfs::subPropertyOf, "?b")
            .body("?x", "?a", "?y")
            .create().rule() // ^^^-- rdfs7
            .head("?x", rdfs::subClassOf, rdfs::Resource)
            .body("?x", rdf::type, rdfs::Class)
            .create().rule() // ^^^-- rdfs8
            .head("?z", rdf::type, "?y")
            .body("?x", rdfs::subClassOf, "?y")
            .body("?z", rdf::type, "?x")
            .create().rule() // ^^^-- rdfs9
            .head("?x", rdfs::subClassOf, "?x")
            .body("?x", rdf::type, rdfs::Class)
            .create().rule() // ^^^-- rdfs10
            .head("?x", rdfs::subClassOf, "?z")
            .body("?x", rdfs::subClassOf, "?y")
            .body("?y", rdfs::subClassOf, "?z")
            .create().rule() // ^^^-- rdfs11
            .head("?x", rdfs::subPropertyOf, rdfs::member)
            .body("?x", rdf::type, rdfs::ContainerMembershipProperty)
            .create().rule() // ^^^-- rdfs12
            .head("?x", rdfs::subClassOf, rdfs::Literal)
            .body("?x", rdf::type, rdfs::Datatype)
            .create()        // ^^^-- rdfs13
            .build();
}

ruleset interesting_rdfs() {
    ruleset::builder b;
    return b.rule()
            .head("?y", rdf::type, "?x")
            .body("?p", rdfs::domain, "?x").body("?y", "?p", "?x")
            .create().rule() // ^^^-- rdfs2
            .head("?z", rdf::type, "?x")
            .body("?prop", rdfs::range, "?x").body("?y", "?prop" ,"?z")
            .create().rule() // ^^^-- rdfs3
            .head("?x", rdfs::subPropertyOf, "?z")
            .body("?x", rdfs::subPropertyOf, "?y")
            .body("?y", rdfs::subPropertyOf, "?z")
            .create().rule() // ^^^-- rdfs5
            .head("?x", "?b", "?y")
            .body("?a", rdfs::subPropertyOf, "?b")
            .body("?x", "?a", "?y")
            .create().rule() // ^^^-- rdfs7
            .head("?z", rdf::type, "?y")
            .body("?x", rdfs::subClassOf, "?y")
            .body("?z", rdf::type, "?x")
            .create().rule() // ^^^-- rdfs9
            .head("?x", rdfs::subClassOf, "?z")
            .body("?x", rdfs::subClassOf, "?y")
            .body("?y", rdfs::subClassOf, "?z")
            .create()        // ^^^-- rdfs11
            .build();
}


}}  // namespace shacldator::inf



