/*
 * dsl_inf_eval.hpp
 *
 *  Created on: Jun 12, 2018
 *      Author: alexis
 */

#ifndef SRC_INF_DSL_INF_EVAL_HPP_
#define SRC_INF_DSL_INF_EVAL_HPP_

#include <boost/function.hpp>
#include "../qry/dsl_eval.hpp"
#include "../qry/dsl.hpp"
#include "../qry/priv/triple_filter_sol_iterator.hpp"
#include "../dqry/triple.hpp"

namespace shacldator {namespace inf {

template<class Engine, class EnginePtr=Engine*> struct evaluator :
        public qry::base_evaluator<evaluator<Engine>, typename Engine::graph_t>{
    typedef qry::base_evaluator<evaluator<Engine>, typename Engine::graph_t
            > base_t;
    typedef Engine engine_t;
    typedef typename engine_t::term_t term_t;
    typedef EnginePtr engine_ptr_t;

    explicit evaluator(engine_ptr_t engine)
                       : base_t(engine->graph), engine(engine) {}
    evaluator(const evaluator&) = default;
    evaluator& operator=(const evaluator&) = default;

    struct fac {
        typedef evaluator result_type;
        typedef typename engine_t::graph_t argument_type;
        fac(engine_ptr_t e) : e(e) {}

        result_type operator()(const argument_type& g) const {
            if (&e->graph != &g) {
                throw std::invalid_argument("inf_evaluator::fac "
                                            "expects &e->graph as g");
            }
            return evaluator(e);
        }

        engine_ptr_t e;
    };

    struct str_tp_evaluator {
        str_tp_evaluator(evaluator* e) : e(e) {}

        template<class S, class P, class O>
        fwd_vrange<typename qry::spo2sol<term_t, S, P, O>::type>
        apply(const qry::triple_pattern<S, P, O>& tp) {
            qry::priv::sol_iterator_transformer<term_t, S, P, O> transformer;
            dqry::triple<term_t> qt = dqry::triple<term_t>
                    ::parse(e->engine->graph.store(), tp.subj, tp.pred, tp.obj);
            return mk_vrange(e->engine->term_range(qt), transformer);
        }

        evaluator* e;
    };
private:
    engine_ptr_t engine;
};

template<class Reasoner> struct auto_evaluator_fac {
    typedef typename std::shared_ptr<Reasoner>reasoner_ptr_t;
    typedef const typename Reasoner::raw_graph_t& argument_type;
    typedef evaluator<Reasoner, reasoner_ptr_t> result_type;

    auto_evaluator_fac(boost::function<reasoner_ptr_t(argument_type)> reasoner_fac)
                       : reasoner_fac(reasoner_fac) {}

    result_type operator()(const argument_type& g) const {
        return result_type(reasoner_fac(g));
    }

    boost::function<reasoner_ptr_t(argument_type)> reasoner_fac;
};

}}  // namespace shacldator::inf


#endif /* SRC_INF_DSL_INF_EVAL_HPP_ */
