/*
 * precook.hpp
 *
 *  Created on: Jun 22, 2018
 *      Author: alexis
 */

#ifndef SRC_INF_MICROWAVE_HPP_
#define SRC_INF_MICROWAVE_HPP_

#include <memory>
#include <utility>
#include <deque>
#include <set>
#include <queue>
#include <stack>
#include <unordered_map>
#include <omp.h>
#include "../graph/graph.hpp"
#include "../graph/str/triple.hpp"
#include "../dqry/triple.hpp"
#include "../dqry/explicit_generator.hpp"
#include "../dqry/generator_iterator.hpp"
#include "../qry/dsl_eval.hpp"
#include "detail/domain_range_map.hpp"
#include "detail/naive_closure_map.hpp"
#include "../ns/rdfs.hpp"
#include "../ns/rdf.hpp"
#include "../utils.hpp"
#include "../viterator.hpp"
#include "../dqry/packed_generator.hpp"
#include "../dqry/empty_generator.hpp"
#include "../dqry/vgenerator.hpp"


namespace shacldator {namespace inf {

struct abstract_microwave {};
template<class X>
struct is_microwave : public std::is_base_of<abstract_microwave, X> {};

template<class Graph,
         class EvaluatorTypeFactory=boost::mpl::quote1<qry::def_evaluator>
> struct microwave : public abstract_microwave {
    typedef Graph graph_t;
    typedef typename graph_t::store_t store_t;
    typedef typename store_t::term_t term_t;
    typedef str::triple<term_t> triple_t;
    typedef statement<graph_t> stmt_t;
    typedef dqry::triple<term_t> qt_t;
    typedef dqry::term<term_t> qterm_t;
    typedef typename EvaluatorTypeFactory::template apply<graph_t>::type ev_t;
    typedef detail::naive_closure_map<term_t> nc_map_t;
    typedef detail::domain_range_map<term_t> dr_map_t;

    microwave(const Graph& g)
            : graph(g), ev(g),
              rdf_type(g.store().resource(rdf::type)),
              qrdf_type(qterm_t::from_term(rdf_type)),
              subcls(new nc_map_t(nc_map_t::build(
                      g.resource(rdfs::subClassOf), true))),
              subprop(new nc_map_t(nc_map_t::build(
                      g.resource(rdfs::subPropertyOf), true))),
              dr(new dr_map_t(dr_map_t::build(g))),
              m_use_dr_with_var_subj(true), m_use_dr(true), m_nest_omp(false) {}
    microwave(const Graph& g, nc_map_t&& subcls,
                            nc_map_t&& subprop,
                            dr_map_t&& dr)
            : graph(g), ev(g),
              rdf_type(g.store().resource(rdf::type)),
              qrdf_type(qterm_t::from_term(rdf_type)),
              subcls(new nc_map_t(std::move(subcls))),
              subprop(new nc_map_t(std::move(subprop))),
              dr(new dr_map_t(std::move(dr))),
              m_use_dr_with_var_subj(true), m_use_dr(true), m_nest_omp(false) {}
    microwave(microwave&&) = default;

    typedef dqry::packed_generator<term_t, triple_t> generator;

private:
    typedef dqry::vgenerator<term_t, triple_t> vgen_t;

    node<graph_t> n(const term_t& t) const {return node<graph_t>(t, graph);}
    node<graph_t> n(const qterm_t& t) const {
        assert(t.is_term());
        return node<graph_t>(t.term_v, graph);
    }

    struct gen_type_explicit : public vgen_t {
        gen_type_explicit(const microwave* parent, const qt_t& qry)
                     : p(parent), q(qry), value(), at_end(false) {
            assert(qry.subj.is_term());
            assert(qry.pred == p->qrdf_type);
            assert(qry.obj.is_term());
        }
        gen_type_explicit& operator=(const gen_type_explicit&) = default;

        const triple_t& get() const {
            return value;
        }

        bool advance() {
            using namespace qry;
            if (at_end) return false;
            at_end = true;
            assert(value == triple_t());

            if (p->ev.str_ask(_t(p->n(q.subj), p->n(q.pred), p->n(q.obj)))) {
                value = q.to_triple();
                return true;
            }
            const auto& set = p->subcls->at(q.obj.term_v);
            for (auto rng = p->ev.str_eval_term(
                                _t(p->n(q.subj), p->n(p->rdf_type), _1));
                     rng.first != rng.second; ++rng.first) {
                if (boost::binary_search(set, *rng.first)) {
                    value = q.to_triple();
                    return true;
                }
            }
            if (!p->use_dr()) return false;

            const auto& d_set = p->dr->domain_of(q.obj.term_v);
            if (d_set.size() > 32) {
                for (auto rng = p->ev.str_eval_term(_t(p->n(q.subj), _1, _2));
                            rng.first != rng.second; ++rng.first) {
                    if (d_set.contains(*rng.first)) {
                        value = q.to_triple();
                        return true;
                    }
                }
            } else {
                for (auto i = d_set.begin(), e = d_set.end(); i != e; ++i) {
                    if (p->ev.str_ask(_t(p->n(q.subj), p->n(*i), _1))) {
                        value = q.to_triple();
                        return true;
                    }
                }
            }
            const auto& r_set = p->dr->range_of(q.obj.term_v);
            if (r_set.size() > 64) {
                for (auto rng = p->ev.str_eval_term(_t(_2, _1, p->n(q.subj)));
                            rng.first != rng.second; ++rng.first) {
                    if (r_set.contains(*rng.first)) {
                        value = q.to_triple();
                        return true;
                    }
                }
            } else {
                for (auto i = r_set.begin(), e = r_set.end(); i != e; ++i) {
                    if (p->ev.str_ask(_t(_1, p->n(*i), p->n(q.subj)))) {
                        value = q.to_triple();
                        return true;
                    }
                }
            }

            assert(value == triple_t());
            return false;
        }

    private:
        const microwave* p;
        qt_t q;
        triple_t value;
        bool at_end;
    };

    struct gen_type : public vgen_t {
        typedef microwave::term_t term_t;
        typedef microwave::triple_t triple_t;

        gen_type(const microwave* parent, const qt_t& qry)
                 : p(parent), q(qry), value(), stage(0),
                   curr_gen(dqry::explicit_generator<ev_t>(p->ev, qry)) {
            assert(qry.pred == p->qrdf_type);
            assert(qry.obj.is_term());
        }
        gen_type(const gen_type&) = default;
        gen_type& operator=(const gen_type&) = default;
        gen_type(gen_type&&) = default;
        gen_type& operator=(gen_type&&) = default;

        const triple_t& get() const {
            return value;
        }
    private:
        struct omp_state {

        };

    private:
        void omp_advance_stage0() {
            omp_data.reset(new std::vector<triple_t>());
            omp_data->reserve(1024);
            term_t q_pred = q.pred.term_v, q_obj = q.obj.term_v;

            /* get explicit matches */
            #pragma omp task
            {
                std::deque<triple_t> tmp;
                while (curr_gen.advance()) tmp.push_back(curr_gen.get());
                #pragma omp critical (microwave__gen_type__advance)
                omp_data->insert(omp_data->end(), tmp.begin(), tmp.end());
            }

            /* search for matches with subclass */
            const auto& set  = p->subcls->at(q_obj);
            for (auto i = set.begin(), e = set.end(); i != e; ++i) {
                if (*i == q_obj) continue;
                qterm_t i_term = qterm_t::from_term(*i);
                #pragma omp task firstprivate(i_term)
                {
                    std::deque<triple_t> tmp;
                    auto g = mk_eg(qt_t(q.subj, q.pred, i_term));
                    while (g.advance())
                        tmp.push_back(g.get().with_obj(q_obj));
                    #pragma omp critical (microwave__gen_type__advance)
                    omp_data->insert(omp_data->end(), tmp.begin(), tmp.end());
                }
            }

            if (p->use_dr_with_var_subj()) {
                qterm_t extra = qterm_t::from_ph(q.max_ph()+1);
                /* search for matches with domain */
                const auto& d_set = p->dr->domain_of(q_obj);
                for (auto i = d_set.begin(), e = d_set.end(); i != e; ++i){
                    qterm_t i_term = qterm_t::from_term(*i);
                    #pragma omp task firstprivate(i_term)
                    {
                        std::deque<triple_t> tmp;
                        auto g = mk_eg(qt_t(q.subj, i_term, extra));
                        while (g.advance()) {
                            tmp.push_back(triple_t(g.get().subj(), q_pred,
                                                                   q_obj));
                        }
                        #pragma omp critical (microwave__gen_type__advance)
                        omp_data->insert(omp_data->end(),
                                        tmp.begin(), tmp.end());
                    }
                }

                const auto& r_set = p->dr->range_of(q_obj);
                for (auto i = r_set.begin(), e = r_set.end(); i != e; ++i){
                    qterm_t i_term = qterm_t::from_term(*i);
                    #pragma omp task firstprivate(i_term)
                    {
                        std::deque<triple_t> tmp;
                        auto g = mk_eg(qt_t(extra, i_term, q.subj));
                        while (g.advance()) {
                            tmp.push_back(triple_t(g.get().obj(), q_pred,
                                                                  q_obj));
                        }
                        #pragma omp critical (microwave__gen_type__advance)
                        omp_data->insert(omp_data->end(),
                                        tmp.begin(), tmp.end());
                    }
                }
            }
            /* wait while all shared variables with auto storage still live */
            #pragma omp taskwait
        }
    public:

        bool omp_advance() {
            if (stage == 0) {
                stage = 0x11;
                omp_idx = 0;
                assert(omp_in_parallel());
                omp_advance_stage0();
            }
            if (stage == 0x11) {
                if (++omp_idx >= omp_data->size()) {
                    stage = 0x12; //end
                } else {
                    value = omp_data->at(omp_idx);
                    return true;
                }
            }
            value = triple_t();
            assert(stage == 0x12);
            return false;
        }
        bool st_advance() {
            if (curr_gen.advance()) {
                value = curr_gen.get();
                return true;
            }
            if (stage == 0) {
                const auto& set = p->subcls->at(q.obj.term_v);
                for (auto i = set.begin(), e = set.end(); i != e; ++i) {
                    if (*i != q.obj.term_v) terms.push(*i);
                }
                stage = 1;
            }
            if (stage == 1) {
                bool ok = curr_gen.advance();
                for (; !ok && !terms.empty(); ok = curr_gen.advance()) {
                    curr_gen = mk_eg(qt_t(q.subj, q.pred,
                                          qterm_t::from_term(terms.front())));
                    terms.pop();
                }
                if (ok) {
                    value = curr_gen.get().with_obj(q.obj.term_v);
                    return true;
                }
                assert(terms.empty());
                if (p->use_dr_with_var_subj()) {
                    stage = 2;
                    queue_all(p->dr->domain_of(q.obj.term_v));
                } else {
                    stage = 4;
                }
            }
            if (stage == 2) {
                bool ok = curr_gen.advance();
                for (; !ok && !terms.empty(); ok = curr_gen.advance()) {
                    curr_gen = mk_eg(qt_t(q.subj,
                                          qterm_t::from_term(terms.front()),
                                          qterm_t::from_ph(q.max_ph()+1)));
                    terms.pop();
                }
                if (ok) {
                    value = triple_t(curr_gen.get().subj(), q.pred.term_v,
                                                            q. obj.term_v);
                    return true;
                }
                assert(terms.empty());
                if (p->use_dr_with_var_subj()) {
                    stage = 3;
                    queue_all(p->dr->range_of(q.obj.term_v));
                } else {
                    stage = 4;
                }
            }
            if (stage == 3) {
                bool ok = curr_gen.advance();
                for (; !ok && !terms.empty(); ok = curr_gen.advance()) {
                    curr_gen = mk_eg(qt_t(qterm_t::from_ph(q.max_ph()+1),
                                          qterm_t::from_term(terms.front()),
                                          q.subj));
                    terms.pop();
                }
                if (ok) {
                    value = triple_t(curr_gen.get().obj(), q.pred.term_v,
                                                           q. obj.term_v);
                    return true;
                }
                stage = 4;
            }
            value = triple_t();
            assert(terms.empty());
            assert(stage == 4);
            return false;
        }

        bool advance() {
            if (p->nest_omp() && omp_in_parallel())
                return omp_advance();
            else
                return st_advance();
        }

    private:
        void queue_all(const ro_set<term_t>& set) {
            for (auto i = set.begin(), e = set.end(); i != e; ++i)
                terms.push(*i);
        }
        dqry::explicit_generator<ev_t> mk_eg(const qt_t& t) const {
            return dqry::explicit_generator<ev_t>(p->ev, t);
        }

        const microwave* p;
        qt_t q;
        mutable triple_t value;
        int stage;
        std::queue<term_t> terms;
        generator curr_gen;
        std::shared_ptr<std::vector<triple_t>> omp_data;
        std::size_t omp_idx;
    };

    struct gen_type_var : public vgen_t {
        typedef microwave::term_t term_t;
        typedef microwave::triple_t triple_t;

        gen_type_var(const microwave* parent, const qt_t& qry)
                     : p(parent), q(qry), value(), eg(p->ev, qry) {
            assert(qry.pred == p->qrdf_type);
            assert(qry.obj.is_ph());
        }
        gen_type_var(const gen_type_var&) = default;
        gen_type_var(gen_type_var&&) = default;
        gen_type_var& operator=(const gen_type_var&) = default;
        gen_type_var& operator=(gen_type_var&&) = default;

        const triple_t& get() const {
            return value;
        }
        bool advance() {
            using namespace qry;
            if (!queue.empty())
                queue.pop();
            while (queue.empty()) {
                if (!eg.advance()) break;
                std::set<term_t> visited;
                std::stack<term_t> stack;
                stack.push(eg.get().obj());
                while (!stack.empty()) {
                    term_t top = stack.top();
                    stack.pop();
                    if (visited.count(top)) continue;
                    queue.push(top);
                    visited.insert(top);
                    for (auto rng = p->ev.str_eval_term(
                                    _t(p->n(top), rdfs::subClassOf, _1));
                            rng.first != rng.second; ++rng.first) {
                        stack.push(*rng.first);
                    }
                }
            }
            if (!queue.empty()) {
                value = eg.get().with_obj(queue.front());
                return true;
            }
            value = triple_t();
            return false;
        }

    private:
        const microwave* p;
        qt_t q;
        triple_t value;
        dqry::explicit_generator<ev_t> eg;
        std::queue<term_t> queue;
    };

    struct gen_prop : public vgen_t {
        typedef microwave::term_t term_t;
        typedef microwave::triple_t triple_t;

        gen_prop(const microwave* parent, const qt_t& qry)
                 : p(parent), q(qry), stage(0), eg(p->ev, qry), value() {
            assert(qry.pred.is_term());
            assert(qry.pred != p->qrdf_type);
        }
        gen_prop(const gen_prop&) = default;
        gen_prop(gen_prop&&) = default;
        gen_prop& operator=(const gen_prop&) = default;
        gen_prop& operator=(gen_prop&&) = default;

        const triple_t& get() const {
            return value;
        }
        bool advance() {
            bool ok = eg.advance();
            if (!ok && stage == 0) { // eg for q exhausted
                const auto& set = p->subprop->at(q.pred.term_v);
                pit = set.begin();
                pend = set.end();
                ++stage;
            }
            for (; !ok && pit != pend; ++pit) {
                if (*pit == q.pred.term_v) continue;
                eg = dqry::explicit_generator<ev_t>(p->ev,
                        qt_t(q.subj, qterm_t::from_term(*pit), q.obj));
                ok = eg.advance();
            }
            value = ok ? eg.get().with_pred(q.pred.term_v)
                       : triple_t();
            return ok;
        }

    private:
        const microwave* p;
        qt_t q;
        int stage;
        typename dr_map_t::set_it_t pit, pend;
        dqry::explicit_generator<ev_t> eg;
        triple_t value;
    };

    struct gen_prop_explicit : public vgen_t {
        typedef microwave::term_t term_t;
        typedef microwave::triple_t triple_t;

        gen_prop_explicit(const microwave* parent, const qt_t& qry)
                          : p(parent), q(qry), value(), at_end(false) {
            assert(qry.subj.is_term());
            assert(qry.pred.is_term() && qry.pred.term_v != p->rdf_type);
        }
        gen_prop_explicit(const gen_prop_explicit&) = default;
        gen_prop_explicit& operator=(const gen_prop_explicit&) = default;
        gen_prop_explicit(gen_prop_explicit&&) = default;
        gen_prop_explicit& operator=(gen_prop_explicit&&) = default;

        const triple_t& get() const {
            return value;
        }
        bool advance() {
            using namespace qry;
            if (at_end) return false;
            at_end = true;
            assert(value == triple_t());

            if (p->ev.str_ask(_t(p->n(q.subj), p->n(q.pred), p->n(q.obj)))) {
                value = q.to_triple();
                return true;
            }
            const auto& set = p->subprop->at(q.pred.term_v);
            for (auto rng = p->ev.str_eval_term(
                        _t(p->n(q.subj), _1, p->n(q.obj)));
                    rng.first != rng.second; ++rng.first) {
                if (boost::binary_search(set, *rng.first)) {
                    value = q.to_triple();
                    return true;
                }
            }
            return false;
        }

    private:
        const microwave* p;
        qt_t q;
        triple_t value;
        bool at_end;

    };

public:

    generator operator()(const qt_t& t) const {
        bool has_null = (t.subj.is_term() && !t.subj.term_v) ||
                        (t.pred.is_term() && !t.pred.term_v) ||
                        (t. obj.is_term() && !t. obj.term_v);
        if (has_null) {
            return dqry::empty_generator<term_t, triple_t>();
        } else if (t.pred == qrdf_type) {
            if (t.obj.is_term()) {
                if (t.subj.is_term())
                    return gen_type_explicit(this, t);
                else //t.subj.is_ph()
                    return gen_type(this, t);
            } else { //t.obj.is_ph()
                return gen_type_var(this, t);
            }
        } else if (t.pred.is_term()) {
            if (t.subj.is_term() && t.obj.is_term())
                return gen_prop_explicit(this, t);
            else
                return gen_prop(this, t);
        }
        return dqry::explicit_generator<ev_t>(ev, t);
    }

    bool use_dr_with_var_subj() const {
        return m_use_dr_with_var_subj;
    }
    void use_dr_with_var_subj(bool value) {
        m_use_dr_with_var_subj = value;
    }
    bool use_dr() const {
        return m_use_dr;
    }
    void use_dr(bool value) {
        m_use_dr = value;
    }
    bool nest_omp() const {
        return m_nest_omp;
    }
    void nest_omp(bool value) {
        m_nest_omp = value;
    }

    typedef dqry::generator_iterator<generator> term_it_t;
    typedef std::pair<term_it_t, term_it_t> term_range_t;
    term_range_t term_range(const qt_t& t) const {
        return dqry::mk_generator_range((*this)(t));
    }
    term_range_t term_range(const char* s, const char* p, const char* o) const {
        try {
            return term_range(qt_t::parse(graph.store(), s, p, o));
        } catch (const term_not_found&) {
            return std::make_pair(term_it_t(), term_it_t());
        }
    }

private:
    struct stmt_upgrader {
        typedef triple_t argument_type;
        typedef stmt_t result_type;
        result_type operator()(const argument_type& t) const {
            return graph->upgrade(t);
        }
        const graph_t* graph;
    };
public:
    typedef transform_fwd_it<dqry::generator_iterator<generator>, stmt_upgrader> it_t;
    typedef std::pair<it_t, it_t> range_t;
    range_t range(const qt_t& t) const {
        return transform_range(dqry::mk_generator_range((*this)(t)),
                               stmt_upgrader{&graph});
    }
    range_t range(const char* s, const char* p, const char* o) const {
        try {
            return range(qt_t::parse(graph.store(), s, p, o));
        } catch (const term_not_found&) {
            return std::make_pair(it_t(), it_t());
        }
    }

    typedef fwd_viterator<stmt_t> vit_t;
    typedef std::pair<vit_t, vit_t> vrange_t;
    vrange_t vrange(const qt_t& t) const {return mk_vrange(range(t));}
    vrange_t vrange(const char* s, const char* p, const char* o) const {
        return mk_vrange(range(s, p, o));
    }

    const graph_t& graph;
private:
    mutable ev_t ev;
    term_t rdf_type;
    dqry::term<term_t> qrdf_type;
    std::shared_ptr<nc_map_t> subcls;
    std::shared_ptr<nc_map_t> subprop;
    std::shared_ptr<dr_map_t> dr;
    bool m_use_dr_with_var_subj, m_use_dr, m_nest_omp;
};


}}  // namespace shacldator::inf


#endif /* SRC_INF_MICROWAVE_HPP_ */
