/*
 * backward.hpp
 *
 *  Created on: May 23, 2018
 *      Author: alexis
 */

#ifndef SRC_INF_BACKWARD_HPP_
#define SRC_INF_BACKWARD_HPP_

#include <memory>
#include <atomic>
#include <vector>
#include <map>
#include <string>
#include <functional>
#include <stack>
#include <boost/mpl/quote.hpp>
#include <boost/mpl/placeholders.hpp>

#include "../dqry/triple.hpp"
#include "../graph/graph.hpp"
#include "../graph/statement.hpp"
#include "../graph/str/augmented/store.hpp"
#include "ruleset.hpp"
#include "dsl_inf_eval.hpp"
#include "dqry/nested_join.hpp"
#include "dqry/generator_iterator.hpp"
#include "dqry/explicit_generator.hpp"
#include "../qry/dsl_eval.hpp"
#include "../viterator.hpp"
#include "../utils.hpp"
#include "../qry/iterators/dsl_join_iterators.hpp"

namespace shacldator {namespace inf {

struct abstract_backward {};
template<class X>
struct is_backward : public std::is_base_of<abstract_backward, X> {};

template<class Graph,
         class AugmentedTypeFactory=str::augmented::safe_type_fac,
         class EvaluatorTypeFactory=boost::mpl::quote1<qry::def_evaluator>
> struct backward : public abstract_backward {

    typedef Graph raw_graph_t;
    typedef typename Graph::store_t raw_store_t;
    typedef typename raw_store_t::term_t raw_term_t;
    typedef typename AugmentedTypeFactory::template apply<raw_store_t>::type
            store_t;
    typedef typename store_t::term_t term_t;
    typedef str::triple<term_t> triple_t;
    typedef shacldator::graph<store_t> graph_t;
    typedef typename EvaluatorTypeFactory::template apply<graph_t>::type ev_t;
    typedef dqry::explicit_generator<ev_t> explicit_g_t;
    typedef statement<graph_t> stmt_t;

    typedef dqry::triple<term_t> qt_t;

    backward(const Graph& g, ruleset&& rs)
             : raw_g(g), graph(store_t(&raw_g.store())), ev(graph),
               rs(std::move(rs)) {
        //register all strings used by rules in advance
        //only blank nodes are introduced on-demand.
        store_t& astr = aug_str();
        for (auto i = rs.begin(), e = rs.end(); i != e; ++i) {
            i->for_each_term([&](const ruleset::term& t) {
                astr.create_term(rs.str(t));
            });
        }
    }
    backward(backward&&) = default;
    backward& operator=(backward&&) = default;
    ~backward() = default;

    struct generator {
        typedef const ruleset::rule* rule_ptr;
        typedef backward::term_t term_t;
        typedef backward::triple_t triple_t;
        static const std::size_t rule_idx_start =
                std::numeric_limits<std::size_t>::max();
    private:
        struct goal {
            goal(const qt_t& q, std::weak_ptr<goal> p) : query(q), parent(p) {}

            bool has_cycle(const qt_t& t) const {
                typedef std::shared_ptr<goal> p_t;
                for (p_t p = parent.lock(); p; p = p->parent.lock()) {
                    if (p->query == t) return true;
                }
                return false;
            }

            qt_t query;
            std::weak_ptr<goal> parent;
        };
        struct D {
            D(const backward* bw, const qt_t& t, std::vector<rule_ptr>&& rules,
              const std::shared_ptr<goal>& parent = std::shared_ptr<goal>())
              : users(1), bw(bw), g(new goal(t, parent)),
                rule_idx(rule_idx_start), rules(std::move(rules)),
                head_i(rule_idx_start) {}
            D(const D& o) : users(1), bw(o.bw),
                            g(o.g), current(o.current),
                            rule_idx(o.rule_idx), rules(o.rules),
                            head(o.head), head_i(o.head_i),
                            body(o.body),
                            explicit_g(o.explicit_g), nj(o.nj) {
            }
            ~D() = default;

            generator create_child(const qt_t& t) {
                return generator(bw, t, g);
            }

            std::string get_pref() {
                if (!g->parent.lock()) return "[B] ";
                int c = 0;
                for(auto p = g->parent.lock(); p; p = p->parent.lock()) ++c;
                std::stringstream ss;
                for (int i = 0; i < c; ++i) ss << "  ";
                ss << "[b] ";
                return ss.str();
            }

            bool advance() {
                using namespace std::placeholders;
                auto f = std::bind(&D::create_child, this, _1);
                current = triple_t();
                std::string pref = get_pref();

                while (rule_idx != rules.size()) {
                    if (rule_idx == rule_idx_start) {
                        if (!explicit_g)
                            explicit_g.reset(new explicit_g_t(bw->ev, g->query));
                        if (!explicit_g->advance()) {
//                            std::cout << pref << "depl_ex " << dbg_query_str() << std::endl;
                            explicit_g.reset();
                            ++rule_idx;
                        } else {
                            current = explicit_g->get();
                            break;
                        }
                    } else {
                        if (!nj) {
                            std::vector<qt_t> v = init_goals();
                            if (!v.empty()) {
                                assert(head_i < head.size());
                                nj.emplace(f, std::move(v), head[head_i], g->query);
                            }
                        }
                        if (!nj || !nj.get().advance()) {
//                            std::cout << pref << "depl (" << rule_idx << "," << head_i << ") "
//                                      << bw->rs.to_str(*rules[rule_idx]) << " hard:";
//                            if (nj) {
//                                auto bound = nj.get().get();
//                                for (unsigned i = 0; i < bound.size(); ++i) {
//                                    if (bound.is_hard(i)) {
//                                        std::cout << " ?" << i << "=" << bw->aug_str().str(bound[i]);
//                                    }
//                                }
//                            }
//                            std::cout << std::endl;
                            nj.unset();
                            if (++head_i >= head.size())
                                ++rule_idx; //rule_idx_start overflows to 0
                            else
                                nj.emplace(body, f, head[head_i], g->query);
                        } else {
                            current = nj.get().get().bind(head[head_i]);
                            assert(g->query.subsumes(qt_t(current)));
                            break;
                        }
                    }
                }

//                if (current.is_complete()) {
//                    std::cout << pref
//                              << (rule_idx==rule_idx_start ? "qry: " : "inf: ")
//                              << current.to_str(bw->aug_str()) << std::endl;
//                }
                return current.is_complete();
            }

            template<class UnryF>
            void parse_rule(std::vector<qt_t>& out,
                            std::map<std::string, unsigned>& nm2ph,
                            const std::vector<ruleset::triple>& triples,
                            UnryF&& f) {
                const ruleset& rs = bw->rs;
                const store_t& astr = bw->aug_str();
                out.reserve(triples.size());
                for (auto i = triples.begin(), e = triples.end(); i != e; ++i){
                    try {
                        qt_t t = qt_t::parse(astr, rs.str(i->subj()).c_str(),
                                                   rs.str(i->pred()).c_str(),
                                                   rs.str(i-> obj()).c_str(),
                                                   nm2ph);
                        if (f(t)) out.push_back(t);
                    } catch (const term_not_found&) {
                        //pass
                    }
                }
            }
            std::vector<qt_t> init_goals() {
                rule_ptr r = rules.at(rule_idx);
                const ruleset& rs = bw->rs;
                std::map<std::string, unsigned> nm2ph;
                r->for_each_body_term([&](const ruleset::term& t) {
                    if (t.is_var())
                        nm2ph.insert(std::make_pair(rs.str(t), nm2ph.size()+1));
                });

                head.clear();
                parse_rule(head, nm2ph, r->head,
                           [&](const qt_t& t) {return t.subsumes(g->query);});
//                std::cout << dbg_query_str() << " :: " << bw->rs.to_str(*r) << std::endl;
//                assert(!head.empty());

                head_i = 0;
                std::vector<qt_t> body_goals;
                if (!head.empty()) {
                    parse_rule(body_goals, nm2ph, r->body,
                               [](const qt_t& t){return true;});
                    if (head.size() > 1)
                        this->body = body_goals;
                }
                return body_goals;
            }
            std::string dbg_query_str() const {
                return g->query.to_str(bw->aug_str());
            }


            std::atomic<int> users;
            std::shared_ptr<goal> g;
            const backward* bw;
            triple_t current;
            std::size_t rule_idx;
            std::vector<rule_ptr> rules;
            std::vector<qt_t> head;
            std::size_t head_i;
            std::vector<qt_t> body;
            copyable_ptr<dqry::explicit_generator<ev_t>> explicit_g;
            pack<dqry::nested_join<generator>, false> nj;
        };

    public:
        generator(const backward* bw, const qt_t& t,
                  std::shared_ptr<goal> parent = std::shared_ptr<goal>())
                  : d(new D(bw, t, init_rules(bw, t, parent), parent)) {
            boost::sort(d->rules, [](rule_ptr l, rule_ptr r)
                    {return cost(l) < cost(r);});
        }
        generator(generator&& o) : d(std::move(o.d)) {}
        generator(const generator& o) : d(o.d) {++d->users;}
        generator& operator=(generator&& o) {
            if (this != &o) d = std::move(o.d);
            return *this;
        }
        generator& operator=(const generator& o) {
            if (this != &o) ++(d = o.d)->users;
            return *this;
        }
        ~generator() {
            if (d) --d->users;
        }

        const triple_t& get() const {
            return d->current;
        }

        bool advance() {
            int count = 1;
            if (!d->users.compare_exchange_strong(count, 1))
                d.reset(new D(*d));
            return d->advance();
        }

    private:
        static std::size_t cost(const rule_ptr& r) {
            std::size_t sum = 0;
            for (auto i = r->body.begin(), e = r->body.end(); i != e; ++i) {
                int vars = (i->subj().is_var() ? 1 : 0)
                         + (i->pred().is_var() ? 1 : 0)
                         + (i-> obj().is_var() ? 1 : 0);
                assert(vars >= 0 && vars <= 3);
                std::size_t x = 0;
                for(int i = 0; i < vars; ++i) x *= 10;
                sum += x;
            }
            return sum;
        }
        static std::vector<rule_ptr>
        init_rules(const backward* bw, const qt_t& t,
                   const std::shared_ptr<goal>& parent_g) {
            return parent_g && parent_g->has_cycle(t) ? std::vector<rule_ptr>{}
                                : bw->rs.relevant_heads(bw->graph.store(), t);
        }

        std::shared_ptr<D> d;
    };

    generator operator()(const qt_t& t) const {
        return generator(this, t);
    }

    typedef dqry::generator_iterator<generator> term_it_t;
    typedef std::pair<term_it_t, term_it_t> term_range_t;
    term_range_t term_range(const qt_t& t) const {
        return dqry::mk_generator_range((*this)(t));
    }
    term_range_t term_range(const char* s, const char* p, const char* o) const {
        try {
            return term_range(qt_t::parse(aug_str(), s, p, o));
        } catch (const term_not_found&) {
            return std::make_pair(term_it_t(), term_it_t());
        }
    }

private:
    struct stmt_upgrader {
        typedef triple_t argument_type;
        typedef stmt_t result_type;
        result_type operator()(const argument_type& t) const {
            return graph->upgrade(t);
        }
        const graph_t* graph;
    };
public:
    typedef transform_fwd_it<dqry::generator_iterator<generator>, stmt_upgrader> it_t;
    typedef std::pair<it_t, it_t> range_t;
    range_t range(const qt_t& t) const {
        return transform_range(dqry::mk_generator_range((*this)(t)),
                               stmt_upgrader{&graph});
    }
    range_t range(const char* s, const char* p, const char* o) const {
        try {
            return range(qt_t::parse(aug_str(), s, p, o));
        } catch (const term_not_found&) {
            return std::make_pair(it_t(), it_t());
        }
    }

    typedef fwd_viterator<stmt_t> vit_t;
    typedef std::pair<vit_t, vit_t> vrange_t;
    vrange_t vrange(const qt_t& t) const {return mk_vrange(range(t));}
    vrange_t vrange(const char* s, const char* p, const char* o) const {
        return mk_vrange(range(s, p, o));
    }

    const Graph& raw_g;
    const graph_t graph;
private:
    store_t& aug_str() const {
        return const_cast<store_t&>(graph.store());
    }

    ev_t ev;
    ruleset rs;
};

template<class Graph> struct backward_eval_fac :
        public auto_evaluator_fac<backward<Graph>> {
    typedef backward<Graph> reasoner_t;
    typedef std::shared_ptr<reasoner_t> reasoner_ptr_t;

    backward_eval_fac(boost::function<ruleset()> rs_prov)
            : auto_evaluator_fac<reasoner_t>([&](const Graph& g)
                  {return reasoner_ptr_t(new reasoner_t(g, rs_prov()));}
              ) {
    }
};


}}  // namespace shacldator::inf



#endif /* SRC_INF_BACKWARD_HPP_ */
