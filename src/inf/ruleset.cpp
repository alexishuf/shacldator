/*
 * ruleset.cpp
 *
 *  Created on: May 22, 2018
 *      Author: alexis
 */

#include "ruleset.hpp"
#include <boost/range/algorithm.hpp>
#include <boost/range/algorithm_ext.hpp>
#include <sstream>

using namespace std;
using boost::for_each;

namespace shacldator {namespace inf {

const string& ruleset::str(const term t) const {
    if (!t) throw invalid_argument("term_str(null) not allowed!");
    return strings.at(t.value());
}
std::string ruleset::to_str(const rule& r) const{
    std::stringstream sst, ss;

    boost::for_each(r.head, [&](const triple& t){sst << to_str(t) << ", ";});
    std::string s = sst.str();
    if (!s.empty()) ss << s.substr(0, s.length()-2);
    sst.str("");
    ss << " <- ";

    boost::for_each(r.body, [&](const triple& t){sst << to_str(t) << ", ";});
    s = sst.str();
    if (!s.empty()) ss << s.substr(0, s.length()-2);
    return ss.str();
}

void put_count(std::vector<size_t>& vec, size_t size) {
    vec.reserve(size);
    for (size_t i = 0; i < size; ++i) vec.push_back(i);
}

vector<const ruleset::rule*> ruleset::relevant_heads_impl(term s, term p,
                                                          term o) const {
    using namespace boost;
    //terms may be null, in which case term_* will always yield empty sets
    vector<size_t> t2;
    {
        vector<size_t> s_rs, p_rs, o_rs, t1;
        if (s) set_union(v_idx_s, t_idx_s.at(s.value()), back_inserter(s_rs));
        else put_count(s_rs, rules.size());
        if (p) set_union(v_idx_p, t_idx_p.at(p.value()), back_inserter(p_rs));
        else put_count(p_rs, rules.size());
        if (o) set_union(v_idx_o, t_idx_o.at(o.value()), back_inserter(o_rs));
        else put_count(o_rs, rules.size());

        t1.reserve(max({s_rs.size(), p_rs.size(), o_rs.size()}));
        set_intersection(s_rs, p_rs, back_inserter(t1));
        t2.reserve(t1.size());
        set_intersection(t1, o_rs, back_inserter(t2));
    }
    vector<const rule*> res;
    res.reserve(t2.size());
    size_t last = numeric_limits<size_t>::max();
    for (auto i = t2.begin(), e = t2.end(); i != e; ++i) {
        if (*i != last) res.push_back(&rules[last = *i]);
    }
    return res;
}

ruleset::term ruleset::get_term(const string& s) const {
    auto i = boost::lower_bound(strings, s);
    if (i == strings.end() || *i != s) return term();
    term::type_t tp;
    switch (*s.begin()) {
        case '"': tp = term::LITERAL; break;
        case '?': tp = term::VAR; break;
        default : tp = term::URI; break;
    }
    auto value = i - strings.begin();
    return term(value ? tp : term::NONE, value);
}

template<class Container> void add_strings(set<string>& set,
                                           const Container& c) {
    for (auto i = c.begin(), e = c.end(); i != e; ++i) {
        set.insert(get<0>(*i));
        set.insert(get<1>(*i));
        set.insert(get<2>(*i));
    }
}

ruleset::triple
ruleset::builder::promote(const ruleset& rs, const proto_triple& pt) {
    triple t(rs.get_term(get<0>(pt)), rs.get_term(get<1>(pt)),
             rs.get_term(get<2>(pt)));
    assert(t.is_complete());
    return t;
}
void ruleset::builder::add_rule(ruleset& rs, const ruleset::rule& r) {
    auto idx = rs.rules.size();
    rs.rules.push_back(r);
    for_each(r.head, [&](const triple& t) {
        assert(t.is_complete());
        if (t.subj().is_var()) rs.v_idx_s.push_back(idx);
        else rs.t_idx_s[t.subj().value()].push_back(idx);

        if (t.pred().is_var()) rs.v_idx_p.push_back(idx);
        else rs.t_idx_p[t.pred().value()].push_back(idx);

        if (t.obj ().is_var()) rs.v_idx_o.push_back(idx);
        else rs.t_idx_o [t.obj ().value()].push_back(idx);
    });
}

ruleset ruleset::builder::build() {
    ruleset rs;
    {
        set<string> sorted;
        for(auto i = rules.begin(), e = rules.end(); i != e; ++i) {
            add_strings(sorted, i->first);
            add_strings(sorted, i->second);
        }
        rs.strings.push_back(string()); //0-index
        for_each(sorted, [&](const string& s) {rs.strings.push_back(s);});
    } //free sorted

    //allocate all indexes
    rs.t_idx_s.assign(rs.strings.size(), vector<size_t>());
    rs.t_idx_p.assign(rs.strings.size(), vector<size_t>());
    rs.t_idx_o.assign(rs.strings.size(), vector<size_t>());
    rs.v_idx_s.reserve(rs.strings.size());
    rs.v_idx_p.reserve(rs.strings.size()/4 + 1); //vars here are less common
    rs.v_idx_o.reserve(rs.strings.size());

    //add all rules and fill indexes
    auto& m = *this;
    for_each(rules, [&](const proto_rule& pr) {
        ruleset::rule r;
        r.head.reserve(pr.first.size());
        for_each(pr.first,
                [&](const proto_triple& p){r.head.push_back(m.promote(rs,p));});
        r.body.reserve(pr.second.size());
        for_each(pr.second,
                [&](const proto_triple& p){r.body.push_back(m.promote(rs,p));});
        m.add_rule(rs, r);
    });

    //free memory from empty vectors
    for_each(rs.t_idx_s, [](vector<size_t>& v){v.shrink_to_fit();});
    for_each(rs.t_idx_p, [](vector<size_t>& v){v.shrink_to_fit();});
    for_each(rs.t_idx_o , [](vector<size_t>& v){v.shrink_to_fit();});
    return rs;
}

ruleset::builder::rule_builder&
ruleset::builder::rule_builder::head(const char* s, const char* p,
                                     const char* o) {
    assert(s);
    assert(p);
    assert(o);
    this->p.first.push_back(proto_triple(s, p, o));
    return *this;
}
ruleset::builder::rule_builder&
ruleset::builder::rule_builder::body(const char* s, const char* p,
                                     const char* o) {
    assert(s);
    assert(p);
    assert(o);
    this->p.second.push_back(proto_triple(s, p, o));
    return *this;
}
ruleset::builder&
ruleset::builder::rule_builder::create() {
    b->rules.push_back(p);
    return *b;
}


}} // namespace shacldator::inf

namespace std {
ostream& operator<<(ostream& o, shacldator::inf::ruleset::term t) {
    using namespace shacldator::inf;
    const char* p = "X";
    switch (t.type()) {
        case ruleset::term::NONE   : p = "N"; break;
        case ruleset::term::LITERAL: p = "L"; break;
        case ruleset::term::VAR    : p = "V"; break;
        case ruleset::term::URI    : p = "U"; break;
        default                    : p = "X"; break;
    }
    return o << p << t.value();
}

ostream& operator<<(ostream& o, shacldator::inf::ruleset::rule r) {
    using namespace shacldator::inf;
    stringstream ss;
    boost::for_each(r.head, [&](const ruleset::triple& t){ss << t << ", ";});
    auto str = ss.str();
    if (!str.empty()) o << str.substr(0, str.length()-2);

    o << " <- ";

    ss.str(string());
    boost::for_each(r.body, [&](const ruleset::triple& t){ss << t << ", ";});
    str = ss.str();
    if (!str.empty()) o << str.substr(0, str.length()-2);
    return o;
}
}  // namespace std
