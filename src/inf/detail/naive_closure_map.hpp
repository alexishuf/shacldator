/*
 * naive_closure_map.hpp
 *
 *  Created on: Jun 21, 2018
 *      Author: alexis
 */

#ifndef SRC_INF_NAIVE_CLOSURE_MAP_HPP_
#define SRC_INF_NAIVE_CLOSURE_MAP_HPP_

#include <vector>
#include <stack>
#include <set>
#include <memory>
#include <type_traits>
#include <iostream>
#include <omp.h>
#include <boost/range/algorithm.hpp>
#include <boost/dynamic_bitset.hpp>
#include "ro_multimap.hpp"
#include "../../graph/graph.hpp"
#include "../../qry/dsl_eval.hpp"

namespace shacldator {namespace inf {namespace detail {

template<class Term> struct naive_closure_map :
        public ro_multimap<Term, Term> {
    typedef Term term_t;
protected:
    naive_closure_map(const naive_closure_map& other) = default;
public:
    naive_closure_map() = default;
    naive_closure_map(naive_closure_map&&) = default;
    naive_closure_map& operator=(naive_closure_map&&) = default;

private:
    template<class Graph>
    static void fill_plain_map(const resource<Graph>& property,
                               bool reverse, naive_closure_map& map) {
        using namespace qry;
        def_evaluator<Graph> ev(property.graph());
        auto rng = ev.str_eval(_t(_1, property, _2));
        //apply all explicit assertions
        for (; rng.first != rng.second; ++rng.first) {
            term_t from, to;
            if (reverse) {
                from = (*rng.first)[_2];
                to = (*rng.first)[_1];
            } else {
                from = (*rng.first)[_1];
                to = (*rng.first)[_2];
            }
            map.add(from, to);
            //handle reflexivity and end-of-tree:
            map.add(from, from);
            map.add(to, to);
        }
    }

public:

    template<class Graph>
    static naive_closure_map omp_reuse_build(const resource<Graph>& property,
                                             bool reverse) {
        naive_closure_map plain_map;
        if (!property) return plain_map;
        fill_plain_map(property, reverse, plain_map);
        std::size_t keys_count = plain_map.keys_count();
        naive_closure_map out_map(plain_map);
        boost::dynamic_bitset<> closed(keys_count);
#pragma omp parallel for schedule(dynamic)
        for (std::size_t pos = 0; pos < keys_count; ++pos) {
            boost::dynamic_bitset<> visited(keys_count);
            std::stack<std::size_t> stack;
            stack.push(pos);
            while (!stack.empty()) {
                auto top = stack.top();
                assert(top != -1);
                stack.pop();
                if (visited[top]) continue;
                visited.set(top);
                const auto& top_set = plain_map.at_pos(top);
                out_map.at_pos(pos).add_all(top_set);
                bool is_closed = false;
#pragma omp critical (naive_closure_map__reuse_build_closed)
                is_closed = closed.test(pos);
                if (!is_closed) {
                    for (auto it = top_set.begin(),
                               e = top_set.end(); it != e; ++it) {
                        stack.push(plain_map.find_pos(*it));
                    }
                }
            }
#pragma omp critical (naive_closure_map__reuse_build_closed)
            closed.set(pos);
        }
        return out_map;
    }


    template<class Graph>
    static naive_closure_map omp_build(const resource<Graph>& property,
                                       bool reverse) {
        naive_closure_map plain_map;
        if (!property) return plain_map;
        fill_plain_map(property, reverse, plain_map);
        std::size_t keys_count = plain_map.keys_count();
        naive_closure_map out_map(plain_map);
        boost::dynamic_bitset<> my_closed(keys_count);
#pragma omp parallel for firstprivate(my_closed) schedule(dynamic)
        for (std::size_t pos = 0; pos < keys_count; ++pos) {
            boost::dynamic_bitset<> visited(keys_count);
            std::stack<std::size_t> stack;
            stack.push(pos);
            while (!stack.empty()) {
                auto top = stack.top();
                assert(top != -1);
                stack.pop();
                if (visited[top]) continue;
                visited.set(top);
                const auto& top_set = plain_map.at_pos(top);
                out_map.at_pos(pos).add_all(top_set);
                if (!my_closed.test(pos)) {
                    for (auto it = top_set.begin(),
                               e = top_set.end(); it != e; ++it) {
                        stack.push(plain_map.find_pos(*it));
                    }
                }
            }
            my_closed.set(pos);
        }
        return out_map;
    }


    template<class Graph>
    static naive_closure_map build(const resource<Graph>& property,
                                   bool reverse) {
        static_assert(std::is_same<typename Graph::store_t::term_t,
                                   term_t>::value,
                      "g's term_t must match naive_closure_map");
        using namespace qry;
        naive_closure_map map;
        if (!property) return map;
        fill_plain_map(property, reverse, map);
        //apply transitivity
        for (auto pos = 0; pos != map.keys_count(); ++pos) {
            boost::dynamic_bitset<> visited(map.keys_count());
            std::stack<std::size_t> stack;
            stack.push(pos);
            while (!stack.empty()) {
                auto top = stack.top();
                assert(top != -1);
                stack.pop();
                if (visited[top]) continue;
                visited.set(top);
                const auto& top_set = map.at_pos(top);
                map.at_pos(pos).add_all(top_set);
                if (top < pos) continue; //below it, all closures are complete
                for (auto it = top_set.begin(),
                           e = top_set.end(); it != e; ++it) {
                    stack.push(map.find_pos(*it));
                }
            }
        }
        return map;
    }
};

namespace naive_closure_map_build {

template<class Graph> struct serial {
    typedef naive_closure_map<typename Graph::store_t::term_t> result_type;
    result_type operator()(const resource<Graph>& p, bool reverse) const {
        return result_type::build(p, reverse);
    }
};

template<class Graph> struct omp {
    typedef naive_closure_map<typename Graph::store_t::term_t> result_type;
    result_type operator()(const resource<Graph>& p, bool reverse) const {
        return result_type::omp_build(p, reverse);
    }
};

template<class Graph> struct omp_reuse {
    typedef naive_closure_map<typename Graph::store_t::term_t> result_type;
    result_type operator()(const resource<Graph>& p, bool reverse) const {
        return result_type::omp_reuse_build(p, reverse);
    }
};

}  // namespace naive_closure_map_build


}}}  // namespace shacldator::inf::detail


#endif /* SRC_INF_NAIVE_CLOSURE_MAP_HPP_ */
