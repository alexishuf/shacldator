/*
 * domain_range_map.hpp
 *
 *  Created on: Jun 21, 2018
 *      Author: alexis
 */

#ifndef SRC_INF_DETAIL_DOMAIN_RANGE_MAP_HPP_
#define SRC_INF_DETAIL_DOMAIN_RANGE_MAP_HPP_

#include "naive_closure_map.hpp"
#include "ro_multimap.hpp"
#include "../../ns/rdfs.hpp"

namespace shacldator {namespace inf {namespace detail {

template<class Term> struct domain_range_map {
    typedef Term term_t;
private:
    domain_range_map(ro_multimap<term_t, term_t>&& domain,
                     ro_multimap<term_t, term_t>&& range)
                     : m_domain_of(std::move(domain)),
                       m_range_of(std::move(range)) {}
public:
    domain_range_map() = default;
    domain_range_map(domain_range_map&&) = default;
    domain_range_map& operator=(domain_range_map&&) = default;

    typedef ro_set<term_t> set_t;
    typedef typename set_t::iterator set_it_t;

    const set_t& domain_of(const term_t& a_class) const {
        return m_domain_of.at(a_class);
    }
    const set_t& range_of(const term_t& a_class) const {
        return m_range_of.at(a_class);
    }

    template<class Graph>
    static domain_range_map build(const Graph& g) {
        static_assert(std::is_same<typename Graph::store_t::term_t,
                                   term_t>::value,
                      "g's term_t must match domain_range_map");
        using namespace qry;
        def_evaluator<Graph> ev(g);
        ro_multimap<term_t, term_t> domain, range;
        {
            auto rng = ev.str_eval(_t(_1, rdfs::domain, _2));
            for (; rng.first != rng.second; ++rng.first)
                domain.add((*rng.first)[_2], (*rng.first)[_1]);
        }
        {
            auto rng = ev.str_eval(_t(_1, rdfs::range, _2));
            for (; rng.first != rng.second; ++rng.first)
                range.add((*rng.first)[_2], (*rng.first)[_1]);
        }
        return domain_range_map(std::move(domain), std::move(range));
    }

    template<class Graph>
       static domain_range_map omp_build(const Graph& g) {
           static_assert(std::is_same<typename Graph::store_t::term_t,
                                      term_t>::value,
                         "g's term_t must match domain_range_map");
           using namespace qry;
           def_evaluator<Graph> ev(g);
           ro_multimap<term_t, term_t> domain, range;
#pragma omp parallel
           {
#pragma omp single nowait
               {
                   auto rng = ev.str_eval(_t(_1, rdfs::domain, _2));
                   for (; rng.first != rng.second; ++rng.first)
                       domain.add((*rng.first)[_2], (*rng.first)[_1]);
               }
#pragma omp single nowait
               {
                   auto rng = ev.str_eval(_t(_1, rdfs::range, _2));
                   for (; rng.first != rng.second; ++rng.first)
                       range.add((*rng.first)[_2], (*rng.first)[_1]);
               }
           }
           return domain_range_map(std::move(domain), std::move(range));
       }

private:
    ro_multimap<term_t, term_t> m_domain_of;
    ro_multimap<term_t, term_t> m_range_of;
};

}}}  // namespace shacldator::inf::detail


#endif /* SRC_INF_DETAIL_DOMAIN_RANGE_MAP_HPP_ */
