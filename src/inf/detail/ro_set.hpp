/*
 * ro_set.hpp
 *
 *  Created on: Jun 23, 2018
 *      Author: alexis
 */

#ifndef SRC_INF_DETAIL_RO_SET_HPP_
#define SRC_INF_DETAIL_RO_SET_HPP_

#include <vector>
#include <boost/range/algorithm.hpp>

template<class V> struct ro_set {
    ro_set() = default;
    ro_set(const ro_set&) = default;
    ro_set(ro_set&&) = default;
    ro_set& operator=(const ro_set&) = default;
    ro_set& operator=(ro_set&&) = default;

    bool contains(const V& value) const {
        return boost::binary_search(data, value);
    }

    typedef typename std::vector<V>::const_iterator iterator;
    typedef iterator const_iterator;
    iterator begin() const {return data.begin();}
    iterator end() const {return data.end();}

    std::size_t size() const {return data.size();}

    bool add(const V& value) {
        auto it2 = boost::lower_bound(data, value);
        if (it2 != data.end() && *it2 == value)
            return false;
        data.insert(it2, value);
        return true;
    }

    template<class Range> bool add_all(const Range& r) {
        if (boost::empty(r)) return false;

        bool change = false;
        auto in = boost::begin(r);
        auto out = boost::lower_bound(data, *in);
        for (auto in_e = boost::end(r); in != in_e; ++in) {
            out = std::lower_bound(out, data.end(), *in);
            if ((change = (out == data.end() || *out != *in)))
                out = data.insert(out, *in);
        }
        return change;
    }

private:
    std::vector<V> data;
};


#endif /* SRC_INF_DETAIL_RO_SET_HPP_ */
