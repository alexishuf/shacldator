/*
 * ro_map.hpp
 *
 *  Created on: Jun 23, 2018
 *      Author: alexis
 */

#ifndef SRC_INF_DETAIL_RO_MULTIMAP_HPP_
#define SRC_INF_DETAIL_RO_MULTIMAP_HPP_

#include "ro_set.hpp"

template<class K, class V> struct ro_multimap {
private:
    struct entry {
        explicit entry(K key) : key(key) {}
        entry(K key, ro_set<V>&& value)
              : key(key), set(std::move(value)) {}
        entry(const entry&) = default;
        entry(entry&&) = default;
        entry& operator=(const entry&) = delete;
        entry& operator=(entry&&) = default;

        bool operator<(const entry& o) const {
            return key < o.key;
        }

        K key;
        ro_set<V> set;
    };

public:
    ro_multimap() = default;
    ro_multimap(const ro_multimap&) = default;
    ro_multimap(ro_multimap&&) = default;
    ro_multimap& operator=(const ro_multimap&) = default;
    ro_multimap& operator=(ro_multimap&&) = default;

    const ro_set<V>& at(const K& k) const {
        thread_local ro_set<V> empty;
        auto it = boost::lower_bound(data, entry(k));
        return (it == data.end() || it->key != k) ? empty : it->set;
    }
    ro_set<V>& at(const K& k) {
        return const_cast<ro_set<V>&>(((const ro_multimap*)this)->at(k));
    }

    const ro_set<V>& at_pos(std::size_t p) const {return data.at(p).set;}
    ro_set<V>& at_pos(std::size_t p) {return data.at(p).set;}

    static const std::size_t npos = -1;

    std::size_t find_pos(const K& k) const {
        auto it = boost::lower_bound(data, entry(k));
        return it == data.end() ? npos : it - data.begin();
    }

    std::size_t keys_count() const {return data.size();}

    bool contains(const K& k, const V& v) const {return at(k).contains(v);}

    ro_set<V>& operator[](const K& k) {
        auto it = boost::lower_bound(data, entry(k));
        if (it == data.end() || it->key != k)
            it = data.insert(it, entry(k));
        return it->set;
    }

    bool add(const K& k, const V& v) {return (*this)[k].add(v);}

private:
    std::vector<entry> data;
};


#endif /* SRC_INF_DETAIL_RO_MULTIMAP_HPP_ */
