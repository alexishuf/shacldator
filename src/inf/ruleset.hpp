/*
 * ruleset.hpp
 *
 *  Created on: May 22, 2018
 *      Author: alexis
 */

#ifndef SRC_INF_RULESET_HPP_
#define SRC_INF_RULESET_HPP_

#include "../graph/terms.hpp"
#include "../graph/str/triple.hpp"
#include <cstdint>
#include <sstream>
#include <tuple>
#include <cassert>
#include <string>
#include <vector>
#include <set>

#include "../dqry/term.hpp"
#include "../dqry/triple.hpp"

namespace shacldator {namespace inf {

class ruleset {
public:
    struct term {
        typedef enum {
            NONE = 0x0,
            LITERAL = 0x01,
            VAR = 0x02,
            URI = 0x03,
            _TYPE_COUNT
        } type_t;

        term() : data(0) {}
        term(const term& o) = default;
        term& operator=(const term& o) = default;
        term(type_t t, uint32_t value)
             : data(((uint32_t)t << 30) | (value & 0x3fffffff)) {
            assert(t < _TYPE_COUNT);
            assert((value & 0xc0000000) == 0);
            assert((t == NONE) == (value == 0));
        }
        ~term() = default;

        inline type_t type() const {return (type_t)((data&0xc0000000) >> 30);}
        inline terms::type terms_type() const {
            type_t t = type();
            switch (t) {
                case LITERAL: return terms::LITERAL;
                case URI: return terms::URI;
                default: break;
            }
            return terms::NONE;
        }
        inline uint32_t value() const {return data & 0x3fffffff;}

        inline bool operator!() const {return !data;}
        inline  operator bool() const {return  data;}

        inline bool is_resource() const {return type() == URI;}
        inline bool is_literal() const {return type() == LITERAL;}
        inline bool is_var() const {return type() == VAR;}

        inline bool operator==(const term o) const {return data == o.data;}
        inline bool operator!=(const term o) const {return data != o.data;}
        inline bool operator<(const term o) const {return data < o.data;}
        inline bool operator<=(const term o) const {return data <= o.data;}
        inline bool operator>(const term o) const {return data > o.data;}
        inline bool operator>=(const term o) const {return data >= o.data;}

        uint32_t data;
    };

    typedef str::triple<term> triple;

    struct rule {
        std::vector<triple> head;
        std::vector<triple> body;

        template<class UnryF> void for_each_head_term(UnryF&& f) const {
            for (auto i = head.begin(), e = head.end(); i != e; ++i) {
                f(i->subj());
                f(i->pred());
                f(i->obj());
            }
        }
        template<class UnryF> void for_each_body_term(UnryF&& f) const {
            for (auto i = body.begin(), e = body.end(); i != e; ++i) {
                f(i->subj());
                f(i->pred());
                f(i->obj());
            }
        }
        template<class UnryF> void for_each_term(UnryF&& f) const {
            for_each_head_term(std::forward<UnryF>(f));
            for_each_body_term(std::forward<UnryF>(f));
        }
        template<class UnryF> void for_each_triple(UnryF&& f) const {
            for (auto i = head.begin(), e = head.end(); i != e; ++i) f(*i);
            for (auto i = body.begin(), e = body.end(); i != e; ++i) f(*i);
        }

        bool operator==(const rule& o) const {
            return head == o.head && body == o.body;
        }
        bool operator<(const rule& o) const {
            return head < o.head || (head == o.head && body < o.body);
        }
        bool operator!=(const rule& o) const {return !(*this == o);}
        bool operator<=(const rule& o) const {return *this < o || *this == o;}
        bool operator>(const rule& o) const {return !(*this <= o);}
        bool operator>=(const rule& o) const {return !(*this < o);}
    };

    struct builder {
        typedef std::tuple<std::string, std::string, std::string> proto_triple;
        typedef std::pair<std::vector<proto_triple>, std::vector<proto_triple>>
                proto_rule;
        typedef std::vector<proto_rule> proto_rules;
        struct rule_builder {
            rule_builder(rule_builder&&) = default;

            rule_builder& head(const char* s, const char* p, const char* o);
            rule_builder& body(const char* s, const char* p, const char* o);
            builder& create();
        private:
            friend builder;
            rule_builder(builder* b) : b(b) {}
            builder* b;
            proto_rule p;
        };

        inline rule_builder rule() {return rule_builder(this);}
        ruleset build();
    private:
        triple promote(const ruleset& rs, const proto_triple& pt);
        void add_rule(ruleset& rs, const ruleset::rule& r);
        proto_rules rules;
    };

    ruleset() = default;
    ruleset(ruleset&&) = default;
    ~ruleset() = default;

    typedef std::vector<rule>::const_iterator iterator;
    typedef iterator const_iterator;
    inline const_iterator begin() const {return rules.begin();}
    inline const_iterator end() const {return rules.end();}
    inline std::size_t size() const {return rules.size();}
    inline const rule& operator[](std::size_t i) const {return rules.at(i);}

    const std::string& str(const term t) const;
    std::string to_str(const triple& t) const {return t.to_str(*this);}
    std::string to_str(const rule& t) const;

    term get_term(const std::string& s) const;
    inline term get_term(const char* s) const {
        return get_term(s ? std::string(s) : std::string());
    }
    inline term get_term(term t) const {return t;}
    inline term get_term(const dqry::term<term>& t) const {
        return t.is_term() ? t.term_v : term();
    }
    template<class GS>
    inline term get_term(const GS& gs,
                         const dqry::term<typename GS::term_t>& t) const {
        if (t.is_ph()) return term();
        std::string s = gs.str(t.term_v);
        if (s.empty()) {
            std::stringstream ss; ss << "Bad term: " << t.term_v;
            throw std::invalid_argument(ss.str());
        }
        return get_term(s);
    }

public:
    std::vector<const rule*> relevant_heads(const dqry::triple<term>& t) const {
        return relevant_heads_impl(get_term(t.subj), get_term(t.pred),
                                   get_term(t.obj));
    }
    template<class GS> std::vector<const rule*>
    relevant_heads(const GS& gs,
                   const dqry::triple<typename GS::term_t>& t) const {
        return relevant_heads_impl(get_term(gs, t.subj), get_term(gs, t.pred),
                                   get_term(gs, t.obj));
    }
    template<class S, class P, class O> std::vector<const rule*>
    relevant_heads(const S& s, const P& p, const O& o) const {
        return relevant_heads_impl(get_term(s), get_term(p), get_term(o));
    }

private:
    std::vector<const rule*> relevant_heads_impl(term s, term p, term o) const;

    std::vector<rule> rules;
    std::vector<std::string> strings;
    std::vector<std::vector<std::size_t>> t_idx_s;
    std::vector<std::vector<std::size_t>> t_idx_p;
    std::vector<std::vector<std::size_t>> t_idx_o;
    std::vector<std::size_t> v_idx_s;
    std::vector<std::size_t> v_idx_p;
    std::vector<std::size_t> v_idx_o;
};

}} // namespace shacldator::inf

namespace std {
ostream& operator<<(ostream& o, shacldator::inf::ruleset::term t);
ostream& operator<<(ostream& o, shacldator::inf::ruleset::rule r);
}  // namespace std


#endif /* SRC_INF_RULESET_HPP_ */
