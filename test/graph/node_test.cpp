/*
 * node_test.cpp
 *
 *  Created on: Apr 30, 2018
 *      Author: alexis
 */

#include <boost/test/unit_test.hpp>
#include <set>
#include <vector>
#include <boost/range/algorithm.hpp>
#include <boost/range/algorithm_ext.hpp>
#include "graph/node.hpp"
#include "graph/graph.hpp"
#include "graph/str/stl/store.hpp"
#include "graph/str/raptor_builder.hpp"
#include "ns/rdf.hpp"
#include "ns/xsd.hpp"
#include "ns/foaf.hpp"

using namespace std;
namespace tt = boost::test_tools;

namespace shacldator {

BOOST_AUTO_TEST_SUITE(node_test);

typedef str::stl::store::term_t term_t;
typedef graph<str::stl::store> graph_t;
typedef shacldator::node<graph_t> node_t;
typedef shacldator::resource<graph_t> resource_t;
typedef shacldator::literal<graph_t> literal_t;

struct fixture {
    static graph<str::stl::store> load() {
        str::raptor_builder<str::stl::store::builder> b;
        b.parse("turtle", "test-files/node-test.ttl");
        return mk_graph(b.builder.build());
    }

    fixture() : g(load()), s(g.store()) {}
    graph<str::stl::store> g;
    str::stl::store& s;
};

#define EX(localName) ("http://example.org/ns#" BOOST_PP_STRINGIZE(localName))


BOOST_FIXTURE_TEST_CASE(default_is_false, fixture) {
    node_t def;
    BOOST_REQUIRE(!def);
    BOOST_REQUIRE(bool(g.resource(EX(A))));
    BOOST_REQUIRE(bool(g.literal("Alice", rdf::langString, "en")));
    BOOST_REQUIRE(bool(node_t(g.resource(EX(A)))));
    BOOST_REQUIRE(bool(node_t(g.literal("Alice", rdf::langString, "en"))));
}

BOOST_FIXTURE_TEST_CASE(null_nodes_equal, fixture) {
    fixture f2;
    node_t a, b, b1(term_t(), g), b2(term_t(), f2.g);
    BOOST_REQUIRE(bool(a == b));
    BOOST_REQUIRE(bool(b == a));
    BOOST_REQUIRE(bool(b1 == b2));
    BOOST_REQUIRE(bool(b2 == b1));
    BOOST_REQUIRE(bool(a == b1));
    BOOST_REQUIRE(bool(b1 == b));
    BOOST_REQUIRE(bool(b2 == a));
    BOOST_REQUIRE(bool(b == b2));
}

BOOST_FIXTURE_TEST_CASE(as_string, fixture) {
    literal_t alice = g.literal("Alice", rdf::langString, "en");
    literal_t bob = g.literal("Bob", xsd::string_, nullptr);
    BOOST_REQUIRE(alice);
    BOOST_REQUIRE(bob);

    BOOST_REQUIRE(alice.as<string>() == "Alice");
    BOOST_REQUIRE(bob.as<string>() == "Bob");
}

BOOST_FIXTURE_TEST_CASE(as_integral, fixture) {
    literal_t alice = g.literal("22", xsd::int_, nullptr);
    literal_t bob = g.literal("23", xsd::nonNegativeInteger_, nullptr);
    BOOST_REQUIRE(alice);
    BOOST_REQUIRE(bob);

    BOOST_REQUIRE(alice.as<string>() == "22");
    BOOST_REQUIRE(alice.as<int>() == 22);
    BOOST_REQUIRE(alice.as<std::size_t>() == std::size_t(22));
    BOOST_REQUIRE(bob.as<string>() == "23");
    BOOST_REQUIRE(bob.as<int>() == 23);
    BOOST_REQUIRE(bob.as<std::size_t>() == std::size_t(23));
}

BOOST_FIXTURE_TEST_CASE(as_floating, fixture) {
    literal_t alice = g.literal("22", xsd::int_, nullptr);
    literal_t charlie = g.literal("22.34", xsd::float_, nullptr);
    BOOST_REQUIRE(alice);
    BOOST_REQUIRE(charlie);

    BOOST_REQUIRE(charlie.as<string>() == "22.34");
    BOOST_TEST(charlie.as<float>() == 22.34, tt::tolerance(0.0001));
    BOOST_TEST(charlie.as<double>() == 22.34, tt::tolerance(0.0001));
}

BOOST_FIXTURE_TEST_CASE(bad_as_integral, fixture) {
    literal_t charlie = g.literal("22.34", xsd::float_, nullptr);
    BOOST_REQUIRE(charlie);

    BOOST_REQUIRE_THROW(charlie.as<int>(), std::bad_cast);
}

BOOST_FIXTURE_TEST_CASE(bad_as_unsigned, fixture) {
    literal_t dave = g.literal("-8", xsd::int_, nullptr);
    BOOST_REQUIRE(dave);

    BOOST_REQUIRE(dave.as<int>() == -8);
    BOOST_REQUIRE_THROW(dave.as<unsigned int>(), bad_literal_cast);
}

BOOST_AUTO_TEST_CASE(is_blank) {
    str::stl::store::builder b;
    auto blank = b.blank_term("_:b0");
    auto type = b.uri_term(rdf::type);
    auto person = b.uri_term(foaf::Person);
    b.add_triple(blank, type, person);
    graph_t g(b.build());
    node_t node = g.resource("_:b0");
    BOOST_REQUIRE(node);
    BOOST_REQUIRE(node.is_resource());
    BOOST_REQUIRE(node.is_blank());
    BOOST_REQUIRE(node_t(node).is_resource());
    BOOST_REQUIRE(node_t(node).is_blank());
    BOOST_REQUIRE(!g.resource(rdf::type).is_blank());
    BOOST_REQUIRE(!node_t(g.resource(rdf::type)).is_blank());
}

BOOST_AUTO_TEST_CASE(null_is_nothing) {
    node_t n;
    BOOST_REQUIRE(!n.is_resource());
    BOOST_REQUIRE(!n.is_literal());
    BOOST_REQUIRE(!n.is_blank());
}

BOOST_AUTO_TEST_CASE(null_as_anything) {
    node_t n;
    BOOST_REQUIRE_NO_THROW(n.as_resource());
    BOOST_REQUIRE_NO_THROW(n.as_literal());
    BOOST_REQUIRE(!n.as_resource());
    BOOST_REQUIRE(!n.as_literal());
    BOOST_REQUIRE(bool(n == n.as_resource()));
    BOOST_REQUIRE(bool(n == n.as_literal()));
}

BOOST_AUTO_TEST_SUITE_END();


}  // namespace shacldator


