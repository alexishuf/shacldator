/*
 * graph_out_test.cpp
 *
 *  Created on: May 2, 2018
 *      Author: alexis
 */


#include <boost/test/unit_test.hpp>
#include <set>
#include <algorithm>
#include <fstream>
#include <boost/range/algorithm_ext.hpp>
#include "graph/raptor_out.hpp"
#include "graph/str/raptor_builder.hpp"
#include "graph/str/stl/store.hpp"

namespace shacldator {

BOOST_AUTO_TEST_SUITE(graph_out_test);

typedef str::stl::store str_t;
typedef graph<str_t> graph_t;
typedef node<graph_t> node_t;
typedef resource<graph_t> resource_t;
typedef statement<graph_t> statement_t;

graph_t load(const char* path) {
    str::raptor_builder<str::stl::store::builder> rb;
    rb.parse("turtle", path);
    return graph_t(rb.builder.build());
}
graph_t load(std::istream& in, const char* base) {
    str::raptor_builder<str::stl::store::builder> rb;
    rb.parse("ntriples", in, base);
    return graph_t(rb.builder.build());
}

BOOST_AUTO_TEST_CASE(serialize_empty) {
    std::stringstream ss;
    raptor_out o(ss, "ntriples", "http://example.org/ns#");
    str_t::builder builder;
    graph_t gIn = graph_t(builder.build());
    o << gIn;
    graph_t gOut = load(ss, "http://example.org/ns#");
    std::set<statement_t> actual, expected;
    boost::insert(expected, gIn);
    boost::insert(actual, gOut);
    BOOST_REQUIRE(bool(actual == expected));
}

BOOST_AUTO_TEST_CASE(serialize_noblanks) {
    std::stringstream ss;
    raptor_out o(ss, "ntriples", "http://example.org/ns#");
    graph_t gIn = load("test-files/out-noblanks.ttl");
    o << gIn;
    ss.seekg(0);
    graph_t gOut = load(ss, "http://example.org/ns#");
    std::set<statement_t> actual, expected;
    boost::insert(expected, gIn);
    boost::insert(actual, gOut);
    std::all_of(actual.begin(), actual.end(),
                [&](const statement_t& s) {return gIn.contains(s);});
    std::all_of(expected.begin(), expected.end(),
                [&](const statement_t& s) {return gOut.contains(s);});
}

BOOST_AUTO_TEST_SUITE_END();

}  // namespace shacldator

