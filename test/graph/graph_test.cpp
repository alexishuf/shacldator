/*
 * graph_test.cpp
 *
 *  Created on: May 2, 2018
 *      Author: alexis
 */

#include <boost/test/unit_test.hpp>
#include <set>
#include <boost/range/algorithm_ext.hpp>
#include "graph/graph.hpp"
#include "graph/node.hpp"
#include "graph/str/stl/store.hpp"
#include "graph/str/raptor_builder.hpp"
#include "ns/rdf.hpp"
#include "ns/foaf.hpp"
#include "ns/xsd.hpp"

namespace shacldator {

BOOST_AUTO_TEST_SUITE(graph_test)

typedef str::stl::store str_t;
typedef graph<str_t> graph_t;
typedef node<graph_t> node_t;
typedef resource<graph_t> resource_t;
typedef literal<graph_t> literal_t;
typedef statement<graph_t> statement_t;


graph_t load(const char* filename) {
    str::raptor_builder<str::stl::store::builder> rb;
    std::string path("test-files/");
    path += filename;
    rb.parse("turtle", path.c_str());
    return graph_t(rb.builder.build());
}

#define EX(localName) ("http://example.org/ns#" BOOST_PP_STRINGIZE(localName))

BOOST_AUTO_TEST_CASE(here_resource) {
    graph_t g1 = load("dsl-test.ttl");
    graph_t g2 = load("dsl-test.ttl");
    BOOST_REQUIRE(bool(g1.here(g2.resource(EX(A))) == g1.resource(EX(A))));
    BOOST_REQUIRE(bool(g1.here(g2.resource(EX(A))) != g2.resource(EX(A))));
    BOOST_REQUIRE(bool(g1.here(g2.resource(EX(A))) != g1.resource(EX(B))));
}

BOOST_AUTO_TEST_CASE(here_literal) {
    graph_t g1 = load("dsl-test.ttl");
    graph_t g2 = load("dsl-test.ttl");
    auto l1 = g1.literal("Alice", rdf::langString, "en");
    auto l2 = g2.literal("Alice", rdf::langString, "en");

    BOOST_REQUIRE(bool(l1 != l2));
    BOOST_REQUIRE(bool(g1.here(l2) != l2));
    BOOST_REQUIRE(bool(g1.here(l2) == l1));
}

BOOST_AUTO_TEST_CASE(here_null) {
    graph_t g1 = load("dsl-test.ttl");
    graph_t g2 = load("dsl-test.ttl");
    BOOST_REQUIRE(bool(g1.here(node_t()) == node_t()));
    BOOST_REQUIRE(bool(g1.here(resource_t()) == resource_t()));
    BOOST_REQUIRE(bool(g1.here(literal_t()) == literal_t()));
    BOOST_REQUIRE(bool(g1.here(g2.resource(EX(A))) != g2.resource(EX(A))));
    BOOST_REQUIRE(bool(g1.here(g2.resource(EX(A))) != g1.resource(EX(B))));
}

BOOST_AUTO_TEST_CASE(contains) {
    graph_t g = load("dsl-test.ttl");
    BOOST_REQUIRE(g.contains(g.resource(EX(X1)), g.resource(foaf::knows),
                             g.resource(EX(X2))));
    BOOST_REQUIRE(g.contains(g.resource(EX(A)), g.resource(foaf::name),
                             g.literal("Alice", rdf::langString, "en")));
    BOOST_REQUIRE_THROW(g.contains(g.resource(EX(A)), g.resource(foaf::name),
                              g.literal("Alice", rdf::langString, "pt")),
                        std::invalid_argument);
    BOOST_REQUIRE_THROW(g.contains(g.resource(EX(A)), g.resource(foaf::name),
                              g.literal("Alice", xsd::string_, nullptr)),
                        std::invalid_argument);
}

BOOST_AUTO_TEST_CASE(contains_cstr) {
    graph_t g = load("dsl-test.ttl");
    BOOST_REQUIRE(g.contains(g.resource(EX(X1)), foaf::knows,
                             g.resource(EX(X2))));
    BOOST_REQUIRE(g.contains(g.resource(EX(A)), foaf::name,
                             g.literal("Alice", rdf::langString, "en")));
    BOOST_REQUIRE_THROW(g.contains(g.resource(EX(A)), foaf::name,
                              g.literal("Alice", rdf::langString, "pt")),
                        std::invalid_argument);

    BOOST_REQUIRE(g.contains(EX(X1), g.resource(foaf::knows),
                             g.resource(EX(X2))));
    BOOST_REQUIRE(g.contains(EX(A), g.resource(foaf::name),
                             g.literal("Alice", rdf::langString, "en")));
    BOOST_REQUIRE_THROW(g.contains(EX(A), g.resource(foaf::name),
                              g.literal("Alice", rdf::langString, "pt")),
                        std::invalid_argument);

    BOOST_REQUIRE(g.contains(g.resource(EX(X1)), g.resource(foaf::knows),
                             EX(X2)));
}

BOOST_AUTO_TEST_CASE(contains_str) {
    graph_t g = load("dsl-test.ttl");
    BOOST_REQUIRE(g.contains(g.resource(EX(X1)), std::string(foaf::knows),
                             g.resource(EX(X2))));
    BOOST_REQUIRE(g.contains(g.resource(EX(A)), std::string(foaf::name),
                             g.literal("Alice", rdf::langString, "en")));

    BOOST_REQUIRE(g.contains(std::string(EX(X1)), g.resource(foaf::knows),
                             g.resource(EX(X2))));
    BOOST_REQUIRE(g.contains(std::string(EX(A)), g.resource(foaf::name),
                             g.literal("Alice", rdf::langString, "en")));

    BOOST_REQUIRE(g.contains(g.resource(EX(X1)), g.resource(foaf::knows),
                             std::string(EX(X2))));
}

BOOST_AUTO_TEST_CASE(iterate_empty) {
    str_t::builder b;
    graph_t g = graph_t(b.build());
    BOOST_REQUIRE(bool(g.begin() == g.end()));
    BOOST_REQUIRE(boost::empty(g));
}

BOOST_AUTO_TEST_CASE(iterate_short) {
    graph_t g = load("short.ttl");
    BOOST_REQUIRE(!boost::empty(g));
    std::set<statement_t> actual, expected{
        statement_t(g.resource(EX(A)), g.resource(rdf::type),
                    g.resource(foaf::Person)),
        statement_t(g.resource(EX(A)), g.resource(foaf::made),
                    g.resource(EX(Book))),
        statement_t(g.resource(EX(A)), g.resource(foaf::name),
                    g.literal("Alice", rdf::langString, "en")),
    };
    boost::insert(actual, g);
    BOOST_REQUIRE(expected.size() == 3);
    BOOST_REQUIRE(bool(actual == expected));
}

BOOST_AUTO_TEST_SUITE_END();

}  // namespace shacldator




