/*
 * store_types.hpp
 *
 *  Created on: Mar 27, 2018
 *      Author: alexis
 */

#ifndef TEST_GRAPH_STR_STORE_TYPES_HPP_
#define TEST_GRAPH_STR_STORE_TYPES_HPP_

#include <boost/mpl/list.hpp>
#include "graph/str/stl/store.hpp"
#include "graph/str/hdt/store.hpp"
#include "graph/str/augmented/store.hpp"

namespace shacldator {namespace str {namespace test{

typedef boost::mpl::list<str::stl::store> builder_str_types;
typedef boost::mpl::list<str::stl::store,
                         str::hdt::store,
                         str::augmented::store<str::stl::store>,
                         str::augmented::store<str::hdt::store>> str_types;

}}} // namespace shacldator::str::test



#endif /* TEST_GRAPH_STR_STORE_TYPES_HPP_ */
