/*
 * augmented_test.cpp
 *
 *  Created on: May 25, 2018
 *      Author: alexis
 */

#include <boost/test/unit_test.hpp>
#include <type_traits>
#include <algorithm>
#include <boost/mpl/list.hpp>
#include <boost/mpl/empty.hpp>
#include <boost/mpl/filter_view.hpp>
#include <boost/range/algorithm_ext.hpp>
#include "graph/str/stl/store.hpp"
#include "graph/graph.hpp"
#include "graph/str/raptor_builder.hpp"
#include "graph/str/augmented/store.hpp"
#include "graph/str/concepts/GraphStore.hpp"
#include "graph/str/concepts/GraphStoreBuilder.hpp"
#include "qry/dsl.hpp"
#include "qry/priv/tp_matcher.hpp"
#include "ns/xsd.hpp"
#include "ns/rdf.hpp"
#include "ns/foaf.hpp"
#include "store_types.hpp"
#include "fixtures.hpp"

using namespace std;

namespace shacldator {namespace str {namespace augmented {

BOOST_AUTO_TEST_SUITE(augmented_test)

using namespace str::test;


typedef boost::mpl::filter_view<str_types, boost::mpl::quote1<is_augmented>>
        selected_types;
BOOST_STATIC_ASSERT(!boost::mpl::empty<selected_types>::value);

#define EX(localName) ("http://example.org/ns#" BOOST_PP_STRINGIZE(localName))

BOOST_AUTO_TEST_CASE_TEMPLATE(create_new_uri, Store, selected_types) {
    simple_fixture<Store> f;
    auto term = f.s->create_term(terms::URI, EX(U));
    BOOST_TEST(f.s->create_term(terms::URI, EX(U)) == term);
    BOOST_TEST(f.s->lex(term) == EX(U));
    BOOST_TEST(f.s->resource(EX(U)) == term);
}
BOOST_AUTO_TEST_CASE_TEMPLATE(create_new_blank, Store, selected_types) {
    simple_fixture<Store> f;
    const char* pope = "_:pope";
    auto term = f.s->create_term(terms::BLANK, pope);
    BOOST_TEST(f.s->create_term(terms::BLANK, pope) == term);
    BOOST_TEST(f.s->lex(term) == pope);
    BOOST_TEST(f.s->resource(pope) == term);
}
BOOST_AUTO_TEST_CASE_TEMPLATE(create_existing_uri, Store, selected_types) {
    simple_fixture<Store> f;
    auto term = f.s->create_term(terms::URI, EX(Alice));
    BOOST_TEST(f.s->lex(term) == EX(Alice));
    BOOST_TEST(f.raw_fixture.s->lex(term.raw) == EX(Alice));
    BOOST_TEST(f.s->lex(term.raw) == EX(Alice));

    BOOST_TEST(f.s->resource(EX(Alice)) == term);
}
BOOST_AUTO_TEST_CASE_TEMPLATE(create_new_plain_lit, Store, selected_types) {
    simple_fixture<Store> f;
    const char* lex = "vania";
    const char* lit = "\"vania\"";
    auto term = f.s->create_term(terms::LITERAL, lit);
    BOOST_TEST(f.s->lex(term) == lex);
    BOOST_TEST(f.s->lex(f.s->datatype(term)) == xsd::string_);

    BOOST_TEST(f.s->literal(lex, nullptr, nullptr) == term);
    BOOST_TEST(f.s->literal(lex, "", nullptr) == term);
    BOOST_TEST(f.s->literal(lex, xsd::string_, nullptr) == term);
    BOOST_TEST(f.s->literal(lex, nullptr, "") == term);

    const char* full = "\"vania\"^^<http://www.w3.org/2001/XMLSchema#string>";
    BOOST_TEST(f.s->create_term(terms::LITERAL, full) == term);
}
BOOST_AUTO_TEST_CASE_TEMPLATE(create_new_string_lit, Store, selected_types) {
    simple_fixture<Store> f;
    const char* lex = "vania";
    const char* dt  =             "http://www.w3.org/2001/XMLSchema#string";
    const char* lit = "\"vania\"^^<http://www.w3.org/2001/XMLSchema#string>";
    auto term = f.s->create_term(terms::LITERAL, lit);
    BOOST_TEST(f.s->lex(term) == lex);
    BOOST_TEST(f.s->lex(f.s->datatype(term)) == dt);
    BOOST_TEST(f.s->langtag(term) == std::string());

    BOOST_TEST(f.s->create_term(terms::LITERAL, "\"vania\"") == term);
    BOOST_TEST(f.s->literal(lex, xsd::string_, nullptr) == term);
    BOOST_TEST(f.s->literal(lex, nullptr, nullptr) == term);
    BOOST_TEST(f.s->literal(lex, "", nullptr) == term);
    BOOST_TEST(f.s->literal(lex, nullptr, "") == term);
}
BOOST_AUTO_TEST_CASE_TEMPLATE(create_new_int_lit, Store, selected_types) {
    simple_fixture<Store> f;
    const char* lex = "27";
    const char* dt  =          "http://www.w3.org/2001/XMLSchema#int";
    const char* lit = "\"27\"^^<http://www.w3.org/2001/XMLSchema#int>";
    auto term = f.s->create_term(terms::LITERAL, lit);
    BOOST_TEST(f.s->lex(term) == lex);
    BOOST_TEST(f.s->lex(f.s->datatype(term)) == dt);
    BOOST_TEST(f.s->langtag(term) == std::string());

    BOOST_TEST(f.s->create_term(terms::LITERAL, lit) == term);
    BOOST_TEST(f.s->literal(lex, dt, nullptr) == term);
    BOOST_TEST(f.s->literal(lex, dt, "") == term);
}
BOOST_AUTO_TEST_CASE_TEMPLATE(create_uri_and_int, Store, selected_types) {
    simple_fixture<Store> f;

    auto termU = f.s->create_term(terms::URI, EX(U));
    const char* lex27 = "27";
    const char* dtInt  =          "http://www.w3.org/2001/XMLSchema#int";
    const char* lit27 = "\"27\"^^<http://www.w3.org/2001/XMLSchema#int>";
    auto term27 = f.s->create_term(terms::LITERAL, lit27);

    BOOST_TEST(f.s->lex(termU) == EX(U));
    BOOST_TEST(f.s->resource(EX(U)) == termU);
    BOOST_TEST(f.s->lex(term27) == lex27);
    BOOST_TEST(f.s->lex(f.s->datatype(term27)) == dtInt);
    BOOST_TEST(f.s->langtag(term27) == std::string());
    BOOST_TEST(f.s->literal(lex27, dtInt, nullptr) == term27);
}
BOOST_AUTO_TEST_CASE_TEMPLATE(create_uri_and_int_reverse, Store, selected_types) {
    simple_fixture<Store> f;

    const char* lex27 = "27";
    const char* dtInt  =          "http://www.w3.org/2001/XMLSchema#int";
    const char* lit27 = "\"27\"^^<http://www.w3.org/2001/XMLSchema#int>";
    auto term27 = f.s->create_term(terms::LITERAL, lit27);
    auto termU = f.s->create_term(terms::URI, EX(U));

    BOOST_TEST(f.s->lex(termU) == EX(U));
    BOOST_TEST(f.s->resource(EX(U)) == termU);
    BOOST_TEST(f.s->lex(term27) == lex27);
    BOOST_TEST(f.s->lex(f.s->datatype(term27)) == dtInt);
    BOOST_TEST(f.s->langtag(term27) == std::string());
    BOOST_TEST(f.s->literal(lex27, dtInt, nullptr) == term27);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(match_101, Store, selected_types) {
    using namespace qry;
    typedef str::triple<typename Store::term_t> triple_t;
    simple_fixture<Store> f;

    auto m = priv::mk_tp_matcher(*f.s, _t(_1, foaf::knows, _1));
    set<triple_t> actual, expected{};
    std::for_each(f.s->begin(), f.s->end(),
                  [&](const triple_t& t){if (m(t)) actual.insert(t);});
    BOOST_TEST(bool(actual == expected));
}

BOOST_AUTO_TEST_CASE(match_101_tp) {
    using namespace qry;
    str::raptor_builder<str::stl::store::builder> b;
    b.parse("turtle", "test-files/tp-matcher-test.ttl");
    auto g = mk_graph(b.builder.build());
    auto& s = g.store();
    typedef std::decay<decltype(g.store())>::type::term_t term_t;
    typedef str::triple<term_t> triple_t;

    auto m = priv::mk_tp_matcher(s, _t(_1, foaf::knows, _1));
    set<triple_t> actual, expected{
        triple_t(s.resource(EX(E)), s.resource(foaf::knows), s.resource(EX(E)))
    };
    BOOST_REQUIRE(expected.begin()->is_complete());
    std::for_each(s.begin(), s.end(),
                  [&](const triple_t& t){if (m(t)) actual.insert(t);});
    BOOST_TEST(bool(actual == expected));
}


BOOST_AUTO_TEST_SUITE_END()

}}}  // namespace shacldator::str::augmented


