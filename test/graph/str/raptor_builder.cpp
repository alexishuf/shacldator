/*
 * raptor_builder.cpp
 *
 *  Created on: Mar 27, 2018
 *      Author: alexis
 */

#include <boost/test/unit_test.hpp>
#include <set>
#include <iterator>
#include <algorithm>
#include <iostream>
#include <unistd.h>
#include "graph/str/raptor_builder.hpp"
#include "ns/foaf.hpp"
#include "ns/xsd.hpp"
#include "store_types.hpp"

using namespace std;

namespace shacldator {namespace str {namespace test{

BOOST_AUTO_TEST_SUITE(raptor_builder_test)

template<class Store> struct fixture {
    fixture() : b(typename Store::builder()) {}

    raptor_builder<typename Store::builder> b;
};


BOOST_AUTO_TEST_CASE(bad_parser) {
    fixture<stl::store> f;
    string missing("test-files/simple.nt");
    BOOST_REQUIRE_THROW(f.b.parse("klingon", missing), invalid_argument);
}

BOOST_AUTO_TEST_CASE(missing_file) {
    fixture<stl::store> f;
    string missing("test-files/missing.nt");
    BOOST_REQUIRE_THROW(f.b.parse("guess", missing), runtime_error);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(simple, Store, builder_str_types) {
    typedef typename Store::term_t term;
    fixture<Store> f;
    f.b.parse("ntriples", string("test-files/simple.nt"));
    Store s = f.b.builder.build();
    set<triple<term>> actual, ex;
    auto get_r = [&](const char* subj, const char* pred, const char* obj) {
        term r = s.resource(subj), p = s.resource(pred), o = s.resource(obj);
        return triple<term>(r, p, o);
    };
    auto get_l = [&](const char* subj, const char* pred,
                     const char* lex, const char* dt, const char* lang) {
        term r = s.resource(subj), p = s.resource(pred);
        term o = s.literal(lex, dt, lang);
        return triple<term>(r, p, o);
    };
    const char *a = "http://example.org/ns#A", *b = "http://example.org/ns#B",
         *c = "_:C";
    ex.insert(get_r(a, foaf::knows, b));
    ex.insert(get_r(b, foaf::knows, a));
    ex.insert(get_r(a, foaf::knows, c));
    ex.insert(get_l(a, foaf::age , "22"     , xsd::integer_, nullptr));
    ex.insert(get_l(a, foaf::name, "Alice"  , xsd::string_ , nullptr));
    ex.insert(get_l(b, foaf::name, "Bob"    , xsd::string_ , nullptr));
    ex.insert(get_l(c, foaf::name, "Charlie", nullptr      , "en"   ));
    copy(s.begin(), s.end(), inserter(actual, actual.end()));
    BOOST_REQUIRE_EQUAL_COLLECTIONS(actual.begin(),   actual.end(),
                                    ex.begin(), ex.end());
}


BOOST_AUTO_TEST_SUITE_END()

}}} // namespace shacldator::str::test

