/*
 * fixtures.hpp
 *
 *  Created on: May 25, 2018
 *      Author: alexis
 */

#ifndef TEST_GRAPH_STR_FIXTURES_HPP_
#define TEST_GRAPH_STR_FIXTURES_HPP_

#include <boost/test/unit_test.hpp>
#include "graph/str/concepts/GraphStore.hpp"
#include "graph/str/concepts/GraphStoreBuilder.hpp"
#include "ns/xsd.hpp"
#include "ns/rdf.hpp"
#include "ns/foaf.hpp"
#include "store_types.hpp"

using namespace std;

namespace shacldator {namespace str {namespace test {

template<class Store> struct builder_fixture {
    builder_fixture() = default;
    ~builder_fixture() = default;
    typename Store::builder b;
};

template<class Store> struct simple_fixture {
    typedef typename Store::term_t term;
    simple_fixture() {
        typename Store::builder b;
        alice = b.uri_term("http://example.org/ns#Alice");
        knows = b.uri_term(foaf::knows);
        age = b.uri_term(foaf::age);
        bob = b.literal_term("Bob", nullptr, "en");
        charlie = b.blank_term("_:Charlie");
        dave = b.literal_term("Dave", nullptr, nullptr);
        eric = b.literal_term("Eric", xsd::string_, nullptr);
        fabian = b.uri_term("http://example.org/ns#Fabian");
        george = b.literal_term("George", "", "pt");
        jane = b.literal_term("Jane", xsd::string_, "");
        twenty = b.literal_term("20", xsd::integer_, nullptr);
        b.add_triple(alice, knows, bob);
        b.add_triple(alice, knows, charlie);
        b.add_triple(alice, knows, dave);
        b.add_triple(alice, knows, eric);
        b.add_triple(alice, knows, george);
        b.add_triple(alice, knows, jane);
        b.add_triple(fabian, age, twenty);
        s = new Store(b.build());
    }
    ~simple_fixture() {delete s;}

    term alice, knows, age, bob, charlie, dave,
         eric, fabian, george, jane, twenty;
    Store* s;
};
template<> struct simple_fixture<str::hdt::store> {
    typedef str::hdt::store store_t;
    typedef typename store_t::term_t term;
    simple_fixture() : s(new store_t("test-files/str/simple.hdt")),
                       alice(s->resource("http://example.org/ns#Alice")),
                       knows(s->resource(foaf::knows)),
                       age(s->resource(foaf::age)),
                       bob(s->literal("Bob", nullptr, "en")),
                       charlie(s->resource("_:Charlie")),
                       dave(s->literal("Dave", nullptr, nullptr)),
                       eric(s->literal("Eric", xsd::string_, "")),
                       fabian(s->resource("http://example.org/ns#Fabian")),
                       george(s->literal("George", "", "pt")),
                       jane(s->literal("Jane", xsd::string_, "")),
                       twenty(s->literal("20", xsd::integer_, nullptr)) {
        BOOST_REQUIRE(alice);
        BOOST_REQUIRE(knows);
        BOOST_REQUIRE(age);
        BOOST_REQUIRE(bob);
        BOOST_REQUIRE(charlie);
        BOOST_REQUIRE(dave);
        BOOST_REQUIRE(eric);
        BOOST_REQUIRE(fabian);
        BOOST_REQUIRE(george);
        BOOST_REQUIRE(jane);
        BOOST_REQUIRE(twenty);
    }
    ~simple_fixture() {delete s;}

    store_t* s;
    term alice, knows, age, bob, charlie, dave,
         eric, fabian, george, jane, twenty;
};

template<class RawStore> struct simple_fixture<str::augmented::store<RawStore>>{
    typedef RawStore raw_store_t;
    typedef str::augmented::store<RawStore> store_t;
    typedef typename store_t::term_t term;

    simple_fixture() : raw_fixture(), s(new store_t(raw_fixture.s)),
                       alice(s->resource("http://example.org/ns#Alice")),
                       knows(s->resource(foaf::knows)),
                       age(s->resource(foaf::age)),
                       bob(s->literal("Bob", nullptr, "en")),
                       charlie(s->resource("_:Charlie")),
                       dave(s->literal("Dave", nullptr, nullptr)),
                       eric(s->literal("Eric", xsd::string_, "")),
                       fabian(s->resource("http://example.org/ns#Fabian")),
                       george(s->literal("George", "", "pt")),
                       jane(s->literal("Jane", xsd::string_, "")),
                       twenty(s->literal("20", xsd::integer_, nullptr)) {
        BOOST_REQUIRE(alice);
        BOOST_REQUIRE(knows);
        BOOST_REQUIRE(age);
        BOOST_REQUIRE(bob);
        BOOST_REQUIRE(charlie);
        BOOST_REQUIRE(dave);
        BOOST_REQUIRE(eric);
        BOOST_REQUIRE(fabian);
        BOOST_REQUIRE(george);
        BOOST_REQUIRE(jane);
        BOOST_REQUIRE(twenty);
        BOOST_REQUIRE(alice != knows);
        BOOST_REQUIRE(alice != bob);
        BOOST_REQUIRE(alice != charlie);
        BOOST_REQUIRE(alice != dave);
        BOOST_REQUIRE(alice != eric);
        BOOST_REQUIRE(eric != dave);
        s->create_term(terms::URI, "_:special-uri-from-fixtures.hpp-1");
        s->create_term(terms::URI, "_:special-uri-from-fixtures.hpp-2");
        s->create_term(terms::LITERAL, "\"special-literal-from-fixtures.hpp-1\"");
        s->create_term(terms::LITERAL, "\"79865473\"^^<http://www.w3.org/2001/XMLSchema#int>");
    }
    ~simple_fixture() {delete s;}

    simple_fixture<raw_store_t> raw_fixture;
    store_t* s;
    term alice, knows, age, bob, charlie, dave,
         eric, fabian, george, jane, twenty;
};

}}}  // namespace shacldator::str::test

#endif /* TEST_GRAPH_STR_FIXTURES_HPP_ */
