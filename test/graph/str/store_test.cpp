/*
 * store_test.cpp
 *
 *  Created on: Mar 26, 2018
 *      Author: alexis
 */

#include <boost/test/unit_test.hpp>
#include <boost/mpl/list.hpp>
#include <boost/range/algorithm_ext.hpp>
#include <exception>
#include <set>
#include <algorithm>
#include <iterator>
#include <stdexcept>
#include "graph/str/stl/store.hpp"
#include "graph/str/concepts/GraphStore.hpp"
#include "graph/str/concepts/GraphStoreBuilder.hpp"
#include "ns/xsd.hpp"
#include "ns/rdf.hpp"
#include "ns/foaf.hpp"
#include "store_types.hpp"
#include "fixtures.hpp"

using namespace std;

namespace shacldator {namespace str {namespace test{
BOOST_AUTO_TEST_SUITE(store)


BOOST_AUTO_TEST_CASE_TEMPLATE(concept, Store, builder_str_types) {
    BOOST_CONCEPT_ASSERT((str::GraphStore<Store>));
}
BOOST_AUTO_TEST_CASE_TEMPLATE(builder_concept, Store, builder_str_types) {
    BOOST_CONCEPT_ASSERT((str::GraphStoreBuilder<typename Store::builder>));
}

BOOST_AUTO_TEST_CASE_TEMPLATE(store_uri, Store, builder_str_types) {
    builder_fixture<Store> f;
    typename Store::term_t a = f.b.uri_term("http://example.org/ns#A");
    BOOST_TEST(a);
    typename Store::term_t b = f.b.uri_term("http://example.org/ns#A");
    BOOST_TEST(a == b);
    typename Store::term_t c = f.b.uri_term("http://example.org/ns#C");
    BOOST_TEST(a != c);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(store_empty_uri, Store, builder_str_types) {
    builder_fixture<Store> f;
    BOOST_CHECK_THROW(f.b.uri_term(""), invalid_argument);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(store_null_uri, Store, builder_str_types) {
    builder_fixture<Store> f;
    BOOST_CHECK_THROW(f.b.uri_term(nullptr), invalid_argument);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(store_empty_blank, Store, builder_str_types) {
    builder_fixture<Store> f;
    typename Store::term_t term;
    BOOST_CHECK_NO_THROW(term = f.b.blank_term(""));
    BOOST_REQUIRE(term);
    BOOST_REQUIRE(f.b.blank_term("") != term);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(store_null_blank, Store, builder_str_types) {
    builder_fixture<Store> f;
    typename Store::term_t term;
    BOOST_CHECK_NO_THROW(term = f.b.blank_term(nullptr));
    BOOST_TEST(term);
    BOOST_REQUIRE(f.b.blank_term(nullptr) != term);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(store_literal_omit_dt, Store, builder_str_types) {
    builder_fixture<Store> f;
    typename Store::term_t term;
    BOOST_CHECK_NO_THROW(term = f.b.literal_term("lex", nullptr, nullptr));
    BOOST_CHECK_NO_THROW(term = f.b.literal_term("lex", "", ""));
    BOOST_TEST(term);
    BOOST_REQUIRE(f.b.literal_term("lex", nullptr, nullptr) == term);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(store_literal_lang, Store, builder_str_types) {
    builder_fixture<Store> f;
    typename Store::term_t term;
    BOOST_CHECK_NO_THROW(term = f.b.literal_term("lex", nullptr, "pt"));
    BOOST_CHECK_NO_THROW(term = f.b.literal_term("lex", "", "pt"));
    BOOST_TEST(term);
    BOOST_REQUIRE(f.b.literal_term("lex", nullptr, "pt") == term);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(store_literal_dt, Store, builder_str_types) {
    builder_fixture<Store> f;
    typename Store::term_t term;
    BOOST_CHECK_NO_THROW(term = f.b.literal_term("lex", xsd::string_, nullptr));
    BOOST_CHECK_NO_THROW(term = f.b.literal_term("lex", xsd::string_, ""));
    BOOST_TEST(term);
    BOOST_REQUIRE(f.b.literal_term("lex", xsd::string_, nullptr) == term);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(store_literal_lang_bad_dt, Store, builder_str_types) {
    builder_fixture<Store> f;
    BOOST_CHECK_THROW(f.b.literal_term("lex", xsd::string_, "pt"),
                      invalid_argument);
    BOOST_CHECK_NO_THROW(f.b.literal_term("lex", "", "pt"));
}

BOOST_AUTO_TEST_CASE_TEMPLATE(store_triple, Store, builder_str_types) {
    builder_fixture<Store> f;
    typedef typename Store::term_t term;
    term subj = f.b.uri_term("http://example.org/ns#Alice");
    term pred = f.b.uri_term(foaf::name);
    term  obj = f.b.literal_term("Alice", nullptr, "en");
    f.b.add_triple(subj, pred, obj);
    BOOST_CHECK_NO_THROW(f.b.add_triple(subj, pred, obj));
}

BOOST_AUTO_TEST_CASE_TEMPLATE(store_triple_lit_subj, Store, builder_str_types) {
    builder_fixture<Store> f;
    typedef typename Store::term_t term;
    term subj = f.b.literal_term("Alice", xsd::string_, nullptr);
    term pred = f.b.uri_term(foaf::knows);
    term  obj = f.b.literal_term("Bob", nullptr, "en");
    BOOST_CHECK_THROW(f.b.add_triple(obj, pred, obj), invalid_argument);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(store_triple_blank_pred, Store, builder_str_types) {
    builder_fixture<Store> f;
    typedef typename Store::term_t term;
    term  subj = f.b.uri_term("http://example.org/ns#Alice");
    term pred1 = f.b.blank_term("_:knows");
    term pred2 = f.b.blank_term("");
    term   obj = f.b.uri_term("http://example.org/ns#Bob");
    BOOST_CHECK_THROW(f.b.add_triple(obj, pred1, obj), invalid_argument);
    BOOST_CHECK_THROW(f.b.add_triple(obj, pred2, obj), invalid_argument);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(get_type, Store, str_types) {
    simple_fixture<Store> f;
    typedef typename Store::term_t term;
    BOOST_CHECK_EQUAL(f.s->type(f.alice), terms::URI);
    BOOST_CHECK_EQUAL(f.s->type(f.knows), terms::URI);
    BOOST_CHECK_EQUAL(f.s->type(f.bob), terms::LITERAL);
    BOOST_CHECK_EQUAL(f.s->type(f.charlie), terms::BLANK);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(get_lex, Store, str_types) {
    simple_fixture<Store> f;
    typedef typename Store::term_t term;
    BOOST_CHECK_EQUAL(f.s->lex(f.alice), "http://example.org/ns#Alice");
    BOOST_CHECK_EQUAL(f.s->lex(f.knows), foaf::knows);
    BOOST_CHECK_EQUAL(f.s->lex(f.bob), "Bob");
    BOOST_CHECK_EQUAL(f.s->lex(f.charlie), "_:Charlie");
    BOOST_CHECK_EQUAL(f.s->lex(f.george), "George");
}

BOOST_AUTO_TEST_CASE_TEMPLATE(get_datatype, Store, str_types) {
    simple_fixture<Store> f;
    typedef typename Store::term_t term;
    BOOST_CHECK_EQUAL(f.s->lex(f.s->datatype(f.bob)), rdf::langString);
    BOOST_CHECK_EQUAL(f.s->lex(f.s->datatype(f.dave)), xsd::string_);
    BOOST_CHECK_EQUAL(f.s->lex(f.s->datatype(f.eric)), xsd::string_);
    BOOST_CHECK_EQUAL(f.s->lex(f.s->datatype(f.george)), rdf::langString);
    BOOST_CHECK_EQUAL(f.s->lex(f.s->datatype(f.jane)), xsd::string_);
    BOOST_CHECK_EQUAL(f.s->lex(f.s->datatype(f.twenty)), xsd::integer_);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(get_langtag, Store, str_types) {
    simple_fixture<Store> f;
    typedef typename Store::term_t term;
    BOOST_CHECK_EQUAL(f.s->langtag(f.bob), "en");
    BOOST_CHECK_EQUAL(f.s->langtag(f.dave), "");
    BOOST_CHECK_EQUAL(f.s->langtag(f.eric), "");
    BOOST_CHECK_EQUAL(f.s->langtag(f.george), "pt");
    BOOST_CHECK_EQUAL(f.s->langtag(f.jane), "");
    BOOST_CHECK_EQUAL(f.s->langtag(f.twenty), "");
}

BOOST_AUTO_TEST_CASE_TEMPLATE(get_langtag_of_resource, Store, str_types) {
    simple_fixture<Store> f;
    typedef typename Store::term_t term;
    BOOST_REQUIRE_THROW(f.s->langtag(f.alice), invalid_argument);
    BOOST_REQUIRE_THROW(f.s->langtag(f.charlie), invalid_argument);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(get_str_null, Store, str_types) {
    simple_fixture<Store> f;
    typedef typename Store::term_t term;
    BOOST_REQUIRE_THROW(f.s->str(term()), std::invalid_argument);
}
BOOST_AUTO_TEST_CASE_TEMPLATE(get_str_uri, Store, str_types) {
    simple_fixture<Store> f;
    BOOST_TEST(f.s->str(f.alice) == "http://example.org/ns#Alice");
}
BOOST_AUTO_TEST_CASE_TEMPLATE(get_str_blank, Store, str_types) {
    simple_fixture<Store> f;
    BOOST_TEST(f.s->str(f.charlie) == "_:Charlie");
}
BOOST_AUTO_TEST_CASE_TEMPLATE(get_str_literal, Store, str_types) {
    simple_fixture<Store> f;
    BOOST_TEST(f.s->str(f.bob) == "\"Bob\"@en");
    BOOST_TEST(f.s->str(f.eric) == "\"Eric\"^^<http://www.w3.org/2001/XMLSchema#string>");
    BOOST_TEST(f.s->str(f.twenty) == "\"20\"^^<http://www.w3.org/2001/XMLSchema#integer>");
    bool ok = f.s->str(f.dave) == "\"Dave\"^^<http://www.w3.org/2001/XMLSchema#string>"
           || f.s->str(f.dave) == "\"Dave\"";
    BOOST_TEST(ok);
}


BOOST_AUTO_TEST_CASE_TEMPLATE(get_pos_empty, Store, str_types) {
    simple_fixture<Store> f;
    typedef typename Store::term_t term;
    BOOST_TEST(f.s->begin_po(f.charlie) == f.s->end_po(f.charlie));
}

BOOST_AUTO_TEST_CASE_TEMPLATE(get_pos_one, Store, str_types) {
    simple_fixture<Store> f;
    typedef typename Store::term_t term;
    auto it = f.s->begin_po(f.fabian);
    BOOST_TEST(*it == triple<term>(f.fabian, f.age, f.twenty));
    BOOST_TEST(++it == f.s->end_po(f.fabian));
}

BOOST_AUTO_TEST_CASE_TEMPLATE(get_pos_more, Store, str_types) {
    simple_fixture<Store> f;
    typedef typename Store::term_t term;
    set<triple<term> > actual, expected;
    expected.insert(triple<term>(f.alice, f.knows, f.bob));
    expected.insert(triple<term>(f.alice, f.knows, f.charlie));
    expected.insert(triple<term>(f.alice, f.knows, f.dave));
    expected.insert(triple<term>(f.alice, f.knows, f.eric));
    expected.insert(triple<term>(f.alice, f.knows, f.george));
    expected.insert(triple<term>(f.alice, f.knows, f.jane));
    copy(f.s->begin_po(f.alice), f.s->end_po(f.alice),
              inserter(actual, actual.end()));
    BOOST_CHECK_EQUAL_COLLECTIONS(  actual.begin(),   actual.end(),
                                  expected.begin(), expected.end());
}

BOOST_AUTO_TEST_CASE_TEMPLATE(iterate_empty, Store, builder_str_types) {
    builder_fixture<Store> f;
    Store s = f.b.build();
    BOOST_REQUIRE_EQUAL(s.begin(), s.end());
}

BOOST_AUTO_TEST_CASE_TEMPLATE(iterate_without_triples, Store, builder_str_types) {
    builder_fixture<Store> f;
    f.b.uri_term(foaf::name);
    f.b.blank_term("_:Alice");
    f.b.literal_term("Bob", xsd::string_, "");
    Store s = f.b.build();
    BOOST_REQUIRE_EQUAL(s.begin(), s.end());
}

BOOST_AUTO_TEST_CASE_TEMPLATE(iterate, Store, str_types) {
    simple_fixture<Store> f;
    typedef typename Store::term_t term;
    set<triple<term>> actual, expected;
    expected.insert(triple<term>(f.alice, f.knows, f.bob));
    expected.insert(triple<term>(f.alice, f.knows, f.charlie));
    expected.insert(triple<term>(f.alice, f.knows, f.dave));
    expected.insert(triple<term>(f.alice, f.knows, f.eric));
    expected.insert(triple<term>(f.alice, f.knows, f.george));
    expected.insert(triple<term>(f.alice, f.knows, f.jane));
    expected.insert(triple<term>(f.fabian, f.age, f.twenty));
    boost::insert(actual, make_pair(f.s->begin(), f.s->end()));
    BOOST_REQUIRE(bool(actual == expected));
//    copy(f.s->begin(), f.s->end(), inserter(actual, actual.end()));
//    BOOST_REQUIRE_EQUAL_COLLECTIONS(actual.begin(),   actual.end(),
//                                  expected.begin(), expected.end());
}

BOOST_AUTO_TEST_CASE_TEMPLATE(contains, Store, str_types) {
    simple_fixture<Store> f;
    typedef typename Store::term_t term;
    BOOST_REQUIRE( f.s->contains(f.alice, f.knows, f.bob));
    BOOST_REQUIRE( f.s->contains(f.alice, f.knows, f.dave));
    BOOST_REQUIRE( f.s->contains(f.alice, f.knows, f.jane));
    BOOST_REQUIRE( f.s->contains(f.alice, f.knows, f.charlie));
    BOOST_REQUIRE(!f.s->contains(f.alice, f.knows, f.alice));
}

BOOST_AUTO_TEST_SUITE_END()
}}} // namespace shacldator::str::test
