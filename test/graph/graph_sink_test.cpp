/*
 * graph_sink_test.cpp
 *
 *  Created on: Apr 30, 2018
 *      Author: alexis
 */

#include <boost/test/unit_test.hpp>
#include <boost/preprocessor/stringize.hpp>
#include "graph/graph_sink.hpp"
#include "graph/graph.hpp"
#include "graph/str/raptor_builder.hpp"
#include "ns/foaf.hpp"
#include "ns/xsd.hpp"
#include "ns/rdf.hpp"
#include "str/store_types.hpp"

using namespace std;

namespace shacldator {

BOOST_AUTO_TEST_SUITE(graph_sink_test);

#define EX(localName) ("http://example.org/ns#" BOOST_PP_STRINGIZE(localName))
#define REX(localName) g.resource(EX(localName))

BOOST_AUTO_TEST_CASE_TEMPLATE(c_strigns, Store, str::test::builder_str_types) {
    typename Store::builder b;
    graph_sink<typename Store::builder> sink(&b);
    sink.add(EX(A), rdf::type, foaf::Person);
    sink.add(EX(A), foaf::name, "Alice", rdf::langString, "en");
    auto g = mk_graph(b.build());
    BOOST_REQUIRE(g.contains(g.resource(EX(A)), g.resource(rdf::type),
                             g.resource(foaf::Person)));
    BOOST_REQUIRE(g.contains(g.resource(EX(A)), g.resource(foaf::name),
                             g.literal("Alice", rdf::langString, "en")));
}

BOOST_AUTO_TEST_CASE_TEMPLATE(nodes, Store, str::test::builder_str_types) {
    str::raptor_builder<typename Store::builder> rb;
    rb.parse("turtle", "test-files/graph_sink-test.ttl");
    auto g = mk_graph(rb.builder.build());

    typename Store::builder b2;
    graph_sink<typename Store::builder> sink(&b2);
    sink.add(REX(A), g.resource(rdf::type), g.resource(foaf::Person));
    sink.add(REX(A), g.resource(foaf::name),
             g.literal("Alice", rdf::langString, "en"));
    auto g2 = mk_graph(b2.build());
    BOOST_REQUIRE(g2.contains(g2.resource(EX(A)), g2.resource(rdf::type),
                              g2.resource(foaf::Person)));
    BOOST_REQUIRE(g2.contains(g2.resource(EX(A)), g2.resource(foaf::name),
                              g2.literal("Alice", rdf::langString, "en")));
}

BOOST_AUTO_TEST_CASE_TEMPLATE(subj_as_cstring, Store, str::test::builder_str_types) {
    str::raptor_builder<typename Store::builder> rb;
    rb.parse("turtle", "test-files/graph_sink-test.ttl");
    auto g = mk_graph(rb.builder.build());

    typename Store::builder b2;
    graph_sink<typename Store::builder> sink(&b2);
    sink.add(EX(A), g.resource(rdf::type), g.resource(foaf::Person));
    auto g2 = mk_graph(b2.build());
    BOOST_REQUIRE(g2.contains(g2.resource(EX(A)), g2.resource(rdf::type),
                              g2.resource(foaf::Person)));
}

BOOST_AUTO_TEST_CASE_TEMPLATE(subj_as_string, Store, str::test::builder_str_types) {
    str::raptor_builder<typename Store::builder> rb;
    rb.parse("turtle", "test-files/graph_sink-test.ttl");
    auto g = mk_graph(rb.builder.build());

    typename Store::builder b2;
    graph_sink<typename Store::builder> sink(&b2);
    sink.add(std::string(EX(A)), g.resource(rdf::type),
             g.resource(foaf::Person));
    auto g2 = mk_graph(b2.build());
    BOOST_REQUIRE(g2.contains(g2.resource(EX(A)), g2.resource(rdf::type),
                              g2.resource(foaf::Person)));
}

BOOST_AUTO_TEST_CASE_TEMPLATE(pred_as_cstring, Store, str::test::builder_str_types) {
    str::raptor_builder<typename Store::builder> rb;
    rb.parse("turtle", "test-files/graph_sink-test.ttl");
    auto g = mk_graph(rb.builder.build());

    typename Store::builder b2;
    graph_sink<typename Store::builder> sink(&b2);
    sink.add(REX(A), foaf::name, g.literal("Alice", rdf::langString, "en"));
    auto g2 = mk_graph(b2.build());
    BOOST_REQUIRE(g2.contains(g2.resource(EX(A)), g2.resource(foaf::name),
                              g2.literal("Alice", rdf::langString, "en")));
}

BOOST_AUTO_TEST_CASE_TEMPLATE(blank, Store, str::test::builder_str_types) {
    typename Store::builder b;
    graph_sink<typename Store::builder> sink(&b);
    std::string b1 = sink.create_blank();
    std::string b2 = sink.create_blank();
    sink.add(b1, rdf::type, foaf::Person);
    sink.add(b2, rdf::type, foaf::Agent);
    auto g = mk_graph(b.build());

    BOOST_REQUIRE(bool(g.resource(b1)));
    BOOST_REQUIRE(bool(g.resource(b2)));
    BOOST_REQUIRE(g.contains(g.resource(b1), g.resource(rdf::type),
                                  g.resource(foaf::Person)));
    BOOST_REQUIRE(g.contains(g.resource(b2), g.resource(rdf::type),
                                  g.resource(foaf::Agent)));
}


BOOST_AUTO_TEST_SUITE_END();

}  // namespace shacldator
