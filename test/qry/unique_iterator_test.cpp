/*
 * unique_iterator_test.cpp
 *
 *  Created on: Apr 23, 2018
 *      Author: alexis
 */

#include <boost/test/unit_test.hpp>
#include <algorithm>
#include <set>
#include <vector>
#include <boost/range/algorithm_ext/insert.hpp>
#include "qry/iterators/unique_iterator.hpp"

namespace shacldator {namespace qry {

BOOST_AUTO_TEST_SUITE(unique_iterator_test);

struct C {
    C() : data(new int(0)) {}
    C(int value) : data(new int(value)) {}
    C(const C& o) : data(new int(*o.data)) {}
    C& operator=(const C& o) {
        if (this != &o) *data = *o.data;
        return *this;
    }
    virtual ~C() {flip(); delete data;}

    void flip() {*data = ~*data;}
    int get() const {return *data;}
    bool operator<(const C& o) const {return get() < o.get();}
    bool operator==(const C& o) const {return get() == o.get();}
    bool operator!=(const C& o) const {return get() != o.get();}

    int* data;
};

typedef set_unique_iterator<std::vector<C>::iterator> it_t;

BOOST_AUTO_TEST_CASE(test_C) {
    C a, b(23);
    {
        C c(72);
        a = c;
        c.flip();
    }
    C d(b);
    BOOST_REQUIRE(b.get() == 23);
    BOOST_REQUIRE(a.get() == 72);
    BOOST_REQUIRE(d.get() == 23);
    d.flip();
    BOOST_REQUIRE(d.get() == ~23);
    BOOST_REQUIRE(b.get() == 23);
    BOOST_REQUIRE(a.get() == 72);
}

BOOST_AUTO_TEST_CASE(empty) {
    std::vector<C> v;
    it_t x(v.begin(), v.end()), y(v.end(), v.end()), z;
    BOOST_REQUIRE(bool(x == y));
    BOOST_REQUIRE(bool(x == z));
    BOOST_REQUIRE(bool(y == z));
    BOOST_REQUIRE(bool(y == x));
    BOOST_REQUIRE(bool(z == x));
    BOOST_REQUIRE(bool(z == y));
}

BOOST_AUTO_TEST_CASE(non_empty) {
    std::vector<C> v{1};
    it_t x(v.begin(), v.end()), y(v.begin(), v.end()),  z(v.end(), v.end()), e;
    BOOST_REQUIRE(bool(x == y));
    BOOST_REQUIRE(bool(y == x));
    BOOST_REQUIRE(bool(x != z));
    BOOST_REQUIRE(bool(x != e));
    BOOST_REQUIRE(bool(z == e));
    BOOST_REQUIRE(bool(e == z));
}

BOOST_AUTO_TEST_CASE(inc_to_end) {
    std::vector<C> v{1};
    it_t x(v.begin(), v.end()), e1(v.end(), v.end()), e2;
    ++x;
    BOOST_REQUIRE(bool(x == e1));
    BOOST_REQUIRE(bool(x == e2));
}

BOOST_AUTO_TEST_CASE(copy) {
    std::vector<C> v{347};
    it_t x, e;
    C val = 798;
    if (x == e) {
        C val2;
        it_t y(v.begin(), v.end());
        x = y;
        val2 = *y;
        BOOST_REQUIRE(bool(++y == e));
        BOOST_REQUIRE(bool(val2 == 347));
        val = val2;
        BOOST_REQUIRE(bool(val == 347));
    }
    BOOST_REQUIRE(bool(val == 347));
    BOOST_REQUIRE(bool(x != e));
    BOOST_REQUIRE(bool(*x == 347));
    BOOST_REQUIRE(bool(++x == e));
}

BOOST_AUTO_TEST_SUITE_END();

}}  // namespace shacldator::qry

