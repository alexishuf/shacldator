/*
 * dsl_join_iterators_test.cpp
 *
 *  Created on: Apr 17, 2018
 *      Author: alexis
 */

#include <boost/test/unit_test.hpp>
#include <set>
#include <algorithm>
#include <iterator>
#include "graph/graph.hpp"
#include "graph/str/stl/store.hpp"
#include "graph/str/raptor_builder.hpp"
#include "ns/foaf.hpp"
#include "ns/rdf.hpp"
#include "qry/dsl.hpp"
#include "qry/dsl_eval.hpp"
#include "qry/priv/triple_filter_sol_iterator.hpp"
#include "qry/iterators/dsl_join_iterators.hpp"


using namespace std;

namespace shacldator {namespace qry {

BOOST_AUTO_TEST_SUITE(dsl_join_iterators_test);

typedef str::stl::store::term_t term_t;
typedef str::triple<term_t> triple_t;
typedef sol<term_t, boost::mpl::vector<ph<1>>>::type sol1_t;
typedef sol<term_t, boost::mpl::vector<ph<1>, ph<2>>>::type sol2_t;

sol1_t mk_sol1(str::stl::store& s, const char* uri1) {
    sol1_t solution;
    solution[_1] = s.resource(uri1);
    return solution;
}
sol1_t mk_sol1(str::stl::store& s, const char* lex, const char* dt,
                                   const char* lang) {
    sol1_t solution;
    solution[_1] = s.literal(lex, dt, lang);
    return solution;
}
sol2_t mk_sol2(str::stl::store& s, const char* uri1, const char* uri2) {
    sol2_t solution;
    solution[_1] = s.resource(uri1);
    solution[_2] = s.resource(uri2);
    return solution;
}
sol2_t mk_sol2(str::stl::store& s, const char* uri1,
                                   const char* lex, const char* dt,
                                   const char* lang) {
    sol2_t solution;
    solution[_1] = s.resource(uri1);
    solution[_2] = s.literal(lex, dt, lang);
    return solution;
}

struct fixture {
    static graph<str::stl::store> load() {
        str::raptor_builder<str::stl::store::builder> b;
        b.parse("turtle", "test-files/join-test.ttl");
        return mk_graph(b.builder.build());
    }

    fixture() : g(load()), s(g.store()) {}
    graph<str::stl::store> g;
    str::stl::store& s;
};

#define EX(localName) ("http://example.org/ns#" BOOST_PP_STRINGIZE(localName))

BOOST_FIXTURE_TEST_CASE(cat_its, fixture) {
    auto tp1  = _t(EX(A), foaf::name, _1);
    auto rng1 = make_pair(
                priv::mk_triple_filter_sol_iterator(s.begin(), s.end(), s, tp1),
                priv::mk_triple_filter_sol_iterator(s.end(), s.end(), s, tp1));
    auto tp2  = _t(EX(B), foaf::name, _1);
    auto rng2 = make_pair(
                priv::mk_triple_filter_sol_iterator(s.begin(), s.end(), s, tp2),
                priv::mk_triple_filter_sol_iterator(s.end(), s.end(), s, tp2));
    auto rng = mk_cat_range(rng1, rng2);

    set<sol1_t> actual, expected;
    copy(rng.first, rng.second, inserter(actual, actual.end()));
    expected.insert(mk_sol1(s, "Alice", rdf::langString, "en"));
    expected.insert(mk_sol1(s, "Bob", rdf::langString, "en"));
    BOOST_REQUIRE(actual.size() == expected.size());
    BOOST_REQUIRE(equal(actual.begin(), actual.end(), expected.begin()));
}

BOOST_FIXTURE_TEST_CASE(left_cat_its, fixture) {
    auto tp1  = _t(EX(A), foaf::name, _1);
    auto rng1 = make_pair(
                priv::mk_triple_filter_sol_iterator(s.begin(), s.end(), s, tp1),
                priv::mk_triple_filter_sol_iterator(s.end(), s.end(), s, tp1));
    auto tp2  = _t(EX(B), foaf::name, _1);
    auto rng2 = make_pair(
                priv::mk_triple_filter_sol_iterator(s.end(), s.end(), s, tp2),
                priv::mk_triple_filter_sol_iterator(s.end(), s.end(), s, tp2));
    auto rng = mk_cat_range(rng1, rng2);

    set<sol1_t> x1, x2;
    copy(rng1.first, rng1.second, inserter(x1, x1.end()));
    copy(rng2.first, rng2.second, inserter(x2, x2.end()));

    set<sol1_t> actual, expected;
    copy(rng.first, rng.second, inserter(actual, actual.end()));
    expected.insert(mk_sol1(s, "Alice", rdf::langString, "en"));
    BOOST_REQUIRE(actual.size() == expected.size());
    BOOST_REQUIRE(equal(actual.begin(), actual.end(), expected.begin()));
}

BOOST_FIXTURE_TEST_CASE(right_cat_its, fixture) {
    auto tp1  = _t(EX(A), foaf::name, _1);
    auto rng1 = make_pair(
                priv::mk_triple_filter_sol_iterator(s.end(), s.end(), s, tp1),
                priv::mk_triple_filter_sol_iterator(s.end(), s.end(), s, tp1));
    auto tp2  = _t(EX(B), foaf::name, _1);
    auto rng2 = make_pair(
                priv::mk_triple_filter_sol_iterator(s.begin(), s.end(), s, tp2),
                priv::mk_triple_filter_sol_iterator(s.end(), s.end(), s, tp2));
    auto rng = mk_cat_range(rng1, rng2);

    set<sol1_t> actual, expected;
    copy(rng.first, rng.second, inserter(actual, actual.end()));
    expected.insert(mk_sol1(s, "Bob", rdf::langString, "en"));
    BOOST_REQUIRE(actual.size() == expected.size());
    BOOST_REQUIRE(equal(actual.begin(), actual.end(), expected.begin()));
}

BOOST_FIXTURE_TEST_CASE(empty_cat_its, fixture) {
    auto tp1  = _t(EX(A), foaf::name, _1);
    auto rng1 = make_pair(
                priv::mk_triple_filter_sol_iterator(s.end(), s.end(), s, tp1),
                priv::mk_triple_filter_sol_iterator(s.end(), s.end(), s, tp1));
    auto tp2  = _t(EX(B), foaf::name, _1);
    auto rng2 = make_pair(
                priv::mk_triple_filter_sol_iterator(s.end(), s.end(), s, tp2),
                priv::mk_triple_filter_sol_iterator(s.end(), s.end(), s, tp2));
    auto rng = mk_cat_range(rng1, rng2);

    bool empty = rng.first == rng.second;
    BOOST_REQUIRE(empty);
}

BOOST_FIXTURE_TEST_CASE(ncat_empty, fixture) {
    auto tp1  = _t(EX(A), foaf::name, _1);
    auto rng1 = make_pair(
                priv::mk_triple_filter_sol_iterator(s.end(), s.end(), s, tp1),
                priv::mk_triple_filter_sol_iterator(s.end(), s.end(), s, tp1)
    );
    ncat_iterator<decltype(rng1.first)> a, b;
    bool eq = a == b;
    BOOST_REQUIRE(eq);
}

BOOST_FIXTURE_TEST_CASE(ncat_2, fixture) {
    auto tp1  = _t(EX(A), foaf::name, _1);
    auto rng1 = make_pair(
                priv::mk_triple_filter_sol_iterator(s.begin(), s.end(), s, tp1),
                priv::mk_triple_filter_sol_iterator(s.end(),   s.end(), s, tp1)
    );
    auto tp2  = _t(EX(B), foaf::name, _1);
    auto rng2 = make_pair(
                priv::mk_triple_filter_sol_iterator(s.begin(), s.end(), s, tp2),
                priv::mk_triple_filter_sol_iterator(s.end(),   s.end(), s, tp2)
    );
    auto rng = mk_ncat_range({rng1, rng2});

    set<sol1_t> actual, expected;
    copy(rng.first, rng.second, inserter(actual, actual.end()));
    expected.insert(mk_sol1(s, "Alice", rdf::langString, "en"));
    expected.insert(mk_sol1(s, "Bob", rdf::langString, "en"));
    BOOST_REQUIRE(actual.size() == expected.size());
    BOOST_REQUIRE(equal(actual.begin(), actual.end(), expected.begin()));
}

BOOST_FIXTURE_TEST_CASE(ncat_middle_empty, fixture) {
    auto tp1  = _t(EX(A), foaf::name, _1);
    auto rng1 = make_pair(
                priv::mk_triple_filter_sol_iterator(s.begin(), s.end(), s, tp1),
                priv::mk_triple_filter_sol_iterator(s.end(),   s.end(), s, tp1)
    );
    auto tp2  = _t(EX(B), foaf::name, _1);
    auto rng2 = make_pair(
                priv::mk_triple_filter_sol_iterator(s.end(), s.end(), s, tp2),
                priv::mk_triple_filter_sol_iterator(s.end(), s.end(), s, tp2)
    );
    auto tp3  = _t(EX(B), foaf::name, _1);
    auto rng3 = make_pair(
                priv::mk_triple_filter_sol_iterator(s.begin(), s.end(), s, tp3),
                priv::mk_triple_filter_sol_iterator(s.end(),   s.end(), s, tp3)
    );
    auto rng = mk_ncat_range({rng1, rng2, rng3});

    set<sol1_t> actual, expected;
    copy(rng.first, rng.second, inserter(actual, actual.end()));
    expected.insert(mk_sol1(s, "Alice", rdf::langString, "en"));
    expected.insert(mk_sol1(s, "Bob", rdf::langString, "en"));
    BOOST_REQUIRE(actual.size() == expected.size());
    BOOST_REQUIRE(equal(actual.begin(), actual.end(), expected.begin()));
}

BOOST_FIXTURE_TEST_CASE(ncat_first_empty, fixture) {
    auto tp1  = _t(EX(A), foaf::name, _1);
    auto rng1 = make_pair(
                priv::mk_triple_filter_sol_iterator(s.end(), s.end(), s, tp1),
                priv::mk_triple_filter_sol_iterator(s.end(), s.end(), s, tp1)
    );
    auto tp2  = _t(EX(A), foaf::name, _1);
    auto rng2 = make_pair(
                priv::mk_triple_filter_sol_iterator(s.begin(), s.end(), s, tp2),
                priv::mk_triple_filter_sol_iterator(s.end(),   s.end(), s, tp2)
    );
    auto tp3  = _t(EX(B), foaf::name, _1);
    auto rng3 = make_pair(
                priv::mk_triple_filter_sol_iterator(s.begin(), s.end(), s, tp3),
                priv::mk_triple_filter_sol_iterator(s.end(),   s.end(), s, tp3)
    );
    auto rng = mk_ncat_range({rng1, rng2, rng3});

    set<sol1_t> actual, expected;
    copy(rng.first, rng.second, inserter(actual, actual.end()));
    expected.insert(mk_sol1(s, "Alice", rdf::langString, "en"));
    expected.insert(mk_sol1(s, "Bob", rdf::langString, "en"));
    BOOST_REQUIRE(actual.size() == expected.size());
    BOOST_REQUIRE(equal(actual.begin(), actual.end(), expected.begin()));
}


BOOST_FIXTURE_TEST_CASE(join_by_name_no_free, fixture) {
    auto tp1  = _t(EX(A), foaf::name, _1);
    auto rng1 = make_pair(
                priv::mk_triple_filter_sol_iterator(s.begin(), s.end(), s, tp1),
                priv::mk_triple_filter_sol_iterator(s.end(), s.end(), s, tp1));
    auto tp2  = _t(EX(A1), foaf::name, _1);
    auto rng2 = make_pair(
                priv::mk_triple_filter_sol_iterator(s.begin(), s.end(), s, tp2),
                priv::mk_triple_filter_sol_iterator(s.end(), s.end(), s, tp2));
    auto rng = mk_hash_join_range(rng1, rng2);

    set<sol1_t> actual, expected;
    copy(rng.first, rng.second, inserter(actual, actual.end()));
    expected.insert(mk_sol1(s, "Alice", rdf::langString, "en"));
    BOOST_REQUIRE(actual.size() == expected.size());
    BOOST_REQUIRE(equal(actual.begin(), actual.end(), expected.begin()));
}

BOOST_FIXTURE_TEST_CASE(join_by_name, fixture) {
    auto tp1  = _t(EX(A), foaf::name, _2);
    auto rng1 = make_pair(
                priv::mk_triple_filter_sol_iterator(s.begin(), s.end(), s, tp1),
                priv::mk_triple_filter_sol_iterator(s.end(), s.end(), s, tp1));
    auto tp2  = _t(_1, foaf::name, _2);
    auto rng2 = make_pair(
                priv::mk_triple_filter_sol_iterator(s.begin(), s.end(), s, tp2),
                priv::mk_triple_filter_sol_iterator(s.end(), s.end(), s, tp2));
    auto rng = mk_hash_join_range(rng1, rng2);

    set<sol2_t> actual, expected;
    copy(rng.first, rng.second, inserter(actual, actual.end()));
    expected.insert(mk_sol2(s, EX(A), "Alice", rdf::langString, "en"));
    expected.insert(mk_sol2(s, EX(A1), "Alice", rdf::langString, "en"));
    BOOST_REQUIRE(actual.size() == expected.size());
    BOOST_REQUIRE(equal(actual.begin(), actual.end(), expected.begin()));
}

BOOST_FIXTURE_TEST_CASE(join_by_name_2, fixture) {
    auto tp1  = _t(EX(A), foaf::name, _2);
    auto rng1 = make_pair(
                priv::mk_triple_filter_sol_iterator(s.begin(), s.end(), s, tp1),
                priv::mk_triple_filter_sol_iterator(s.end(), s.end(), s, tp1));
    auto tp2  = _t(_1, _3, _2);
    auto rng2 = make_pair(
                priv::mk_triple_filter_sol_iterator(s.begin(), s.end(), s, tp2),
                priv::mk_triple_filter_sol_iterator(s.end(), s.end(), s, tp2));
    auto rng = mk_hash_join_range(rng1, rng2);

    set<sol2_t> actual, expected;
    copy(rng.first, rng.second, inserter(actual, actual.end()));
    expected.insert(mk_sol2(s, EX(A), "Alice", rdf::langString, "en"));
    expected.insert(mk_sol2(s, EX(A1), "Alice", rdf::langString, "en"));
    BOOST_REQUIRE(actual.size() == expected.size());
    BOOST_REQUIRE(equal(actual.begin(), actual.end(), expected.begin()));
}

BOOST_FIXTURE_TEST_CASE(join_by_name_bob, fixture) {
    auto tp1  = _t(EX(B), foaf::name, _2);
    auto rng1 = make_pair(
                priv::mk_triple_filter_sol_iterator(s.begin(), s.end(), s, tp1),
                priv::mk_triple_filter_sol_iterator(s.end(), s.end(), s, tp1));
    auto tp2  = _t(_1, foaf::name, _2);
    auto rng2 = make_pair(
                priv::mk_triple_filter_sol_iterator(s.begin(), s.end(), s, tp2),
                priv::mk_triple_filter_sol_iterator(s.end(), s.end(), s, tp2));
    auto rng = mk_hash_join_range(rng1, rng2);

    set<sol2_t> actual, expected;
    copy(rng.first, rng.second, inserter(actual, actual.end()));
    expected.insert(mk_sol2(s, EX(B), "Bob", rdf::langString, "en"));
    expected.insert(mk_sol2(s, EX(B1), "Bob", rdf::langString, "en"));
    expected.insert(mk_sol2(s, EX(B2), "Bob", rdf::langString, "en"));
    expected.insert(mk_sol2(s, EX(B3), "Bob", rdf::langString, "en"));
    BOOST_REQUIRE(actual.size() == expected.size());
    BOOST_REQUIRE(equal(actual.begin(), actual.end(), expected.begin()));
}

BOOST_AUTO_TEST_SUITE_END();

}}  // namespace shacldator::qry
