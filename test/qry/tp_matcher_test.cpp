/*
 * tp_matcher_test.cpp
 *
 *  Created on: Apr 3, 2018
 *      Author: alexis
 */

#include <boost/test/unit_test.hpp>
#include <boost/preprocessor/stringize.hpp>
#include <boost/iterator/filter_iterator.hpp>
#include <algorithm>
#include <set>
#include "ns/foaf.hpp"
#include "ns/rdf.hpp"
#include "ns/xsd.hpp"
#include "qry/dsl.hpp"
#include "qry/priv/tp_matcher.hpp"
#include "graph/str/raptor_builder.hpp"
#include "graph/str/stl/store.hpp"
#include "graph/graph.hpp"

using namespace std;
using namespace boost;

namespace shacldator{namespace qry{ namespace test{

BOOST_AUTO_TEST_SUITE(tp_matcher)

typedef str::stl::store::term_t term_t;
typedef str::triple<term_t> triple_t;

triple_t rt(str::stl::store& s,
            const char* subj, const char* pred, const char* obj) {
    return str::mk_triple(s.resource(subj), s.resource(pred),
                          s.resource(obj));
}

triple_t lt(str::stl::store& s, const char* subj, const char* pred,
            const char* lex, const char* dt, const char* lang) {
    return str::mk_triple(s.resource(subj), s.resource(pred),
                          s.literal(lex, dt, lang));
}

struct fixture {
    static graph<str::stl::store> load() {
        str::raptor_builder<str::stl::store::builder> b;
        b.parse("turtle", "test-files/tp-matcher-test.ttl");
        return mk_graph(b.builder.build());
    }

    fixture() : g(load()), s(g.store()) {}
    graph<str::stl::store> g;
    str::stl::store& s;
};

#define EX(localName) ("http://example.org/ns#" BOOST_PP_STRINGIZE(localName))

BOOST_FIXTURE_TEST_CASE(match_url, fixture) {
    auto m = priv::mk_tp_matcher(g.store(), _t(EX(A), _1, _2));
    set<triple_t> actual, expected;
    copy_if(make_filter_iterator(m, s.begin(), s.end()),
            make_filter_iterator(m, s.end(),   s.end()),
            inserter(actual, actual.end()), m);
    expected.insert(rt(s, EX(A), rdf::type, foaf::Person));
    expected.insert(lt(s, EX(A), foaf::name, "Alice", 0, "en"));
    BOOST_REQUIRE(actual.size() == expected.size());
    BOOST_REQUIRE(std::equal(actual.begin(), actual.end(), expected.begin()));
}

BOOST_FIXTURE_TEST_CASE(match_pred, fixture) {
    auto m = priv::mk_tp_matcher(g.store(), _t(_1, rdf::type, _2));
    set<triple_t> actual, expected;
    copy_if(make_filter_iterator(m, s.begin(), s.end()),
            make_filter_iterator(m, s.end(),   s.end()),
            inserter(actual, actual.end()), m);
    expected.insert(rt(s, EX(A), rdf::type, foaf::Person));
    expected.insert(rt(s, EX(B), rdf::type, foaf::Agent));
    expected.insert(rt(s, EX(D), rdf::type, foaf::Document));
    expected.insert(rt(s, EX(E), rdf::type, foaf::Person));
    expected.insert(rt(s, EX(F), rdf::type, foaf::Document));
    BOOST_REQUIRE(actual.size() == expected.size());
    BOOST_REQUIRE(std::equal(actual.begin(), actual.end(), expected.begin()));
}

BOOST_FIXTURE_TEST_CASE(match_pred_and_obj, fixture) {
    auto m = priv::mk_tp_matcher(g.store(), _t(_1, rdf::type, foaf::Person));
    set<triple_t> actual, expected;
    copy_if(make_filter_iterator(m, s.begin(), s.end()),
            make_filter_iterator(m, s.end(),   s.end()),
            inserter(actual, actual.end()), m);
    expected.insert(rt(s, EX(A), rdf::type, foaf::Person));
    expected.insert(rt(s, EX(E), rdf::type, foaf::Person));
    BOOST_REQUIRE(actual.size() == expected.size());
    BOOST_REQUIRE(std::equal(actual.begin(), actual.end(), expected.begin()));
}

BOOST_FIXTURE_TEST_CASE(match_obj, fixture) {
    auto m = priv::mk_tp_matcher(g.store(), _t(_1, _2, foaf::Person));
    set<triple_t> actual, expected;
    copy_if(make_filter_iterator(m, s.begin(), s.end()),
            make_filter_iterator(m, s.end(),   s.end()),
            inserter(actual, actual.end()), m);
    expected.insert(rt(s, EX(A), rdf::type, foaf::Person));
    expected.insert(rt(s, EX(E), rdf::type, foaf::Person));
    BOOST_REQUIRE(actual.size() == expected.size());
    BOOST_REQUIRE(std::equal(actual.begin(), actual.end(), expected.begin()));
}

BOOST_FIXTURE_TEST_CASE(match_101, fixture) {
    auto m = priv::mk_tp_matcher(g.store(), _t(_1, foaf::knows, _1));
    set<triple_t> actual, expected{rt(s, EX(E), foaf::knows, EX(E))};
    copy_if(make_filter_iterator(m, s.begin(), s.end()),
            make_filter_iterator(m, s.end(),   s.end()),
            inserter(actual, actual.end()), m);
    BOOST_REQUIRE(actual.size() == expected.size());
    BOOST_REQUIRE(std::equal(actual.begin(), actual.end(), expected.begin()));
}

BOOST_FIXTURE_TEST_CASE(match_111, fixture) {
    auto m = priv::mk_tp_matcher(g.store(), _t(_1, _1, _1));
    set<triple_t> actual, expected{
        rt(s, foaf::account, foaf::account, foaf::account)};
    copy_if(make_filter_iterator(m, s.begin(), s.end()),
            make_filter_iterator(m, s.end(),   s.end()),
            inserter(actual, actual.end()), m);
    BOOST_REQUIRE(actual.size() == expected.size());
    BOOST_REQUIRE(std::equal(actual.begin(), actual.end(), expected.begin()));
}

BOOST_FIXTURE_TEST_CASE(match_110, fixture) {
    auto m = priv::mk_tp_matcher(g.store(), _t(_1, _1, _2));
    set<triple_t> actual, expected{
                rt(s, foaf::account, foaf::account, foaf::account),
                rt(s, foaf::account, foaf::account, EX(X))};
    copy_if(make_filter_iterator(m, s.begin(), s.end()),
            make_filter_iterator(m, s.end(),   s.end()),
            inserter(actual, actual.end()), m);
    BOOST_REQUIRE(actual.size() == expected.size());
    BOOST_REQUIRE(std::equal(actual.begin(), actual.end(), expected.begin()));
}

BOOST_FIXTURE_TEST_CASE(match_011, fixture) {
    auto m = priv::mk_tp_matcher(g.store(), _t(_1, _2, _2));
    set<triple_t> actual, expected{
                rt(s, foaf::account, foaf::account, foaf::account),
                rt(s, EX(Y), foaf::account, foaf::account)};
    copy_if(make_filter_iterator(m, s.begin(), s.end()),
            make_filter_iterator(m, s.end(),   s.end()),
            inserter(actual, actual.end()), m);
    BOOST_REQUIRE(actual.size() == expected.size());
    BOOST_REQUIRE(std::equal(actual.begin(), actual.end(), expected.begin()));
}

BOOST_FIXTURE_TEST_CASE(match_lex, fixture) {
    auto m = priv::mk_tp_matcher(g.store(),
            _t(_1, _2, lit::shallow("Alice", 0, 0)));
    set<triple_t> actual, expected;
    copy_if(make_filter_iterator(m, s.begin(), s.end()),
            make_filter_iterator(m, s.end(),   s.end()),
            inserter(actual, actual.end()), m);
    expected.insert(lt(s, EX(A), foaf::name, "Alice", 0, "en"));
    BOOST_REQUIRE(actual.size() == expected.size());
    BOOST_REQUIRE(std::equal(actual.begin(), actual.end(), expected.begin()));
}

BOOST_FIXTURE_TEST_CASE(match_lex_bad_dt, fixture) {
    auto m = priv::mk_tp_matcher(g.store(),
            _t(_1, _2, lit::shallow("Alice", xsd::string_, 0)));
    bool ok = make_filter_iterator(m, s.begin(), s.end()) ==
              make_filter_iterator(m, s.end(),   s.end());
    BOOST_REQUIRE(ok);
}

BOOST_FIXTURE_TEST_CASE(match_lex_bad_lang, fixture) {
    auto m = priv::mk_tp_matcher(g.store(),
            _t(_1, _2, lit::shallow("Alice", rdf::langString, "pt")));
    bool ok = make_filter_iterator(m, s.begin(), s.end()) ==
              make_filter_iterator(m, s.end(),   s.end());
    BOOST_REQUIRE(ok);
}

BOOST_FIXTURE_TEST_CASE(match_lex_good_lang, fixture) {
    auto m = priv::mk_tp_matcher(g.store(),
            _t(_1, _2, lit::shallow("Alice", 0, "en")));
    set<triple_t> actual, expected;
    copy_if(make_filter_iterator(m, s.begin(), s.end()),
            make_filter_iterator(m, s.end(),   s.end()),
            inserter(actual, actual.end()), m);
    expected.insert(lt(s, EX(A), foaf::name, "Alice", 0, "en"));
    BOOST_REQUIRE(actual.size() == expected.size());
    BOOST_REQUIRE(std::equal(actual.begin(), actual.end(), expected.begin()));
}

BOOST_FIXTURE_TEST_CASE(match_lex_good_dt, fixture) {
    auto m = priv::mk_tp_matcher(g.store(),
            _t(_1, _2, lit::shallow("Alice", rdf::langString, 0)));
    set<triple_t> actual, expected;
    copy_if(make_filter_iterator(m, s.begin(), s.end()),
            make_filter_iterator(m, s.end(),   s.end()),
            inserter(actual, actual.end()), m);
    expected.insert(lt(s, EX(A), foaf::name, "Alice", 0, "en"));
    BOOST_REQUIRE(actual.size() == expected.size());
    BOOST_REQUIRE(std::equal(actual.begin(), actual.end(), expected.begin()));
}

BOOST_FIXTURE_TEST_CASE(match_lex_by_dt, fixture) {
    auto m = priv::mk_tp_matcher(g.store(),
            _t(_1, _2, lit::shallow(0, rdf::langString, 0)));
    set<triple_t> actual, expected;
    copy_if(make_filter_iterator(m, s.begin(), s.end()),
            make_filter_iterator(m, s.end(),   s.end()),
            inserter(actual, actual.end()), m);
    expected.insert(lt(s, EX(A), foaf::name, "Alice", 0, "en"));
    expected.insert(lt(s, EX(B), foaf::name, "Bob", 0, "pt"));
    BOOST_REQUIRE(actual.size() == expected.size());
    BOOST_REQUIRE(std::equal(actual.begin(), actual.end(), expected.begin()));
}

BOOST_AUTO_TEST_SUITE_END()

}}} // namespace shacldator::qry::test



