/*
 * collections_iterator.cpp
 *
 *  Created on: Apr 21, 2018
 *      Author: alexis
 */

#include <boost/test/unit_test.hpp>
#include <set>
#include <algorithm>
#include <iterator>
#include <boost/range/algorithm/copy.hpp>
#include "graph/graph.hpp"
#include "graph/str/stl/store.hpp"
#include "graph/str/raptor_builder.hpp"
#include "ns/foaf.hpp"
#include "ns/rdf.hpp"
#include "qry/dsl.hpp"
#include "qry/dsl_eval.hpp"
#include "qry/iterators/collection_iterator.hpp"


using namespace std;

namespace shacldator {namespace qry {

BOOST_AUTO_TEST_SUITE(collection_iterator_test);

BOOST_AUTO_TEST_CASE(empty) {
    auto rng = mk_collection_range(std::vector<int>{});
    bool eq = rng.first == rng.second;
    BOOST_REQUIRE(eq);
}

BOOST_AUTO_TEST_CASE(singleton) {
    auto rng = mk_singleton_range(798);
    BOOST_REQUIRE(*rng.first == 798);
    ++rng.first;
    bool eq = rng.first == rng.second;
    BOOST_REQUIRE(eq);
}

BOOST_AUTO_TEST_CASE(vsingleton) {
    auto rng = mk_vsingleton_range(457);
    BOOST_REQUIRE(*rng.first == 457);
    ++rng.first;
    bool eq = rng.first == rng.second;
    BOOST_REQUIRE(eq);
}

BOOST_AUTO_TEST_CASE(two_numbers) {
    auto rng = mk_collection_range(std::vector<int>{78, 47});
    set<int> actual, expected{78, 47};
    boost::copy(rng, std::inserter(actual, actual.end()));
    BOOST_REQUIRE(actual == expected);
}

BOOST_AUTO_TEST_SUITE_END()


}}  // namespace shacldator::qry



