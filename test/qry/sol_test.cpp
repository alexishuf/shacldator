/*
 * sol_test.cpp
 *
 *  Created on: Apr 25, 2018
 *      Author: alexis
 */

#include <boost/test/unit_test.hpp>
#include <algorithm>
#include <set>
#include <vector>
#include <boost/range/algorithm.hpp>
#include <boost/range/algorithm_ext.hpp>
#include "graph/graph.hpp"
#include "graph/str/stl/store.hpp"
#include "graph/str/raptor_builder.hpp"
#include "viterator.hpp"
#include "ns/foaf.hpp"
#include "ns/rdf.hpp"
#include "qry/sol.hpp"

using namespace std;

namespace shacldator {namespace qry {

BOOST_AUTO_TEST_SUITE(sol_test);

typedef str::stl::store::term_t term_t;
typedef graph<str::stl::store> graph_t;
typedef shacldator::node<graph_t> node_t;
typedef str::triple<term_t> triple_t;
typedef sol<node_t, boost::mpl::vector<ph<1>>>::type sol1_t;
typedef sol<node_t, boost::mpl::vector<ph<1>, ph<2>>>::type sol2_t;

sol1_t mk_sol1(graph_t& g, const char* uri1) {
    sol1_t solution;
    solution[_1] = g.resource(uri1);
    return solution;
}
sol1_t mk_sol1(graph_t& g, const char* lex, const char* dt, const char* lang) {
    sol1_t solution;
    solution[_1] = g.literal(lex, dt, lang);
    return solution;
}
sol2_t mk_sol2(graph_t& g, const char* uri1, const char* uri2) {
    sol2_t solution;
    solution[_1] = g.resource(uri1);
    solution[_2] = g.resource(uri2);
    return solution;
}
sol2_t mk_sol2(graph_t& g, const char* uri1, const char* lex, const char* dt,
                                             const char* lang) {
    sol2_t solution;
    solution[_1] = g.resource(uri1);
    solution[_2] = g.literal(lex, dt, lang);
    return solution;
}

struct fixture {
    static graph<str::stl::store> load() {
        str::raptor_builder<str::stl::store::builder> b;
        b.parse("turtle", "test-files/dsl-test.ttl");
        return mk_graph(b.builder.build());
    }

    fixture() : g(load()), s(g.store()) {}
    graph<str::stl::store> g;
    str::stl::store& s;
};

#define EX(localName) ("http://example.org/ns#" BOOST_PP_STRINGIZE(localName))
#define REX(localName) g.resource(EX(localName))

BOOST_FIXTURE_TEST_CASE(sol_eq, fixture) {
    sol1_t a=mk_sol1(g, EX(X1)), b=mk_sol1(g, EX(X1)), c=mk_sol1(g, EX(X2));
    BOOST_REQUIRE(bool(a == b));
    BOOST_REQUIRE(bool(b == a));
    BOOST_REQUIRE(bool(a != c));
    BOOST_REQUIRE(bool(b != c));
    BOOST_REQUIRE(bool(c != a));
    BOOST_REQUIRE(bool(c != b));
}

BOOST_FIXTURE_TEST_CASE(sol_lt, fixture) {
    vector<sol1_t> v{mk_sol1(g,EX(X1)), mk_sol1(g,EX(X1)), mk_sol1(g,EX(X2))},
                   sorted, actual;
    boost::insert(sorted, sorted.end(), v);
    boost::insert(actual, actual.end(), v);
    boost::sort(sorted,
                [](const sol1_t& l, const sol1_t& r){return l[_1] < r[_1];});
    boost::sort(actual);
    BOOST_REQUIRE(bool(actual == sorted));
}

BOOST_FIXTURE_TEST_CASE(sol2_eq, fixture) {
    sol2_t a = mk_sol2(g, EX(X1), EX(X3)), b = mk_sol2(g, EX(X1), EX(X3)),
           c = mk_sol2(g, EX(X2), EX(X4));
    BOOST_REQUIRE(bool(a == b));
    BOOST_REQUIRE(bool(b == a));
    BOOST_REQUIRE(bool(a != c));
    BOOST_REQUIRE(bool(b != c));
    BOOST_REQUIRE(bool(c != a));
    BOOST_REQUIRE(bool(c != b));
}

BOOST_FIXTURE_TEST_CASE(sol2_lt, fixture) {
    vector<sol2_t> v{mk_sol2(g,EX(X1), EX(X)), mk_sol2(g,EX(X2), EX(X3)),
                     mk_sol2(g,EX(X3), EX(X4))},
                   sorted, actual;
    boost::insert(sorted, sorted.end(), v);
    boost::insert(actual, actual.end(), v);
    boost::sort(sorted,
                [](const sol1_t& l, const sol1_t& r){return l[_1] < r[_1];});
    boost::sort(actual);
    BOOST_REQUIRE(bool(actual == sorted));
}

BOOST_FIXTURE_TEST_CASE(sol_get_first, fixture) {
    BOOST_REQUIRE(bool(get_first_term(mk_sol1(g, EX(X))) == REX(X)));
    BOOST_REQUIRE(bool(get_first_term(mk_sol2(g, EX(X1), EX(X2))) == REX(X1)));
    BOOST_REQUIRE(bool(get_first_term(mk_sol2(g, EX(X3), EX(X2))) == REX(X3)));
}


BOOST_AUTO_TEST_SUITE_END();

}}  // namespace shacldator::qry



