/*
 * dsl_test.cpp
 *
 *  Created on: Apr 18, 2018
 *      Author: alexis
 */

#include <boost/test/unit_test.hpp>
#include <algorithm>
#include <set>
#include <vector>
#include <boost/range/algorithm.hpp>
#include <boost/range/algorithm_ext.hpp>
#include "graph/graph.hpp"
#include "graph/str/stl/store.hpp"
#include "graph/str/raptor_builder.hpp"
#include "viterator.hpp"
#include "ns/foaf.hpp"
#include "ns/rdf.hpp"
#include "qry/dsl.hpp"
#include "qry/dsl_eval.hpp"

using namespace std;

namespace shacldator {namespace qry{

BOOST_AUTO_TEST_SUITE(dsl_test);

typedef str::stl::store::term_t term_t;
typedef graph<str::stl::store> graph_t;
typedef shacldator::node<graph_t> node_t;
typedef shacldator::resource<graph_t> resource_t;
typedef str::triple<term_t> triple_t;
typedef sol<node_t, boost::mpl::vector<ph<1>>>::type sol1_t;
typedef sol<node_t, boost::mpl::vector<ph<1>, ph<2>>>::type sol2_t;

sol1_t mk_sol1(graph_t& g, const char* uri1) {
    sol1_t solution;
    solution[_1] = g.resource(uri1);
    return solution;
}
sol1_t mk_sol1(graph_t& g, const char* lex, const char* dt, const char* lang) {
    sol1_t solution;
    solution[_1] = g.literal(lex, dt, lang);
    return solution;
}
sol2_t mk_sol2(graph_t& g, const char* uri1, const char* uri2) {
    sol2_t solution;
    solution[_1] = g.resource(uri1);
    solution[_2] = g.resource(uri2);
    return solution;
}
sol2_t mk_sol2(graph_t& g, const char* uri1, const char* lex, const char* dt,
                                             const char* lang) {
    sol2_t solution;
    solution[_1] = g.resource(uri1);
    solution[_2] = g.literal(lex, dt, lang);
    return solution;
}

struct fixture {
    static graph<str::stl::store> load() {
        str::raptor_builder<str::stl::store::builder> b;
        b.parse("turtle", "test-files/dsl-test.ttl");
        return mk_graph(b.builder.build());
    }

    fixture() : g(load()), s(g.store()) {}
    graph<str::stl::store> g;
    str::stl::store& s;
};

#define EX(localName) ("http://example.org/ns#" BOOST_PP_STRINGIZE(localName))
#define REX(localName) g.resource(EX(localName))

BOOST_FIXTURE_TEST_CASE(qry_1, fixture) {
    auto rng = mk_evaluator(g).eval(_t(EX(A), foaf::name, _1));

    set<sol1_t> actual, expected;
    copy(rng.first, rng.second, inserter(actual, actual.end()));
    expected.insert(mk_sol1(g, "Alice", rdf::langString, "en"));

    BOOST_REQUIRE(actual.size() == expected.size());
    BOOST_REQUIRE(equal(actual.begin(), actual.end(), expected.begin()));
}

BOOST_FIXTURE_TEST_CASE(qry_2, fixture) {
    auto rng = mk_evaluator(g).eval(_t(_1, foaf::name, _2));

    set<sol2_t> actual, expected;
    copy(rng.first, rng.second, inserter(actual, actual.end()));
    expected.insert(mk_sol2(g, EX(A), "Alice", rdf::langString, "en"));
    expected.insert(mk_sol2(g, EX(B), "Bob",   rdf::langString, "en"));

    BOOST_REQUIRE(actual.size() == expected.size());
    BOOST_REQUIRE(equal(actual.begin(), actual.end(), expected.begin()));
}

BOOST_FIXTURE_TEST_CASE(qry_3, fixture) {
    auto rng = mk_evaluator(g).eval(_t(EX(X1), foaf::knows, _1));

    set<sol1_t> actual, expected;
    copy(rng.first, rng.second, inserter(actual, actual.end()));
    expected.insert(mk_sol1(g, EX(X2)));
    expected.insert(mk_sol1(g, EX(X3)));

    BOOST_REQUIRE(actual.size() == expected.size());
    BOOST_REQUIRE(equal(actual.begin(), actual.end(), expected.begin()));
}

BOOST_FIXTURE_TEST_CASE(qry_4, fixture) {
    auto rng = mk_evaluator(g).eval(_t(_1, foaf::knows, EX(X2)));

    set<sol1_t> actual, expected;
    copy(rng.first, rng.second, inserter(actual, actual.end()));
    expected.insert(mk_sol1(g, EX(X1)));
    expected.insert(mk_sol1(g, EX(X3)));

    BOOST_REQUIRE(actual.size() == expected.size());
    BOOST_REQUIRE(equal(actual.begin(), actual.end(), expected.begin()));
}

BOOST_FIXTURE_TEST_CASE(qry_5, fixture) {
    auto rng = mk_evaluator(g).eval(_t(_1, foaf::knows, EX(X2))
            | _t(_1, foaf::knows, EX(X4)));


    set<sol1_t> actual, expected;
    copy(rng.first, rng.second, inserter(actual, actual.end()));
    expected.insert(mk_sol1(g, EX(X1)));
    expected.insert(mk_sol1(g, EX(X3)));
    expected.insert(mk_sol1(g, EX(X2)));
    expected.insert(mk_sol1(g, EX(X5)));

    BOOST_REQUIRE(actual.size() == expected.size());
    BOOST_REQUIRE(equal(actual.begin(), actual.end(), expected.begin()));
}

BOOST_FIXTURE_TEST_CASE(qry_6, fixture) {
    auto rng = mk_evaluator(g).eval(_t(_1, foaf::knows, EX(X2))
            & _t(EX(X2), foaf::knows, _2));

    set<sol2_t> actual, expected;
    copy(rng.first, rng.second, inserter(actual, actual.end()));
    expected.insert(mk_sol2(g, EX(X1), EX(X4)));
    expected.insert(mk_sol2(g, EX(X1), EX(X5)));
    expected.insert(mk_sol2(g, EX(X3), EX(X4)));
    expected.insert(mk_sol2(g, EX(X3), EX(X5)));

    BOOST_REQUIRE(actual.size() == expected.size());
    BOOST_REQUIRE(equal(actual.begin(), actual.end(), expected.begin()));
}

BOOST_FIXTURE_TEST_CASE(qry_7, fixture) {
    auto rng = mk_evaluator(g).eval(_t(_1, foaf::knows, EX(X2))
            & _t(EX(2), foaf::knows, _2));

    set<sol2_t> actual, expected;
    bool empty = rng.first == rng.second;
    BOOST_REQUIRE(empty);
}

BOOST_AUTO_TEST_CASE(viterator_concepts) {
    typedef viterator<sol1_t> t1;
    typedef viterator<sol1_t, std::input_iterator_tag> t_in;
    typedef viterator<sol1_t, std::forward_iterator_tag> t_fwd;
    BOOST_STATIC_ASSERT(std::is_convertible<t1, t1>::value);
    BOOST_STATIC_ASSERT(std::is_convertible<t_fwd, t_in>::value);
//    BOOST_STATIC_ASSERT(!std::is_convertible<t_in, t_fwd>::value);
}


BOOST_FIXTURE_TEST_CASE(viterator_1, fixture) {
    auto rng = mk_vrange(mk_evaluator(g).eval(_t(_1, foaf::knows, EX(X2))));

    set<sol1_t> actual, expected{mk_sol1(g, EX(X1)), mk_sol1(g, EX(X3))};
    boost::insert(actual, rng);
    BOOST_REQUIRE(bool(actual == expected));
}

BOOST_FIXTURE_TEST_CASE(viterator_explicit, fixture) {
    typedef viterator<sol1_t, std::input_iterator_tag> it_t;
    std::pair<it_t, it_t> rng = mk_vrange(mk_evaluator(g).eval(_t(_1, foaf::knows, EX(X2))));

    set<sol1_t> actual, expected{mk_sol1(g, EX(X1)), mk_sol1(g, EX(X3))};
    boost::insert(actual, rng);
    BOOST_REQUIRE(bool(actual == expected));
}

BOOST_FIXTURE_TEST_CASE(viterator_project_subj, fixture) {
    auto rng = mk_vrange(mk_evaluator(g).eval(_t(_1, foaf::name, _2)(_1)));

    set<sol1_t> actual, expected{mk_sol1(g, EX(A)), mk_sol1(g, EX(B))};
    boost::insert(actual, rng);
    BOOST_REQUIRE(bool(actual == expected));
}

BOOST_FIXTURE_TEST_CASE(qry_8, fixture) {
    auto rng = mk_evaluator(g).eval(_u(_t(EX(A), foaf::mbox, _1)));

    BOOST_REQUIRE(bool(rng.first == rng.second));
    BOOST_REQUIRE(boost::empty(rng));
    vector<sol1_t> actual, expected;
    boost::insert(actual, actual.end(), rng);
    BOOST_REQUIRE(bool(actual == expected));
}

BOOST_FIXTURE_TEST_CASE(qry_9, fixture) {
    auto rng = mk_evaluator(g).eval(_u(_t(EX(A), rdf::type, _1)));

    vector<sol1_t> actual, expected{mk_sol1(g, foaf::Person)};
    boost::insert(actual, actual.end(), rng);
    BOOST_REQUIRE(bool(actual == expected));
}


BOOST_FIXTURE_TEST_CASE(qry_10, fixture) {
    auto rng = mk_evaluator(g).eval(_u(_t(_1, foaf::knows, _2)(_1)));
    vector<sol1_t> vec;
    set<sol1_t> actual, expected{mk_sol1(g, EX(X) ),  mk_sol1(g, EX(X1)),
                                 mk_sol1(g, EX(X2)),  mk_sol1(g, EX(X3)),
                                 mk_sol1(g, EX(X5))};
    boost::insert(vec, vec.end(), rng);
    boost::insert(actual, vec);
    BOOST_REQUIRE(bool(actual == expected));
    BOOST_REQUIRE(actual.size() == expected.size());
}

BOOST_FIXTURE_TEST_CASE(qry_11, fixture) {
    auto rng = mk_evaluator(g).eval_resource(_t(EX(X1), foaf::knows, _1));
    set<resource_t> actual, expected{REX(X2), REX(X3)};
    boost::insert(actual, rng);
    BOOST_REQUIRE(bool(actual == expected));
}

BOOST_FIXTURE_TEST_CASE(qry_12, fixture) {
    auto rng = mk_evaluator(g).eval_node(_t(EX(X1), foaf::knows, _1)
                                       | _t(EX(A),  foaf::name,  _1));
    set<node_t> actual, expected{
        REX(X2), REX(X3),
        g.literal("Alice", rdf::langString, "en")
    };
    boost::insert(actual, rng);
    BOOST_REQUIRE(bool(actual == expected));
}

BOOST_FIXTURE_TEST_CASE(qry_13, fixture) {
    bool r = mk_evaluator(g).ask(_t(EX(A), rdf::type, foaf::Person));
    BOOST_REQUIRE(r);
}

BOOST_FIXTURE_TEST_CASE(qry_14, fixture) {
    bool r = mk_evaluator(g).ask(_t(_1, rdf::type, foaf::Person));
    BOOST_REQUIRE(r);
    r = mk_evaluator(g).ask(_t(EX(A), _1, foaf::Person));
    BOOST_REQUIRE(r);
    r = mk_evaluator(g).ask(_t(EX(A), rdf::type, _1));
    BOOST_REQUIRE(r);
    r = mk_evaluator(g).ask(_t(EX(A), _1, _2));
    BOOST_REQUIRE(r);
    r = mk_evaluator(g).ask(_t(_1, _2, foaf::Person));
    BOOST_REQUIRE(r);
    r = mk_evaluator(g).ask(_t(_1, rdf::type, _2));
    BOOST_REQUIRE(r);
    r = mk_evaluator(g).ask(_t(_1, _2, _3));
    BOOST_REQUIRE(r);
    r = mk_evaluator(g).ask(_t(_1, _2, _3)(_1));
    BOOST_REQUIRE(r);
}

BOOST_AUTO_TEST_SUITE_END();

}} //namespace shacldator::qry


