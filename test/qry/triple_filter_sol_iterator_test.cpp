/*
 * triple_filter_sol_iterator_test.cpp
 *
 *  Created on: Apr 3, 2018
 *      Author: alexis
 */

#include <boost/test/unit_test.hpp>
#include "graph/str/stl/store.hpp"
#include <type_traits>
#include <algorithm>
#include <set>
#include "ns/foaf.hpp"
#include "ns/rdf.hpp"
#include "ns/xsd.hpp"
#include "qry/dsl.hpp"
#include "qry/dsl_eval.hpp"
#include "qry/priv/triple_filter_sol_iterator.hpp"
#include "graph/str/raptor_builder.hpp"
#include "graph/str/stl/store.hpp"
#include "graph/graph.hpp"


using namespace std;

namespace shacldator{namespace qry{ namespace test{

BOOST_AUTO_TEST_SUITE(triple_filter_sol_iterator_test)

typedef str::stl::store::term_t term_t;
typedef str::triple<term_t> triple_t;
typedef sol<term_t, boost::mpl::set<ph<1>>>::type sol1_t;
typedef sol<term_t, boost::mpl::set<ph<1>, ph<2>>>::type sol_t;
typedef sol<term_t, boost::mpl::set<ph<1>, ph<2>,
                                    ph<3>>>::type sol3_t;


sol1_t mk_sol1(str::stl::store& s, const char* uri1) {
    sol1_t solution;
    solution[_1] = s.resource(uri1);
    return solution;
}

sol3_t mk_sol3(const triple_t& t) {
    sol3_t solution;
    solution[_1] = t.subj();
    solution[_2] = t.pred();
    solution[_3] = t.obj();
    return solution;
}

sol_t mk_sol(str::stl::store& s, const char* uri1, const char* uri2) {
    sol_t solution;
    solution[_1] = s.resource(uri1);
    solution[_2] = s.resource(uri2);
    return solution;
}
sol_t mk_sol(str::stl::store& s, const char* uri1, const char* lex2,
                                 const char* dt2,  const char* lang2) {
    sol_t solution;
    solution[_1] = s.resource(uri1);
    solution[_2] = s.literal(lex2, dt2, lang2);
    return solution;
}

struct fixture {
    static graph<str::stl::store> load() {
        str::raptor_builder<str::stl::store::builder> b;
        b.parse("turtle", "test-files/tp-matcher-test.ttl");
        return mk_graph(b.builder.build());
    }

    fixture() : g(load()), s(g.store()) {}
    graph<str::stl::store> g;
    str::stl::store& s;
};


#define EX(localName) ("http://example.org/ns#" BOOST_PP_STRINGIZE(localName))

BOOST_FIXTURE_TEST_CASE(match_url, fixture) {
    auto tp = _t(EX(A), _1, _2);
    auto it  = priv::mk_triple_filter_sol_iterator(s.begin(), s.end(), s, tp);
    auto end = priv::mk_triple_filter_sol_iterator(s.end(),   s.end(), s, tp);
    BOOST_STATIC_ASSERT(is_same<decltype(it)::value_type, sol_t>::value);
    set<sol_t> actual, expected;
    copy(it, end, inserter(actual, actual.end()));
    expected.insert(mk_sol(s, rdf::type, foaf::Person));
    expected.insert(mk_sol(s, foaf::name, "Alice", rdf::langString, "en"));
    BOOST_REQUIRE(actual.size() == expected.size());
    BOOST_REQUIRE(equal(actual.begin(), actual.end(), expected.begin()));
}

BOOST_FIXTURE_TEST_CASE(match_pred, fixture) {
    auto tp = _t(_2, rdf::type, _1);
    auto it  = priv::mk_triple_filter_sol_iterator(s.begin(), s.end(), s, tp);
    auto end = priv::mk_triple_filter_sol_iterator(s.end(),   s.end(), s, tp);
    set<sol_t> actual, expected;
    copy(it, end, inserter(actual, actual.end()));
    expected.insert(mk_sol(s, foaf::Person, EX(A)));
    expected.insert(mk_sol(s, foaf::Agent, EX(B)));
    expected.insert(mk_sol(s, foaf::Document, EX(D)));
    expected.insert(mk_sol(s, foaf::Person, EX(E)));
    expected.insert(mk_sol(s, foaf::Document, EX(F)));
    BOOST_REQUIRE(actual.size() == expected.size());
    BOOST_REQUIRE(equal(actual.begin(), actual.end(), expected.begin()));
}

BOOST_FIXTURE_TEST_CASE(match_obj, fixture) {
    auto tp = _t(_1, _2, lit::shallow("Alice", 0, 0));
    auto it  = priv::mk_triple_filter_sol_iterator(s.begin(), s.end(), s, tp);
    auto end = priv::mk_triple_filter_sol_iterator(s.end(),   s.end(), s, tp);
    set<sol_t> actual, expected;
    copy(it, end, inserter(actual, actual.end()));
    expected.insert(mk_sol(s, EX(A), foaf::name));
    BOOST_REQUIRE(actual.size() == expected.size());
    BOOST_REQUIRE(equal(actual.begin(), actual.end(), expected.begin()));
}

BOOST_FIXTURE_TEST_CASE(match_no_obj, fixture) {
    auto tp = _t(_1, _2, lit::shallow("Charlie", 0, 0));
    auto it  = priv::mk_triple_filter_sol_iterator(s.begin(), s.end(), s, tp);
    auto end = priv::mk_triple_filter_sol_iterator(s.end(),   s.end(), s, tp);
    bool ok = it == end;
    BOOST_REQUIRE(ok);
}

BOOST_FIXTURE_TEST_CASE(match_single_ph, fixture) {
    auto tp = _t(EX(D), _1, EX(T1));
    auto it  = priv::mk_triple_filter_sol_iterator(s.begin(), s.end(), s, tp);
    auto end = priv::mk_triple_filter_sol_iterator(s.end(),   s.end(), s, tp);
    set<sol1_t> actual, expected;
    copy(it, end, inserter(actual, actual.end()));
    expected.insert(mk_sol1(s, foaf::topic));
    BOOST_REQUIRE(actual.size() == expected.size());
    BOOST_REQUIRE(equal(actual.begin(), actual.end(), expected.begin()));
}

BOOST_FIXTURE_TEST_CASE(match_101, fixture) {
    auto tp = _t(_1, foaf::knows, _1);
    auto it  = priv::mk_triple_filter_sol_iterator(s.begin(), s.end(), s, tp);
    auto end = priv::mk_triple_filter_sol_iterator(s.end(),   s.end(), s, tp);
    set<sol1_t> actual, expected{mk_sol1(s, EX(E))};
    copy(it, end, inserter(actual, actual.end()));
    BOOST_REQUIRE(actual.size() == expected.size());
    BOOST_REQUIRE(equal(actual.begin(), actual.end(), expected.begin()));
}
BOOST_FIXTURE_TEST_CASE(match_101_var, fixture) {
    auto tp = _t(_1, _2, _1);
    auto it  = priv::mk_triple_filter_sol_iterator(s.begin(), s.end(), s, tp);
    auto end = priv::mk_triple_filter_sol_iterator(s.end(),   s.end(), s, tp);
    set<sol1_t> actual, expected{mk_sol1(s, EX(E)), mk_sol1(s, foaf::account)};
    copy(it, end, inserter(actual, actual.end()));
    BOOST_REQUIRE(actual.size() == expected.size());
    BOOST_REQUIRE(equal(actual.begin(), actual.end(), expected.begin()));
}

BOOST_FIXTURE_TEST_CASE(match_all, fixture) {
    auto tp = _t(_1, _2, _3);
    auto it  = priv::mk_triple_filter_sol_iterator(s.begin(), s.end(), s, tp);
    auto end = priv::mk_triple_filter_sol_iterator(s.end(),   s.end(), s, tp);
    set<sol3_t> actual, expected;
    copy(it, end, inserter(actual, actual.end()));
    transform(s.begin(), s.end(), inserter(expected, expected.end()),
              mk_sol3);
    BOOST_REQUIRE(actual.size() == expected.size());
    BOOST_REQUIRE(equal(actual.begin(), actual.end(), expected.begin()));
}

BOOST_AUTO_TEST_SUITE_END()

}}} // namespace shacldator::qry::test
