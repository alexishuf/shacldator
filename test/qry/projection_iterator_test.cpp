/*
 * projection_iterator_test.cpp
 *
 *  Created on: Apr 21, 2018
 *      Author: alexis
 */

#include <boost/test/unit_test.hpp>
#include <set>
#include <algorithm>
#include <iterator>
#include <boost/range/algorithm_ext/insert.hpp>
#include "graph/graph.hpp"
#include "graph/str/stl/store.hpp"
#include "graph/str/raptor_builder.hpp"
#include "ns/foaf.hpp"
#include "ns/rdf.hpp"
#include "qry/dsl.hpp"
#include "qry/dsl_eval.hpp"
#include "qry/priv/triple_filter_sol_iterator.hpp"
#include "qry/iterators/projection_iterator.hpp"


using namespace std;

namespace shacldator {namespace qry {

BOOST_AUTO_TEST_SUITE(projection_iterator_test);

typedef str::stl::store::term_t term_t;
typedef str::triple<term_t> triple_t;
typedef sol<term_t, boost::mpl::vector<ph<1>>>::type sol1_t;
typedef sol<term_t, boost::mpl::vector<ph<1>, ph<2>>>::type sol2_t;

sol1_t mk_sol1(str::stl::store& s, const char* uri1) {
    sol1_t solution;
    solution[_1] = s.resource(uri1);
    return solution;
}
sol1_t mk_sol1(str::stl::store& s, const char* lex, const char* dt,
                                   const char* lang) {
    sol1_t solution;
    solution[_1] = s.literal(lex, dt, lang);
    return solution;
}
sol2_t mk_sol2(str::stl::store& s, const char* uri1, const char* uri2) {
    sol2_t solution;
    solution[_1] = s.resource(uri1);
    solution[_2] = s.resource(uri2);
    return solution;
}
sol2_t mk_sol2(str::stl::store& s, const char* uri1,
                                   const char* lex, const char* dt,
                                   const char* lang) {
    sol2_t solution;
    solution[_1] = s.resource(uri1);
    solution[_2] = s.literal(lex, dt, lang);
    return solution;
}

struct fixture {
    static graph<str::stl::store> load() {
        str::raptor_builder<str::stl::store::builder> b;
        b.parse("turtle", "test-files/projection-test.ttl");
        return mk_graph(b.builder.build());
    }

    fixture() : g(load()), s(g.store()) {}
    graph<str::stl::store> g;
    str::stl::store& s;
};

#define EX(localName) ("http://example.org/ns#" BOOST_PP_STRINGIZE(localName))

BOOST_FIXTURE_TEST_CASE(project_empty, fixture) {
    auto tp1  = _t(_1, foaf::img, _2);
    auto rng1 = make_pair(
                priv::mk_triple_filter_sol_iterator(s.begin(), s.end(), s, tp1),
                priv::mk_triple_filter_sol_iterator(s.end(), s.end(), s, tp1));
    auto rng = mk_projection_range(rng1, _1);

    BOOST_REQUIRE(bool(rng.first == rng.second));

    set<sol1_t> actual, expected;
    boost::insert(actual, rng);
    BOOST_REQUIRE(bool(actual == expected));
}

BOOST_FIXTURE_TEST_CASE(project_objects, fixture) {
    auto tp1  = _t(_2, foaf::name, _1);
    auto rng1 = make_pair(
                priv::mk_triple_filter_sol_iterator(s.begin(), s.end(), s, tp1),
                priv::mk_triple_filter_sol_iterator(s.end(), s.end(), s, tp1));
    auto rng = mk_projection_range(rng1, _1);

    set<sol1_t> actual, expected{
        mk_sol1(s, "Alice", rdf::langString, "en"),
        mk_sol1(s, "Bob", rdf::langString, "en"),
        mk_sol1(s, "Charlie", rdf::langString, "en"),
    };
    boost::insert(actual, rng);
    BOOST_REQUIRE(bool(actual == expected));
}

BOOST_FIXTURE_TEST_CASE(project_subjects, fixture) {
    auto tp1  = _t(_1, foaf::name, _2);
    auto rng1 = make_pair(
                priv::mk_triple_filter_sol_iterator(s.begin(), s.end(), s, tp1),
                priv::mk_triple_filter_sol_iterator(s.end(), s.end(), s, tp1));
    auto rng = mk_projection_range(rng1, _1);

    set<sol1_t> actual, expected{
        mk_sol1(s, EX(A)),
        mk_sol1(s, EX(B)),
        mk_sol1(s, EX(C)),
    };
    boost::insert(actual, rng);
    BOOST_REQUIRE(bool(actual == expected));
}

BOOST_FIXTURE_TEST_CASE(project_made_object, fixture) {
    auto tp1  = _t(_2, foaf::made, _1);
    auto rng1 = make_pair(
                priv::mk_triple_filter_sol_iterator(s.begin(), s.end(), s, tp1),
                priv::mk_triple_filter_sol_iterator(s.end(), s.end(), s, tp1));
    auto rng = mk_projection_range(rng1, _1);

    set<sol1_t> actual, expected{mk_sol1(s, EX(Book))};
    boost::insert(actual, rng);
    BOOST_REQUIRE(bool(actual == expected));
}

BOOST_AUTO_TEST_SUITE_END();


}} //shacldator::qry
