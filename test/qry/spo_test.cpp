/*
 * spo_test.cpp
 *
 *  Created on: Apr 22, 2018
 *      Author: alexis
 */

#include <boost/test/unit_test.hpp>
#include <algorithm>
#include <set>
#include <boost/range/algorithm_ext/insert.hpp>
#include "graph/graph.hpp"
#include "graph/str/stl/store.hpp"
#include "graph/str/raptor_builder.hpp"
#include "qry/spo.hpp"
#include "ns/foaf.hpp"
#include "ns/rdf.hpp"

using namespace std;

namespace shacldator {namespace qry {

BOOST_AUTO_TEST_SUITE(spo_test);

typedef str::stl::store::term_t term_t;
typedef graph<str::stl::store> graph_t;
typedef shacldator::node<graph_t> node_t;
typedef shacldator::resource<graph_t> resource_t;

struct fixture {
    static graph<str::stl::store> load() {
        str::raptor_builder<str::stl::store::builder> b;
        b.parse("turtle", "test-files/dsl-test.ttl");
        return mk_graph(b.builder.build());
    }

    fixture() : g(load()), str(g.store()) {}
    graph<str::stl::store> g;
    str::stl::store& str;
};

#define EX(localName) ("http://example.org/ns#" BOOST_PP_STRINGIZE(localName))
#define REX(localName) g.resource(EX(localName))

BOOST_FIXTURE_TEST_CASE(qry_alice, fixture) {
    BOOST_REQUIRE(s(g.resource(foaf::name),
                    lit::shallow("Alice", rdf::langString, "en")
                  ).contains(EX(A)));
}
BOOST_FIXTURE_TEST_CASE(qry_deref, fixture) {
    resource_t r = s(g.resource(foaf::name),
                      lit::shallow("Alice", rdf::langString, "en"))();
    std::string uri = s(g.resource(foaf::name),
                        lit::shallow("Alice", rdf::langString, "en"))().uri();
    BOOST_REQUIRE(bool(r == REX(A)));
    BOOST_REQUIRE(bool(uri == EX(A)));
}
BOOST_FIXTURE_TEST_CASE(qry_alice_resource, fixture) {
    BOOST_REQUIRE(s(g.resource(foaf::name),
                    lit::shallow("Alice", rdf::langString, "en")
                  ).contains(REX(A)));
}

BOOST_FIXTURE_TEST_CASE(qry_name_uri, fixture) {
    BOOST_REQUIRE(p(REX(A),
                    lit::shallow("Alice", rdf::langString, "en")
                  ).contains(foaf::name));
}
BOOST_FIXTURE_TEST_CASE(qry_name, fixture) {
    BOOST_REQUIRE(p(REX(A),
                    lit::shallow("Alice", rdf::langString, "en")
                  ).contains(g.resource(foaf::name)));
}

BOOST_FIXTURE_TEST_CASE(qry_contains_uri, fixture) {
    BOOST_REQUIRE(o(REX(A), rdf::type).contains(foaf::Person));
}
BOOST_FIXTURE_TEST_CASE(qry_contains_resource, fixture) {
    BOOST_REQUIRE(o(REX(A), rdf::type).contains(g.resource(foaf::Person)));
}
BOOST_FIXTURE_TEST_CASE(qry_prop_contains_uri, fixture) {
    BOOST_REQUIRE(o(REX(A), g.resource(rdf::type)).contains(foaf::Person));
}
BOOST_FIXTURE_TEST_CASE(qry_prop_contains_resource, fixture) {
    BOOST_REQUIRE(o(REX(A), g.resource(rdf::type))
            .contains(g.resource(foaf::Person)));
}

BOOST_FIXTURE_TEST_CASE(qry_o_iteration, fixture) {
    static_assert(is_convertible<decltype(*o(REX(A), rdf::type).begin()), node_t
            >::value, "MUST be convertible to node_t");
    set<node_t> actual, expected{g.resource(foaf::Person)};
    boost::insert(actual, o(REX(A), rdf::type));
    BOOST_REQUIRE(bool(actual == expected));
}
BOOST_FIXTURE_TEST_CASE(qry_o_iteration_empty, fixture) {
    set<node_t> actual, expected;
    boost::insert(actual, o(REX(A), foaf::knows));
    BOOST_REQUIRE(bool(actual == expected));
}
BOOST_FIXTURE_TEST_CASE(qry_o_iteration_many, fixture) {
    set<node_t> actual, expected{REX(X2), REX(X3)};
    boost::insert(actual, o(REX(X1), foaf::knows));
    BOOST_REQUIRE(bool(actual == expected));
}

BOOST_FIXTURE_TEST_CASE(qry_p_iteration_empty, fixture) {
    static_assert(is_convertible<decltype(*p(REX(A), REX(W)).begin()), node_t
            >::value, "MUST be convertible to node_t");
    set<node_t> actual, expected;
    boost::insert(actual, p(REX(A), foaf::knows));
    BOOST_REQUIRE(bool(actual == expected));
}
BOOST_FIXTURE_TEST_CASE(qry_p_iteration, fixture) {
    set<node_t> actual, expected{g.resource(rdf::type)};
    boost::insert(actual, p(REX(A), foaf::Person));
    BOOST_REQUIRE(bool(actual == expected));
}

BOOST_FIXTURE_TEST_CASE(qry_s_iteration_empty, fixture) {
    static_assert(is_convertible<decltype(*s(rdf::type, REX(W)).begin()), node_t
            >::value, "MUST be convertible to node_t");
    set<node_t> actual, expected;
    boost::insert(actual, s(foaf::knows, REX(A)));
    BOOST_REQUIRE(bool(actual == expected));
}
BOOST_FIXTURE_TEST_CASE(qry_s_iteration, fixture) {
    set<node_t> actual, expected{REX(X1), REX(X3)};
    boost::insert(actual, s(foaf::knows, REX(X2)));
    BOOST_REQUIRE(bool(actual == expected));
}


BOOST_AUTO_TEST_SUITE_END();

}}  // namespace shacldator::qry


