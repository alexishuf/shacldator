/*
 * main_test.cpp
 *
 *  Created on: Jun 18, 2018
 *      Author: alexis
 */

#include <boost/test/unit_test.hpp>
#include <sstream>
#include "shacldator_runner.hpp"

using namespace std;

namespace shacldator {

BOOST_AUTO_TEST_SUITE(shacldator_runner_test);

BOOST_AUTO_TEST_CASE(presidentes) {
    std::stringstream ss;
    shacldator_runner r;
    bool ok = r.set_dg("test-files/shacl/graphs/presidentes.hdt")
            .set_sg("test-files/shacl/dbo/OfficeHolderShape.ttl")
            .set_out(ss)
            .run();
    BOOST_TEST(ok);
}

BOOST_AUTO_TEST_CASE(none_validator) {
    std::stringstream ss;
    shacldator_runner r;
    bool ok = r.set_dg("test-files/shacl/graphs/presidentes.hdt")
            .set_sg("test-files/shacl/dbo/OfficeHolderShape.ttl")
            .set_validator("none")
            .set_out(ss)
            .run();
    BOOST_TEST(ok);
}

BOOST_AUTO_TEST_CASE(dbpedia) {
    std::stringstream ss;
    shacldator_runner r;
    r.set_dg("test-files/shacl/graphs/dbpedia.hdt")
            .set_sg("test-files/shacl/dbo/dbo-shapes-14.ttl")
            .set_out(ss);
    BOOST_REQUIRE_NO_THROW(r.run());
}

BOOST_AUTO_TEST_CASE(dbpedia_omp) {
    std::stringstream ss;
    shacldator_runner r;
    r.set_dg("test-files/shacl/graphs/dbpedia.hdt")
            .set_sg("test-files/shacl/dbo/dbo-shapes-14.ttl")
            .set_validator("omp")
            .set_out(ss);
    BOOST_REQUIRE_NO_THROW(r.run());
}

BOOST_AUTO_TEST_CASE(dbpedia_inf) {
    std::stringstream ss;
    shacldator_runner r;
    r.set_inf("rdfs--")
        .set_dg("test-files/shacl/graphs/generated-2+o.hdt")
        .set_sg("test-files/shacl/dbo/dbo-inf-shapes-1.ttl")
        .set_out(ss);
    BOOST_REQUIRE_NO_THROW(r.run());
}

//BOOST_AUTO_TEST_CASE(dbpedia_inf_async_setup) {
//    std::stringstream ss;
//    shacldator_runner r;
//    r.set_inf("rdfs--")
//        .set_dg("test-files/shacl/graphs/generated-2+o.hdt")
//        .set_sg("test-files/shacl/dbo/dbo-inf-shapes-1.ttl")
//        .set_inf_opt("async_inf_setup")
//        .set_out(ss);
//    BOOST_REQUIRE_NO_THROW(r.run());
//}

BOOST_AUTO_TEST_CASE(dbpedia_inf_no_validator_closure_omp) {
    std::stringstream ss;
    shacldator_runner r;
    r.set_inf("rdfs--")
            .set_inf_opt("build", "omp")
            .set_dg("test-files/shacl/graphs/generated-2+o.hdt")
            .set_sg("test-files/shacl/dbo/dbo-inf-shapes-1.ttl")
            .set_validator("none")
            .set_out(ss);
    BOOST_REQUIRE_NO_THROW(r.run());
}

BOOST_AUTO_TEST_CASE(dbpedia_inf_no_validator_closure_omp_reuse) {
    std::stringstream ss;
    shacldator_runner r;
    r.set_inf("rdfs--")
        .set_inf_opt("build", "omp_reuse")
        .set_dg("test-files/shacl/graphs/generated-2+o.hdt")
        .set_sg("test-files/shacl/dbo/dbo-inf-shapes-1.ttl")
        .set_validator("none")
        .set_out(ss);
    BOOST_REQUIRE_NO_THROW(r.run());
}

BOOST_AUTO_TEST_SUITE_END();

}  // namespace shacldator
