/*
 * domain_range_map_test.cpp
 *
 *  Created on: Jun 21, 2018
 *      Author: alexis
 */

#include <boost/test/unit_test.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/range/algorithm_ext.hpp>
#include "inf/detail/domain_range_map.hpp"
#include "graph/str/stl/store.hpp"
#include "graph/str/raptor_builder.hpp"
#include "ns/rdfs.hpp"

namespace shacldator {namespace inf {namespace detail {

BOOST_AUTO_TEST_SUITE(domain_range_map_test);

typedef str::stl::store str_t;
typedef str_t::term_t term_t;
typedef graph<str_t> graph_t;
typedef resource<graph_t> res_t;
typedef domain_range_map<term_t> map_t;

graph_t load(const char* filename) {
    str::raptor_builder<str::stl::store::builder> rb;
    std::string path("test-files/");
    path += filename;
    rb.parse("turtle", path.c_str());
    return graph_t(rb.builder.build());
}

#define EX(localName) ("http://example.org/ns#" BOOST_PP_STRINGIZE(localName))
#define TEX(localName) g.store().resource(EX(localName))


BOOST_AUTO_TEST_CASE(no_domain_range) {
    graph_t g = load("closure-test.ttl");
    map_t m = map_t::build(g);
    std::set<term_t> actual, expected{};
    boost::insert(actual, m.domain_of(TEX(A)));
    boost::insert(actual, m.domain_of(TEX(A1)));
    boost::insert(actual, m.domain_of(TEX(B)));
    boost::insert(actual, m.domain_of(TEX(C)));
    boost::insert(actual, m.domain_of(TEX(F)));
    boost::insert(actual, m.range_of(TEX(A)));
    boost::insert(actual, m.range_of(TEX(A1)));
    boost::insert(actual, m.range_of(TEX(B)));
    boost::insert(actual, m.range_of(TEX(C)));
    boost::insert(actual, m.range_of(TEX(F)));
    BOOST_TEST(actual == expected);
}

BOOST_AUTO_TEST_CASE(serial_build) {
    graph_t g = load("domain-range-test.ttl");
    map_t m = map_t::build(g);
    {
        std::set<term_t> actual, expected{TEX(p1)};
        boost::insert(actual, m.domain_of(TEX(A)));
        BOOST_TEST(actual == expected);
    }
    {
        std::set<term_t> actual, expected{TEX(p1), TEX(p2)};
        boost::insert(actual, m.range_of(TEX(B)));
        BOOST_TEST(actual == expected);
    }
    {
        std::set<term_t> actual, expected{TEX(p2)};
        boost::insert(actual, m.domain_of(TEX(A1)));
        BOOST_TEST(actual == expected);
    }
    {
        std::set<term_t> actual, expected{TEX(p3)};
        boost::insert(actual, m.domain_of(TEX(C)));
        BOOST_TEST(actual == expected);
    }
    {
        std::set<term_t> actual, expected{TEX(p3)};
        boost::insert(actual, m.domain_of(TEX(D)));
        BOOST_TEST(actual == expected);
    }
    {
        std::set<term_t> actual, expected{TEX(p3)};
        boost::insert(actual, m.range_of(TEX(E)));
        BOOST_TEST(actual == expected);
    }
    {
        std::set<term_t> actual, expected{};
        boost::insert(actual, m.domain_of(TEX(F)));
        boost::insert(actual, m.range_of(TEX(F)));
        BOOST_TEST(actual == expected);
    }
}

BOOST_AUTO_TEST_SUITE_END();

}}}  // namespace shacldator::inf::detail

