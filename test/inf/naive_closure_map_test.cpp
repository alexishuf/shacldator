/*
 * naive_closure_map_test.cpp
 *
 *  Created on: Jun 21, 2018
 *      Author: alexis
 */

#include <boost/test/unit_test.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/range/algorithm_ext.hpp>
#include <boost/mpl/vector.hpp>
#include "inf/detail/naive_closure_map.hpp"
#include "graph/str/stl/store.hpp"
#include "graph/str/raptor_builder.hpp"
#include "ns/rdfs.hpp"

namespace shacldator {namespace inf {namespace detail {

BOOST_AUTO_TEST_SUITE(naive_closure_map_test);

typedef str::stl::store str_t;
typedef str_t::term_t term_t;
typedef graph<str_t> graph_t;
typedef resource<graph_t> res_t;
typedef naive_closure_map<term_t> map_t;

graph_t load(const char* filename) {
    str::raptor_builder<str::stl::store::builder> rb;
    std::string path("test-files/");
    path += filename;
    rb.parse("turtle", path.c_str());
    return graph_t(rb.builder.build());
}

#define EX(localName) ("http://example.org/ns#" BOOST_PP_STRINGIZE(localName))
#define TEX(localName) g.store().resource(EX(localName))


BOOST_AUTO_TEST_CASE(missing_property) {
    graph_t g = load("closure-test.ttl");
    map_t m = map_t::build(res_t(), false);
    std::set<term_t> actual, expected{};
    boost::insert(actual, m.at(TEX(A)));
    boost::insert(actual, m.at(TEX(A1)));
    boost::insert(actual, m.at(TEX(B)));
    boost::insert(actual, m.at(TEX(C)));
    BOOST_TEST(actual == expected);
}

typedef boost::mpl::vector<
            naive_closure_map_build::serial<graph_t>,
            naive_closure_map_build::omp<graph_t>,
            naive_closure_map_build::omp_reuse<graph_t>> builder_types;

BOOST_AUTO_TEST_CASE_TEMPLATE(build, B, builder_types) {
    graph_t g = load("closure-test.ttl");
    map_t m = map_t::build(g.resource(rdfs::subClassOf), true);
    {
        std::set<term_t> actual, expected{TEX(A12)};
        boost::insert(actual, m.at(TEX(A12)));
        BOOST_TEST(actual == expected);
    }
    {
        std::set<term_t> actual, expected{TEX(B), TEX(B1)};
        boost::insert(actual, m.at(TEX(B)));
        BOOST_TEST(actual == expected);
    }
    {
        std::set<term_t> actual, expected{TEX(A), TEX(A1), TEX(A11), TEX(A12),
                                          TEX(A2), TEX(A21), TEX(B1)};
        boost::insert(actual, m.at(TEX(A)));
        BOOST_TEST(actual == expected);
    }
    {
        std::set<term_t> actual, expected{TEX(C), TEX(C1), TEX(C11)};
        boost::insert(actual, m.at(TEX(C)));
        BOOST_TEST(actual == expected);

        actual.clear();
        boost::insert(actual, m.at(TEX(C1)));
        BOOST_TEST(actual == expected);

        actual.clear();
        boost::insert(actual, m.at(TEX(C11)));
        BOOST_TEST(actual == expected);
    }
}

BOOST_AUTO_TEST_SUITE_END();

}}}  // namespace shacldator::inf::detail

