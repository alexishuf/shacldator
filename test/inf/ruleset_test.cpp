/*
 * ruleset_test.cpp
 *
 *  Created on: May 22, 2018
 *      Author: alexis
 */

#include <boost/test/unit_test.hpp>
#include "inf/ruleset.hpp"
#include "ns/rdf.hpp"
#include "ns/rdfs.hpp"
#include "ns/foaf.hpp"
#include "ns/owl.hpp"

using namespace std;
namespace shacldator {namespace inf{

BOOST_AUTO_TEST_SUITE(ruleset_test);

#define EX(localName) ("http://example.org/ns#" BOOST_PP_STRINGIZE(localName))

struct relevant_fixture {
    static ruleset init() {
        ruleset::builder b;
        b.rule().head("?a", rdf::type, rdfs::Class)              // 0
                .head("?b", rdf::type, rdfs::Class)
                .body("?a", rdfs::domain, "?b").create();
        b.rule().head("?x", owl::sameAs, "?y")                   // 1
                .body("?y", owl::sameAs, "?x").create();
        b.rule().head("?a", "?p", "?c")                          // 2
                .body("?p", rdf::type, owl::TransitiveProperty)
                .body("?a", "?p", "?b")
                .body("?b", "?p", "?c").create();
        b.rule().head("?x", rdf::type, owl::Thing)               // 3
                .body("?x", "?p", "?y").create();
        return b.build();
    }
    relevant_fixture() : rs(init()) {}
    ruleset rs;
};

BOOST_AUTO_TEST_CASE(term_default) {
    ruleset::term t;
    BOOST_TEST(!t);
    BOOST_TEST(!bool(t));
}

BOOST_AUTO_TEST_CASE(term) {
    ruleset::term a(ruleset::term::VAR, 1), b(ruleset::term::VAR, 1),
                  c(ruleset::term::URI, 1), d(ruleset::term::VAR, 2),
                  e(ruleset::term::LITERAL, 2);
    BOOST_TEST(!a == false);
    BOOST_TEST(!b == false);
    BOOST_TEST(!c == false);
    BOOST_TEST(!d == false);

    BOOST_TEST(a == a); BOOST_TEST(c == c); BOOST_TEST(d == d);
    BOOST_TEST(a <= a); BOOST_TEST(c <= c); BOOST_TEST(d <= d);
    BOOST_TEST(a >= a); BOOST_TEST(c >= c); BOOST_TEST(d >= d);
    BOOST_TEST(a == b); BOOST_TEST(b == a);
    BOOST_TEST((a > b) == false); BOOST_TEST((a < b) == false);
    BOOST_TEST((b > a) == false); BOOST_TEST((b < a) == false);
    BOOST_TEST(a != c); BOOST_TEST(c != a);
    BOOST_TEST(a != d); BOOST_TEST(d != a);
    BOOST_TEST(a < c); BOOST_TEST(c > a);
    BOOST_TEST(b < d); BOOST_TEST(d > b);

    BOOST_TEST(a.type() == ruleset::term::VAR);
    BOOST_TEST(c.type() == ruleset::term::URI);

    BOOST_TEST(a.value() == 1);
    BOOST_TEST(d.value() == 2);

    BOOST_TEST(a.is_var());
    BOOST_TEST(!a.is_resource());
    BOOST_TEST(!a.is_literal());

    BOOST_TEST(!c.is_var());
    BOOST_TEST(c.is_resource());
    BOOST_TEST(!c.is_literal());

    BOOST_TEST(!e.is_var());
    BOOST_TEST(!e.is_resource());
    BOOST_TEST(e.is_literal());
}

BOOST_AUTO_TEST_CASE(rule) {
    ruleset::term t1(ruleset::term::VAR, 1),
                  t2(ruleset::term::VAR, 2),
                  t3(ruleset::term::VAR, 3);
    ruleset::rule a, b;
    BOOST_TEST(a == b);
    BOOST_TEST(!(a != b)); BOOST_TEST(!(a > b)); BOOST_TEST(!(a < b));

    a.head.push_back(ruleset::triple(t1, t2, t3));
    BOOST_TEST(a != b);
    BOOST_TEST(!(a == b)); BOOST_TEST(a > b); BOOST_TEST(!(a < b));

    b.head.push_back(ruleset::triple(t1, t2, t3));
    BOOST_TEST(a == b);
    BOOST_TEST(!(a != b)); BOOST_TEST(!(a > b)); BOOST_TEST(!(a < b));

    a.body.push_back(ruleset::triple(t1, t2, t3));
    BOOST_TEST(a != b);
    BOOST_TEST(!(a == b)); BOOST_TEST(a > b); BOOST_TEST(!(a < b));

    b.body.push_back(ruleset::triple(t1, t2, t3));
    BOOST_TEST(a == b);
    BOOST_TEST(!(a != b)); BOOST_TEST(!(a > b)); BOOST_TEST(!(a < b));
}

BOOST_AUTO_TEST_CASE(rule_terms) {
    ruleset::term t1(ruleset::term::VAR, 1),
                  t2(ruleset::term::VAR, 2),
                  t3(ruleset::term::VAR, 3);
    ruleset::rule r;
    r.head.push_back(ruleset::triple(t1, t2, t3));
    r.head.push_back(ruleset::triple(t2, t3, t1));
    r.body.push_back(ruleset::triple(t3, t1, t2));

    vector<ruleset::term> actual, expected{t1, t2, t3, t2, t3, t1, t3, t1, t2};
    r.for_each_term([&](const ruleset::term& t) {actual.push_back(t);});
    BOOST_TEST(actual == expected);
}

BOOST_AUTO_TEST_CASE(rule_triples) {
    ruleset::term t1(ruleset::term::VAR, 1),
                  t2(ruleset::term::VAR, 2),
                  t3(ruleset::term::VAR, 3);
    ruleset::rule r;
    r.head.push_back(ruleset::triple(t1, t2, t3));
    r.head.push_back(ruleset::triple(t2, t3, t1));
    r.body.push_back(ruleset::triple(t3, t1, t2));

    vector<ruleset::triple> actual, expected{
        ruleset::triple(t1, t2, t3),
        ruleset::triple(t2, t3, t1),
        ruleset::triple(t3, t1, t2),
    };
    r.for_each_triple([&](const ruleset::triple& t) {actual.push_back(t);});
    BOOST_TEST(actual == expected);
}

BOOST_AUTO_TEST_CASE(empty) {
    ruleset::builder b;
    auto rs = b.build();
    BOOST_TEST(rs.size() == 0);
    BOOST_TEST(bool(rs.begin() == rs.end()));
}

BOOST_AUTO_TEST_CASE(single_rule) {
    ruleset::builder b;
    auto rs = b.rule().head("?b", rdf::type, rdfs::Class)
                      .body("?a", rdfs::domain, "?b").create().build();
    BOOST_TEST(bool(rs.begin() != rs.end()));
    BOOST_TEST(rs.size() == 1);

    BOOST_TEST(rs[0].head.size() == 1);
    BOOST_TEST(rs[0].body.size() == 1);
    BOOST_TEST(rs.str(rs[0].head[0].subj()) == "?b");
    BOOST_TEST(rs.str(rs[0].head[0].pred()) == rdf::type);
    BOOST_TEST(rs.str(rs[0].head[0].obj()) == rdfs::Class);
    BOOST_TEST(rs.str(rs[0].body[0].subj()) == "?a");
    BOOST_TEST(rs.str(rs[0].body[0].pred()) == rdfs::domain);
    BOOST_TEST(rs.str(rs[0].body[0].obj()) == "?b");
}

BOOST_AUTO_TEST_CASE(relevant_on_empty) {
    ruleset::builder b;
    ruleset rs = b.build();
    auto vec = rs.relevant_heads(EX(A), rdf::type, foaf::Person);
    BOOST_TEST(vec.empty());

    vec = rs.relevant_heads(EX(A), rdf::type, rs.get_term(foaf::Person));
    BOOST_TEST(vec.empty());

    vec = rs.relevant_heads(EX(A), std::string(rdf::type), foaf::Person);
    BOOST_TEST(vec.empty());
}

BOOST_FIXTURE_TEST_CASE(get_term, relevant_fixture) {
    BOOST_TEST(rs.get_term(rdf::type) == rs.get_term(rdf::type));
    BOOST_TEST(rs.get_term(rdf::type) != rs.get_term(EX(Alice)));
    BOOST_TEST(rs.get_term(rdf::type) == rs.get_term(string(rdf::type)));
    BOOST_TEST(rs.get_term(rdf::type) != rs.get_term(rdfs::Class));
    BOOST_TEST(rs.get_term(rdf::type) != rs.get_term(rdfs::domain));
    BOOST_TEST(rs.get_term(rdf::type) != rs.get_term("?a"));
    BOOST_TEST(rs.get_term(rdf::type) != rs.get_term(""));
    BOOST_TEST(rs.get_term(rdf::type) != rs.get_term(nullptr));
}
BOOST_FIXTURE_TEST_CASE(relevant_1, relevant_fixture) {
    auto actual = rs.relevant_heads(EX(Alice), rdf::type, rdfs::Class);
    decltype(actual) expected{&rs[0], &rs[2]};
    BOOST_TEST(actual == expected);
}
BOOST_FIXTURE_TEST_CASE(relevant_2, relevant_fixture) {
    auto actual = rs.relevant_heads(nullptr, rdf::type, rdfs::Class);
    decltype(actual) expected{&rs[0], &rs[2]};
    BOOST_TEST(actual == expected);
}
BOOST_FIXTURE_TEST_CASE(relevant_3, relevant_fixture) {
    auto actual = rs.relevant_heads("?", rdf::type, rdfs::Class);
    decltype(actual) expected{&rs[0], &rs[2]};
    BOOST_TEST(actual == expected);
}
BOOST_FIXTURE_TEST_CASE(relevant_4, relevant_fixture) {
    auto actual = rs.relevant_heads("?", "", nullptr);
    decltype(actual) expected{&rs[0], &rs[1], &rs[2], &rs[3]};
    BOOST_TEST(actual == expected);
}
BOOST_FIXTURE_TEST_CASE(relevant_5, relevant_fixture) {
    auto actual = rs.relevant_heads("?", owl::sameAs, EX(A));
    decltype(actual) expected{&rs[1], &rs[2]};
    BOOST_TEST(actual == expected);
}
BOOST_FIXTURE_TEST_CASE(relevant_6, relevant_fixture) {
    auto actual = rs.relevant_heads("?", rdf::type, "?");
    decltype(actual) expected{&rs[0], &rs[2], &rs[3]};
    BOOST_TEST(actual == expected);
}
BOOST_FIXTURE_TEST_CASE(relevant_7, relevant_fixture) {
    auto actual = rs.relevant_heads("?", rdf::type, owl::Thing);
    decltype(actual) expected{&rs[2], &rs[3]};
    BOOST_TEST(actual == expected);
}


BOOST_AUTO_TEST_SUITE_END();

}}  // namespace shacldator::inf


