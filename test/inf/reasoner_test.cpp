/*
 * backward_test.cpp
 *
 *  Created on: May 28, 2018
 *      Author: alexis
 */

#include <boost/test/unit_test.hpp>
#include <boost/mpl/vector.hpp>
#include <boost/range/algorithm_ext.hpp>
#include <vector>
#include "../../src/inf/microwave.hpp"
#include "inf/backward.hpp"
#include "inf/dsl_inf_eval.hpp"
#include "graph/str/stl/store.hpp"
#include "graph/graph.hpp"
#include "graph/str/raptor_builder.hpp"
#include "ns/rdf.hpp"
#include "ns/rdfs.hpp"
#include "ns/foaf.hpp"
#include "ns/xsd.hpp"

using namespace std;

namespace shacldator {namespace inf {

BOOST_AUTO_TEST_SUITE(reasoner_test)

typedef str::stl::store str_t;
typedef graph<str_t> graph_t;
typedef boost::mpl::vector<backward<graph_t>, microwave<graph_t>> reasoners_t;

graph_t load(const char* filename) {
    str::raptor_builder<str::stl::store::builder> rb;
    std::string path("test-files/");
    path += filename;
    rb.parse("turtle", path.c_str());
    return graph_t(rb.builder.build());
}

#define EX(localName) ("http://example.org/ns#" BOOST_PP_STRINGIZE(localName))
#define REX(localName) f.r.graph.resource(EX(localName))

template<class R> struct fixture1 {
    static ruleset mk_rs() {
        ruleset::builder b;
        return b.rule().head("?x", rdf::type, "?b")
                       .body("?x", rdf::type, "?a")
                       .body("?a", rdfs::subClassOf, "?b").create()
                .rule().head("?a", rdfs::subClassOf, "?c")
                       .body("?a", rdfs::subClassOf, "?b")
                       .body("?b", rdfs::subClassOf, "?c").create().build();
    }

    fixture1() : raw_g(load("inf/data-1.ttl")), r(raw_g, mk_rs()), re(&r) {}

    typedef node<typename R::graph_t> node_t;

    statement<typename R::graph_t> stmt(const char* s, const char* p,
                                        const char* o) const {
        return r.graph.parse_statement(s, p, o);
    }

    graph_t raw_g;
    R r;
    evaluator<R> re;
};
template<class X, class Y> struct fixture1<microwave<X, Y>> {
    fixture1() : raw_g(load("inf/data-1.ttl")), r(raw_g), re(&r) {}

    typedef node<typename microwave<X, Y>::graph_t> node_t;

    statement<typename microwave<X, Y>::graph_t>
    stmt(const char* s, const char* p, const char* o) const {
        return r.graph.parse_statement(s, p, o);
    }

    graph_t raw_g;
    microwave<X, Y> r;
    evaluator<microwave<X, Y>> re;
};


BOOST_AUTO_TEST_CASE_TEMPLATE(existing_triple, R, reasoners_t) {
    fixture1<R> f;
    BOOST_REQUIRE(f.raw_g.contains(EX(a), rdf::type, EX(A1)));
    typedef statement<typename R::graph_t> stmt_t;
    set<stmt_t> actual;
    boost::insert(actual, f.r.range(EX(a), rdf::type, EX(A1)));
    set<stmt_t> expected{f.stmt(EX(a), rdf::type, EX(A1))};
    BOOST_TEST(actual == expected);
}


BOOST_AUTO_TEST_CASE_TEMPLATE(non_existing_triples, R, reasoners_t) {
    fixture1<R> f;
    BOOST_REQUIRE(!f.raw_g.contains(EX(a), rdf::type, EX(A23)));
    BOOST_REQUIRE(!f.raw_g.contains(EX(A), rdf::type, EX(a)));
    typedef statement<typename R::graph_t> stmt_t;
    set<stmt_t> actual;
    boost::insert(actual, f.r.range(EX(a), rdf::type, EX(A23)));
    boost::insert(actual, f.r.range(EX(A), rdf::type, EX(a)));
    set<stmt_t> expected;
    BOOST_TEST(actual == expected);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(inferred_triple, R, reasoners_t) {
    fixture1<R> f;
    BOOST_REQUIRE(!f.raw_g.contains(EX(a), rdf::type, EX(A)));
    typedef statement<typename R::graph_t> stmt_t;
    set<stmt_t> actual;
    boost::insert(actual, f.r.range(EX(a), rdf::type, EX(A)));
    set<stmt_t> expected{f.stmt(EX(a), rdf::type, EX(A))};
    BOOST_TEST(actual == expected);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(infer_class_from_domain, R, reasoners_t) {
    if (is_backward<R>::value) return; //TODO broken test
    fixture1<R> f;
    typedef statement<typename R::graph_t> stmt_t;
    set<stmt_t> actual;
    boost::insert(actual, f.r.range(EX(c), rdf::type, EX(A1)));
    set<stmt_t> expected{f.stmt(EX(c), rdf::type, EX(A1))};
    BOOST_TEST(actual == expected);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(infer_superclass_from_domain, R, reasoners_t) {
    if (is_backward<R>::value) return; //TODO broken test
    if (is_microwave<R>::value) return; //not supported by design
    fixture1<R> f;
    typedef statement<typename R::graph_t> stmt_t;
    set<stmt_t> actual;
    boost::insert(actual, f.r.range(EX(c), rdf::type, EX(A)));
    set<stmt_t> expected{f.stmt(EX(c), rdf::type, EX(A))};
    BOOST_TEST(actual == expected);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(infer_superprop, R, reasoners_t) {
    if (is_backward<R>::value) return; //TODO broken test
    fixture1<R> f;
    typedef statement<typename R::graph_t> stmt_t;
    set<stmt_t> actual;
    boost::insert(actual, f.r.range(EX(c), EX(p), EX(b)));
    set<stmt_t> expected{f.stmt(EX(c), EX(p), EX(b))};
    BOOST_TEST(actual == expected);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(infer_superprop_var_obj, R, reasoners_t) {
    if (is_backward<R>::value) return; //TODO broken test
    fixture1<R> f;
    typedef statement<typename R::graph_t> stmt_t;
    set<stmt_t> actual;
    boost::insert(actual, f.r.range(EX(c), EX(p), "?1"));
    set<stmt_t> expected{f.stmt(EX(c), EX(p), EX(b))};
    BOOST_TEST(actual == expected);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(instances_of, R, reasoners_t) {
    fixture1<R> f;
    BOOST_REQUIRE(!f.raw_g.contains(EX(a), rdf::type, EX(A)));
    BOOST_REQUIRE(f.raw_g.contains(EX(a), rdf::type, EX(A1)));
    typedef statement<typename R::graph_t> stmt_t;
    set<stmt_t> actual;
    boost::insert(actual, f.r.range("?1", rdf::type, EX(A)));
    set<stmt_t> expected{f.stmt(EX(a), rdf::type, EX(A)),
                         f.stmt(EX(b), rdf::type, EX(A))};
    set<string> actual_str;
    boost::transform(actual, inserter(actual_str, actual_str.end()),
                     [](const stmt_t& s){return s.to_str();});
    BOOST_TEST(actual == expected);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(infer_classes, R, reasoners_t) {
    fixture1<R> f;
    BOOST_REQUIRE(!f.raw_g.contains(EX(a), rdf::type, EX(A)));
    BOOST_REQUIRE(f.raw_g.contains(EX(a), rdf::type, EX(A1)));
    typedef statement<typename R::graph_t> stmt_t;
    set<stmt_t> actual;
    boost::insert(actual, f.r.range(EX(a), rdf::type, "?1"));
    set<stmt_t> expected{f.stmt(EX(a), rdf::type, EX(A)),
                         f.stmt(EX(a), rdf::type, EX(A1))};
    set<string> actual_str;
    boost::transform(actual, inserter(actual_str, actual_str.end()),
                     [](const stmt_t& s){return s.to_str();});
    BOOST_TEST(actual == expected);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(inferred_triples_eval, R, reasoners_t) {
    using namespace qry;
    typedef typename fixture1<R>::node_t node_t;
    fixture1<R> f;
    set<node_t> actual, expected{REX(A), REX(A1)};
    boost::insert(actual, f.re.deval_node(_t(EX(a), rdf::type, _1)));
    set<string> actual_str;
    boost::transform(actual, inserter(actual_str, actual_str.end()),
                     [](const node_t& n){return n.to_str();});
    BOOST_TEST(actual == expected);
}


BOOST_AUTO_TEST_SUITE_END()

}}  // namespace shacldator::inf



