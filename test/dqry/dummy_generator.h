/*
 * dummy_generator.h
 *
 *  Created on: May 28, 2018
 *      Author: alexis
 */

#ifndef TEST_INF_DUMMY_GENERATOR_H_
#define TEST_INF_DUMMY_GENERATOR_H_

#include <vector>
#include <memory>

#include "../../src/dqry/term.hpp"
#include "../../src/dqry/triple.hpp"
#include "graph/str/triple.hpp"
#include "utils.hpp"

namespace shacldator {namespace dqry {

template<class Term> struct dummy_generator {
    typedef Term term_t;
    typedef str::triple<term_t> triple_t;
    typedef typename std::vector<triple_t>::const_iterator it_t;
    BOOST_STATIC_ASSERT(std::is_lvalue_reference<
            typename it_t::reference>::value);
    static const std::ptrdiff_t epos =
            std::numeric_limits<std::ptrdiff_t>::max();

    dummy_generator(const std::vector<triple_t>& list) : vec(list), pos(-1) {}
    dummy_generator(const std::initializer_list<triple_t>& list)
                    : vec(list), pos(-1) {}
    dummy_generator(const dummy_generator& o) = default;
    dummy_generator& operator=(const dummy_generator& o) = default;

    const triple_t& get() const {
        return pos < 0 || pos == vec.size() ? null_triple : vec[pos];
    }

    bool advance() {
        if (pos == vec.size()) return false;
        return ++pos < vec.size();
    }

    std::vector<triple_t> vec;
    std::ptrdiff_t pos;
    triple_t null_triple;
};

namespace int_term {

typedef int term;
typedef str::triple<term> triple_t;
typedef dqry::term<term> qry_term_t;
typedef triple<term> qry_triple_t;

typedef dummy_generator<term> dummy_generator_t;

qry_triple_t mk_qt(int s, int p, int o);

}  // namespace int_term

}} // namespace shacldator::dqry

#endif /* TEST_INF_DUMMY_GENERATOR_H_ */
