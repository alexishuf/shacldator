/*
 * path_generators.cpp
 *
 *  Created on: Jun 2, 2018
 *      Author: alexis
 */

#include <boost/test/unit_test.hpp>
#include <boost/range/algorithm_ext.hpp>
#include "graph/graph.hpp"
#include "qry/dsl_eval.hpp"
#include "dqry/path_generators.hpp"
#include "graph/str/stl/store.hpp"
#include "graph/str/raptor_builder.hpp"

using namespace std;
namespace shacldator {namespace dqry {namespace path {

BOOST_AUTO_TEST_SUITE(path_generators_test)

typedef str::stl::store::term_t term_t;
typedef graph<str::stl::store> graph_t;
typedef qry::def_evaluator<graph_t> ev_t;

struct fixture {
    static graph_t load() {
        str::raptor_builder<str::stl::store::builder> b;
        b.parse("turtle", "test-files/path-test.ttl");
        return mk_graph(b.builder.build());
    }

    fixture() : g(load()), s(g.store()), ev(g) {}
    graph_t g;
    str::stl::store& s;
    ev_t ev;
};

template<class Gen> vector<typename Gen::term_t>
to_vec(Gen g, term_t s) {
    vector<typename Gen::term_t> out;
    g.set_subj(s);
    boost::insert(out, out.end(), mk_generator_range(g));
    return out;
}
template<class Gen> set<typename Gen::term_t>
to_set(Gen g, term_t s) {
    set<typename Gen::term_t> out;
    g.set_subj(s);
    boost::insert(out, mk_generator_range(g));
    return out;
}

#define EX(localName) ("http://example.org/ns#" BOOST_PP_STRINGIZE(localName))
#define REX(localName) s.resource(EX(localName))

BOOST_FIXTURE_TEST_CASE(prop_simple, fixture) {
    prop<ev_t> gen(REX(p1), ev);
    gen.set_subj(REX(s1));
    BOOST_REQUIRE(gen.advance());
    BOOST_TEST(gen.get() == REX(o1));
    BOOST_TEST(!gen.advance());
}
BOOST_FIXTURE_TEST_CASE(prop_none, fixture) {
    prop<ev_t> gen(REX(p2), ev);
    gen.set_subj(REX(s1));
    BOOST_REQUIRE(!gen.advance());
}
BOOST_FIXTURE_TEST_CASE(prop_two, fixture) {
    prop<ev_t> gen(REX(p3), ev);
    gen.set_subj(REX(s1));
    BOOST_REQUIRE(gen.advance());
    BOOST_TEST(gen.get() == REX(o1));
    BOOST_REQUIRE(gen.advance());
    BOOST_TEST(gen.get() == REX(o2));
    BOOST_TEST(!gen.advance());
}
BOOST_FIXTURE_TEST_CASE(iterate_empty, fixture) {
    prop<ev_t> gen(REX(p2), ev);
    gen.set_subj(REX(s1));
    auto rng = mk_generator_range(gen);
    BOOST_TEST(rng.first == rng.second);
}
BOOST_FIXTURE_TEST_CASE(iterate_one, fixture) {
    prop<ev_t> gen(REX(p1), ev);
    gen.set_subj(REX(s1));
    auto rng = mk_generator_range(gen);
    BOOST_REQUIRE(rng.first != rng.second);
    BOOST_TEST(*rng.first == REX(o1));
    ++rng.first;
    BOOST_TEST(rng.first == rng.second);
}
BOOST_FIXTURE_TEST_CASE(iterate_two, fixture) {
    vector<term_t> actual = to_vec(prop<ev_t>(REX(p3), ev), REX(s1)),
                   expected{REX(o1), REX(o2)};
    BOOST_REQUIRE(actual == expected);
}

BOOST_AUTO_TEST_CASE(empty_1) {
    empty<term_t> e;
    BOOST_TEST(e.get() == term_t());
    BOOST_TEST(!e.advance());
    e.set_subj(term_t(666));
    BOOST_TEST(e.get() == term_t());
    BOOST_TEST(!e.advance());
}

BOOST_FIXTURE_TEST_CASE(reverse, fixture) {
    vector<term_t> actual = to_vec(rev<ev_t>(REX(p3), ev), REX(o1)),
                   expected{REX(s1)};
    BOOST_TEST(actual == expected);
}
BOOST_FIXTURE_TEST_CASE(reverse_empty, fixture) {
    vector<term_t> actual = to_vec(rev<ev_t>(REX(p5), ev), REX(o3)),
                   expected{};
    BOOST_TEST(actual == expected);
}
BOOST_FIXTURE_TEST_CASE(reverse_nullterm, fixture) {
    vector<term_t> actual = to_vec(rev<ev_t>(term_t(), ev), REX(o3)),
                   expected{};
    BOOST_TEST(actual == expected);
}
BOOST_FIXTURE_TEST_CASE(reverse_two, fixture) {
    vector<term_t> actual = to_vec(rev<ev_t>(REX(p4), ev), REX(o3)),
                   expected{REX(s3), REX(s4)};
    BOOST_TEST(actual == expected);
}
BOOST_FIXTURE_TEST_CASE(neg_none, fixture) {
    vector<term_t> actual = to_vec(neg<ev_t>(REX(p4), ev), REX(s4)),
                   expected{};
    BOOST_TEST(actual == expected);
}
BOOST_FIXTURE_TEST_CASE(neg_one, fixture) {
    vector<term_t> actual = to_vec(neg<ev_t>(REX(p3), ev), REX(s1)),
                   expected{REX(o1)};
    BOOST_TEST(actual == expected);
}
BOOST_FIXTURE_TEST_CASE(neg_null_subj, fixture) {
    vector<term_t> actual = to_vec(neg<ev_t>(REX(p3), ev), term_t()),
                   expected{};
    BOOST_TEST(actual == expected);
}
BOOST_FIXTURE_TEST_CASE(opt_empty, fixture) {
    vector<term_t> actual = to_vec(opt<term_t>(prop<ev_t>(REX(p5), ev)),
                                   term_t()),
                   expected{};
    BOOST_TEST(actual == expected);
}
BOOST_FIXTURE_TEST_CASE(opt_one, fixture) {
    vector<term_t> actual = to_vec(opt<term_t>(prop<ev_t>(REX(p5), ev)),
                                   REX(o5)),
                   expected{REX(o5)};
    BOOST_TEST(actual == expected);
}
BOOST_FIXTURE_TEST_CASE(opt_two, fixture) {
    vector<term_t> actual = to_vec(opt<term_t>(prop<ev_t>(REX(p6), ev)),
                                   REX(s6)),
                   expected{REX(s6), REX(o6)};
    BOOST_TEST(actual == expected);
}

BOOST_FIXTURE_TEST_CASE(plus_empty, fixture) {
    vector<term_t> actual = to_vec(plus<term_t>(prop<ev_t>(REX(p5), ev)),
                                   term_t()),
                   expected{};
    BOOST_TEST(actual == expected);
}
BOOST_FIXTURE_TEST_CASE(plus_empty_2, fixture) {
    vector<term_t> actual = to_vec(plus<term_t>(prop<ev_t>(REX(p5), ev)),
                                   REX(o5)),
                   expected{};
    BOOST_TEST(actual == expected);
}
BOOST_FIXTURE_TEST_CASE(plus_one, fixture) {
    vector<term_t> actual = to_vec(plus<term_t>(prop<ev_t>(REX(p5), ev)),
                                   REX(s5)),
                   expected{REX(o5)};
    BOOST_TEST(actual == expected);
}
BOOST_FIXTURE_TEST_CASE(plus_two, fixture) {
    vector<term_t> actual = to_vec(plus<term_t>(prop<ev_t>(REX(p6), ev)),
                                   REX(s6)),
                   expected{REX(o6), REX(o7)};
    BOOST_TEST(actual == expected);
}

BOOST_FIXTURE_TEST_CASE(star_empty, fixture) {
    vector<term_t> actual = to_vec(star<term_t>(prop<ev_t>(REX(p5), ev)),
                                   term_t()),
                   expected{};
    BOOST_TEST(actual == expected);
}
BOOST_FIXTURE_TEST_CASE(star_one, fixture) {
    vector<term_t> actual = to_vec(star<term_t>(prop<ev_t>(REX(p5), ev)),
                                   REX(o5)),
                   expected{REX(o5)};
    BOOST_TEST(actual == expected);
}
BOOST_FIXTURE_TEST_CASE(star_two, fixture) {
    vector<term_t> actual = to_vec(star<term_t>(prop<ev_t>(REX(p5), ev)),
                                   REX(s5)),
                   expected{REX(s5), REX(o5)};
    BOOST_TEST(actual == expected);
}
BOOST_FIXTURE_TEST_CASE(star_three, fixture) {
    vector<term_t> actual = to_vec(star<term_t>(prop<ev_t>(REX(p6), ev)),
                                   REX(s6)),
                   expected{REX(s6), REX(o6), REX(o7)};
    BOOST_TEST(actual == expected);
}

BOOST_FIXTURE_TEST_CASE(star_tree, fixture) {
    vector<term_t> actual = to_vec(star<term_t>(prop<ev_t>(REX(p8), ev)),
                                   REX(n1));
    set<term_t> expected{REX(n1), REX(n11), REX(n12), REX(n111), REX(n112),
                         REX(n121), REX(n122), REX(n1111), REX(n1112),
                         REX(n1121), REX(n1122), REX(n1211), REX(n1212),
                         REX(n1221), REX(n1222)};
    set<term_t> actual_set;
    boost::insert(actual_set, actual);
    BOOST_REQUIRE(actual_set == expected);
    BOOST_TEST(actual.size() == actual_set.size());
}

BOOST_FIXTURE_TEST_CASE(plus_tree, fixture) {
    vector<term_t> actual = to_vec(plus<term_t>(prop<ev_t>(REX(p8), ev)),
                                   REX(n1));
    set<term_t> expected{REX(n11), REX(n12), REX(n111), REX(n112),
                         REX(n121), REX(n122), REX(n1111), REX(n1112),
                         REX(n1121), REX(n1122), REX(n1211), REX(n1212),
                         REX(n1221), REX(n1222)};
    set<term_t> actual_set;
    boost::insert(actual_set, actual);
    BOOST_REQUIRE(actual_set == expected);
    BOOST_TEST(actual.size() == actual_set.size());
}

BOOST_FIXTURE_TEST_CASE(alt_none, fixture) {
    vector<packed_generator<term_t>> vec;
    alt<term_t> alt(vec);
    BOOST_TEST(alt.get() == term_t());
    BOOST_TEST(!alt.advance());
    alt.set_subj(REX(n1));
    BOOST_TEST(alt.get() == term_t());
    BOOST_TEST(!alt.advance());
}

BOOST_FIXTURE_TEST_CASE(alt_one_fail, fixture) {
    vector<term_t> actual = to_vec(alt<term_t>(vector<prop<ev_t>>{
                                           prop<ev_t>(REX(p1), ev),
                                           prop<ev_t>(REX(p2), ev)}),
                                   REX(s2)),
                   expected{REX(o1)};
    BOOST_TEST(actual == expected);
}

BOOST_FIXTURE_TEST_CASE(alt_repeat, fixture) {
    vector<term_t> actual = to_vec(alt<term_t>(vector<prop<ev_t>>{
                                            prop<ev_t>(REX(p1), ev),
                                            prop<ev_t>(REX(p2), ev),
                                            prop<ev_t>(REX(p3), ev)}),
                                   REX(s1)),
                   expected{REX(o1), REX(o1), REX(o2)};
    BOOST_TEST(actual == expected);
}

BOOST_FIXTURE_TEST_CASE(seq_empty, fixture) {
    seq<term_t> seq(vector<packed_generator<term_t>>{});
    BOOST_TEST(seq.get() == term_t());
    BOOST_TEST(!seq.advance());
    seq.set_subj(REX(s10));
    BOOST_TEST(seq.get() == term_t());
    BOOST_TEST(!seq.advance());
}

BOOST_FIXTURE_TEST_CASE(seq_1, fixture) {
    vector<term_t> actual = to_vec(seq<term_t>(vector<prop<ev_t>>{
                                            prop<ev_t>(REX(p10), ev),
                                            prop<ev_t>(REX(p11), ev)}),
                                   REX(s10)),
                   expected{REX(s13)};
    BOOST_TEST(actual == expected);
}

BOOST_FIXTURE_TEST_CASE(seq_2, fixture) {
    vector<term_t> actual = to_vec(seq<term_t>(vector<packed_generator<term_t>>{
                                            prop<ev_t>(REX(p10), ev),
                                            opt<term_t>(prop<ev_t>(REX(p11), ev))}),
                                   REX(s10)),
                   expected{REX(s11), REX(s13), REX(s12)};
    BOOST_TEST(actual == expected);
}


BOOST_FIXTURE_TEST_CASE(seq_3, fixture) {
    vector<term_t> actual = to_vec(seq<term_t>(vector<packed_generator<term_t>>{
                                            opt<term_t>(prop<ev_t>(REX(p10), ev)),
                                            prop<ev_t>(REX(p11), ev)}),
                                   REX(s10)),
                   expected{REX(s1), REX(s13)};
    BOOST_TEST(actual == expected);
}

BOOST_AUTO_TEST_SUITE_END()

}}}  // namespace shacldator::dqry::path

