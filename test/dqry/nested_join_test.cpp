/*
 * nested_join_test.cpp
 *
 *  Created on: May 27, 2018
 *      Author: alexis
 */

#include <boost/test/unit_test.hpp>
#include <memory>
#include <functional>
#include <vector>
#include <map>
#include <initializer_list>
#include <boost/range/algorithm.hpp>
#include "dummy_generator.h"
#include "dqry/nested_join.hpp"
#include "ns/rdf.hpp"

using namespace std;
using namespace std::placeholders;
namespace shacldator {namespace dqry {

BOOST_AUTO_TEST_SUITE(nested_join_test)

using namespace int_term;

typedef map<qry_triple_t, vector<triple_t>> gen_map_t;

vector<qry_triple_t> mk_goals(const gen_map_t& m) {
    vector<qry_triple_t> v;
    boost::transform(m, back_inserter(v),
                     [](const gen_map_t::value_type& v) {return v.first;});
    return v;
}
bool match(const qry_term_t& generic, const qry_term_t& spec) {
    return generic == spec ? true : generic.is_ph() && !spec.is_ph();
}
bool match(const qry_triple_t& generic, const qry_triple_t& spec) {
    return     match(generic.subj, spec.subj)
            && match(generic.pred, spec.pred)
            && match(generic. obj, spec. obj);
}
boost::function<dummy_generator_t (const qry_triple_t&)>
mk_fac(const gen_map_t& m) {
    return [&](const qry_triple_t& t) {
        for(auto i = m.begin(), e = m.end(); i != e; ++i) {
            if (!match(i->first, t)) continue;
            vector<triple_t> result;
            for (auto j=i->second.begin(),e2=i->second.end(); j != e2; ++j) {
                if (match(t, qry_triple_t(*j)))
                    result.push_back(*j);
            }
            return dummy_generator_t(result);
        }
        return dummy_generator_t({});
    };
}

#define EX(localName) ("http://example.org/ns#" BOOST_PP_STRINGIZE(localName))

BOOST_AUTO_TEST_CASE(no_goal) {
    gen_map_t m;
    nested_join<dummy_generator_t> nj(mk_goals(m), mk_fac(m));
    BOOST_TEST(!nj.advance());
    BOOST_TEST(!nj.get().complete());
}

BOOST_AUTO_TEST_CASE(single_goal_no_solution) {
    gen_map_t m{make_pair(mk_qt(-1, 2, 3), vector<triple_t>{})};
    nested_join<dummy_generator_t> nj(mk_goals(m), mk_fac(m));
    BOOST_TEST(!nj.advance());
    BOOST_TEST(!nj.get().complete());
}

BOOST_AUTO_TEST_CASE(two_goals_no_solution) {
    gen_map_t m{make_pair(mk_qt(-1, 2, 3), vector<triple_t>{}),
                make_pair(mk_qt(4, 2, -1), vector<triple_t>{})};
    nested_join<dummy_generator_t> nj(mk_goals(m), mk_fac(m));
    BOOST_TEST(!nj.advance());
    BOOST_TEST(!nj.get().complete());
}

BOOST_AUTO_TEST_CASE(unconnected) {
    gen_map_t m{make_pair(mk_qt(-1, 2, 3), vector<triple_t>{}),
                make_pair(mk_qt(4, 5, 6), vector<triple_t>{})};
    nested_join<dummy_generator_t> nj(mk_goals(m), mk_fac(m));
    BOOST_TEST(!nj.advance());
    BOOST_TEST(!nj.get().complete());
}

BOOST_AUTO_TEST_CASE(missing_triple) {
    gen_map_t m{make_pair(mk_qt(1, 2, 3), vector<triple_t>{}),
                make_pair(mk_qt(-1, 4, 5),
                          vector<triple_t>{triple_t(1, 4, 5)})};
    nested_join<dummy_generator_t> nj(mk_goals(m), mk_fac(m));
    BOOST_TEST(!nj.advance());
    BOOST_TEST(!nj.get().complete());
}

BOOST_AUTO_TEST_CASE(single_goal) {
    gen_map_t m{make_pair(mk_qt(-1, 2, 3),
                          vector<triple_t>{triple_t(9, 2, 3)})};
    nested_join<dummy_generator_t> nj(mk_goals(m), mk_fac(m));
    BOOST_TEST(!nj.get().complete());
    BOOST_TEST(nj.advance());
    BOOST_TEST(nj.get().size() == 2);
    BOOST_TEST(nj.get()[1] == 9);
    BOOST_TEST(!nj.advance());
}

BOOST_AUTO_TEST_CASE(two_goals) {
    gen_map_t m{make_pair(mk_qt(-1, 2,  3),
                          vector<triple_t>{triple_t(9, 2, 3)}),
                make_pair(mk_qt( 4, -2, -1),
                          vector<triple_t>{triple_t(4, 3, 8),
                                           triple_t(4, 4, 9)})};
    nested_join<dummy_generator_t> nj(mk_goals(m), mk_fac(m));
    BOOST_TEST(!nj.get().complete());
    BOOST_TEST(nj.advance());
    BOOST_TEST(nj.get()[1] == 9);
    BOOST_TEST(nj.get()[2] == 4);
    BOOST_TEST(!nj.advance());
}

BOOST_AUTO_TEST_CASE(two_goals_2) {
    gen_map_t m{make_pair(mk_qt(-1, 2,  3),
                          vector<triple_t>{triple_t(9, 2, 3)}),
                make_pair(mk_qt( 4, -2, -1),
                          vector<triple_t>{triple_t(4, 4, 9),
                                           triple_t(4, 3, 8)})};
    nested_join<dummy_generator_t> nj(mk_goals(m), mk_fac(m));
    BOOST_TEST(nj.advance());
    BOOST_TEST(nj.get()[1] == 9);
    BOOST_TEST(nj.get()[2] == 4);
    BOOST_TEST(!nj.advance());
    BOOST_TEST(!nj.get().complete());
}

BOOST_AUTO_TEST_CASE(two_goals_3) {
    gen_map_t m{make_pair(mk_qt(-1, 2,  3),
                          vector<triple_t>{triple_t(9, 2, 3)}),
                make_pair(mk_qt( 4, -2, -1),
                          vector<triple_t>{triple_t(4, 4, 9),
                                           triple_t(4, 3, 9)})};
    nested_join<dummy_generator_t> nj(mk_goals(m), mk_fac(m));
    BOOST_TEST(!nj.get().complete());
    BOOST_TEST(nj.advance());
    BOOST_TEST(nj.get()[1] == 9);
    BOOST_TEST(nj.get()[2] == 4);
    BOOST_TEST(nj.advance());
    BOOST_TEST(nj.get()[1] == 9);
    BOOST_TEST(nj.get()[2] == 3);
    BOOST_TEST(!nj.advance());
    BOOST_TEST(!nj.get().complete());
}

BOOST_AUTO_TEST_CASE(two_goals_4) {
    gen_map_t m{make_pair(mk_qt(-1, 2,  3),
                          vector<triple_t>{triple_t(9, 2, 3),
                                           triple_t(7, 2, 3)}),
                make_pair(mk_qt( 4, -2, -1),
                          vector<triple_t>{triple_t(4, 4, 9),
                                           triple_t(4, 3, 7)})};
    nested_join<dummy_generator_t> nj(mk_goals(m), mk_fac(m));
    BOOST_TEST(nj.advance());
    BOOST_TEST(nj.get()[1] == 9);
    BOOST_TEST(nj.get()[2] == 4);
    BOOST_TEST(nj.advance());
    BOOST_TEST(nj.get()[1] == 7);
    BOOST_TEST(nj.get()[2] == 3);
    BOOST_TEST(!nj.advance());
}

BOOST_AUTO_TEST_CASE(two_goals_5) {
    gen_map_t m{make_pair(mk_qt(-1, 2,  3),
                          vector<triple_t>{triple_t(9, 2, 3),
                                           triple_t(7, 2, 3)}),
                make_pair(mk_qt( 4, -2, -1),
                          vector<triple_t>{triple_t(4, 3, 7),
                                           triple_t(4, 4, 9)})};
    nested_join<dummy_generator_t> nj(mk_goals(m), mk_fac(m));
    BOOST_TEST(nj.advance());
    BOOST_TEST(nj.get()[1] == 9);
    BOOST_TEST(nj.get()[2] == 4);
    BOOST_TEST(nj.advance());
    BOOST_TEST(nj.get()[1] == 7);
    BOOST_TEST(nj.get()[2] == 3);
    BOOST_TEST(!nj.advance());
}

BOOST_AUTO_TEST_CASE(hard_bound_1) {
    gen_map_t m{make_pair(mk_qt(-1, 2,  3),
                          vector<triple_t>{triple_t(9, 2, 3),
                                           triple_t(7, 2, 3)}),
                make_pair(mk_qt( 4, -2, -1),
                          vector<triple_t>{triple_t(4, 3, 7),
                                           triple_t(4, 4, 9)})};
    nested_join<dummy_generator_t> nj(mk_goals(m), mk_fac(m),
                                      mk_qt(4, -2, -1), mk_qt(4, 3, -1));
    BOOST_TEST(nj.get().is_hard(2));
    BOOST_TEST(!nj.get().is_hard(1));
    BOOST_TEST(nj.advance());
    BOOST_TEST(nj.get()[1] == 7);
    BOOST_TEST(nj.get()[2] == 3);
    BOOST_TEST(!nj.advance());
}

BOOST_AUTO_TEST_CASE(hard_bound_2) {
    gen_map_t m{make_pair(mk_qt(-1, 2,  3),
                          vector<triple_t>{triple_t(9, 2, 3),
                                           triple_t(7, 2, 3)}),
                make_pair(mk_qt( 4, -2, -1),
                          vector<triple_t>{triple_t(4, 3, 7),
                                           triple_t(4, 4, 9)})};
    nested_join<dummy_generator_t> nj(mk_goals(m), mk_fac(m),
                                      mk_qt(4, -2, -1), mk_qt(4, 3, -79));
    BOOST_TEST(!nj.get().complete());
    BOOST_TEST(nj.get().is_hard(2));
    BOOST_TEST(nj.get()[2] == 3);
    BOOST_TEST(!nj.get().is_hard(1));
    BOOST_TEST(nj.advance());
    BOOST_TEST(nj.get().complete());
    BOOST_TEST(nj.get().is_hard(2));
    BOOST_TEST(!nj.get().is_hard(1));
    BOOST_TEST(nj.get()[1] == 7);
    BOOST_TEST(nj.get()[2] == 3);
    BOOST_TEST(!nj.advance());
    BOOST_TEST(!nj.get().complete());
}


BOOST_AUTO_TEST_CASE(hard_bound_oor) {
    gen_map_t m{make_pair(mk_qt(-1, 2,  3),
                          vector<triple_t>{triple_t(9, 2, 3),
                                           triple_t(7, 2, 3)}),
                make_pair(mk_qt( 4, -2, -1),
                          vector<triple_t>{triple_t(4, 3, 7),
                                           triple_t(4, 4, 9)})};
    BOOST_CHECK_THROW(nested_join<dummy_generator_t>(mk_goals(m), mk_fac(m),
                                                     mk_qt(4, -3, -1),
                                                     mk_qt(4, 3, -1)),
                     std::invalid_argument);
}


BOOST_AUTO_TEST_CASE(hard_bound_wrong) {
    gen_map_t m{make_pair(mk_qt(-1, 2,  3),
                          vector<triple_t>{triple_t(9, 2, 3),
                                           triple_t(7, 2, 3)}),
                make_pair(mk_qt( 4, -2, -1),
                          vector<triple_t>{triple_t(4, 3, 7),
                                           triple_t(4, 4, 9)})};
    nested_join<dummy_generator_t> nj(mk_goals(m), mk_fac(m),
                                      mk_qt(-2, -2, -1), mk_qt(4, 3, -1));
    BOOST_TEST(!nj.advance());
}

BOOST_AUTO_TEST_SUITE_END()

}}  // namespace shacldator::dqry

