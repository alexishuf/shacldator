/*
 * generator_iterator_test.cpp
 *
 *  Created on: May 28, 2018
 *      Author: alexis
 */

#include <boost/test/unit_test.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/range/algorithm_ext.hpp>
#include <iostream>
#include "dummy_generator.h"
#include "dqry/generator_iterator.hpp"
#include "ns/rdf.hpp"

namespace shacldator {namespace dqry {

BOOST_AUTO_TEST_SUITE(generator_iterator_test);

using namespace int_term;

BOOST_AUTO_TEST_CASE(empty) {
    dummy_generator_t e({});
    auto rng = mk_generator_range(e);
    BOOST_TEST(rng.first == rng.second);
}

BOOST_AUTO_TEST_CASE(single_direct) {
    std::vector<triple_t> actual, expected{triple_t(1, 2, 3)};
    dummy_generator_t e(expected);
    auto r = mk_generator_range(e);
    BOOST_TEST(r.first != r.second);
    actual.push_back(*r.first);
    ++r.first;
    BOOST_TEST(r.first == r.second);
    BOOST_TEST(actual == expected);
}
BOOST_AUTO_TEST_CASE(single_cp) {
    std::vector<triple_t> actual, expected{triple_t(1, 2, 3)};
    dummy_generator_t e(expected);
    auto r = mk_generator_range(e);
    auto r2 = r;
    BOOST_TEST(r2.first != r2.second);
    actual.push_back(*r2.first);
    ++r2.first;
    BOOST_TEST(r2.first == r2.second);
    BOOST_TEST(actual == expected);
}
BOOST_AUTO_TEST_CASE(single) {
    std::vector<triple_t> actual, expected{triple_t(1, 2, 3)};
    dummy_generator_t e(expected);
    boost::insert(actual, actual.end(), mk_generator_range(e));
    BOOST_TEST(actual == expected);
}
BOOST_AUTO_TEST_CASE(dup) {
    std::vector<triple_t> actual, expected{triple_t(1, 2, 3), triple_t(1, 2, 3)};
    dummy_generator_t e(expected);
    boost::insert(actual, actual.end(), mk_generator_range(e));
    BOOST_TEST(actual == expected);
}
BOOST_AUTO_TEST_CASE(three) {
    std::vector<triple_t> actual, expected{triple_t(1, 2, 3), triple_t(1, 2, 3),
                                           triple_t(4, 5, 6)};
    dummy_generator_t e(expected);
    boost::insert(actual, actual.end(), mk_generator_range(e));
    BOOST_TEST(actual == expected);
}

BOOST_AUTO_TEST_SUITE_END();

}}  // namespace shacldator::dqry


