/*
 * explicit_generator_test.cpp
 *
 *  Created on: May 31, 2018
 *      Author: alexis
 */

#include <boost/test/unit_test.hpp>
#include <vector>
#include <set>
#include <boost/range/algorithm_ext.hpp>
#include <boost/range/algorithm.hpp>
#include "../graph/str/fixtures.hpp"
#include "graph/graph.hpp"
#include "qry/dsl_eval.hpp"
#include "dqry/concepts/Generator.hpp"
#include "dqry/explicit_generator.hpp"
#include "dqry/generator_iterator.hpp"

using namespace std;


namespace shacldator {namespace dqry {

BOOST_AUTO_TEST_SUITE(explicit_generator_test)

using str::test::str_types;
using str::test::simple_fixture;

template<class Store> struct fixture :
        public str::test::simple_fixture<Store> {
    fixture() : g(std::move(*this->s)), e(g) {
        delete this->s;
        this->s = 0;
    }
    graph<Store> g;
    qry::def_evaluator<graph<Store>> e;
};

template<class Store> using gen_t = explicit_generator<
        qry::def_evaluator<graph<Store>>>;
template<class Store> using qt_t    = triple <typename Store::term_t>;
template<class Store> using qterm_t = term   <typename Store::term_t>;
template<class Store> using tr_t    = str::triple<typename Store::term_t>;

BOOST_AUTO_TEST_CASE_TEMPLATE(concept, S, str_types) {
    BOOST_CONCEPT_ASSERT((Generator<gen_t<S>>));
}

BOOST_AUTO_TEST_CASE_TEMPLATE(single_triple, S, str_types) {
    fixture<S> f;
    gen_t<S> g(f.e, qt_t<S>(qterm_t<S>::from_term(f.alice),
                            qterm_t<S>::from_term(f.knows),
                            qterm_t<S>::from_term(f.bob)));
    BOOST_TEST(g.get() == tr_t<S>());
    BOOST_TEST(g.advance());
    BOOST_TEST(g.get() == tr_t<S>(f.alice, f.knows, f.bob));
    BOOST_TEST(!g.advance());
}

BOOST_AUTO_TEST_CASE_TEMPLATE(iterate_objects, S, str_types) {
    typedef typename S::term_t term;
    fixture<S> f;
    set<term> actual, expected{f.bob, f.charlie, f.dave,
                               f.eric, f.george, f.jane};
    auto rng = mk_generator_range(gen_t<S>(f.e, qt_t<S>(
            qterm_t<S>::from_term(f.alice),
            qterm_t<S>::from_term(f.knows),
            qterm_t<S>::from_ph(1))));
    boost::transform(rng, inserter(actual, actual.end()),
                     [](const tr_t<S>& t) {return t.obj();});
    BOOST_TEST(actual == expected);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(iterate_objects_4, S, str_types) {
    typedef typename S::term_t term;
    fixture<S> f;
    set<term> actual, expected{f.bob, f.charlie, f.dave,
                               f.eric, f.george, f.jane};
    auto rng = mk_generator_range(gen_t<S>(f.e, qt_t<S>(
            qterm_t<S>::from_term(f.alice),
            qterm_t<S>::from_term(f.knows),
            qterm_t<S>::from_ph(4))));
    boost::transform(rng, inserter(actual, actual.end()),
                     [](const tr_t<S>& t) {return t.obj();});
    BOOST_TEST(actual == expected);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(iterate_by_pred, S, str_types) {
    typedef typename S::term_t term;
    fixture<S> f;
    set<tr_t<S>> actual, expected{tr_t<S>(f.fabian, f.age, f.twenty)};
    auto rng = mk_generator_range(gen_t<S>(f.e, qt_t<S>(
            qterm_t<S>::from_term(f.fabian),
            qterm_t<S>::from_ph(7),
            qterm_t<S>::from_term(f.twenty))));
    boost::insert(actual, rng);
    BOOST_TEST(actual == expected);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(iterate_two_vars, S, str_types) {
    typedef typename S::term_t term;
    fixture<S> f;
    set<tr_t<S>> actual, expected{tr_t<S>(f.alice, f.knows, f.eric)};
    auto rng = mk_generator_range(gen_t<S>(f.e, qt_t<S>(
            qterm_t<S>::from_ph(1),
            qterm_t<S>::from_ph(2),
            qterm_t<S>::from_term(f.eric))));
    boost::insert(actual, rng);
    BOOST_TEST(actual == expected);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(iterate_two_vars_normalize, S, str_types) {
    typedef typename S::term_t term;
    fixture<S> f;
    set<tr_t<S>> actual, expected{tr_t<S>(f.alice, f.knows, f.eric)};
    auto rng = mk_generator_range(gen_t<S>(f.e, qt_t<S>(
            qterm_t<S>::from_ph(4),
            qterm_t<S>::from_ph(1),
            qterm_t<S>::from_term(f.eric))));
    boost::insert(actual, rng);
    BOOST_TEST(actual == expected);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(iterate_all, S, str_types) {
    typedef typename S::term_t term;
    fixture<S> f;
    set<tr_t<S>> actual, expected{tr_t<S>(f.alice,  f.knows, f.bob),
                                  tr_t<S>(f.alice,  f.knows, f.charlie),
                                  tr_t<S>(f.alice,  f.knows, f.dave),
                                  tr_t<S>(f.alice,  f.knows, f.eric),
                                  tr_t<S>(f.alice,  f.knows, f.george),
                                  tr_t<S>(f.alice,  f.knows, f.jane),
                                  tr_t<S>(f.fabian, f.age,   f.twenty)};
    auto rng = mk_generator_range(gen_t<S>(f.e, qt_t<S>(
            qterm_t<S>::from_ph(1),
            qterm_t<S>::from_ph(2),
            qterm_t<S>::from_ph(3))));
    boost::insert(actual, rng);
    BOOST_TEST(actual == expected);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(iterate_repeated_pattern_1, S, str_types) {
    typedef typename S::term_t term;
    fixture<S> f;
    gen_t<S> g(f.e, qt_t<S>(qterm_t<S>::from_ph(7),
                            qterm_t<S>::from_ph(7),
                            qterm_t<S>::from_ph(1)));
    BOOST_TEST(g.get() == tr_t<S>());
    BOOST_TEST(!g.advance());
    BOOST_TEST(g.get() == tr_t<S>());
}

BOOST_AUTO_TEST_CASE_TEMPLATE(iterate_repeated_pattern_2, S, str_types) {
    typedef typename S::term_t term;
    fixture<S> f;
    gen_t<S> g(f.e, qt_t<S>(qterm_t<S>::from_ph(7),
                            qterm_t<S>::from_ph(7),
                            qterm_t<S>::from_ph(7)));
    BOOST_TEST(!g.advance());
}


BOOST_AUTO_TEST_CASE_TEMPLATE(iterate_repeated_pattern_3, S, str_types) {
    typedef typename S::term_t term;
    fixture<S> f;
    gen_t<S> g(f.e, qt_t<S>(qterm_t<S>::from_ph(7),
                            qterm_t<S>::from_ph(1),
                            qterm_t<S>::from_ph(1)));
    BOOST_TEST(!g.advance());
}

BOOST_AUTO_TEST_CASE_TEMPLATE(iterate_repeated_pattern_4, S, str_types) {
    typedef typename S::term_t term;
    fixture<S> f;
    gen_t<S> g(f.e, qt_t<S>(qterm_t<S>::from_ph(7),
                            qterm_t<S>::from_ph(1),
                            qterm_t<S>::from_ph(7)));
    BOOST_TEST(!g.advance());
}

BOOST_AUTO_TEST_SUITE_END();

}}  // namespace shacldator::dqry

