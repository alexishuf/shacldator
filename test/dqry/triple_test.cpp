/*
 * qry_triple_test.cpp
 *
 *  Created on: May 23, 2018
 *      Author: alexis
 */

#include "../../src/dqry/triple.hpp"

#include <boost/test/unit_test.hpp>
#include "graph/str/stl/store.hpp"

using namespace std;
namespace shacldator {namespace dqry {

BOOST_AUTO_TEST_SUITE(triple_test);

typedef str::stl::term term_t;
typedef term<term_t> qry_term_t;
typedef triple<term_t> qry_triple_t;

BOOST_AUTO_TEST_CASE(construction) {
    qry_triple_t t(qry_term_t::from_ph(1), qry_term_t::from_ph(2),
                   qry_term_t::from_ph(3));
    BOOST_TEST(t.subj == qry_term_t::from_ph(1));
    BOOST_TEST(t.pred == qry_term_t::from_ph(2));
    BOOST_TEST(t.obj  == qry_term_t::from_ph(3));
}

BOOST_AUTO_TEST_CASE(cmp) {
    qry_triple_t a(qry_term_t::from_ph(1), qry_term_t::from_ph(2),
                   qry_term_t::from_ph(3));
    qry_triple_t b(qry_term_t::from_ph(1), qry_term_t::from_ph(2),
                   qry_term_t::from_ph(3));
    qry_triple_t c(qry_term_t::from_ph(1), qry_term_t::from_ph(3),
                   qry_term_t::from_ph(2));
    BOOST_TEST(a == a); BOOST_TEST(a == b); BOOST_TEST(b == a);
    BOOST_TEST(a != c); BOOST_TEST(c != b);
    BOOST_TEST(a <= a); BOOST_TEST(a <= b);
    BOOST_TEST(  a < c ); BOOST_TEST(  c > b );
    BOOST_TEST(!(a > c)); BOOST_TEST(!(c < b));
    BOOST_TEST(  a <= c ); BOOST_TEST(  c >= b );
    BOOST_TEST(!(a >= c)); BOOST_TEST(!(c <= b));
}

BOOST_AUTO_TEST_CASE(subsumes_simple) {
    qry_triple_t a(qry_term_t::from_ph(1), qry_term_t::from_term(1),
                   qry_term_t::from_ph(2));
    qry_triple_t b(qry_term_t::from_ph(2), qry_term_t::from_term(1),
                   qry_term_t::from_term(4));
    BOOST_TEST(a.subsumes(a));
    BOOST_TEST(b.subsumes(b));
    BOOST_TEST(a.subsumes(b));
    BOOST_TEST(!b.subsumes(a));
}

BOOST_AUTO_TEST_CASE(subsumes_inconsistent) {
    qry_triple_t a(qry_term_t::from_ph(1), qry_term_t::from_term(1),
                   qry_term_t::from_ph(1));
    qry_triple_t b(qry_term_t::from_term(2), qry_term_t::from_term(1),
                   qry_term_t::from_term(4));
    BOOST_TEST(a.subsumes(a));
    BOOST_TEST(b.subsumes(b));
    BOOST_TEST(!a.subsumes(b));
    BOOST_TEST(!b.subsumes(a));
}

BOOST_AUTO_TEST_SUITE_END()

}}  // namespace shacldator::dqry




