/*
 * qry_term_test.cpp
 *
 *  Created on: May 23, 2018
 *      Author: alexis
 */

#include "../../src/dqry/term.hpp"

#include <boost/test/unit_test.hpp>
#include "graph/str/stl/store.hpp"
#include "graph/str/raptor_builder.hpp"
#include "ns/xsd.hpp"

using namespace std;
namespace shacldator {namespace dqry{

BOOST_AUTO_TEST_SUITE(term_test);

typedef str::stl::store::term_t term_t;
typedef term<term_t> qry_term_t;

struct fixture {
    static str::stl::store load() {
        str::raptor_builder<str::stl::store::builder> b;
        b.parse("turtle", "test-files/node-test.ttl");
        return b.builder.build();
    }

    fixture() : s(load()) {}
    str::stl::store s;
};

#define EX(localName) ("http://example.org/ns#" BOOST_PP_STRINGIZE(localName))

BOOST_FIXTURE_TEST_CASE(placeholder, fixture) {
    qry_term_t a(qry_term_t::from_ph(1)), b(qry_term_t::from_ph(1)),
               c(qry_term_t::from_ph(2));
    BOOST_TEST(a == a); BOOST_TEST(a == b); BOOST_TEST(b == a);
    BOOST_TEST(a != c); BOOST_TEST(c != b);
    BOOST_TEST(  a < c ); BOOST_TEST(  c > a );
    BOOST_TEST(!(a > c)); BOOST_TEST(!(c < a));
    BOOST_TEST(a <= a); BOOST_TEST(a <= b);
    BOOST_TEST(a >= a); BOOST_TEST(a >= b);
    BOOST_TEST(a <= c ); BOOST_TEST(c >= a );
}

BOOST_FIXTURE_TEST_CASE(term, fixture) {
    auto ta = s.resource(EX(A)), tb = s.resource(EX(B));
    BOOST_REQUIRE(ta < tb);
    qry_term_t a(qry_term_t::from_term(ta)), b(qry_term_t::from_term(ta)),
               c(qry_term_t::from_term(tb));
    BOOST_TEST(a == a); BOOST_TEST(a == b); BOOST_TEST(b == a);
    BOOST_TEST(a != c); BOOST_TEST(c != b);
    BOOST_TEST(  a < c ); BOOST_TEST(  c > a );
    BOOST_TEST(!(a > c)); BOOST_TEST(!(c < a));
    BOOST_TEST(a <= a); BOOST_TEST(a <= b);
    BOOST_TEST(a >= a); BOOST_TEST(a >= b);
    BOOST_TEST(a <= c ); BOOST_TEST(c >= a );
}

BOOST_FIXTURE_TEST_CASE(term_and_ph, fixture) {
    auto ta = s.resource(EX(C));
    BOOST_REQUIRE(1 < ta);
    qry_term_t a(qry_term_t::from_ph(1)), b(qry_term_t::from_ph(1)),
               c(qry_term_t::from_term(ta));
    BOOST_TEST(a == a); BOOST_TEST(a == b); BOOST_TEST(b == a);
    BOOST_TEST(a != c); BOOST_TEST(c != b);
    BOOST_TEST(  a < c ); BOOST_TEST(  c > a );
    BOOST_TEST(!(a > c)); BOOST_TEST(!(c < a));
    BOOST_TEST(a <= a); BOOST_TEST(a <= b);
    BOOST_TEST(a >= a); BOOST_TEST(a >= b);
    BOOST_TEST(a <= c ); BOOST_TEST(c >= a );
}

BOOST_FIXTURE_TEST_CASE(subsumes, fixture) {
    qry_term_t a(qry_term_t::from_ph(1)), b(qry_term_t::from_ph(2)),
               c(qry_term_t::from_term(s.resource(EX(C)))),
               d(qry_term_t::from_term(s.resource(EX(C)))),
               e(qry_term_t::from_term(s.resource(EX(A))));
    BOOST_TEST(a.subsumes(a));
    BOOST_TEST(a.subsumes(b));
    BOOST_TEST(a.subsumes(c));
    BOOST_TEST(b.subsumes(a));
    BOOST_TEST(b.subsumes(c));
    BOOST_TEST(!c.subsumes(a));
    BOOST_TEST(!d.subsumes(b));
    BOOST_TEST(!e.subsumes(a));
    BOOST_TEST(d.subsumes(c));
    BOOST_TEST(c.subsumes(d));
    BOOST_TEST(!e.subsumes(c));
    BOOST_TEST(!c.subsumes(e));
}

BOOST_FIXTURE_TEST_CASE(parse_var, fixture) {
    map<string, unsigned> map;
    qry_term_t t = qry_term_t::parse(s, "?x", map);
    BOOST_TEST(t.is_ph());
    BOOST_TEST(t.ph == 1);
    BOOST_TEST(map.at("?x") == 1);
}
BOOST_FIXTURE_TEST_CASE(parse_vars, fixture) {
    map<string, unsigned> map;
    map.insert(make_pair("?x", 777));
    qry_term_t t = qry_term_t::parse(s, "?x", map);
    BOOST_TEST(t.is_ph());
    BOOST_TEST(t.ph == 777);
    BOOST_TEST(map.at("?x") == 777);
}
BOOST_FIXTURE_TEST_CASE(parse_uri, fixture) {
    map<string, unsigned> map;
    qry_term_t t = qry_term_t::parse(s, EX(A), map);
    BOOST_TEST(t.is_term());
    BOOST_TEST(t.term_v == s.resource(EX(A)));
    BOOST_TEST(map.size() == 0);
}
BOOST_FIXTURE_TEST_CASE(parse_bad_uri, fixture) {
    map<string, unsigned> map;
    BOOST_CHECK_THROW(qry_term_t::parse(s, EX(Z), map), term_not_found);
    BOOST_TEST(map.size() == 0);
}
BOOST_FIXTURE_TEST_CASE(parse_literal, fixture) {
    map<string, unsigned> map;
    qry_term_t t = qry_term_t::parse(s, "\"Dave\"@en", map);
    BOOST_TEST(t.is_term());
    BOOST_TEST(t.term_v == s.literal("Dave", "", "en"));
    qry_term_t t2 = qry_term_t::parse(s, "\"22\"^^<http://www.w3.org/2001/XMLSchema#int>", map);
    BOOST_TEST(t2.is_term());
    BOOST_TEST(t2.term_v == s.literal("22", xsd::int_, ""));
    BOOST_TEST(map.size() == 0);
}
BOOST_FIXTURE_TEST_CASE(parse_bad_literal, fixture) {
    map<string, unsigned> map;
    BOOST_CHECK_THROW(qry_term_t::parse(s, "\"Zed\"", map),
                      term_not_found);
    string plain = string("\"") + EX(A) + "\"";
    BOOST_CHECK_THROW(qry_term_t::parse(s, plain.c_str(), map),
                      term_not_found);
    BOOST_CHECK_THROW(qry_term_t::parse(s, "\"23\"^^<http://www.w3.org/2001/XMLSchema#int>", map),
                      term_not_found);
    BOOST_TEST(map.size() == 0);
}

BOOST_AUTO_TEST_SUITE_END()

}} // namespace shacldator::dqry

