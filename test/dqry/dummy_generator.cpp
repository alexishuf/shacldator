/*
 * dummy_generator.cpp
 *
 *  Created on: May 28, 2018
 *      Author: alexis
 */

#include "dummy_generator.h"

namespace shacldator {namespace dqry {

namespace int_term {

qry_triple_t mk_qt(int s, int p, int o) {
    qry_term_t st = s<0? qry_term_t::from_ph((-1)*s) : qry_term_t::from_term(s),
               pt = p<0? qry_term_t::from_ph((-1)*p) : qry_term_t::from_term(p),
               ot = o<0? qry_term_t::from_ph((-1)*o) : qry_term_t::from_term(o);
    return qry_triple_t(st, pt, ot);
}

}  // namespace int_term


}} // namespace shacldator::dqry
