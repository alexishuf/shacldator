/*
 * shapes_graph_test.cpp
 *
 *  Created on: Apr 23, 2018
 *      Author: alexis
 */

#include <boost/test/unit_test.hpp>
#include <type_traits>
#include <algorithm>
#include <set>
#include <boost/range.hpp>
#include <boost/range/adaptors.hpp>
#include <boost/range/algorithm_ext.hpp>
#include "graph/graph.hpp"
#include "graph/str/stl/store.hpp"
#include "graph/str/raptor_builder.hpp"
#include "qry/spo.hpp"
#include "viterator.hpp"
#include "ns/foaf.hpp"
#include "ns/rdf.hpp"
#include "shacl/shapes_graph.hpp"

using namespace std;
using namespace boost::adaptors;

namespace shacldator { namespace shacl {

BOOST_AUTO_TEST_SUITE(shapes_graph_test);

using namespace qry;

typedef str::stl::store::term_t term_t;
typedef graph<str::stl::store> graph_t;
typedef node<graph_t> node_t;
typedef resource<graph_t> resource_t;
typedef shape<graph_t> shape_t;

struct fixture {
    static graph<str::stl::store> load() {
        str::raptor_builder<str::stl::store::builder> b;
        b.parse("turtle", "test-files/shacl/target-test.ttl");
        return mk_graph(b.builder.build());
    }

    fixture() : g(load()), sg(&g) {}
    graph_t g;
    shapes_graph<graph_t> sg;
};

#define EX(localName) ("http://example.org/ns#" BOOST_PP_STRINGIZE(localName))
#define REX(localName) g.resource(EX(localName))
#define SEX(localName) sg.shape(EX(localName))

BOOST_FIXTURE_TEST_CASE(iterate_only_shapes, fixture) {
    set<shape_t> shapes, actual;
    BOOST_REQUIRE(!boost::empty(sg.shapes_range()));
    boost::insert(shapes, sg.shapes_range());
    auto is_shape = [](const shape_t& s){
        return o(s, rdf::type).contains(sh::NodeShape);};
    boost::insert(actual, shapes | filtered(is_shape));
    BOOST_REQUIRE(bool(actual == shapes));
}

BOOST_FIXTURE_TEST_CASE(iterate_no_repeats, fixture) {
    set<shape_t> expected;
    vector<shape_t> actual;
    boost::insert(actual, actual.end(), sg.shapes_range());
    boost::insert(expected, actual);
    BOOST_REQUIRE(bool(actual.size() == expected.size()));
}

BOOST_FIXTURE_TEST_CASE(iterate_contain_shapes, fixture) {
    set<shape_t> shapes;
    boost::insert(shapes, sg.shapes_range());
    BOOST_REQUIRE(shapes.count(SEX(S1)));
    BOOST_REQUIRE(shapes.count(SEX(S2)));
}

BOOST_FIXTURE_TEST_CASE(single_class_no_matches, fixture) {
    set<resource_t> actual;
    auto tgts = sg.targets(SEX(S1), g);
    boost::insert(actual, tgts);
    BOOST_REQUIRE(actual.empty());
    BOOST_REQUIRE(boost::empty(tgts));
}

BOOST_FIXTURE_TEST_CASE(single_class_one_match, fixture) {
    set<resource_t> actual, expected{REX(I2)};
    auto tgts = sg.targets(SEX(S2), g);
    boost::insert(actual, tgts);
    BOOST_REQUIRE(bool(actual == expected));
}

BOOST_AUTO_TEST_SUITE_END();


}}  // namespace shacldator::shacl

