/*
 * graph_validation_test.cpp
 *
 *  Created on: May 1, 2018
 *      Author: alexis
 */

#include <boost/test/unit_test.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/range/algorithm_ext.hpp>
#include <sstream>
#include "shacl/graph_validator.hpp"
#include "shacl/omp_graph_validator.hpp"
#include "graph/graph.hpp"
#include "graph/raptor_out.hpp"
#include "graph/str/stl/store.hpp"
#include "graph/str/raptor_builder.hpp"
#include "qry/dsl_eval.hpp"
#include "inf/backward.hpp"
#include "inf/rulesets.hpp"
#include "inf/dsl_inf_eval.hpp"
#include "ns/foaf.hpp"
#include "ns/rdf.hpp"
#include "ns/sh.hpp"
#include "ns/owl.hpp"
#include "shacl/shapes_graph.hpp"


using namespace shacldator::qry;
using namespace boost;

namespace shacldator {namespace shacl {

BOOST_AUTO_TEST_SUITE(graph_validation_test);

typedef str::stl::store::term_t term_t;
typedef graph<str::stl::store> graph_t;
typedef def_evaluator<graph_t> eval_t;
typedef shapes_graph<graph_t> shapes_graph_t;
typedef shacldator::node<graph_t> node_t;
typedef shacldator::resource<graph_t> resource_t;

typedef inf::backward<graph_t> bw_t;
typedef inf::evaluator<bw_t> bw_eval_t;

template<template <class, class, class, class> class Validator>
struct wrapper {
    template<class DG, class DGEvaluator> struct apply {
        static bool run(const DG& dg, DGEvaluator dge,
                        const shapes_graph_t& sg,
                        str::stl::store::builder& str_builder) {
            return validate<Validator>(dge, sg, str_builder);
        }
    };
};

typedef mpl::vector<wrapper<val::serial>,
                    wrapper<val::omp_coarse>
        > validator_types;

graph_t load(const char* filename) {
    str::raptor_builder<str::stl::store::builder> rb;
    std::string path = std::string("test-files/shacl/graphs/") + filename;
    rb.parse("turtle", path);
    return graph_t(rb.builder.build());
}

template<class V, class DGEvaluatorFac=eval_t::fac> struct boilerplate {
    typedef typename DGEvaluatorFac::result_type dg_eval_t;
private:
    static graph_t validate(const graph_t& dg, const dg_eval_t& dge,
                            const shapes_graph_t& sg, bool* conforms) {
        str::stl::store::builder builder;
        *conforms = V::template apply<graph_t, dg_eval_t>::run(dg, dge, sg, builder);
        return graph_t(builder.build());
    }
public:
    boilerplate(const char* fileName,
                const DGEvaluatorFac& dge_fac=DGEvaluatorFac())
                : dg(load(fileName)), dge(dge_fac(dg)), sg(&dg),
                  conforms(true), out(validate(dg, dge, sg, &conforms)),
                  out_ev(eval_t(out)) {
        out_nt = graph_to_str(out, "ntriples", "http://example.org/ns#");
    }

    graph_t dg;
    dg_eval_t dge;
    shapes_graph_t sg;
    bool conforms;
    graph_t out;
    eval_t out_ev;
    std::string out_nt;
};

template<class V> struct inf_boilerplate {
    typedef typename bw_t::graph_t dg_t;

    static graph_t validate(const dg_t& dg,
                            const inf::evaluator<bw_t>& dge,
                            const shapes_graph_t& sg, bool* conforms) {
        str::stl::store::builder builder;
        *conforms = V::template apply<dg_t, inf::evaluator<bw_t>>
                     ::run(dg, dge, sg, builder);
        return graph_t(builder.build());
    }

    inf_boilerplate(const char* fileName)
                : raw_dg(load(fileName)), bw(raw_dg, inf::interesting_rdfs()),
                  dg(bw.graph), dge(&bw), sg(&raw_dg),
                  conforms(true), out(validate(dg, dge, sg, &conforms)),
                  out_ev(eval_t(out)) {
        out_nt = graph_to_str(out, "ntriples", "http://example.org/ns#");
    }

    graph_t raw_dg;
    bw_t bw;
    const dg_t& dg;
    inf::evaluator<bw_t> dge;
    shapes_graph_t sg;
    bool conforms;
    graph_t out;
    eval_t out_ev;
    std::string out_nt;
};

#define EX(localName) ("http://example.org/ns#" BOOST_PP_STRINGIZE(localName))

BOOST_AUTO_TEST_CASE_TEMPLATE(class_, V, validator_types) {
    boilerplate<V> b("class.ttl");
    BOOST_REQUIRE(!b.conforms);
    bool ask = b.out_ev.ask(_t(_2, rdf::type, sh::ValidationReport)
            &               _t(_2, sh::conforms,
                               lit::shallow("false", xsd::boolean_))
            &               _t(_1, rdf::type, sh::ValidationResult)
            &               _t(_1, sh::focusNode, EX(Invalid))
            &               _t(_1, sh::resultSeverity, sh::Violation)
            &               _t(_1, sh::sourceConstraintComponent,
                                   sh::ClassConstraintComponent)
            &               _t(_1, sh::sourceShape, EX(S1))
            );
    BOOST_TEST(ask, "ntriple of output:\n" << b.out_nt);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(class_of_object, V, validator_types) {
    boilerplate<V> b("class-of-object.ttl");
    BOOST_REQUIRE(!b.conforms);
    bool inv = b.out_ev.ask(_t(_1, rdf::type, sh::ValidationResult)
            &               _t(_1, sh::focusNode, EX(Invalid))
            &               _t(_1, sh::value,  EX(Valid1))
            &               _t(_1, sh::sourceConstraintComponent,
                                   sh::ClassConstraintComponent)
            &               _t(_1, sh::sourceShape, EX(S1))
            );
    bool val1 = !b.out_ev.ask(_t(_1, sh::focusNode, EX(Valid1)));
    bool val2 = !b.out_ev.ask(_t(_1, sh::focusNode, EX(Valid2)));
    BOOST_TEST_REQUIRE(inv , "ntriple of output:\n" << b.out_nt);
    BOOST_TEST_REQUIRE(val1, "ntriple of output:\n" << b.out_nt);
    BOOST_TEST_REQUIRE(val2, "ntriple of output:\n" << b.out_nt);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(recursive_shape, V, validator_types) {
    boilerplate<V> b("recursive-shape.ttl");
    BOOST_REQUIRE(!b.conforms);
    auto r1 = b.out_ev.eval_resource(_t(_1, rdf::type, sh::ValidationResult)
                        &    _t(_1, sh::focusNode, EX(Invalid1))
                        &    _t(_1, sh::sourceConstraintComponent,
                                    sh::NodeKindConstraintComponent)
                        &    _t(_1, sh::sourceShape, EX(S1)));
    auto r2 = b.out_ev.eval_resource(_t(_1, rdf::type, sh::ValidationResult)
                        &    _t(_1, sh::focusNode, _2)
                        &    _t(_1, sh::value, EX(Invalid1))
                        &    _t(_1, sh::sourceConstraintComponent,
                                    sh::NodeKindConstraintComponent)
                        &    _t(_1, sh::sourceShape, EX(S1)));
    auto r3 = b.out_ev.eval_resource(_t(_1, rdf::type, sh::ValidationResult)
                        &    _t(_1, sh::focusNode, _2)
                        &    _t(_1, sh::value, EX(Invalid1))
                        &    _t(_1, sh::sourceConstraintComponent,
                                    sh::NodeConstraintComponent)
                        &    _t(_1, sh::sourceShape, EX(S1)));
    BOOST_TEST_REQUIRE(size(r1) == 1, "ntriple of output:\n" << b.out_nt);
    BOOST_TEST_REQUIRE(size(r2) == 1, "ntriple of output:\n" << b.out_nt);
    BOOST_TEST_REQUIRE(size(r3) == 1, "ntriple of output:\n" << b.out_nt);
    BOOST_TEST_REQUIRE(bool(*begin(r1) != *begin(r2)),
                                      "ntriple of output:\n" << b.out_nt);

    auto r4 = b.out_ev.eval_resource(_t(_1, rdf::type, sh::ValidationResult));
    BOOST_TEST_REQUIRE(size(r4) == 3, "ntriple of output:\n" << b.out_nt);
}

BOOST_AUTO_TEST_CASE_TEMPLATE(recursive_node, V, validator_types) {
    boilerplate<V> b("recursive-node.ttl");
    BOOST_REQUIRE(!b.conforms);
    bool val1 = !b.out_ev.ask(_t(_1, sh::focusNode, EX(Valid1)));
    bool val2 = !b.out_ev.ask(_t(_1, sh::focusNode, EX(Valid2)));

    auto r1 = b.out_ev.eval_resource(_t(_1, rdf::type, sh::ValidationResult)
                        &    _t(_1, sh::focusNode, EX(Invalid1))
                        &    _t(_1, sh::sourceConstraintComponent,
                                    sh::ClassConstraintComponent)
                        &    _t(_1, sh::sourceShape, EX(S1)));
    auto r2 = b.out_ev.eval_resource(_t(_1, rdf::type, sh::ValidationResult)
                        &    _t(_1, sh::focusNode, EX(Invalid2))
                        &    _t(_1, sh::value, EX(Invalid1))
                        &    _t(_1, sh::sourceConstraintComponent,
                                    sh::NodeConstraintComponent)
                        &    _t(_1, sh::sourceShape, EX(S1)));
    auto r3 = b.out_ev.eval_resource(_t(_1, rdf::type, sh::ValidationResult)
                        &    _t(_1, sh::focusNode, EX(Invalid2))
                        &    _t(_1, sh::value, EX(Invalid1))
                        &    _t(_1, sh::sourceConstraintComponent,
                                    sh::ClassConstraintComponent)
                        &    _t(_1, sh::sourceShape, EX(S1)));
    auto r4 = b.out_ev.eval_resource(_t(_1, rdf::type, sh::ValidationResult)
                        &    _t(_1, sh::focusNode, EX(Invalid3))
                        &    _t(_1, sh::sourceConstraintComponent,
                                    sh::ClassConstraintComponent)
                        &    _t(_1, sh::sourceShape, EX(S1)));


    BOOST_TEST_REQUIRE(val1, "ntriple of output:\n" << b.out_nt);
    BOOST_TEST_REQUIRE(val2, "ntriple of output:\n" << b.out_nt);
    BOOST_TEST_REQUIRE(size(r1) == 1, "ntriple of output:\n" << b.out_nt);
    BOOST_TEST_REQUIRE(size(r2) == 1, "ntriple of output:\n" << b.out_nt);
    BOOST_TEST_REQUIRE(size(r3) == 1, "ntriple of output:\n" << b.out_nt);
    BOOST_TEST_REQUIRE(size(r4) == 1, "ntriple of output:\n" << b.out_nt);
}
BOOST_AUTO_TEST_CASE_TEMPLATE(noinf_subProperty, V, validator_types) {
    boilerplate<V> b("inf-subProperty.ttl");
    BOOST_REQUIRE(!b.conforms);
    bool val1 = !b.out_ev.ask(_t(_1, sh::focusNode, EX(:Valid)));
    auto r1 = b.out_ev.eval_resource(_t(_1, rdf::type, sh::ValidationResult)
                               &    _t(_1, sh::focusNode, EX(Invalid1))
                               &    _t(_1, sh::sourceConstraintComponent,
                                           sh::MinCountConstraintComponent)
                               &    _t(_1, sh::sourceShape, EX(PessoaShape)));
    auto r2 = b.out_ev.eval_resource(_t(_1, rdf::type, sh::ValidationResult)
                               &    _t(_1, sh::focusNode, EX(ValidInf))
                               &    _t(_1, sh::sourceConstraintComponent,
                                           sh::MinCountConstraintComponent)
                               &    _t(_1, sh::sourceShape, EX(PessoaShape)));
    BOOST_TEST_REQUIRE(val1, "ntriple of output:\n" << b.out_nt);
    BOOST_TEST_REQUIRE(size(r1) == 1, "ntriple of output:\n" << b.out_nt);
    BOOST_TEST_REQUIRE(size(r2) == 1, "ntriple of output:\n" << b.out_nt);
}
BOOST_AUTO_TEST_CASE_TEMPLATE(inf_subProperty, V, validator_types) {
    inf_boilerplate<V> b("inf-subProperty.ttl");
    BOOST_REQUIRE(!b.conforms);
    bool val1 = !b.out_ev.ask(_t(_1, sh::focusNode, EX(:Valid)));
    bool val2 = !b.out_ev.ask(_t(_1, sh::focusNode, EX(:ValidInf)));
    auto r1 = b.out_ev.eval_resource(_t(_1, rdf::type, sh::ValidationResult)
                               &    _t(_1, sh::focusNode, EX(Invalid1))
                               &    _t(_1, sh::sourceConstraintComponent,
                                           sh::MinCountConstraintComponent)
                               &    _t(_1, sh::sourceShape, EX(PessoaShape)));
    BOOST_TEST_REQUIRE(val1, "ntriple of output:\n" << b.out_nt);
    BOOST_TEST_REQUIRE(val2, "ntriple of output:\n" << b.out_nt);
    BOOST_TEST_REQUIRE(size(r1) == 1, "ntriple of output:\n" << b.out_nt);
}


BOOST_AUTO_TEST_SUITE_END();

}}  // namespace shacldator::shacl
