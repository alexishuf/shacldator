/*
 * property_path_test.cpp
 *
 *  Created on: Jun 3, 2018
 *      Author: alexis
 */

#include <boost/test/unit_test.hpp>
#include <set>
#include "shacl/property_paths.hpp"
#include <boost/range/algorithm_ext.hpp>
#include "graph/graph.hpp"
#include "graph/str/stl/store.hpp"
#include "graph/str/raptor_builder.hpp"
#include "qry/spo.hpp"
#include "ns/sh.hpp"

using namespace std;

namespace shacldator {namespace shacl {

BOOST_AUTO_TEST_SUITE(property_path_test)

typedef str::stl::store::term_t term_t;
typedef graph<str::stl::store> graph_t;
typedef resource<graph_t> resource_t;
typedef node<graph_t> node_t;
typedef qry::def_evaluator<graph_t> ev_t;

#define EX(localName) ("http://example.org/ns#" BOOST_PP_STRINGIZE(localName))
#define REX(localName) s.resource(EX(localName))

struct fixture {
    static graph_t load() {
        str::raptor_builder<str::stl::store::builder> b;
        b.parse("turtle", "test-files/path-test.ttl");
        return mk_graph(b.builder.build());
    }

    fixture() : g(load()), s(g.store()), ev(g) {}

    set<term_t> to_set(term_t marker_t) {
        set<term_t> out;
        resource_t marker(marker_t, g);
        auto gen = parse_pp(qry::o(marker, sh::path)().as_resource(), ev);
        gen.set_subj(qry::o(marker, EX(subject))().term());
        boost::insert(out, dqry::path::mk_generator_range(gen));
        return out;
    }

    set<term_t> to_expected(term_t marker) {
        set<term_t> out;
        qry::o(resource_t(marker, g), EX(expected))
                .for_each([&](const node_t& n){out.insert(n.term());});
        return out;
    }

    graph_t g;
    str::stl::store& s;
    ev_t ev;
};


BOOST_FIXTURE_TEST_CASE(direct, fixture) {
    set<term_t> ac = to_set(REX(m1)), ex = to_expected(REX(m1));
    BOOST_TEST(ac == ex);
}

BOOST_FIXTURE_TEST_CASE(reverse, fixture) {
    set<term_t> ac = to_set(REX(m2)), ex = to_expected(REX(m2));
    BOOST_TEST(ac == ex);
}

BOOST_FIXTURE_TEST_CASE(opt, fixture) {
    set<term_t> ac = to_set(REX(m3)), ex = to_expected(REX(m3));
    BOOST_TEST(ac == ex);
}

BOOST_FIXTURE_TEST_CASE(plus_one, fixture) {
    set<term_t> ac = to_set(REX(m4)), ex = to_expected(REX(m4));
    BOOST_TEST(ac == ex);
}

BOOST_FIXTURE_TEST_CASE(star_three, fixture) {
    set<term_t> ac = to_set(REX(m5)), ex = to_expected(REX(m5));
    BOOST_TEST(ac == ex);
}

BOOST_FIXTURE_TEST_CASE(alt, fixture) {
    set<term_t> ac = to_set(REX(m6)), ex = to_expected(REX(m6));
    BOOST_TEST(ac == ex);
}

BOOST_FIXTURE_TEST_CASE(seq, fixture) {
    set<term_t> ac = to_set(REX(m7)), ex = to_expected(REX(m7));
    BOOST_TEST(ac == ex);
}

BOOST_AUTO_TEST_SUITE_END()


}}  // namespace shacldator::shacl


