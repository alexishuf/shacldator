/*
 * viterator_test.cpp
 *
 *  Created on: Apr 30, 2018
 *      Author: alexis
 */

#include <boost/test/unit_test.hpp>
#include <boost/range/algorithm_ext/insert.hpp>
#include "viterator.hpp"

using namespace std;

namespace shacldator {

BOOST_AUTO_TEST_SUITE(viterator_test);

struct C1 {
    C1(int v) : v(v) {}
    C1(const C1&) = default;
    C1& operator=(const C1&) = default;
    bool operator==(const C1& o) const {return v == o.v;}
    bool operator!=(const C1& o) const {return v != o.v;}
    int v;
};
struct C2 : public C1 {
    C2(int v) : C1(v) {}
    C2(const C2&) = default;
    C2& operator=(const C2&) = default;
};

BOOST_AUTO_TEST_CASE(empty) {
    empty_vrange<int> e;
    BOOST_REQUIRE(boost::empty(e));
    BOOST_REQUIRE(bool(boost::begin(e) == boost::end(e)));
    BOOST_REQUIRE(bool(e.first == e.second));
}

BOOST_AUTO_TEST_CASE(for_vector) {
    vector<int> actual, expected{79, 17, 64};
    auto rng = mk_vrange(expected);
    boost::insert(actual, actual.end(), rng);
    BOOST_REQUIRE(!boost::empty(rng));
    BOOST_REQUIRE(actual == expected);
}

BOOST_AUTO_TEST_CASE(assign_from_specialized) {
    vector<C2> vec{C2(23)};
    auto range = mk_vrange(vec);
    fwd_viterator<C1> generic = range.first;
    BOOST_REQUIRE(bool(*generic == *range.first));
    ++range.first;
    BOOST_REQUIRE(bool(generic != range.second));
    BOOST_REQUIRE(bool(*generic == C1(23)));
    ++generic;
    BOOST_REQUIRE(bool(generic == range.second));
}

BOOST_AUTO_TEST_SUITE_END();

}  // namespace shacldator

