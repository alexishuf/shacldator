/*
 * utils.cpp
 *
 *  Created on: Apr 24, 2018
 *      Author: alexis
 */

#include <boost/test/unit_test.hpp>
#include <set>
#include <boost/range.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/range/algorithm_ext.hpp>
#include <boost/mpl/vector.hpp>
#include "utils.hpp"

using namespace std;

namespace shacldator {

BOOST_AUTO_TEST_SUITE(utils_test);

struct C {
    C(int v) : data(v) {}
    C(const C&) = default;
    C& operator=(const C&) = default;
    ~C() {data = ~data;}
    bool operator==(const C& o) const {return data == o.data;}
    bool operator!=(const C& o) const {return data != o.data;}
    int data;
};

struct D {
    D() : data(0) {}
    D(int v) : data(v) {}
    D(const D&) = default;
    D& operator=(const D&) = default;
    ~D() {data = ~data;}
    bool operator==(const D& o) const {return data == o.data;}
    bool operator!=(const D& o) const {return data != o.data;}
    int data;
};

struct doubler {
    typedef int argument_type;
    typedef double result_type;
    double operator()(int in) const {return in * 2.0;}
};

typedef boost::mpl::vector<pack<C>, pack<D>> pack_types;
BOOST_AUTO_TEST_CASE_TEMPLATE(pack, P, pack_types) {
    P a, b;
    if (std::is_default_constructible<typename P::packed_type>::value) {
        BOOST_REQUIRE(a.get() == 0);
        BOOST_REQUIRE(b.get() == 0);
    }

    a = P(23);
    b = P(47);
    BOOST_REQUIRE(a.get() == 23);
    BOOST_REQUIRE(b.get() == 47);

    a = b;
    BOOST_REQUIRE(a.get() == 47);
    b = P(79);
    BOOST_REQUIRE(a.get() == 47);
    BOOST_REQUIRE(b.get() == 79);

    if (a.get() == 47) {
        P c(179);
        BOOST_REQUIRE(c.get() == 179);
        a = c;
        c = P(147);
        BOOST_REQUIRE(c.get() == 147);
        BOOST_REQUIRE(a.get() == 179);
    }
    BOOST_REQUIRE(a.get() == 179);
    BOOST_REQUIRE(b.get() == 79);
}

BOOST_AUTO_TEST_CASE(default_constructible_it) {
    typedef def_constr_in_it<std::vector<int>::iterator> it_t;
    std::vector<int> empty;
    it_t it = empty.begin(), end = empty.end();
    BOOST_REQUIRE(bool(it == end));

    std::vector<int> vec{27, 87, 46, 19, 38};
    std::vector<int> vec_copy;
    boost::insert(vec_copy, vec_copy.end(),
                  std::make_pair(it_t(vec.begin()), it_t(vec.end())));
    BOOST_REQUIRE(bool(vec == vec_copy));
}

BOOST_AUTO_TEST_CASE(transform_fwd_it_test) {
    doubler t;
    std::vector<int> actual, expected, in{1, 78, 74};
    for (auto i = in.begin(), e = in.end(); i != e; ++i)
        expected.push_back(*i * 2.0);
    auto t_rng = transform_range(in, t);
    boost::insert(actual, actual.end(), t_rng);
    BOOST_REQUIRE(bool(actual == expected));
}

BOOST_AUTO_TEST_CASE(transform_fwd_it_empty) {
    doubler t;
    std::vector<int> actual, expected, in{};
    auto t_rng = transform_range(in, t);
    boost::insert(actual, actual.end(), t_rng);
    BOOST_REQUIRE(bool(actual == expected));
}

BOOST_AUTO_TEST_CASE(size_t_overflow) {
    size_t max = numeric_limits<std::size_t>::max(), zero(0);
    BOOST_TEST(max+1 == zero);
    BOOST_TEST(zero-1 == max);
}

BOOST_AUTO_TEST_SUITE_END();

} // namespace shacldator

