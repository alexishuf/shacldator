/*
 * sh_test.cpp
 *
 *  Created on: Apr 30, 2018
 *      Author: alexis
 */


#include <boost/test/unit_test.hpp>
#include "ns/sh.hpp"

using namespace std;

namespace shacldator {

BOOST_AUTO_TEST_SUITE(sh_test);

BOOST_AUTO_TEST_CASE(bad_canonical) {
    BOOST_REQUIRE_THROW(sh::canonical("kommst du mit zu mir"),
                        std::out_of_range);
}

BOOST_AUTO_TEST_CASE(identity_canonical) {
    BOOST_REQUIRE(sh::canonical(sh::NodeShape) == sh::NodeShape);
}

BOOST_AUTO_TEST_CASE(good_canonical) {
    std::string copy(sh::NodeShape);
    BOOST_REQUIRE(sh::canonical(copy) == sh::NodeShape);
}

BOOST_AUTO_TEST_CASE(good_underline_canonical) {
    std::string copy(sh::and_);
    BOOST_REQUIRE(sh::canonical(copy) == sh::and_);
}

BOOST_AUTO_TEST_SUITE_END();

}  // namespace shacldator
